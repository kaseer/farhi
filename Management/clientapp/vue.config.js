module.exports = {
    outputDir: "../wwwroot",
    filenameHashing: false,
    devServer:{
        port:8080,
        https:false,
         proxy:{
             '^/api':{
                 //target: 'https://localhost:5001',
                 //target: 'https://localhost:44370',
                //  target:"http://localhost:7794",
                 //target: "http://localhost:48874"
                target: "https://Farhi-Management.naqra.ly"
                // target: "http://localhost:12756"
             }
         },
        
    },
    chainWebpack: config => {
        config.module
          .rule('images')
            .use('url-loader')
              .loader('url-loader')
              .tap(options => Object.assign(options, { limit: 100240 }))
    }
}