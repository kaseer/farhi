import Vue from 'vue';
// import App from './App.vue';
import './plugins/element.js'
import Layout from './layout/Layout.vue'


// import ElementUI from 'element-ui';
// import locale from 'element-ui/lib/index.js';
import router from './router';
// import dataService from './shared/DataService';
import BlockUIService from './shared/BlockUIService.js';
import loading from './shared/loading.js';
import validation from './shared/ValidationRules.js';
import globalAlerts from './shared/GlobalAlerts.js';
import './shared/globalFilters.js';
import './plugins/element.js'
import './styles/Vendor.scss';
import './styles/Site.scss'
import './styles/iconfont/material-icons.css'
import {store} from './store'

// Vue.use(ElementUI, { locale });

// Vue.prototype.$http = dataService;
Vue.prototype.$loading = BlockUIService;
Vue.prototype.$blockUI = loading;
Vue.prototype.$validation = validation;
Vue.prototype.$globalAlerts = globalAlerts;

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(Layout)
}).$mount('#app')
