import Vue from 'vue'
import Vuex from 'vuex'
import lookup from './modules/lookup'
import halls from './modules/halls'
import hallBranches from './modules/hallBranches'
import stores from './modules/stores'
import storeBranches from './modules/storeBranches'
import loading from './modules/ui/loading'
import sliderManagement from './modules/sliderManagement'
import storeTypes from './modules/storeTypes'
import productCategories from './modules/productCategories'
import productSubCategories from './modules/productSubCategories'
import farhiAdmins from './modules/farhiAdmins'
import farhiUsers from './modules/farhiUsers'
// import farhiStoreUsers from './modules/farhiStoreUsers'
import customers from './modules/customers'
import subscriptionPrices from './modules/subscriptionPrices'

Vue.use(Vuex)

export const store = new Vuex.Store({

    modules: {
        // ui:{
        loading,
        // },
        lookup,
        halls,
        hallBranches,
        stores,
        storeBranches, 
        sliderManagement,
        storeTypes,
        productCategories,
        productSubCategories,
        farhiAdmins,
        farhiUsers,
        // farhiStoreUsers,
        customers,
        subscriptionPrices
    }
})
