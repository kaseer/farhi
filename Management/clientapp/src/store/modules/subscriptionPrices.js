﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    subscriptionPrices: [],
    durationTypes: [
        {value:1, label: "سنة"},
        {value:2, label: "شهر"},
        {value:3, label: "اسبوع"},
    ],
    subscriptionTypes: [
        {value:1, label: "اشتراك صالة"},
        {value:2, label: "اشتراك محل"},
        {value:3, label: "غير ذلك"},
    ],
    subscriptionPrice: {},
    subscriptionPriceForEdit: {},
    total: 0,
}
const mutations = {
    setSubscriptionPrices(state, payload) {
        state.subscriptionPrices = payload
    },
    setSubscriptionPrice(state, payload) {
        state.subscriptionPrice = payload
    },
    setSubscriptionPriceForEdit(state, payload) {
        state.subscriptionPriceForEdit = payload
    },
    add(state, payload) {
        state.subscriptionPrices.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.subscriptionPrices[index] = payload;
    },
    lock(state, { index }) {
        state.subscriptionPrices[index].status = 2;
    },
    unlock(state, { index }) {
        state.subscriptionPrices[index].status = 1;
    },
    delete(state, index) {
        state.subscriptionPrices.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.subscriptionPrices.findIndex(x => x.subscriptionPriceId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.subscriptionPrices.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.data;
            let total = res.data.total;
            commit('setSubscriptionPrices', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails(_, id) {

        try {
            loading.Start();
            const res = await api.subscriptionPrices.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
            // commit('setSubscriptionPrice', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getForEdit(_, id) {
        try {
            loading.Start();
            const res = await api.subscriptionPrices.getForEdit(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.subscriptionPrices.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message(message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload ) {
        try {
            loading.Start();
            const res = await api.subscriptionPrices.edit( payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(payload.subscriptionPriceId);
            commit('edit', { index, payload })
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }
    },
    async delete({ commit, state }, id) {
        let index = getters.index(id);
        let subscriptionPriceName = state.subscriptionPrices[index].description;
        let title = `هذا الامر سيقوم بحذف الاشتراك ${subscriptionPriceName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.subscriptionPrices.delete(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(id);
                commit('delete', index)
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};