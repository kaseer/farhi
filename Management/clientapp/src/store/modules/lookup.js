﻿import api from '@/api/index';
import globalAlerts from '@/shared/GlobalAlerts';
// import loading from '@/shared/loading';

const state = {
    addresses: [],
    halls: [],
    hallBranches: [],
    reservationTypes: [],
    subscriptionPrices: [],
    // subscriptionPricesDetails: [],
    storeTypes: [],
    stores: [],
    storeBranches: [],
    categories: [],
    loading: false,
    days: [
        {value:1, text:"الاثنين"},
        {value:2, text:"الثلاثاء"},
        {value:3, text:"الاربعاء"},
        {value:4, text:"الخميس"},
        {value:5, text:"الجمعة"},
        {value:6, text:"السبت"},
        {value:7, text:"الاحد"},
    ],
}
const getters = {
    
}
const mutations = {
    startLoad(state) {
        state.loading = true
    },
    stopLoad(state) {
        state.loading = false
    },
    setAddresses(state, payload) {
        state.addresses = payload
    },
    setHalls(state, payload) {
        state.halls = payload
    },
    setHallBranches(state, payload) {
        state.hallBranches = payload
    },
    setReservationTypes(state, payload) {
        state.reservationTypes = payload
    },
    // setSubscriptionPrices(state, payload) {
    //     state.subscriptionPrices = payload
    // },
    // setSubscriptionPricesDetails(state, payload) {
    //     state.subscriptionPricesDetails = payload
    // },
    setStoreTypes(state, payload) {
        state.storeTypes = payload
    },
    setStores(state, payload) {
        state.stores = payload
    },
    setStoreBranches(state, payload) {
        state.storeBranches = payload
    },
    setCategories(state, payload) {
        state.categories = payload
    },
    setSubscriptionPrices(state, payload) {
        state.subscriptionPrices = payload
    },
}
const actions = {
    async getAddresses({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getAddresses();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.addresses;
            commit('setAddresses', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getHalls({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getHalls();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.halls;
            commit('setHalls', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getHallBranches({ commit }, id) {
        try {
            commit('startLoad');
            const res = await api.lookup.getHallBranches(id);
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.hallBranches;
            commit('setHallBranches', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getReservationTypes({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getReservationTypes();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.reservationTypes;
            commit('setReservationTypes', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    // async getSubscriptionPrices({ commit },type) {
    //     try {
    //         commit('startLoad');
    //         const res = await api.lookup.getSubscriptionPrices(type);
    //         commit('stopLoad');
    //         if (!res.data.statusCode) {
    //             globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.subscriptionPrices;
    //         commit('setSubscriptionPrices', list);
    //     }
    //     catch (err) {
    //         globalAlerts.catch(err)
    //     }
    // },
    // async getSubscriptionPricesDetails({ commit },subscriptionPriceId) {
    //     try {
    //         loading.Start();
    //         const res = await api.lookup.getSubscriptionPricesDetails(subscriptionPriceId);
    //         loading.Stop();
    //         if (!res.data.statusCode) {
    //             globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.subscriptionPricesDetails;
    //         commit('setSubscriptionPricesDetails', list);
    //     }
    //     catch (err) {
    //         globalAlerts.catch(err)
    //     }
    // },
    async getStoreTypes({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getStoreTypes();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.storeTypes;
            commit('setStoreTypes', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getStores({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getStores();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.stores;
            commit('setStores', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getStoreBranches({ commit }, id) {
        try {
            commit('startLoad');
            const res = await api.lookup.getStoreBranches(id);
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.storeBranches;
            commit('setStoreBranches', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getCategories({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getCategories();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.categories;
            commit('setCategories', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getSubscriptionPrices({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getSubscriptionPrices();
            commit('stopLoad');
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.subscriptionPrices;
            commit('setSubscriptionPrices', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};