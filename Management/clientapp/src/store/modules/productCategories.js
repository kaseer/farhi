﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    productCategories: [],
    total: 0,
}
const mutations = {
    setProductCategories(state, payload) {
        state.productCategories = payload
    },
    add(state, payload) {
        state.productCategories.unshift(payload);
    },
    edit(state, {id,payload} ) {
        let index = state.productCategories.findIndex(x => x.productCategoryId == id);
        state.productCategories[index].description = payload;
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.productCategories.findIndex(x => x.productCategoryId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.productCategories.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.productCategories;
            let total = res.data.total;
            commit('setProductCategories', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.productCategories.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message(message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload ) {
        try {
            loading.Start();
            const res = await api.productCategories.edit(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let message = res.data.message;
            commit('edit', {id: payload.productCategoryId, payload: payload.description} )
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};