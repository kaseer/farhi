﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    customers: [],
    customer: {},
    total: 0,
}
const mutations = {
    setCustomers(state, payload) {
        state.customers = payload
    },
    setCustomer(state, payload) {
        state.customer = payload
    },
    lock(state, { index }) {
        state.customers[index].status = 2;
    },
    unlock(state, { index }) {
        state.customers[index].status = 1;
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.customers.findIndex(x => x.customerId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.customers.getAll(query);
            console.log(res)

            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.data;
            let total = res.data.total;
            commit('setCustomers', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails(_, id) {

        try {
            loading.Start();
            const res = await api.customers.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
            // commit('setCustomer', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async lock({ commit, state }, id) {
        let index = getters.index(id);
        let loginName = state.customers[index].loginName;
        let title = `هذا الامر سيقوم بتجميد المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.customers.lock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('lock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async unlock({ commit, state, getters }, id) {
        let index = getters.index(id);
        let loginName = state.customers[index].loginName;
        let title = `هذا الامر سيقوم بفك تجميد المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.customers.unlock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;

                commit('unlock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    
    async resetPassword({ state }, id) {
        console.log(id)
        let index = getters.index(id);
        let loginName = state.customers[index].loginName;
        console.log(loginName)

        let title = `هذا الامر باعادة تعيين كلمة مرور المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.customers.resetPassword(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let title = res.data.message;
                let message = "كلمة المرور الجديدة هي : " + res.data.data;
                globalAlerts.alert(title,message,"success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};