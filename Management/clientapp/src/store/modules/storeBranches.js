﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';
// import { Promise } from 'core-js';

const state = {
    storeBranches: [],
    storeBranch: {},
    storeBranchForEdit: {},
    total: 0,
    // video: "",
    oldImages: []
}
const mutations = {
    setStoreBranches(state, payload) {
        state.storeBranches = payload
    },
    setStoreBranch(state, payload) {
        state.storeBranch = payload
    },
    setStoreBranchForEdit(state, payload) {
        state.storeBranchForEdit = payload
    },
    add(state, payload) {
        state.storeBranches.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.storeBranches[index] = payload;
    },
    lock(state, id ) {
        let index = state.storeBranches.findIndex(x => x.storeBranchId == id);
        state.storeBranches[index].status = 2;
    },
    unlock(state, id) {
        let index = state.storeBranches.findIndex(x => x.storeBranchId == id);
        state.storeBranches[index].status = 1;
    },
    assignMainBranch(state, storeBranchId) {
        state.storeBranches.forEach((branch)=>{
            branch.mainBranchId = storeBranchId;
        });
        // state.storeBranches[mainBranchIndex].mainBranchId = null;
        // state.storeBranches[index].mainBranchId = state.storeBranches[index].storeBranchId;
    },
    delete(state, id) {
        let index = state.storeBranches.findIndex(x => x.storeBranchId == id);
        state.storeBranches.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
    // setVideo(state, payload) {
    //     state.video = payload
    // },
    setImages(state, payload) {
        state.oldImages = payload
    },
}
const getters = {
    index(id) {
        console.log(id)
        return state.storeBranches.findIndex(x => x.storeBranchId == id);
    },
    mainBranch() {
        return state.storeBranches.findIndex(x => x.mainBranchId != null);
    },
    oldImages(images) {
        let data = state.oldImages;
        images.forEach((image) => {
            data = data.filter(x => x.imageId != image);
        });
        return data;
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.storeBranches.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.storeBranches;
            let total = res.data.total;
            commit('setStoreBranches', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails({commit}, id) {

        try {
            loading.Start();
            const res = await api.storeBranches.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.storeBranchData;
            // return Promise.resolve(data);
            commit('setStoreBranch', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getForEdit(_, id) {
        try {
            loading.Start();
            const res = await api.storeBranches.getForEdit(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.storeBranch;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.storeBranches.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.addedBranch;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit(_, payload) {
        try {
            loading.Start();
            const res = await api.storeBranches.edit( payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            // payload = res.data.storeBranch;
            let message = res.data.message;
            // let index = getters.index(payload.storeBranchId);
            // commit('edit', { index, payload })
            globalAlerts.message( message, "success");
            // return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async lock({commit}, item) {
        
        // let storeBranchName = state.storeBranches[index].storeBranchName;
        let title = `هذا الامر سيقوم بتجميد الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.storeBranches.lock(item.storeBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('lock', item.storeBranchId);
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async unlock({ commit}, item) {
        // let storeBranchName = state.storeBranches[index].storeBranchName;
        let title = `هذا الامر سيقوم بفك تجميد الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.storeBranches.unlock(item.storeBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('unlock', item.storeBranchId)
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async delete({ commit }, item) {
        // let storeBranchName = state.storeBranches[index].storeBranchName;
        let title = `هذا الامر سيقوم بحذف الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.storeBranches.delete(item.storeBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('delete', item.storeBranchId)
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async assignMainBranch({ commit }, item) {
        // let storeBranchName = state.storeBranches[index].storeBranchName;
        let title = `هذا الامر سيقوم  الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.storeBranches.assignMainBranch(item.storeBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                // let index = getters.index(item.storeBranchId);
                // let mainBranchIndex = getters.mainBranch();
                commit('assignMainBranch', item.storeBranchId)
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async getVideo(_, id) {

        try {
            loading.Start();
            const res = await api.storeBranches.getVideo(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
            // commit('setVideo', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getImages({commit}, id) {

        try {
            loading.Start();
            const res = await api.storeBranches.getImages(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.images;
            // return Promise.resolve(data);
            commit('setImages', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },

    async deleteOldImages({commit}, data) {

        try {
            loading.Start();
            const res = await api.storeBranches.deleteOldImages(data);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let message = res.data.message;
            let oldImages = getters.oldImages(data);
            // return Promise.resolve(data);
            commit('setImages', oldImages);
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    
    async addImages({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.storeBranches.addImages(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.oldImages;
            let message = res.data.message;
            commit('setImages', data)
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    
    async deleteVideo(_, id) {
        try {
            loading.Start();
            const res = await api.storeBranches.deleteVideo(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let message = res.data.message;
            globalAlerts.message( message, "success");
            // return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async addSubscription(_, payload) {
        try {
            loading.Start();
            const res = await api.storeBranches.addSubscription(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            
            let message = res.data.message;
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};