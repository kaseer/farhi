﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';
import { Promise } from 'core-js';

const state = {
    hallBranches: [],
    hallBranch: {},
    hallBranchForEdit: {},
    total: 0,
    // video: "",
    oldImages: []
}
const mutations = {
    setHallBranches(state, payload) {
        state.hallBranches = payload
    },
    setHallBranch(state, payload) {
        state.hallBranch = payload
    },
    setHallBranchForEdit(state, payload) {
        state.hallBranchForEdit = payload
    },
    add(state, payload) {
        state.hallBranches.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.hallBranches[index] = payload;
    },
    lock(state, { index }) {
        state.hallBranches[index].status = 2;
    },
    unlock(state, { index }) {
        state.hallBranches[index].status = 1;
    },
    assignMainBranch(state, hallBranchId) {
        state.hallBranches.forEach((branch)=>{
            branch.mainBranchId = hallBranchId;
        });
        // state.hallBranches[mainBranchIndex].mainBranchId = null;
        // state.hallBranches[index].mainBranchId = state.hallBranches[index].hallBranchId;
    },
    delete(state, index) {
        state.hallBranches.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
    // setVideo(state, payload) {
    //     state.video = payload
    // },
    setImages(state, payload) {
        state.oldImages = payload
    },
}
const getters = {
    index(id) {
        return state.hallBranches.findIndex(x => x.hallBranchId == id);
    },
    mainBranch() {
        return state.hallBranches.findIndex(x => x.mainBranchId != null);
    },
    oldImages(images) {
        let data = state.oldImages;
        images.forEach((image) => {
            data = data.filter(x => x.imageId != image);
        });
        return data;
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.hallBranches.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.hallBranches;
            let total = res.data.total;
            commit('setHallBranches', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails({commit}, id) {

        try {
            loading.Start();
            const res = await api.hallBranches.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.hallBranchData;
            // return Promise.resolve(data);
            commit('setHallBranch', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getForEdit(_, id) {
        try {
            loading.Start();
            const res = await api.hallBranches.getForEdit(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.hallBranchData;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.hallBranches.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.addedBranch;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit(_, payload) {
        try {
            loading.Start();
            const res = await api.hallBranches.edit( payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            // payload = res.data.hallBranch;
            let message = res.data.message;
            // let index = getters.index(payload.hallBranchId);
            // commit('edit', { index, payload })
            globalAlerts.message( message, "success");
            // return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async lock({ commit, getters}, item) {
        
        // let hallBranchName = state.hallBranches[index].hallBranchName;
        let title = `هذا الامر سيقوم بتجميد الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.hallBranches.lock(item.hallBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(item.hallBranchId);
                commit('lock', { index })
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async unlock({ commit, getters}, item) {
        // let hallBranchName = state.hallBranches[index].hallBranchName;
        let title = `هذا الامر سيقوم بفك تجميد الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.hallBranches.unlock(item.hallBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(item.hallBranchId);
                commit('unlock', { index })
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async delete({ commit, getters }, item) {
        // let hallBranchName = state.hallBranches[index].hallBranchName;
        let title = `هذا الامر سيقوم بحذف الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.hallBranches.delete(item.hallBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(item.hallBranchId);
                commit('delete', index)
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async assignMainBranch({ commit }, item) {
        // let hallBranchName = state.hallBranches[index].hallBranchName;
        let title = `هذا الامر سيقوم  الفرع ${item.addressName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.hallBranches.assignMainBranch(item.hallBranchId);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                // let index = getters.index(item.hallBranchId);
                // let mainBranchIndex = getters.mainBranch();
                commit('assignMainBranch', item.hallBranchId)
                globalAlerts.message( message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async getVideo(_, id) {

        try {
            loading.Start();
            const res = await api.hallBranches.getVideo(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.video;
            return Promise.resolve(data);
            // commit('setVideo', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getImages({commit}, id) {

        try {
            loading.Start();
            const res = await api.hallBranches.getImages(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.images;
            // return Promise.resolve(data);
            commit('setImages', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },

    async deleteOldImages({commit}, data) {

        try {
            loading.Start();
            const res = await api.hallBranches.deleteOldImages(data);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let message = res.data.message;
            let oldImages = getters.oldImages(data);
            // return Promise.resolve(data);
            commit('setImages', oldImages);
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    
    async addImages({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.hallBranches.addImages(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.oldImages;
            let message = res.data.message;
            commit('setImages', data)
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    
    async deleteVideo(_, id) {
        try {
            loading.Start();
            const res = await api.hallBranches.deleteVideo(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let message = res.data.message;
            globalAlerts.message( message, "success");
            // return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async addSubscription(_, payload) {
        try {
            loading.Start();
            const res = await api.hallBranches.addSubscription(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            
            let message = res.data.message;
            globalAlerts.message( message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};