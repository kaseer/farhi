﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    stores: [],
    store: {},
    storeForEdit: {},
    total: 0,
}
const mutations = {
    setStores(state, payload) {
        state.stores = payload
    },
    setStore(state, payload) {
        state.store = payload
    },
    setStoreForEdit(state, payload) {
        state.storeForEdit = payload
    },
    add(state, payload) {
        state.stores.unshift(payload);
    },
    edit(state, {id,payload} ) {
        let index = state.stores.findIndex(x => x.storeId == id);
        state.stores[index] = payload;
    },
    lock(state, { index }) {
        state.stores[index].status = 2;
    },
    unlock(state, { index }) {
        state.stores[index].status = 1;
    },
    delete(state, index) {
        state.stores.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.stores.findIndex(x => x.storeId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.stores.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.stores;
            let total = res.data.total;
            commit('setStores', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails(_, id) {

        try {
            loading.Start();
            const res = await api.stores.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data;
            return Promise.resolve(data);
            // commit('setStore', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getForEdit(_, id) {
        try {
            loading.Start();
            const res = await api.stores.getForEdit(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.stores.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.store;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message(message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload ) {
        try {
            loading.Start();
            const res = await api.stores.edit( payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            payload = res.data.store;
            let message = res.data.message;
            commit('edit', {id :payload.storeId, payload} )
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }
    },
    async lock({ commit, state }, id) {
        let index = getters.index(id);
        let storeName = state.stores[index].storeName;
        let title = `هذا الامر سيقوم بتجميد المحل ${storeName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.stores.lock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('lock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async unlock({ commit, state }, id) {
        let index = getters.index(id);
        let storeName = state.stores[index].storeName;
        let title = `هذا الامر سيقوم بفك تجميد المحل ${storeName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.stores.unlock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;

                commit('unlock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async delete({ commit, state }, id) {
        let index = getters.index(id);
        let storeName = state.stores[index].storeName;
        let title = `هذا الامر سيقوم بحذف المحل ${storeName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.stores.delete(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(id);
                commit('delete', index)
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};