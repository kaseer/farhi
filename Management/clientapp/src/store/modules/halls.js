﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    halls: [],
    hall: {},
    hallForEdit: {},
    total: 0,
}
const mutations = {
    setHalls(state, payload) {
        state.halls = payload
    },
    setHall(state, payload) {
        state.hall = payload
    },
    setHallForEdit(state, payload) {
        state.hallForEdit = payload
    },
    add(state, payload) {
        state.halls.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.halls[index] = payload;
    },
    lock(state, { index }) {
        state.halls[index].status = 2;
    },
    unlock(state, { index }) {
        state.halls[index].status = 1;
    },
    delete(state, index) {
        state.halls.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.halls.findIndex(x => x.hallId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.halls.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.halls;
            let total = res.data.total;
            commit('setHalls', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails(_, id) {

        try {
            loading.Start();
            const res = await api.halls.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data;
            return Promise.resolve(data);
            // commit('setHall', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getForEdit(_, id) {
        try {
            loading.Start();
            const res = await api.halls.getForEdit(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.halls.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.hall;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message(message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload ) {
        try {
            loading.Start();
            const res = await api.halls.edit( payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            payload = res.data.hall;
            let message = res.data.message;
            let index = getters.index(payload.hallId);
            commit('edit', { index, payload })
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }
    },
    async lock({ commit, state }, id) {
        let index = getters.index(id);
        let hallName = state.halls[index].hallName;
        let title = `هذا الامر سيقوم بتجميد الصالة ${hallName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.halls.lock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('lock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async unlock({ commit, state }, id) {
        let index = getters.index(id);
        let hallName = state.halls[index].hallName;
        let title = `هذا الامر سيقوم بفك تجميد الصالة ${hallName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.halls.unlock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;

                commit('unlock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async delete({ commit, state }, id) {
        let index = getters.index(id);
        let hallName = state.halls[index].hallName;
        let title = `هذا الامر سيقوم بحذف الصالة ${hallName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.halls.delete(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(id);
                commit('delete', index)
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};