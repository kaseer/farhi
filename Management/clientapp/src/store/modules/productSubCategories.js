﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';
import lookup from './lookup';

const state = {
    productSubCategories: [],
    total: 0,
}
const mutations = {
    setProductSubCategories(state, payload) {
        state.productSubCategories = payload
    },
    add(state, payload) {
        state.productSubCategories.unshift(payload);
    },
    edit(state, {id,payload} ) {
        let index = state.productSubCategories.findIndex(x => x.productSubCategoryId == id);
        state.productSubCategories[index].description = payload.description;
        state.productSubCategories[index].categoryId = payload.categoryId;
        state.productSubCategories[index].category = lookup.state.categories.find(x => x.value == payload.categoryId).label;
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.productSubCategories.findIndex(x => x.productCategoryId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.productSubCategories.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.productSubCategories;
            let total = res.data.total;
            commit('setProductSubCategories', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.productSubCategories.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message(message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload ) {
        try {
            loading.Start();
            const res = await api.productSubCategories.edit(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let message = res.data.message;
            commit('edit', {id: payload.productSubCategoryId, payload} )
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};