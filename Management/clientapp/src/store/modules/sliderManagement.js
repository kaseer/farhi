﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    sliders: [],
    slider: {},
}
const mutations = {
    setSliders(state, payload) {
        state.sliders = payload
    },
    setSlider(state, payload) {
        state.slider = payload
    },
    add(state, payload) {
        console.log(payload)
        state.sliders.unshift(payload)
    },
    edit(state, { id, payload }) {
        let index = state.sliders.findIndex(x => x.sliderManagementId == id);
        state.sliders[index] = payload;
    },
    delete(state, id) {
        let index = state.sliders.findIndex(x => x.sliderManagementId == id);
        state.sliders.splice(index, 1);
    },
}
const getters = {

}
const actions = {
    async getAll({ commit }) {
        try {
            loading.Start();
            const res = await api.sliderManagement.getAll();
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let list = res.data.sliders;
            commit('setSliders', list);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async get(_, id) {

        try {
            loading.Start();
            const res = await api.sliderManagement.get(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data;
            return Promise.resolve(data);
            // commit('setSlider', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async add({ commit },payload) {

        try {
            
            loading.Start();
            // const res = await api.sliderManagement.add(payload);
            loading.Stop();
            // if (!res.data.statusCode) {
            //     globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            // }
            // let data = res.data.oldImages;
            // let message = res.data.message;
            
            commit('add',payload)
            // globalAlerts.message("message", "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload) {
        try {
            loading.Start();
            const res = await api.sliderManagement.edit(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            payload = res.data.item;
            let message = res.data.message;
            commit('edit', { id: payload.sliderManagementId, payload })
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            //return new Promise.reject(err);
        }
    },
    async delete({ commit, state }, id) {
        let index = getters.index(id);
        let sliderName = state.sliders[index].sliderName;
        let title = `هذا الامر سيقوم بحذف الوسائط ${sliderName}. الاستمرار?`;
        globalAlerts.confirmation(title)
            .then(async () => {
                try {
                    loading.Start();
                    const res = await api.sliderManagement.delete(id);
                    loading.Stop();
                    if (!res.data.statusCode) {
                        globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                    }
                    let message = res.data.message;
                    commit('delete', id)
                    globalAlerts.message(message, "success");
                }
                catch (err) {
                    globalAlerts.catch(err)
                }
            }).catch(() => { });
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};