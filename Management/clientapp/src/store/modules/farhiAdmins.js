﻿import api from '@/api/index';
import loading from '@/shared/loading';
import globalAlerts from '@/shared/GlobalAlerts';

const state = {
    users: [],
    user: {},
    userForEdit: {},
    total: 0,
}
const mutations = {
    setUsers(state, payload) {
        state.users = payload
    },
    setUser(state, payload) {
        state.user = payload
    },
    setUserForEdit(state, payload) {
        state.userForEdit = payload
    },
    add(state, payload) {
        state.users.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.users[index] = payload;
    },
    lock(state, { index }) {
        state.users[index].status = 2;
    },
    unlock(state, { index }) {
        state.users[index].status = 1;
    },
    delete(state, index) {
        state.users.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.users.findIndex(x => x.adminId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            loading.Start();
            const res = await api.farhiAdmins.getAll(query);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
            }
            let list = res.data.data;
            let total = res.data.total;
            commit('setUsers', list);
            commit('total', total);
        }
        catch (err) {
            globalAlerts.catch(err)
        }
    },
    async getDetails(_, id) {

        try {
            loading.Start();
            const res = await api.farhiAdmins.getDetails(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
            // commit('setUser', data)
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
    async getForEdit(_, id) {
        try {
            loading.Start();
            const res = await api.farhiAdmins.getForEdit(id);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            loading.Start();
            const res = await api.farhiAdmins.add(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            globalAlerts.message(message, "success");
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, payload ) {
        try {
            loading.Start();
            const res = await api.farhiAdmins.edit(payload);
            loading.Stop();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(payload.userId);
            commit('edit', { index, payload })
            globalAlerts.message(message, "success");
            //return new Promise.resolve();
        }
        catch (err) {
            globalAlerts.catch(err)
            return new Promise.reject(new Error());
        }
    },
    async lock({ commit, state }, id) {
        let index = getters.index(id);
        let loginName = state.users[index].loginName;
        let title = `هذا الامر سيقوم بتجميد المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.farhiAdmins.lock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                commit('lock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async unlock({ commit, state }, id) {
        let index = getters.index(id);
        let loginName = state.users[index].loginName;
        let title = `هذا الامر سيقوم بفك تجميد المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.farhiAdmins.unlock(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;

                commit('unlock', { index })
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    async delete({ commit, state }, id) {
        let index = getters.index(id);
        let loginName = state.users[index].loginName;
        let title = `هذا الامر سيقوم بحذف المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.farhiAdmins.delete(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let message = res.data.message;
                let index = getters.index(id);
                commit('delete', index)
                globalAlerts.message(message, "success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    
    async resetPassword({ state }, id) {
        console.log(id)
        let index = getters.index(id);
        let loginName = state.users[index].loginName;
        console.log(loginName)

        let title = `هذا الامر باعادة تعيين كلمة مرور المستخدم ${loginName}. الاستمرار?`;
        globalAlerts.confirmation(title)
        .then(async ()=>{
            try {
                loading.Start();
                const res = await api.farhiAdmins.resetPassword(id);
                loading.Stop();
                if (!res.data.statusCode) {
                    globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
                }
                let title = res.data.message;
                let message = "كلمة المرور الجديدة هي : " + res.data.data;
                globalAlerts.alert(title,message,"success");
            }
            catch (err) {
                globalAlerts.catch(err)
            }
        }).catch(() => {});
    },
    
    async getFeatures() {

        try {
            const res = await api.farhiAdmins.getFeatures();
            if (!res.data.statusCode) {
                globalAlerts.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            globalAlerts.catch(err);
            return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};