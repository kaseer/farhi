export default {
  created() {
    this.getAdminData()
  },
  props:{
    userId: Number
  },
  data: () => {
    return {
      adminData: {},
      activeCollapse: '',
    }
  },

  methods: {
    
    getAdminData(){
      this.$store.dispatch("farhiAdmins/getDetails", this.userId).then((res)=>{
        this.adminData = res
      })
    },
    back() {
      this.$parent.status = 0;
    }
  }
}
