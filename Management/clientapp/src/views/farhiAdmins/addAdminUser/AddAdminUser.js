export default {
  created() {
    this.setRules();
    this.getFeatures();
  },
  computed:{
    
  },

  data: () => {
    return {
      rules: {},
      adminData: {
        adminId: 0,
        adminName: "",
        loginName:"",
        phoneNo1: null,
        phoneNo2: null,
        email: null,
        password:null,
        permissions:[]
      },
      activeCollapse: '',
      features: [],
      loading: false
    }
  },

  methods: {
    generatePassword(){
      this.adminData.password = this.$parent.generatePassword()
    },
    getFeatures(){
      this.loading = true
      this.$store.dispatch("farhiAdmins/getFeatures").then((res)=>{
        this.features = res
        this.loading = false
      })
    },
    handleCheckAllChange(flag,index) {
      let permissions = this.features[index].permissions;
      if (flag){
        permissions.forEach(p => {
          if(!this.adminData.permissions.find(x=>x == p.permissionId))
            this.adminData.permissions.push(p.permissionId);
        });
        return;
      }
      else {
        permissions.forEach(p => {
          let index = this.adminData.permissions.indexOf(p.permissionId);
          if (index > -1) {
            this.adminData.permissions.splice(index, 1);
          }
        });
      }
    },
    handleCheckedChange(featureIndex) {
      let check = this.features[featureIndex].permissions;
      let data = [];
      check.forEach(x=>{
        if(this.adminData.permissions.find(c=>c == x.permissionId)){
          data.push(x.permissionId);
        }
      });
      if(data.length == check.length)
        this.features[featureIndex].checkAll = true;
      else
        this.features[featureIndex].checkAll = false;
    },
    setRules() {
      this.rules = {
        adminName: [
          { validator: this.$validation.checkInput(" اسم المستخدم ",40), trigger: 'change' }
        ],
        loginName: [
          { validator: this.$validation.checkUserName(), trigger: 'change' }
        ],
        phoneNo1: [
          { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
        ],
        phoneNo2: [
          { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
        ],
        email: [
          { required: "true", type: "email", message: "يرجي ادخال البريد الالكتروني", trigger: 'change' },
        ],
        password: [
          { validator: this.$validation.checkPassword("العنوان"), trigger: 'change' }
        ],
      }
    },
    add(){
      this.$refs["adminData"].validate((valid) => {
        if(valid){
          this.$store.dispatch("farhiAdmins/add",this.adminData).then(()=>{
            //this.$refs.resetForm();
            this.back();
          });
        }
        else
          this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');
      })
    },
    back() {
      this.$parent.status = 0;
    }
  }
}
