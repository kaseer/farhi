import AddAdminUser from './addAdminUser/AddAdminUser.vue'
import EditAdminUser from './editAdminUser/EditAdminUser.vue'
import ViewAdminUser from './viewAdminUser/ViewAdminUser.vue'

export default {
  components: {
    AddAdminUser,
    EditAdminUser,
    ViewAdminUser
  },
  computed:{
    total(){
      return this.$store.state.farhiAdmins.total
    },
    users(){
      return this.$store.state.farhiAdmins.users
    }
  },
  created() {
    this.getUsers();
    this.generatePassword()
  },
  data: () => {
    return {
      filter: {
        pageNo: 1,
        pageSize: 10,
        search: ""
      },
      status: 0,
      userId: 0
    }
  },

  methods: {
    getUsers() {
      this.$store.dispatch("farhiAdmins/getAll", this.filter);
    },
    reload() {
      this.filter.pageNo = 1;
      this.getUsers();
    },
    pageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getUsers();
    },
    add() {
      this.status = 1;
    },
    view(user) {
      this.status = 3;
      this.userId = user.adminId
    },
    edit(user) {
      this.userId = user.adminId
      this.status = 2;
    },
    lock(item) {
      this.$store.dispatch('farhiAdmins/lock', item.adminId);
    },
    unlock(item) {
      this.$store.dispatch('farhiAdmins/unlock', item.adminId);
    },
    deleteUser(item) {
      this.$store.dispatch('farhiAdmins/delete', item.adminId);
    },
    resetPassword(item) {
      this.$store.dispatch('farhiAdmins/resetPassword', item.adminId);
    },
    generatePassword(){
      var result = "";
      let lowercases = "abcdefghijklmnopqrstuvwxyz";
      let numbers = "0123456789";
      let uppercases = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      let sympols = "#?!@$%^&*-";
      for ( var i = 0; i < 2; i++ ) {
        result += lowercases.charAt(Math.floor(Math.random() * lowercases.length));
        result += numbers.charAt(Math.floor(Math.random() * numbers.length));
        result += uppercases.charAt(Math.floor(Math.random() * uppercases.length));
        result += sympols.charAt(Math.floor(Math.random() * sympols.length));
      }
      return result;
    }
  }
}
