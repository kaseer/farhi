// import { Slider, SliderItem } from "vue-easy-slider";

export default {
    // components: {
    //   Slider,
    //   SliderItem
    // },
    created() {
        this.setRules();
        this.getStores();
        this.getHalls();
        this.getAll();
    },
    computed:{
        stores() {
            return this.$store.state.lookup.stores
        },
        halls() {
            return this.$store.state.lookup.halls
        },
        sliders() {
            return this.$store.state.sliderManagement.sliders
        },
        loading() {
            return this.$store.state.lookup.loading
        },
    },
    data: () => {
        return {
            rules: {},
            addDialog: false,
            editFlag: false,
            sliderValue: 0,
            sliderData: {
                customerType: null,
                hallStoreId: null,
                image: [],
                description: null
            },
        }
    },

    methods: {

        getStores() {
            this.$store.dispatch('lookup/getStores');
        },
        getHalls() {
            this.$store.dispatch('lookup/getHalls');
        },
        getAll() {
            this.$store.dispatch('sliderManagement/getAll');
        },
        handlePreview(file) {
            console.log(file);
        },
        handleAvatarSuccess(res) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.sliderData.image = event.target.result;
            };
            reader.readAsDataURL(res.raw);
            //console.log(this.sliderData.images);
            //this.showUpload = URL.createObjectURL(file.raw);
        },
        setRules() {
            this.rules = {}
        },
        addToSliderDialog() {
            this.addDialog = true;
        },
        deleteSlider(index) {
            this.$confirm('This will permanently Delete the file. Continue?', 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                this.$message({
                    type: 'success',
                    message: 'Locked completed'
                });
                this.sliders.splice(index, 1);
            }).catch(() => { });
        },
        editSlider() {
            this.editFlag = true;
        },
        add(){
            let data = {
                sliderManagementId: 1,
                description: "c.Description",
                path: "/img/الهناء2121_1_4.jpg",
                mediaType: 1
            };
            this.$store.dispatch('sliderManagement/add',data);
            this.closeSliderDialog();
        },
        closeSliderDialog() {
            this.sliderData.customerType = null;
            this.sliderData.hallStoreId = null;
            this.sliderData.description = null;
            this.sliderData.image = null;
            this.addDialog = false;
        },
        back() {
            this.$parent.status = 0;
        }
    }
}
