﻿import viewStore from "./viewStore/ViewStore.vue";
import editStore from "./editStore/EditStore.vue";
import addStore from "./addStore/AddStore.vue";

export default {
    created() {
        this.getAll();
    },
    components: {
        'view-store': viewStore,
        'edit-store': editStore,
        'add-store': addStore,
    },
    computed: {
        stores() {
            return this.$store.state.stores.stores
        },
        total() {
            return Math.floor(this.$store.state.stores.total);
        }
    },
    data: () => {
        return {
            filter: {
                pageNo: 1,
                pageSize: 10,
                searchByName: ''
            },
            status: 0,
            storeId: null
        }
    },

    methods: {
        getAll() {
            this.$store.dispatch('stores/getAll', this.filter);
        },
        pageChanged(pageNo) {
            this.filter.pageNo = pageNo;
            this.getAll();
        },
        reload() {
            this.filter.pageNo = 1;
            this.getAll();
        },
        change() {
            this.status = 1;
        },
        viewStore(index) {
            this.status = 3;
            this.storeId = this.stores[index].storeId;
        },
        editStore(index) {
            this.status = 2;
            this.storeId = this.stores[index].storeId;
        },

        lockStore(index) {
            this.$store.dispatch('stores/lock', this.stores[index].storeId);
        },
        unlockStore(index) {
            this.$store.dispatch('stores/unlock', this.stores[index].storeId);
        },
        deleteStore(index) {
            this.$store.dispatch('stores/delete', this.stores[index].storeId);
        }
    }
}
