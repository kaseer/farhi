export default {
    created() {
        this.viewStore();
    },

    props: {
        storeId: Number,
    },
    data: () => {
        return {
            storeData: {
                storeName: null,
                storeType: null,
                ownerName: null,
                phoneNo1: null,
                phoneNo2: null,
                email: null,
                logo: null,
                storeBranch: {
                    addressName: null,
                    addressDescription: null,
                    phoneNo1: null,
                    phoneNo2: null,
                    longitude: null,
                    latitude: null,
                    startTime: null,
                    endTime: null,
                },
                farhiUser: {
                    userName: null,
                    loginName: null
                }
            },
        }
    },

    methods: {
        viewStore() {
            this.$store.dispatch('stores/getDetails', this.storeId)
                .then((val) => {
                    this.storeData = val.storeData;
                    this.storeData.logo = val.image;
                });
        },
        back() {
            this.$parent.status = 0;
        }
    }
}
