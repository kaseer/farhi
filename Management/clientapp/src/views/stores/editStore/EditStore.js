export default {
    created() {

        this.setRules();
        this.getAddresses();
        this.getStoreTypes();
        this.getForEdit();
    },
    props: {
        storeId: Number,
    },
    computed: {
        addresses() {
            return this.$store.state.lookup.addresses
        },
        storeTypes() {
            return this.$store.state.lookup.storeTypes;
        },
        loading() {
            return this.$store.state.lookup.loading;
        }
    },
    data: () => {
        return {
            rules: {},
            storeData: {
                storeId: null,
                storeTypeId: null,
                storeName: null,
                ownerName: null,
                phoneNo1: null,
                phoneNo2: null,
                email: null,
                logo: '',
                storeBranch: {
                    addressId: null,
                    addressDescription: null,
                    phoneNo1: null,
                    phoneNo2: null,
                    longitude: null,
                    latitude: null,
                    timeRange: ['', ''],
                },
                //storePermission: true
                // storeAppointmants: [],
                // storeServices: []
            },
        }
    },

    methods: {
        getStoreTypes() {
            this.$store.dispatch('lookup/getStoreTypes');
        },
        getForEdit() {
            this.$store.dispatch('stores/getForEdit', this.storeId)
                .then((val) => {
                    this.storeData = val.storeData;
                    this.storeData.storeId = this.storeId;
                    this.storeData.logo = val.image;
                });
        },
        getAddresses() {
            this.$store.dispatch('lookup/getAddresses');
        },
        handleAvatarSuccess(res, file) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.storeData.logo = event.target.result;
            };
            reader.readAsDataURL(res.raw);
            console.log(file);
        },
        beforeAvatarUpload(file) {
            console.log(file)
        },
        setRules() {
            this.rules = {
                storeName: [
                    { validator: this.$validation.checkInput("المحل", 30), trigger: 'change' }
                ],
                ownerName: [
                    { validator: this.$validation.checkInput("اسم المالك", 40), trigger: 'change' }
                ],
                phoneNo1: [
                    //{ required: "true", message: "يرجي ادخال رقم الهاتف", trigger: 'change' },
                    { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                ],
                phoneNo2: [
                    { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                ],
                storeBranch: {
                    phoneNo1: [
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    phoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    email: [
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                    addressId: [
                        { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                    ],
                    addressDescription: [
                        { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                    ],
                    longitude: [
                        { validator: this.$validation.checkLongitude(), trigger: 'change' }
                    ],
                    latitude: [
                        { validator: this.$validation.checkLatitude(), trigger: 'change' }
                    ],

                    timeRange: [
                        { validator: this.$validation.checkDate(), trigger: 'change' }
                    ],
                },
            }
        },
        editItem() {
            this.$refs["storeData"].validate((valid) => {
                if (valid) {
                    this.$store.dispatch('stores/edit', this.storeData)
                    .then(() => {
                        this.back();
                    })
                    .catch(() => { });

                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');

                    return false;
                }
            });

        },
        back() {
            this.$parent.status = 0;
        }

    }
}
