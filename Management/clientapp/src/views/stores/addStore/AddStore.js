export default {
    created() {
        this.setRules();
        this.getAddresses();
        this.getStoreTypes();
        this.setStoreAppointments();
        this.setAppointmentRules();
    },

    computed: {
        addresses() {
            return this.$store.state.lookup.addresses
        },
        storeTypes() {
            return this.$store.state.lookup.storeTypes;
        },
        loading() {
            return this.$store.state.lookup.loading;
        },
        workDays(){
            return this.$store.state.lookup.days
        }
    },
    data: () => {
        return {
            rules: {},
            appointmentRules: {},
            storeData: {
                storeName: null,
                storeTypeId: null,
                ownerName: null,
                phoneNo1: null,
                phoneNo2: null,
                email: null,
                logo: '',
                storeBranch: {
                    addressId: null,
                    addressDescription: null,
                    phoneNo1: null,
                    phoneNo2: null,
                    longitude: null,
                    latitude: null,
                    timeRange: ['', ''],
                    storeAppointments: []
                },
                farhiUser: {
                    userName: null,
                    loginName: null,
                    password: null,
                    userPhoneNo1: null,
                    userPhoneNo2: null,
                    userEmail: null,
                },
                //storePermission: true
                // storeAppointmants: [],
                // storeServices: []
            },
            reservationTypes: [],
            workDaysCount: 0,
        }
    },

    methods: {
        setAllAppointments() {
            this.storeData.storeBranch.storeAppointments.forEach((time) => {
                time.timeRange = this.storeData.storeBranch.timeRange;
            });
        },
        setStoreAppointments() {
            for (var i = 1; i <= 7; i++) {
                this.storeData.storeBranch.storeAppointments.push({
                    workDayId: i,
                    timeRange: ['', ''],
                })
            }
        },
        deleteDay(index) {
            if (this.storeData.storeBranch.storeAppointments.length == 1) {
                this.$globalAlerts.message("لا بد من تسجيل موعد يوم واحد علي الاقل", 'error');
                return;
            }
            this.$confirm(`هذا الامر سيقوم بحذف موعد اليوم بالكامل ... الاستمرار ؟`, 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                this.storeData.storeBranch.storeAppointments.splice(index, 1)
            }).catch(() => { });
        },
        setAppointmentRules() {
            this.appointmentRules = {
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
            }
        },
        getStoreTypes() {
            this.$store.dispatch('lookup/getStoreTypes');
        },

        getAddresses() {
            this.$store.dispatch('lookup/getAddresses');
        },


        handleAvatarSuccess(res, file) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.storeData.logo = event.target.result;
            };
            reader.readAsDataURL(res.raw);
            console.log(file);
        },
        beforeAvatarUpload(file) {
            console.log(file)
        },
        setRules() {
            this.rules = {
                storeName: [
                    { validator: this.$validation.checkInput("المحل", 30), trigger: 'change' }
                ],
                storeTypeId: [
                    { validator: this.$validation.checkSelect("نوع المحل"), trigger: 'change' }
                ],
                ownerName: [
                    { validator: this.$validation.checkInput("اسم المالك", 40), trigger: 'change' }
                ],
                phoneNo1: [
                    //{ required: "true", message: "يرجي ادخال رقم الهاتف", trigger: 'change' },
                    { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                ],
                phoneNo2: [
                    { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                ],
                storeBranch: {
                    phoneNo1: [
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    phoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    email: [
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                    addressId: [
                        { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                    ],
                    addressDescription: [
                        { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                    ],
                    longitude: [
                        { validator: this.$validation.checkLongitude(), trigger: 'change' }
                    ],
                    latitude: [
                        { validator: this.$validation.checkLatitude(), trigger: 'change' }
                    ],
                    //workDayFrom: [
                    //    { validator: this.$validation.checkSelect("بداية العمل"), trigger: 'change' }
                    //],
                    //workDayTo: [
                    //    { validator: this.$validation.checkSelect("نهاية العمل"), trigger: 'change' }
                    //],
                    timeRange: [
                        { validator: this.$validation.checkDate(), trigger: 'change' }
                    ],
                },
                farhiUser: {
                    userName: [
                        { validator: this.$validation.checkInput("اسم المستخدم", 30), trigger: 'change' }
                    ],
                    loginName: [
                        { validator: this.$validation.checkInput("اسم الدخول", 30), trigger: 'change' }
                    ],
                    password: [
                        { validator: this.$validation.checkPassword(), trigger: 'change' }
                    ],
                    userPhoneNo1: [
                        //{ required: "true", message: "يرجي ادخال رقم الهاتف", trigger: 'change' },
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    userPhoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    userEmail: [
                        { required: "true", type: "email", message: "يرجي ادخال البريد الالكتروني", trigger: 'change' },
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                },



                //storePermission: [{ required: true}]
            }
        },
        addItem() {
            let validFlag = true;
            this.$refs["storeData"].validate((valid) => {
                if (valid) {
                    for (var i = 0; i < this.$refs["storeAppointments"].length; i++) {
                        this.$refs["storeAppointments"][i].validate((valid) => {
                            if (!valid)
                                validFlag = false;
                        });
                    }
                    if (!validFlag) {
                        this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
                        return;
                    }
                    
                    this.$store.dispatch('stores/add', this.storeData)
                    .then(() => {
                        this.back();
                    })
                    .catch(() => { });

                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');

                    return false;
                }
            });

        },
        back() {
            this.$parent.status = 0;
        }

    }
}
