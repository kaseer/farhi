

export default {
  data: () => {
    return {
      pageSize: 10,
      total: 100,
      pageNo: 1
    }
  },

  methods: {
    pageChanged(pageNo) {
      this.pageNo = pageNo;
    },
  }
}
