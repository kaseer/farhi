// import viewstore from "./viewStore/ViewStore.vue";
import editStoreBranch from "./editStoreBranch/EditStoreBranch.vue";
import addStoreBranch from "./addStoreBranch/AddStoreBranch.vue";
import addMedia from "./addMedia/AddMedia.vue";
import viewStoreBranch from "./viewStoreBranch/ViewStoreBranch.vue";
import addSubscription from "./addSubscription/AddSubscription.vue";

export default {
    created() {
        this.getStores();
        this.getStoreTypes();
    },
    components: {
        'view-store-branch': viewStoreBranch,
        'edit-store-branch': editStoreBranch,
        'add-store-branch': addStoreBranch,
        'add-media': addMedia,
        'add-subscription': addSubscription
    },
    computed:{
        stores() {
            return this.$store.state.lookup.stores
        },
        storeTypes() {
            return this.$store.state.lookup.storeTypes
        },
        loading() {
            return this.$store.state.lookup.loading
        },
        storeBranches() {
            return this.$store.state.storeBranches.storeBranches
        },
        total() {
            return this.$store.state.storeBranches.total
        },
    },
    data: () => {
        return {
            filter: {
                pageNo: 1,
                pageSize: 10,
                storeId: null,
                storeTypeId: 0,
                searchByAddress: ''
            },
            // stores: [],
            // storeBranches: [],
            // total: 0,
            status: 0,
            storeBranchId: null,
            // storeTypes: [],
            storeId: null
        }
    },

    methods: {
        getStoreTypes() {
            this.$store.dispatch('lookup/getStoreTypes');
        },
        getStores() {
            this.$store.dispatch('lookup/getStores');
        },
        getStoreBranches() {
            this.$store.dispatch('storeBranches/getAll',this.filter);
        },
        reload() {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.filter.pageNo = 1;
            this.getStoreBranches();
        },
        pageChanged(pageNo) {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.filter.pageNo = pageNo;
            this.getStoreBranches();
        },
        change() {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.storeId = this.filter.storeId;
            this.status = 1;
        },
        viewStoreBranch(index) {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.storeBranchId = this.storeBranches[index].storeBranchId;
            this.status = 3;
        },
        editStoreBranch(index) {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.storeBranchId = this.storeBranches[index].storeBranchId;
            this.status = 2;
        },
        addMedia(index) {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.storeBranchId = this.storeBranches[index].storeBranchId;
            this.status = 4;
        },
        addSubscription(index) {
            if (!this.filter.storeId) {
                this.$globalAlerts.message("يرجي اختيار المحل", 'warning');
                return;
            }
            this.storeBranchId = this.storeBranches[index].storeBranchId;
            this.status = 5;
        },
        lockStoreBranch(item) {
            this.$store.dispatch('storeBranches/lock',item);
        },
        unlockStoreBranch(item) {
            this.$store.dispatch('storeBranches/unlock',item);
        },
        deleteStoreBranch(item) {
            this.$store.dispatch('storeBranches/delete',item);
        },
        assignMainBranch(item) {
            this.$store.dispatch('storeBranches/assignMainBranch',item);
        }
    }
}
