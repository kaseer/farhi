export default {
    created() {
        this.storeBranch.storeBranchId = this.storeBranchId;
        this.getForEdit();
        this.setRules();
        this.$store.dispatch('lookup/getAddresses');
        this.setAppointmentRules();
    },
    computed: {
        addresses() {
            return this.$store.state.lookup.addresses
        },
        workDays(){
            return this.$store.state.lookup.days
        }
    },

    props: {
        storeBranchId: Number
    },
    data: () => {
        return {
            rules: {},
            appointmentRules: {},
            storeBranch: {
                storeId: null,
                storeBranchId: null,
                addressId: null,
                addressDescription: null,
                phoneNo1: null,
                phoneNo2: null,
                longitude: null,
                latitude: null,
                timeRange: ['', ''],
                storeAppointments: []
            },
            workDaysCount: 0,
            noneSelectedDays: []
        }
    },

    methods: {
        getForEdit() {
            this.$store.dispatch('storeBranches/getForEdit', this.storeBranchId)
                .then((res) => {
                    this.storeBranch = res;
                    this.storeBranch.storeId = this.$parent.filter.storeId;
                    this.storeBranch.storeBranchId = this.storeBranchId;
                    if (this.storeBranch.storeAppointments <= 0) {
                        this.setStoreAppointments();
                    }
                    else {
                        this.storeBranch.storeAppointments.forEach((appointment) => {
                            let dayId = this.workDays.find(x => x.dayId == appointment.workDayId).dayId;
                            if (!dayId) {
                                this.noneSelectedDays.push(dayId);
                            }
                        });
                    }

                    this.flag = true;
                })
                .catch(() => { });

        },

        setStoreAppointments() {
            for (var i = 1; i <= 7; i++) {
                this.storeBranch.storeAppointments.push({
                    workDayId: i,
                    timeRange: ['', ''],
                })
            }
        },
        deleteDay(index) {
            if (this.storeBranch.storeAppointments.length == 1) {
                this.$globalAlerts.message("لا بد من تسجيل موعد يوم واحد علي الاقل", 'error');
                return;
            }
            this.$confirm(`هذا الامر سيقوم بحذف موعد اليوم بالكامل ... الاستمرار ؟`, 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                let dayId = this.storeBranch.storeAppointments[index].workDayId
                this.storeBranch.storeAppointments.splice(index, 1)
                this.noneSelectedDays.push(dayId);
            }).catch(() => { });
        },
        addDay(day, index) {
            this.storeBranch.storeAppointments.push({
                workDayId: day,
                timeRange: this.storeBranch.timeRange,
            });
            this.noneSelectedDays.splice(index, 1);
        },
        setAppointmentRules() {
            this.appointmentRules = {
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
            }
        },

        setRules() {
            this.rules = {
                storeBranch: {
                    phoneNo1: [
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    phoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    email: [
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                    addressId: [
                        { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                    ],
                    addressDescription: [
                        { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                    ],
                    longitude: [
                        { validator: this.$validation.checkLongitude(), trigger: 'change' }
                    ],
                    latitude: [
                        { validator: this.$validation.checkLatitude(), trigger: 'change' }
                    ],
                    timeRange: [
                        { validator: this.$validation.checkTime(), trigger: 'change' }
                    ],
                },
            }
        },
        editItem() {
            let validFlag = true;
            this.$refs["storeBranch"].validate((valid) => {
                if (valid) {
                    for (var i = 0; i < this.$refs["storeAppointments"].length; i++) {
                        this.$refs["storeAppointments"][i].validate((valid) => {
                            if (!valid)
                                validFlag = false;
                        });
                    }
                    if (!validFlag) {
                        this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
                        return;
                    }

                    this.$store.dispatch('storeBranches/edit', this.storeBranch)
                        .then(() => {
                            this.back();
                        })
                        .catch(() => { });

                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');

                    return false;
                }
            });

        },
        back() {
            this.$parent.status = 0;
        }

    }
}
