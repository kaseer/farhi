export default {
    created() {
        this.setRules();
        this.setAppointmentRules();
        this.$store.dispatch('lookup/getAddresses');
        this.setStoreAppointments();
        this.storeBranch.storeId = this.storeId;
    },
    props: {
        storeId: Number
    },
    computed:{
        addresses() {
            return this.$store.state.lookup.addresses
        },
        loading() {
            return this.$store.state.lookup.loading;
        },
        workDays(){
            return this.$store.state.lookup.days
        }
    },
    data: () => {
        return {
            rules: {},
            appointmentRules: {},
            storeBranch: {
                storeId: null,
                storeBranch: null,
                addressId: null,
                addressDescription: null,
                phoneNo1: null,
                phoneNo2: null,
                longitude: null,
                latitude: null,
                timeRange: ['', ''],
                storeAppointments: []
            },
            workDaysCount: 0,
            noneSelectedDays: []
        }
    },

    methods: {
        setAllAppointments() {
            this.storeBranch.storeAppointments.forEach((time) => {
                time.timeRange = this.storeBranch.timeRange;
            });
        },
        setStoreAppointments() {
            for (var i = 1; i <= 7; i++) {
                this.storeBranch.storeAppointments.push({
                    workDayId: i,
                    timeRange: ['', ''],
                })
            }
        },
        deleteDay(index) {
            if (this.storeBranch.storeAppointments.length == 1) {
                this.$globalAlerts.message("لا بد من تسجيل موعد يوم واحد علي الاقل", 'error');
                return;
            }
            this.$confirm(`هذا الامر سيقوم بحذف موعد اليوم بالكامل ... الاستمرار ؟`, 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                let dayId = this.storeBranch.storeAppointments[index].workDayId
                this.storeBranch.storeAppointments.splice(index, 1)
                this.noneSelectedDays.push(dayId);
            }).catch(() => { });
        },
        addDay(day, index) {
            this.storeBranch.storeAppointments.push({
                workDayId: day,
                timeRange: this.storeBranch.timeRange,
            });
            this.noneSelectedDays.splice(index, 1);
        },
        setAppointmentRules() {
            this.appointmentRules = {
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
            }
        },

        setRules() {
            this.rules = {
                storeBranch: {
                    phoneNo1: [
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    phoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    email: [
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                    addressId: [
                        { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                    ],
                    addressDescription: [
                        { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                    ],
                    longitude: [
                        { validator: this.$validation.checkLongitude(), trigger: 'change' }
                    ],
                    latitude: [
                        { validator: this.$validation.checkLatitude(), trigger: 'change' }
                    ],
                    timeRange: [
                        { validator: this.$validation.checkTime(), trigger: 'change' }
                    ],
                },
            }
        },
        addItem() {
            let validFlag = true;
            this.$refs["storeBranch"].validate((valid) => {
                if (valid) {
                    for (var i = 0; i < this.$refs["storeAppointments"].length; i++) {
                        this.$refs["storeAppointments"][i].validate((valid) => {
                            if (!valid)
                                validFlag = false;
                        });
                    }
                    if (!validFlag) {
                        this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
                        return;
                    }
                    this.$store.dispatch('storeBranches/add', this.storeBranch)
                        .then(() => {
                            this.back();
                        })
                        .catch(() => { });
                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');
                    return false;
                }
            });
        },
        back() {
            this.$parent.status = 0;
        }

    }
}
