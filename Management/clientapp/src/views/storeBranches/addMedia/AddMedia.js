import EleUploadVideo from 'vue-ele-upload-video'

export default {
    props: {
        storeBranchId: Number
    },
    created() {
        this.getVideo();
        this.getImages();
    },
    components: {
        EleUploadVideo
    },
    computed:{
        oldImages() {
            return this.$store.state.storeBranches.oldImages
        },
        
    },
    data: () => {
        return {
            activeName: 'first',
            categories: [],
            subCategories: [],
            showUpload: false,
            images: [],
            oldImagesLoading: true,
            video: '',
            checkedImages: [],
            //selected: {

            //},

        }
    },
    methods: {

        handleRemove(file, fileList) {
            var index = fileList.map(function (e) { return e.uid; }).indexOf(file.uid);
            this.images.splice(index, 1)
        },
        // handlePreview(file) {
        //     console.log(file);
        // },
        handleAvatarSuccess(res) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.images.push(event.target.result);
            };
            reader.readAsDataURL(res.raw);
        },
        handleUploadError() {
            this.$message.error(`خطأ في تحميل الملف`);
        },
        handleResponse(_, file) {
            const reader = new FileReader();
            reader.onload = (event) => {
                this.video = event.target.result;
            };
            reader.readAsDataURL(file.raw);
            return this.video;
            //return  'https://s3.pstatp.com/aweme/resource/web/static/image/index/tvc-v2_30097df.mp4' 
        },
        getVideo() {
            this.$store.dispatch('storeBranches/getVideo', this.storeBranchId)
            .then(res => {
                this.video = res;
            })
            .catch(() => {});
        },
        getImages() {
            this.$store.dispatch('storeBranches/getImages', this.storeBranchId)
                .then(()=>{
                    this.oldImagesLoading = false;
                })
                .catch(()=>{});
        },
        deleteOldImages() {
            this.$store.dispatch('storeBranches/deleteOldImages', this.checkedImages)
            .then(()=>{
                this.checkedImages = [];
            })
            .catch(()=>{});
        },
        
        submitUpload() {
            let data = {
                storeBranchId:this.storeBranchId,
                images: this.images
            };
            this.$store.dispatch('storeBranches/addImages', data)
            .then(()=>{
                this.images = [];
                this.$refs.upload.submit();
            });
        },
        
        async deleteVideo() {
            this.$store.dispatch('storeBranches/deleteVideo', this.storeBranchId)
            .then(()=>{
                this.video = "";
            })
            .catch(()=>{});
        },
        handleExceed() {
            this.$message.warning(`لايمكن تحميل اكثر من 10 صور`);
        },
        Back() {
            this.$parent.status = 0;
        }
    }
}