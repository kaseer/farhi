export default {
  mounted() {
    this.getStoreBranchData();
  },
    props: {
        storeBranchId: Number
    },
    computed: {
      storeBranchData() {
        return this.$store.state.storeBranches.storeBranch
      },
    },
  data: () => {
    return {
        flag: false
    }
  },

  methods: {
    getStoreBranchData(){
      this.$store.dispatch('storeBranches/getDetails', this.storeBranchId);
    },

    back() {
      this.$parent.status = 0;
    }
  }
}
