export default {
    created() {
        this.getSubscriptionPrices();
        this.data.storeBranchId = this.storeBranchId
    },
    props: {
        storeBranchId: Number
    },
    computed: {
        subscriptionPrices() {
            return this.$store.state.lookup.subscriptionPrices
        },
        subscriptionPricesDetails() {
            return this.$store.state.lookup.subscriptionPricesDetails
        },
        loading() {
            return this.$store.state.lookup.loading
        }
    },
    data: () => {
        return {
            data: {
                storeBranchId: null,
                subscriptionPriceDetailsId: null,
            },
            subscriptionPriceId: null,
            //   subscriptionPrices:[],
            // subscriptionPricesDetails: [],
            rules: {},
            subscriptionData: {
                storeBranchId: null,
                subscriptionDetails: []
            },
            index: null,
            durationTypes: []
        }
    },

    methods: {
        getSubscriptionPrices() {
            this.$store.dispatch('lookup/getSubscriptionPrices',2);
        },
        getSubscriptionPricesDetails() {
            this.$store.dispatch('lookup/getSubscriptionPricesDetails',this.subscriptionPriceId);
        },
        selectSubscription(val) {
            this.data.subscriptionPriceDetailsId = val.subscriptionPriceDetailsId;
        },
        addSubscription() {
            if (this.data.storeBranchId == null || this.data.subscriptionPriceDetailsId == null) {
                this.$globalAlerts.message("يرجي اختيار المحل و الاشتراك المطلوب", 'error');
                return;
            }
            this.$store.dispatch('storeBranches/addSubscription',this.data).then(()=>{
                this.back();
            }).catch(() => {});
        },

        back() {
            this.$parent.status = 0;
        }


    }
}
