
export default {
  components: {
    // 'view-hall' :viewHall ,
    // 'edit-hall' :editHall ,
    // 'add-hall' :addHall
  },
  created() {
    this.getAll();
  },
  computed: {
    productCategories() {
      return this.$store.state.productCategories.productCategories
    },
    total() {
      return Math.floor(this.$store.state.productCategories.total);
    }
  },
  data: () => {
    return {
      productCategory: {
        productCategoryId:0,
        description: ""
      },
      title: "",
      dialogType: "",
      filter: {
        pageNo: 1,
        pageSize: 10,
        search: ""
      },
      
      dialogFlag: false,
      editFlag: false,
      status: 0,
    }
  },

  methods: {
    getAll() {
      this.$store.dispatch('productCategories/getAll', this.filter);
    },
    reload() {
      this.filter.pageNo = 1;
      this.getAll();
    },
    PageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getAll();
    },
    addType() {
      this.title = "إضافة نوع";
      this.dialogType = "add";
      this.dialogFlag = true;
    },
    editType(index) {
      this.title = "تعديل نوع";
      this.dialogType = "edit";
      this.dialogFlag = true;
      this.productCategory.productCategoryId = this.productCategories[index].productCategoryId;
      this.productCategory.description = this.productCategories[index].description;
    },
    dialogOperation(){
      this.$store.dispatch(`productCategories/${this.dialogType}`, this.productCategory).then(()=>{
        this.close()
      });
    },
    
    close() {
      this.dialogFlag = false;
      this.productCategory.description = "";
    }
  }
}
