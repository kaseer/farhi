export default {
  created() {
    this.$store.dispatch("subscriptionPrices/getDetails",this.itemId).then((res)=> {
      this.subscriptionData = res;
    });
  },
  props:{
    itemId: Number
  },
  data: () => {
    return {
      subscriptionData: {
        description: null,
        subscriptionDetails: []
      },
    }
  },

  methods: {
   
    back() {
      this.$parent.status = 0;
    }
  }
}
