export default {
  created() {
    this.setRules();
    this.setSSRules();
  },
  computed: {
    durationTypes() {
      return this.$store.state.subscriptionPrices.durationTypes
    },
    subscriptionTypes() {
      return this.$store.state.subscriptionPrices.subscriptionTypes
    },
    loading() {
      return this.$store.state.lookup.loading
    }
  },
  data: () => {
    return {
      rules: {},
      ssRules: {},
      subscriptionData: {
        description: "",
        type: null,
        subscriptionDetails: []
      },
      index: 0,
    }
  },

  methods: {

    deleteItem(index) {
      this.index--;
      this.subscriptionData.subscriptionDetails.splice(index, 1);
    },
    addSubscriptionDetails() {
      // console.log(this.subscriptionData.subscriptionDetails)
      // let index = this.subscriptionData.subscriptionDetails.length - 1;
      if (this.subscriptionData.subscriptionDetails.length == 0) {
        this.addDetails()
        return;
      }
      let count = 0;
      for (var i = 0; i < this.$refs["subscriptionDetails"].length; i++) {
        this.$refs["subscriptionDetails"][i].validate((valid) => {
          if (!valid) count++
        });
      }
      if (count == 0) this.addDetails();
    },

    addDetails() {
      this.subscriptionData.subscriptionDetails.push({
        durationTime: null,
        durationType: null,
        price: null,
      });
    },
    setRules() {
      this.rules = {
        description: [
          { validator: this.$validation.checkInput("الوصف", 70), trigger: 'change' }
        ],
        type: [
          { validator: this.$validation.checkSelect("نوع الاشتراك"), trigger: 'change' }
        ],
      }
    },
    setSSRules() {
      this.ssRules = {
        durationTime: [
          { validator: this.$validation.checkInputNumber("قيمة المدة", 30), trigger: 'change' }
        ],
        durationType: [
          { validator: this.$validation.checkSelect("نوع المدة"), trigger: 'change' }
        ],
        price: [
          { validator: this.$validation.checkInputNumber("السعر", 2000), trigger: 'change' }
        ],
      }
    },
    add() {
      let validFlag = true;
      this.$refs["subscriptionData"].validate((valid) => {
        if (valid) {
          for (var i = 0; i < this.$refs["subscriptionDetails"].length; i++) {
            this.$refs["subscriptionDetails"][i].validate((valid) => {
              if (!valid)
                validFlag = false;
            });
          }
          if (!validFlag) {
            this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
            return;
          }

          this.$store.dispatch('subscriptionPrices/add', this.subscriptionData)
            .then(() => {
              this.back();
            })
            .catch(() => { });

        } else {
          this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');

          return false;
        }
      });
    },
    back() {
      this.$parent.status = 0;
    }
  }
}
