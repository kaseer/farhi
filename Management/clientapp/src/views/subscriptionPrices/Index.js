import addSubscription from './add/AddSubscription.vue'
import EditSubscription from './edit/EditSubscription.vue'
import ViewSubscription from './view/ViewSubscription.vue'

export default {
  components: {
    addSubscription,
    EditSubscription,
    ViewSubscription,
  },
  created() {
    this.getSubscriptionPrices();
  },
  computed:{
    subscriptionPrices(){
      return this.$store.state.subscriptionPrices.subscriptionPrices
    },
    total(){
      return this.$store.state.subscriptionPrices.total
    },
  },
  data: () => {
    return {
      filter: {
        pageNo: 1,
        pageSize: 10,
        search: ""
      },
      status: 0,
      itemId: null
    }
  },

  methods: {
    getSubscriptionPrices() {
      this.$store.dispatch("subscriptionPrices/getAll", this.filter)
    },
    reload(){
      this.filter.pageNo = 1;
      this.getSubscriptionPrices()
    },
    pageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getSubscriptionPrices()
    },
    add() {
      this.status = 1;
    },
    view(item) {
      this.status = 3;
      this.itemId = item.subscriptionPriceId
    },
    edit(item) {
      this.itemId = item.subscriptionPriceId
      this.status = 2;
    },
    deleteSubscription(item) {
      this.$store.dispatch('subscriptionPrices/delete', item.subscriptionPriceId);
    },
  }
}
