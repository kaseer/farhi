// import viewHall from "./viewHall/ViewHall.vue";
import editHallBranch from "./editHallBranch/EditHallBranch.vue";
import addHallBranch from "./addHallBranch/AddHallBranch.vue";
import addMedia from "./addMedia/AddMedia.vue";
import viewHallBranch from "./viewHallBranch/ViewHallBranch.vue";
import addSubscription from "./addSubscription/AddSubscription.vue";

export default {
    created() {
        this.getHalls();
    },
    components: {
        'view-hall-branch': viewHallBranch,
        'edit-hall-branch': editHallBranch,
        'add-hall-branch': addHallBranch,
        'add-media': addMedia,
        'add-subscription': addSubscription
    },
    data: () => {
        return {
            filter: {
                pageNo: 1,
                pageSize: 10,
                hallId: null,
                searchByAddress: ''
            },
            // halls: [],
            // hallBranches: [],
            // total: 0,
            status: 0,
            hallBranchId: null,
        }
    },
    computed:{
        halls() {
            return this.$store.state.lookup.halls
        },
        loading() {
            return this.$store.state.lookup.loading
        },
        hallBranches() {
            return this.$store.state.hallBranches.hallBranches
        },
        total() {
            return this.$store.state.hallBranches.total
        },
    },

    methods: {
        getHalls() {
            this.$store.dispatch('lookup/getHalls');
        },

        getHallBranches() {
            this.$store.dispatch('hallBranches/getAll',this.filter);
        },
        reload() {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.filter.pageNo = 1;
            this.getHallBranches();
        },
        pageChanged(pageNo) {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.filter.pageNo = pageNo;
            this.getHallBranches();
        },
        change() {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.status = 1;
        },
        viewHallBranch(index) {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.hallBranchId = this.hallBranches[index].hallBranchId;
            this.status = 3;
        },
        editHallBranch(index) {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.hallBranchId = this.hallBranches[index].hallBranchId;
            this.status = 2;
        },
        addMedia(index) {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.hallBranchId = this.hallBranches[index].hallBranchId;
            this.status = 4;
        },
        addSubscription(index) {
            if (!this.filter.hallId) {
                this.$globalAlerts.message("يرجي اختيار الصالة", 'warning');
                return;
            }
            this.hallBranchId = this.hallBranches[index].hallBranchId;
            this.status = 5;
        },
        lockHallBranch(item) {
            this.$store.dispatch('hallBranches/lock',item);
            
        },
        unlockHallBranch(item) {
            this.$store.dispatch('hallBranches/unlock',item);
            
        },
        deleteHallBranch(item) {
            this.$store.dispatch('hallBranches/delete',item);
            
        },
        assignMainBranch(item) {
            this.$store.dispatch('hallBranches/assignMainBranch',item);
            // this.hallBranches[i].mainBranchId = null;
            // this.hallBranches[index].mainBranchId = this.hallBranches[index].hallBranchId;
            
        }
    }
}
