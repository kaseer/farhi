export default {
  created() {
    this.viewHallBranch();
  },
  props: {
    hallBranchId: Number
  },
  computed: {
    hallBranchData() {
      return this.$store.state.hallBranches.hallBranch
    },
  },
  data: () => {
    return {
      flag: false
    }
  },

  methods: {
    viewHallBranch() {
      this.$store.dispatch('hallBranches/getDetails', this.hallBranchId)
    },

    back() {
      this.$parent.status = 0;
    }
  }
}
