import { Number } from "core-js";

export default {
    created() {
        this.hallBranchData.hallBranchId = this.hallBranchId;
        this.getForEdit();
        this.setRules();
        this.$store.dispatch('lookup/getAddresses');
        this.$store.dispatch('lookup/getReservationTypes');
    },
    computed:{
        addresses() {
            return this.$store.state.lookup.addresses
        },
        reservationTypes() {
            return this.$store.state.lookup.reservationTypes
        },
        workDays(){
            return this.$store.state.lookup.days
        }
    },
    props: {
        hallBranchId: Number,
    },
    data: () => {
        return {
            rules: {},
            appointmentRules: {},
            servicesRules: {},
            hallBranchData: {
                hallId:null,
                hallBranchId: null,
                addressId: null,
                addressDescription: null,
                phoneNo1: null,
                phoneNo2: null,
                longitude: null,
                latitude: null,
                peapleCapacity: null,
                carsCapacity: null,
                timeRange: ['', ''],
                hallAppointmants: [],
                hallServices: []
            },
            noneSelectedDays: [],
            workDaysCount: 0,
            editServiceIndex: null,
            flag: false,
            activeCollapse: '',
        }
    },

    methods: {
        getForEdit() {
            this.$store.dispatch('hallBranches/getForEdit', this.hallBranchId)
                    .then((res) => {
                        this.hallBranchData = res;
                        this.hallBranchData.hallId = this.$parent.filter.hallId;
                        this.hallBranchData.hallBranchId = this.hallBranchId;
                        this.setAppointmentRules();
                        this.setServicesRules();
                        if (this.hallBranchData.hallAppointments.length <= 0) {
                            this.noneSelectedDays = this.workDays;
                        }
                            
                        else {
                            this.workDays.forEach((day) => {
                                let check = this.hallBranchData.hallAppointments.find(x => x.workDayId == day.dayId);
                                if (!check) {
                                    this.noneSelectedDays.push({
                                        dayId: day.dayId,
                                        dayName: day.dayName
                                    });
                                }
                            });
                        }
                    })
                    .catch(() => { });
        },
        addDay(day, index) {
            this.hallBranchData.hallAppointments.push({
                workDayId: day.dayId,
                appointmentData: [{
                    id:null,
                    reservationType: null,
                    timeRange: ['', ''],
                    price: 0,
                    reservationStatus: 0
                }]
            });
            this.noneSelectedDays.splice(index, 1);
        },
        addApponitment(index) {
            this.hallBranchData.hallAppointments[index].appointmentData.push({
                id :null,
                reservationType: null,
                timeRange: ['', ''],
                price: 0,
                reservationStatus: null
            });
        },
        deleteApponitment(index, detailsIndex) {
            this.hallBranchData.hallAppointments[index].appointmentData.splice(detailsIndex, 1)
        },
        deleteDay(index) {
            if (this.hallBranchData.hallAppointments.filter(x => x.reservationStatus != null).length) {
                this.$globalAlerts.message("لا يمكن حذف يوم يحتوي علي موعد محجوز", 'error');
                return;
            }
            if (this.hallBranchData.hallAppointments.length == 1) {
                this.$globalAlerts.message("لا بد من تسجيل موعد يوم واحد علي الاقل", 'error');
                return;
            }
            this.$confirm(`هذا الامر سيقوم بحذف موعد اليوم بالكامل ... الاستمرار ؟`, 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                let dayId = this.hallBranchData.hallAppointments[index].workDayId
                this.hallBranchData.hallAppointments.splice(index, 1)
                this.noneSelectedDays.push({
                    dayId: dayId,
                    dayName: this.$options.filters.dayName(dayId)
                });
            }).catch(() => { });
        },
        addService() {
            this.hallBranchData.hallServices.push({
                id: null,
                description: null,
                price: 0,
                reservationStatus: null
            });
        },
        editService(index) {
            this.editServiceIndex = index;
        },
        deleteService(index) {
            this.hallBranchData.hallServices.splice(index, 1)
        },


        setRules() {
            this.rules = {
                phoneNo1: [
                    { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                ],
                phoneNo2: [
                    { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                ],
                email: [
                    { validator: this.$validation.checkEmail(), trigger: 'change' }
                ],
                addressId: [
                    { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                ],
                addressDescription: [
                    { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                ],
                longitude: [
                    { validator: this.$validation.checkLongitude(), trigger: 'change' }
                ],
                latitude: [
                    { validator: this.$validation.checkLatitude(), trigger: 'change' }
                ],
                peapleCapacity: [
                    { validator: this.$validation.checkInputNumber("عدد الاشخاص", 5000), trigger: 'change' }
                ],
                carsCapacity: [
                    { validator: this.$validation.checkInputNumber("عدد السيارات", 400), trigger: 'change' }
                ],
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],

            }

        },

        setAppointmentRules() {
            this.appointmentRules = {
                reservationType: [
                    { validator: this.$validation.checkSelect("نوع الموعد"), trigger: 'change' }
                ],
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
                price: [
                    { validator: this.$validation.checkInputNumber("السعر", 999999999), trigger: 'change' }
                ],
            }
        },

        setServicesRules() {
            this.servicesRules = {
                description: [
                    { validator: this.$validation.checkInput('وصف الخدمة', 100), trigger: 'change' }
                ],
                price: [
                    { validator: this.$validation.checkInputNumber("السعر", 999999999), trigger: 'change' }
                ],
            }
        },

        editItem() {
            if (!this.$refs["hallAppointments"]) this.$refs["hallAppointments"] = [];
            if (!this.$refs["hallServices"]) this.$refs["hallServices"] = [];
            let validFlag = true;
            this.$refs["hallBranchData"].validate((valid) => {
                if (valid) {
                    for (var i = 0; i < this.$refs["hallAppointments"].length; i++) {
                        this.$refs["hallAppointments"][i].validate((valid) => {
                            if (!valid)
                                validFlag = false;
                        });
                    }
                    if (!this.$refs["hallAppointments"].length) validFlag = false;
                    if (!validFlag) {
                        this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
                        return;
                    }

                    if (this.hallBranchData.hallServices.length > 0) {
                        for (i = 0; i < this.$refs["hallServices"].length; i++) {
                            this.$refs["hallServices"][i].validate((valid) => {
                                if (!valid)
                                    validFlag = false;
                            });
                        }
                        if (!this.$refs["hallServices"].length) validFlag = false;

                        if (!validFlag) {
                            this.$globalAlerts.message("الرجاء إدخال بيانات خدمات الصالة بشكل صحيح", 'error');
                            return;
                        }
                    }
                    this.$store.dispatch('hallBranches/edit', this.hallBranchData)
                    .then(() => {
                        console.log("sdfsdfsd")
                        this.back();
                    })
                    .catch(() => {});
                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');
                    return false;
                }
            });


        },
        back() {
            this.$parent.status = 0;
        }
    }
}
