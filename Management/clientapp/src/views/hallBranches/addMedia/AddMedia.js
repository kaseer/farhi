import EleUploadVideo from 'vue-ele-upload-video'

export default {
    props: {
        hallBranchId: Number
    },
    created() {
        this.getVideo();
        this.getImages();
    },
    components: {
        EleUploadVideo
    },
    
    computed:{
        // video() {
        //     return this.$store.state.hallBranches.video
        // },
        oldImages() {
            return this.$store.state.hallBranches.oldImages
        },
        
    },
    data: () => {
        return {
            activeName: 'first',
            categories: [],
            subCategories: [],
            showUpload: false,
            images: [],
            // oldImages: [],
            oldImagesLoading: true,
            video: '',
            checkedImages: [],
            //selected: {

            //},

        }
    },
    methods: {

        handleRemove(file, fileList) {
            var index = fileList.map(function (e) { return e.uid; }).indexOf(file.uid);
            this.images.splice(index, 1)
        },
        // handlePreview(file) {
        //     console.log(file);
        // },
        handleAvatarSuccess(res) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.images.push(event.target.result);
            };
            reader.readAsDataURL(res.raw);
        },
        handleUploadError() {
            this.$message.error(`خطأ في تحميل الملف`);
        },
        handleResponse(_, file) {
            const reader = new FileReader();
            reader.onload = (event) => {
                this.video = event.target.result;
            };
            reader.readAsDataURL(file.raw);
            return this.video;
            //return  'https://s3.pstatp.com/aweme/resource/web/static/image/index/tvc-v2_30097df.mp4' 
        },
        getVideo() {
            this.$store.dispatch('hallBranches/getVideo', this.hallBranchId)
            .then(res => {
                this.video = res;
            })
            .catch(() => {});
        },
        getImages() {
            this.$store.dispatch('hallBranches/getImages', this.hallBranchId)
                .then(()=>{
                    this.oldImagesLoading = false;
                })
                .catch(()=>{});
        },
        deleteOldImages() {
            this.$store.dispatch('hallBranches/deleteOldImages', this.checkedImages)
            .then(()=>{
                this.checkedImages = [];
            })
            .catch(()=>{});
        },
        
        submitUpload() {
            let data = {
                hallBranchId:this.hallBranchId,
                images: this.images
            };
            this.$store.dispatch('hallBranches/addImages', data)
            .then(()=>{
                this.images = [];
                this.$refs.upload.submit();
            });
        },
        
        async deleteVideo() {
            this.$store.dispatch('hallBranches/deleteVideo', this.hallBranchId)
            .then(()=>{
                this.video = "";
            })
            .catch(()=>{});
        },
        handleExceed() {
            this.$message.warning(`لايمكن تحميل اكثر من 10 صور`);
        },
        Back() {
            this.$parent.status = 0;
        }
    }
}