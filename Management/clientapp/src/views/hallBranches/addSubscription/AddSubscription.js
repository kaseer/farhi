export default {
    created() {
        this.getSubscriptionPrices();
        this.data.hallBranchId = this.hallBranchId
    },
    props: {
        hallBranchId: Number
    },
    computed: {
        subscriptionPrices() {
            return this.$store.state.lookup.subscriptionPrices
        },
        subscriptionPricesDetails() {
            return this.$store.state.lookup.subscriptionPricesDetails
        },
        loading() {
            return this.$store.state.lookup.loading
        }
    },
    data: () => {
        return {
            data: {
                hallBranchId: null,
                subscriptionPriceDetailsId: null,
            },
            subscriptionPriceId: null,
            //   subscriptionPrices:[],
            // subscriptionPricesDetails: [],
            rules: {},
            subscriptionData: {
                hallBranchId: null,
                subscriptionDetails: []
            },
            index: null,
            durationTypes: []
        }
    },

    methods: {
        getSubscriptionPrices() {
            this.$store.dispatch('lookup/getSubscriptionPrices',1);
        },
        getSubscriptionPricesDetails() {
            this.$store.dispatch('lookup/getSubscriptionPricesDetails',this.subscriptionPriceId);
        },
        selectSubscription(val) {
            this.data.subscriptionPriceDetailsId = val.subscriptionPriceDetailsId;
        },
        addSubscription() {
            if (this.data.hallBranchId == null || this.data.subscriptionPriceDetailsId == null) {
                this.$globalAlerts.message("يرجي اختيار الصالة و الاشتراك المطلوب", 'error');
                return;
            }
            this.$store.dispatch('hallBranches/addSubscription',this.data).then(()=>{
                this.back();
            }).catch(() => {});
        },

        back() {
            this.$parent.status = 0;
        }


    }
}
