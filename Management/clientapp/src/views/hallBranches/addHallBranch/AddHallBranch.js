export default {
    created() {
        this.setHallAppointments();
        this.hallBranchData.hallId = this.$parent.filter.hallId;
        this.setRules();
        this.setAppointmentRules();
        this.setServicesRules();
        this.$store.dispatch('lookup/getAddresses');
        this.$store.dispatch('lookup/getReservationTypes');
    },
    computed: {
        addresses() {
            return this.$store.state.lookup.addresses
        },
        reservationTypes() {
            return this.$store.state.lookup.reservationTypes;
        },
        loading() {
            return this.$store.state.lookup.loading;
        },
        
        workDays(){
            return this.$store.state.lookup.days
        }
    },

    data: () => {
        return {
            rules: {},
            appointmentRules: {},
            servicesRules: {},
            hallBranchData: {
                hallId: null,
                hallBranchId: null,
                addressId: null,
                addressDescription: null,
                phoneNo1: null,
                phoneNo2: null,
                longitude: null,
                latitude: null,
                peapleCapacity: null,
                carsCapacity: null,
                workDayFrom: null,
                workDayTo: null,
                timeRange: ['', ''],
                hallAppointments: [],
                hallServices: []
            },
            workDaysCount: 0,
            editServiceIndex: null,
            activeCollapse: null,
        }
    },

    methods: {
        setHallAppointments() {
            for (var i = 1; i <= 7; i++) {
                this.hallBranchData.hallAppointments.push({
                    workDayId: i,
                    appointmentData: [{
                        reservationType: null,
                        timeRange: ['', ''],
                        price: 0,
                    }]
                })
            }
        },
        calculateWorkDays() {
            //if (!this.hallBranchData.workDayTo || !this.hallBranchData.workDayFrom)
            //    return;
            //if (this.hallBranchData.workDayTo < this.hallBranchData.workDayFrom) {
            //    this.workDaysCount = (7 - this.hallBranchData.workDayFrom) + 1 + this.hallBranchData.workDayTo;
            //}
            //else if (this.hallBranchData.workDayTo == this.hallBranchData.workDayFrom) {
            //    this.workDaysCount = 7;
            //}
            //else
            //    this.workDaysCount = (this.hallBranchData.workDayTo - this.hallBranchData.workDayFrom) + 1;

            //this.hallBranchData.hallAppointments = [];
            //for (var i = 0; i < this.workDaysCount; i++) {
            //    this.hallBranchData.hallAppointments.push({
            //        reservationType: null,
            //        timeRange: ['', ''],
            //        price: 0,
            //    });
            //}
            //this.getReservationTypes();
        },


        addApponitment(index) {
            this.hallBranchData.hallAppointments[index].appointmentData.push({
                id: null,
                reservationType: null,
                timeRange: ['', ''],
                price: 0,
                reservationStatus: null
            });
        },
        deleteApponitment(index,detailsIndex) {
            this.hallBranchData.hallAppointments[index].appointmentData.splice(detailsIndex, 1)
        },
        deleteDay(index) {
            if (this.hallBranchData.hallAppointments.length == 1) {
                this.$globalAlerts.message("لا بد من تسجيل موعد يوم واحد علي الاقل", 'error');
                return;
            }
            this.$confirm(`هذا الامر سيقوم بحذف موعد اليوم بالكامل ... الاستمرار ؟`, 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                this.hallBranchData.hallAppointments.splice(index, 1)
            }).catch(() => { });
        },
        addService() {
            this.hallBranchData.hallServices.push({
                id: null,
                description: null,
                price: 0,
                reservationStatus: null
            });
        },
        editService(index) {
            this.editServiceIndex = index;
        },
        deleteService(index) {
            this.hallBranchData.hallServices.splice(index, 1)
        },


        setRules() {
            this.rules = {
                phoneNo1: [
                    { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                ],
                phoneNo2: [
                    { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                ],
                email: [
                    { validator: this.$validation.checkEmail(), trigger: 'change' }
                ],
                addressId: [
                    { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                ],
                addressDescription: [
                    { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                ],
                longitude: [
                    { validator: this.$validation.checkLongitude(), trigger: 'change' }
                ],
                latitude: [
                    { validator: this.$validation.checkLatitude(), trigger: 'change' }
                ],
                peapleCapacity: [
                    { validator: this.$validation.checkInputNumber("عدد الاشخاص", 5000), trigger: 'change' }
                ],
                carsCapacity: [
                    { validator: this.$validation.checkInputNumber("عدد السيارات", 400), trigger: 'change' }
                ],
                //workDayFrom: [
                //    { validator: this.$validation.checkSelect("بداية العمل"), trigger: 'change' }
                //],
                //workDayTo: [
                //    { validator: this.$validation.checkSelect("نهاية العمل"), trigger: 'change' }
                //],
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
             
            }
          
        },

        setAppointmentRules() {
            this.appointmentRules = {
                reservationType: [
                    { validator: this.$validation.checkSelect("نوع الموعد"), trigger: 'change' }
                ],
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
                price: [
                    { validator: this.$validation.checkInputNumber("السعر", 999999999), trigger: 'change' }
                ],
            }
        },

        setServicesRules() {
            this.servicesRules = {
                description: [
                    { validator: this.$validation.checkInput('وصف الخدمة', 100), trigger: 'change' }
                ],
                price: [
                    { validator: this.$validation.checkInputNumber("السعر", 999999999), trigger: 'change' }
                ],
            }
        },

        addItem() {
            let validFlag = true;
            this.$refs["hallBranchData"].validate((valid) => {
                if (valid) {
                    for (var i = 0; i < this.$refs["hallAppointments"].length; i++) {
                        this.$refs["hallAppointments"][i].validate((valid) => {
                            if (!valid)
                                validFlag = false;
                        });
                    }
                    if (!validFlag) {
                        this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
                        return;
                    }

                    if (this.hallBranchData.hallServices.length > 0) {
                        for (i = 0; i < this.$refs["hallServices"].length; i++) {
                            this.$refs["hallServices"][i].validate((valid) => {
                                if (!valid)
                                    validFlag = false;
                            });
                        }
                        if (!validFlag) {
                            this.$globalAlerts.message("الرجاء إدخال بيانات خدمات الصالة بشكل صحيح", 'error');
                            return;
                        }
                    }


                    this.$store.dispatch('hallBranches/add', this.hallBranchData)
                    .then(() => {
                        this.back();
                    })
                    .catch(() => { });
                    

                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');

                    return false;
                }
            });

          
        },
        back() {
          this.$parent.status = 0;
        }
    }
}
