
export default {
  computed:{
    categories(){
      return this.$store.state.lookup.categories;
    },
    productSubCategories(){
      return this.$store.state.productSubCategories.productSubCategories;
    },
    total() {
      return Math.floor(this.$store.state.productSubCategories.total);
    }
  },
  created(){
    this.getCategories();
    this.getSubCategories();
  },
  data: () => {
    return {
      filter:{
        pageNo: 1,
        pageSize: 10,
        categoryId: 0,
        search: ""
      },
      title:"",
      productSubCategory: {
        productSubCategoryId:0,
        categoryId:null,
        description: null,
      },
      dialogType: "",
      dialogFlag: false,
      rules: {}
    }
  },

  methods: {
    getCategories(){
      this.$store.dispatch('lookup/getCategories');
    },
    getSubCategories(){
      this.$store.dispatch('productSubCategories/getAll', this.filter);
    },
    reload(){
      this.filter.pageNo = 1;
      this.getSubCategories();
    },
    PageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getSubCategories();
    },
    setRules(){
      this.rules = {
        description: [
          { validator: this.$validation.checkInput("النوع", 30), trigger: 'change' }
        ],
        categoryId: [
          { required: "true", trigger: 'change' }
        ],
      }
    },
    add() {
      this.title = "إضافة نوع";
      this.dialogType = "add";
      this.dialogFlag = true;
    this.setRules();

    },
    edit(productSubCategory) {
      this.title = "تعديل نوع";
      this.dialogType = "edit";
      this.dialogFlag=true;
      this.productSubCategory.categoryId = productSubCategory.categoryId;
      this.productSubCategory.productSubCategoryId = productSubCategory.productSubCategoryId;
      this.productSubCategory.description = productSubCategory.description;
    },
    dialogOperation(){
      this.$refs["productSubCategory"].validate((valid) => {
        if (valid) {
          this.$store.dispatch(`productSubCategories/${this.dialogType}`, this.productSubCategory).then(()=>{
            this.close()
          });
        } else {
            this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');
            return false;
        }
      });
      
    },
    
    close() {
      this.dialogFlag = false;
      this.productSubCategory.description = "";
      this.productSubCategory.categoryId = null;
      this.productSubCategory.productSubCategoryId = null;
    },
  }
}
