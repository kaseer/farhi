import ViewCustomer from './viewCustomer/ViewCustomer.vue'

export default {
  components: {
    ViewCustomer,
  },
  created() {
    this.getCustomers();
  },
  computed:{
    total(){
      return this.$store.state.customers.total
    },
    customers(){
      return this.$store.state.customers.customers
    }
  },
  data: () => {
    return {
      filter: {
        pageNo: 1,
        pageSize: 10,
        search: ""
      },
      status: 0,
      itemId:0
    }
  },

  methods: {
    getCustomers() {
      this.$store.dispatch("customers/getAll",this.filter)
    },
    reload() {
      this.filter.pageNo = 1;
      this.getCustomers()
    },
    pageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getCustomers()
    },
    view(item) {
      this.itemId = item.customerId
      this.status = 3;
    },
    resetPassword(item) {
      this.$store.dispatch('customers/resetPassword', item.customerId);
    },
  }
}
