export default {
  created() {
    this.getCustomerData()
  },
  props:{
    itemId: Number
  },
  data: () => {
    return {
      customer: {
        productReservations:[],
        hallReservations:[]
      },
      activeCollapse: '',
    }
  },

  methods: {
    
    getCustomerData(){
      this.$store.dispatch("customers/getDetails", this.itemId).then((res)=>{
        this.customer = res
      })
    },
    back() {
      this.$parent.status = 0;
    }
  }
}
