import AddUser from './addFarhiUser/AddFarhiUser.vue'
import EditUser from './editFarhiUser/EditFarhiUser.vue'
import ViewUser from './viewFarhiUser/ViewFarhiUser.vue'

export default {
  created() {
    this.getHalls();
    this.getStores();
    this.getUsers()
  },
  components: {
    AddUser,
    EditUser,
    ViewUser
  },
  computed:{
    total(){
      return this.$store.state.farhiUsers.total
    },
    users(){
      return this.$store.state.farhiUsers.users
    }
  },
  data: () => {
    return {
      filter: {
        pageNo: 1,
        pageSize: 10,
        search: ""
      },
      status: 0,
    }
  },

  methods: {
    getHalls() {
      this.$store.dispatch("lookup/getHalls")
    },
    getStores() {
      this.$store.dispatch("lookup/getStores")
    },
    // getHallBranches() {
    //   this.$store.dispatch("lookup/getHallBranches",this.filter.hallId)
    // },
    getUsers() {
      this.$store.dispatch("farhiUsers/getAll",this.filter)
    },
    reload() {
      this.filter.pageNo = 1;
      this.getUsers()
    },
    pageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getUsers()
    },
    add() {
      this.status = 1;
    },
    view(user) {
      this.status = 3;
      this.userId = user.farhiUserId
    },
    edit(user) {
      this.userId = user.farhiUserId
      this.status = 2;
    },
    lock(item) {
      this.$store.dispatch('farhiUsers/lock', item.farhiUserId);
    },
    unlock(item) {
      this.$store.dispatch('farhiUsers/unlock', item.farhiUserId);
    },
    deleteUser(item) {
      this.$store.dispatch('farhiUsers/delete', item.farhiUserId);
    },
    resetPassword(item) {
      this.$store.dispatch('farhiUsers/resetPassword', item.farhiUserId);
    },
    generatePassword(){
      var result = "";
      let lowercases = "abcdefghijklmnopqrstuvwxyz";
      let numbers = "0123456789";
      let uppercases = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      let sympols = "#?!@$%^&*-";
      for ( var i = 0; i < 2; i++ ) {
        result += lowercases.charAt(Math.floor(Math.random() * lowercases.length));
        result += numbers.charAt(Math.floor(Math.random() * numbers.length));
        result += uppercases.charAt(Math.floor(Math.random() * uppercases.length));
        result += sympols.charAt(Math.floor(Math.random() * sympols.length));
      }
      return result;
    }
  }
}
