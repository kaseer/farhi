export default {
  created() {
    this.setRules();
    this.getFeatures();
    this.getForEdit();
  },
  props:{
    itemId:Number
  },
  computed:{
    hallBranches(){
      return this.$store.state.lookup.hallBranches
    },
    halls(){
      return this.$store.state.lookup.halls
    },
    storeBranches(){
      return this.$store.state.lookup.storeBranches
    },
    stores(){
      return this.$store.state.lookup.stores
    },
    loading() {
      return this.$store.state.lookup.loading
    },
  },
  watch:{
    activeCollapse(val){
      if(!val) return;
      this.$store.dispatch("lookup/getHallBranches",val)
    },
    activeCollapseStore(val){
      if(!val) return;
      this.$store.dispatch("lookup/getStoreBranches",val)
    }
  },
  data: () => {
    return {
      rules: {},
      user: {
        farhiUserId: 0,
        userName: "",
        loginName:"",
        phoneNo1: null,
        phoneNo2: null,
        email: null,
        permissions:[],
        halls:[],
        stores:[]
      },
      activeCollapse: '',
      activeCollapseStore: '',
      features: [],
      addHallFlag: false,
      addStoreFlag: false,
      hall: {
        id: null,
        branches:[]
      },
      store: {
        id: null,
        branches:[]
      }
    }
  },

  methods: {
    
    getForEdit(){
      this.$store.dispatch("farhiUsers/getForEdit",this.itemId).then((res)=>{
        this.user = res;
        //this.$store.dispatch("lookup/getHallBranches",res.hallId)
      })
    },
    addHallDialog(){
      this.activeCollapse = ''
      this.addHallFlag = true;
      this.hall.branches = [];
      this.$store.dispatch("lookup/getHallBranches",0)
    },

    addHall(){
      
      if(this.hall.id == null) {
        this.$globalAlerts.message("يرجي اختيار الصالة ", "info")
        return;
      }
      if(this.user.halls.find(x => x.id == this.hall.id) != null) {
        this.$globalAlerts.message("هذه الصالة مضافة", "error")
        return;
      }
      let hall = {
        id: this.hall.id,
        branches: this.hall.branches
      }
      this.user.halls.push(hall);
      this.close();
    },
    
    addStoreDialog(){
      this.activeCollapseStore = ''
      this.addStoreFlag = true;
      this.store.branches = [];
      this.$store.dispatch("lookup/getStoreBranches",0)
    },

    addStore(){
      
      if(this.store.id == null) {
        this.$globalAlerts.message("يرجي اختيار المحل ", "info")
        return;
      }
      if(this.user.stores.find(x => x.id == this.store.id) != null) {
        this.$globalAlerts.message("هذا المحل مضاف", "error")
        return;
      }
      let store = {
        id: this.store.id,
        branches: this.store.branches
      }
      this.user.stores.push(store);
      this.close();
    },
    deleteHall(index){
      this.user.halls.splice(index,1);
    },
    
    deleteStore(index){
      this.user.stores.splice(index,1);
    },
    checkStores(index){
      if(this.user.stores[index].branches.length == 0) this.user.stores.splice(index,1)
    },
    checkHalls(index){
      if(this.user.halls[index].branches.length == 0) this.user.halls.splice(index,1)
    },
    close(){
      this.addHallFlag = false;
      this.addStoreFlag = false;
      this.hall.id = null;
      this.hall.branches = [];
      this.store.id = null;
      this.store.branches = [];
    },

    getHallBranches() {
      this.hall.branches = [];
      this.$store.dispatch("lookup/getHallBranches",this.hall.id)
    },
    
    getStoreBranches() {
      this.store.branches = [];
      this.$store.dispatch("lookup/getStoreBranches",this.store.id)
    },
    getFeatures(){
      this.$store.dispatch("farhiUsers/getFeatures").then((res)=>{
        this.features = res
      })
    },
    handleCheckAllChange(flag,index) {
      let permissions = this.features[index].permissions;
      if (flag){
        permissions.forEach(p => {
          if(!this.user.permissions.find(x=>x == p.permissionId))
            this.user.permissions.push(p.permissionId);
        });
        return;
      }
      else {
        permissions.forEach(p => {
          let index = this.user.permissions.indexOf(p.permissionId);
          if (index > -1) {
            this.user.permissions.splice(index, 1);
          }
        });
      }
    },
    handleCheckedChange(featureIndex) {
      let check = this.features[featureIndex].permissions;
      let data = [];
      check.forEach(x=>{
        if(this.user.permissions.find(c=>c == x.permissionId)){
          data.push(x.permissionId);
        }
      });
      if(data.length == check.length)
        this.features[featureIndex].checkAll = true;
      else
        this.features[featureIndex].checkAll = false;
    },
    setRules() {
      this.rules = {
        usreName: [
          { validator: this.$validation.checkInput(" اسم المستخدم ",40), trigger: 'change' }
        ],
        loginName: [
          { validator: this.$validation.checkUserName(), trigger: 'change' }
        ],
        phoneNo1: [
          { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
        ],
        phoneNo2: [
          { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
        ],
        email: [
          { required: "true", type: "email", message: "يرجي ادخال البريد الالكتروني", trigger: 'change' },
        ],
      }
    },
    edit(){

      this.$refs["user"].validate((valid) => {
        if(valid){

          this.$store.dispatch("farhiUsers/edit",this.user).then(()=>{
            //this.$refs.resetForm();
            this.back();
          });
        }
        else
          this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');
      })
    },
    back() {
      this.$parent.status = 0;
    }
  }
}
