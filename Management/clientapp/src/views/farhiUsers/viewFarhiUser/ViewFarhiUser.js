export default {
  created() {
    this.getUserData()
  },
  props:{
    itemId: Number
  },
  data: () => {
    return {
      user: {
        features:[],
        halls:[],
        stores: []
      },
      activeCollapse: '',
      activeCollapseStore: '',
      activeCollapsePermission: ''
    }
  },

  methods: {
    
    getUserData(){
      this.$store.dispatch("farhiUsers/getDetails", this.itemId).then((res)=>{
        this.user = res
      })
    },
    back() {
      this.$parent.status = 0;
    }
  }
}
