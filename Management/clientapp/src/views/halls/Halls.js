﻿import viewHall from "./viewHall/ViewHall.vue";
import editHall from "./editHall/EditHall.vue";
import addHall from "./addHall/AddHall.vue";

export default {
    created() {
        this.getHalls();
    },
    components: {
        'view-hall': viewHall,
        'edit-hall': editHall,
        'add-hall': addHall
    },
    data: () => {
        return {
            filter: {
                pageNo: 1,
                pageSize: 10,
                searchByName: ''
            },
            hallId: null,
            status: 0,
        }
    },
    computed: {
        halls() {
            return this.$store.state.halls.halls
        },
        total() {
            return Math.floor(this.$store.state.halls.total);
        }
    },
    methods: {
        getHalls() {
            this.$store.dispatch('halls/getAll', this.filter);
        },
        reload() {
            this.filter.pageNo = 1;
            this.getHalls();
        },
        pageChanged(pageNo) {
            this.filter.pageNo = pageNo;
            this.getHalls();
        },
        change() {
            this.status = 1;
        },
        viewHall(index) {
            this.status = 3;
            this.hallId = this.halls[index].hallId;
        },
        editHall(index) {
            this.status = 2;
            this.hallId = this.halls[index].hallId;
        },
        lockHall(item) {
            this.$store.dispatch('halls/lock', item.hallId);
        },
        unlockHall(item) {
            this.$store.dispatch('halls/unlock', item.hallId);
        },
        deleteHall(item) {
            this.$store.dispatch('halls/delete', item.hallId);
        }
    }
}
