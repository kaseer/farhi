export default {
    created() {

        this.hallId = this.$parent.hallId;
        this.getDetails();
    },

    data: () => {
        return {
            hallId: null,
            hallData: {
                hallName: null,
                ownerName: null,
                phoneNo1: null,
                phoneNo2: null,
                email: null,
                logo: null,
                hallBranch: {
                    addressName: null,
                    addressDescription: null,
                    phoneNo1: null,
                    phoneNo2: null,
                    longitude: null,
                    latitude: null,
                    peapleCapacity: null,
                    carsCapacity: null,
                    startTime: null,
                    endTime: null,
                },
                farhiUser: {
                    userName: null,
                    loginName: null
                }
            },
        }
    },

    methods: {
        getDetails() {
            this.$store.dispatch('halls/getDetails', this.hallId)
                .then((val) => {
                    this.hallData = val.hallData;
                    this.hallData.logo = val.image;
                });
        },
        back() {
            this.$parent.status = 0;
        }
    }
}
