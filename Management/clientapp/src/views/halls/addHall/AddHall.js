export default {
    created() {
        this.setHallAppointments();
        this.setRules();
        this.setAppointmentRules();
        this.setServicesRules();
        this.$store.dispatch('lookup/getAddresses');
        this.$store.dispatch('lookup/getReservationTypes');
    },
    computed: {
        addresses() {
            return this.$store.state.lookup.addresses
        },
        reservationTypes() {
            return this.$store.state.lookup.reservationTypes;
        },
        loading() {
            return this.$store.state.lookup.loading;
        },
        workDays(){
            return this.$store.state.lookup.days
        }
    },
    data: () => {
        return {
            rules: {},
            appointmentRules: {},
            servicesRules: {},
            // addresses: [],
            hallData: {
                hallName: null,
                ownerName: null,
                phoneNo1: null,
                phoneNo2: null,
                email: null,
                logo: '',
                hallBranch: {
                    hallId: null,
                    hallBranchId: null,
                    addressId: null,
                    addressDescription: null,
                    phoneNo1: null,
                    phoneNo2: null,
                    longitude: null,
                    latitude: null,
                    peapleCapacity: null,
                    carsCapacity: null,
                    timeRange: ['', ''],
                    hallAppointments: [],
                    hallServices: []
                },
                farhiUser: {
                    userName: null,
                    loginName: null,
                    password: null,
                    userPhoneNo1: null,
                    userPhoneNo2: null,
                    userEmail: null,
                },
            //hallPermission: true
            // hallAppointmants: [],
            // hallServices: []
            },
            workDaysCount: 0,
            editServiceIndex: null,
            activeCollapse: null,
        }
    },

    methods: {
        setHallAppointments() {
            for (var i = 1; i <= 7; i++) {
                this.hallData.hallBranch.hallAppointments.push({
                    workDayId: i,
                    appointmentData: [{
                        reservationType: null,
                        timeRange: ['', ''],
                        price: 0,
                    }]
                })
            }
        },
        handleAvatarSuccess(res, file) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.hallData.logo = event.target.result ;
            };
            reader.readAsDataURL(res.raw);
            console.log(file);
        },
        beforeAvatarUpload(file) {
          console.log(file)
        },
        addApponitment(index) {
            this.hallData.hallBranch.hallAppointments[index].appointmentData.push({
                id: null,
                reservationType: null,
                timeRange: ['', ''],
                price: 0,
                reservationStatus: null
            });
        },
        deleteApponitment(index, detailsIndex) {
            this.hallData.hallBranch.hallAppointments[index].appointmentData.splice(detailsIndex, 1)
        },
        deleteDay(index) {
            if (this.hallData.hallBranch.hallAppointments.length == 1) {
                this.$globalAlerts.message("لا بد من تسجيل موعد يوم واحد علي الاقل", 'error');
                return;
            }
            this.$confirm(`هذا الامر سيقوم بحذف موعد اليوم بالكامل ... الاستمرار ؟`, 'Warning', {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
            }).then(() => {
                this.hallData.hallBranch.hallAppointments.splice(index, 1)
            }).catch(() => { });
        },
        addService() {
            this.hallData.hallBranch.hallServices.push({
                id: null,
                description: null,
                price: 0,
                reservationStatus: null
            });
        },
        editService(index) {
            this.editServiceIndex = index;
        },
        deleteService(index) {
            this.hallData.hallBranch.hallServices.splice(index, 1)
        },

        setRules() {
            this.rules = {
                hallName: [
                    { validator: this.$validation.checkInput("الصالة", 30), trigger: 'change' }
                ],
                ownerName: [
                    { validator: this.$validation.checkInput("اسم المالك", 40), trigger: 'change' }
                ],
                phoneNo1: [
                    //{ required: "true", message: "يرجي ادخال رقم الهاتف", trigger: 'change' },
                    { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                ],
                phoneNo2: [
                    { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                ],
                hallBranch: {
                    phoneNo1: [
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    phoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    email: [
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                    addressId: [
                        { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                    ],
                    addressDescription: [
                        { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                    ],
                    longitude: [
                        { validator: this.$validation.checkLongitude(), trigger: 'change' }
                    ],
                    latitude: [
                        { validator: this.$validation.checkLatitude(), trigger: 'change' }
                    ],
                    peapleCapacity: [
                        { validator: this.$validation.checkInputNumber("عدد الاشخاص", 5000), trigger: 'change' }
                    ],
                    carsCapacity: [
                        { validator: this.$validation.checkInputNumber("عدد السيارات", 400), trigger: 'change' }
                    ],
                    timeRange: [
                        { validator: this.$validation.checkDate(), trigger: 'change' }
                    ],
                },
                farhiUser: {
                    userName: [
                        { validator: this.$validation.checkInput("اسم المستخدم", 30), trigger: 'change' }
                    ],
                    loginName: [
                        { validator: this.$validation.checkInput("اسم الدخول", 30), trigger: 'change' }
                    ],
                    password: [
                        { validator: this.$validation.checkPassword(), trigger: 'change' }
                    ],
                    userPhoneNo1: [
                        //{ required: "true", message: "يرجي ادخال رقم الهاتف", trigger: 'change' },
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    userPhoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    userEmail: [
                        { required: "true", type: "email", message: "يرجي ادخال البريد الالكتروني", trigger: 'change' },
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                },
                
                
                
                //hallPermission: [{ required: true}]
              }
        },

        setAppointmentRules() {
            this.appointmentRules = {
                reservationType: [
                    { validator: this.$validation.checkSelect("نوع الموعد"), trigger: 'change' }
                ],
                timeRange: [
                    { validator: this.$validation.checkTime(), trigger: 'change' }
                ],
                price: [
                    { validator: this.$validation.checkInputNumber("السعر", 999999999), trigger: 'change' }
                ],
            }
        },

        setServicesRules() {
            this.servicesRules = {
                description: [
                    { validator: this.$validation.checkInput('وصف الخدمة', 100), trigger: 'change' }
                ],
                price: [
                    { validator: this.$validation.checkInputNumber("السعر", 999999999), trigger: 'change' }
                ],
            }
        },

        addItem() {
            let validFlag = true;
            this.$refs["hallData"].validate((valid) => {
                if (valid) {
                    for (var i = 0; i < this.$refs["hallAppointments"].length; i++) {
                        this.$refs["hallAppointments"][i].validate((valid) => {
                            if (!valid)
                                validFlag = false;
                        });
                    }
                    if (!validFlag) {
                        this.$globalAlerts.message("الرجاء إدخال بيانات مواعيد الصالة بشكل صحيح", 'error');
                        return;
                    }

                    if (this.hallData.hallBranch.hallServices.length > 0) {
                        for (i = 0; i < this.$refs["hallServices"].length; i++) {
                            this.$refs["hallServices"][i].validate((valid) => {
                                if (!valid)
                                    validFlag = false;
                            });
                        }
                        if (!validFlag) {
                            this.$globalAlerts.message("الرجاء إدخال بيانات خدمات الصالة بشكل صحيح", 'error');
                            return;
                        }
                    }
                    this.$store.dispatch('halls/add', this.hallData)
                    .then(() => {
                        this.back();
                    })
                    .catch(() => { });
                  
                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');
                
                    return false;
                }
            });

        },
        back() {
          this.$parent.status = 0;
        }

    }
}
