export default {
    created() {
        
        this.setRules();
        this.$store.dispatch('lookup/getAddresses');
        this.getForEdit();
    },
    computed: {
        addresses() {
            return this.$store.state.lookup.addresses
        },
        loading() {
            return this.$store.state.lookup.loading;
        },
        workDays(){
            return this.$store.state.lookup.days
        }
    },

    data: () => {
        return {
            rules: {},
            hallData: {
                hallId:null,
                hallName: null,
                ownerName: null,
                phoneNo1: null,
                phoneNo2: null,
                email: null,
                logo: '',
                hallBranch: {
                    addressId: null,
                    addressDescription: null,
                    phoneNo1: null,
                    phoneNo2: null,
                    longitude: null,
                    latitude: null,
                    peapleCapacity: null,
                    carsCapacity: null,
                    timeRange: ['', ''],
                    hallAppointments: [],
                    hallServices: []
                },
                //hallPermission: true
                // hallAppointmants: [],
                // hallServices: []
            },
            reservationTypes: [],
            workDaysCount: 0,
        }
    },

    methods: {
        getForEdit() {
            this.$store.dispatch('halls/getForEdit',this.$parent.hallId).then((val)=>{
                this.hallData = val.hallData;
                this.hallData.hallId = this.$parent.hallId;
                this.hallData.logo = val.image;
            });
        },


        handleAvatarSuccess(res, file) {

            const reader = new FileReader();
            reader.onload = (event) => {
                this.hallData.logo = event.target.result;
            };
            reader.readAsDataURL(res.raw);
            console.log(file);
        },
        beforeAvatarUpload(file) {
            console.log(file)
        },
        setRules() {
            this.rules = {
                hallName: [
                    { validator: this.$validation.checkInput("الصالة", 30), trigger: 'change' }
                ],
                ownerName: [
                    { validator: this.$validation.checkInput("اسم المالك", 40), trigger: 'change' }
                ],
                phoneNo1: [
                    //{ required: "true", message: "يرجي ادخال رقم الهاتف", trigger: 'change' },
                    { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                ],
                phoneNo2: [
                    { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                ],
                hallBranch: {
                    phoneNo1: [
                        { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
                    ],
                    phoneNo2: [
                        { validator: this.$validation.checkNullPhoneNumber(), trigger: 'change' }
                    ],
                    email: [
                        { validator: this.$validation.checkEmail(), trigger: 'change' }
                    ],
                    addressId: [
                        { validator: this.$validation.checkSelect("العنوان"), trigger: 'change' }
                    ],
                    addressDescription: [
                        { validator: this.$validation.checkNullInput(50), trigger: 'change' }
                    ],
                    longitude: [
                        { validator: this.$validation.checkLongitude(), trigger: 'change' }
                    ],
                    latitude: [
                        { validator: this.$validation.checkLatitude(), trigger: 'change' }
                    ],
                    peapleCapacity: [
                        { validator: this.$validation.checkInputNumber("عدد الاشخاص", 5000), trigger: 'change' }
                    ],
                    carsCapacity: [
                        { validator: this.$validation.checkInputNumber("عدد السيارات", 400), trigger: 'change' }
                    ],
                    timeRange: [
                        { validator: this.$validation.checkDate(), trigger: 'change' }
                    ],
                },
            }
        },
        editItem() {
            this.$refs["hallData"].validate((valid) => {
                if (valid) {
                    this.$store.dispatch('halls/edit', this.hallData)
                    .then(() => {
                        this.back();
                    })
                    .catch(() => { });

                } else {
                    this.$globalAlerts.message("الرجاء إدخال البيانات بشكل صحيح", 'error');

                    return false;
                }
            });

        },
        back() {
            this.$parent.status = 0;
        }

    }
}
