
export default {
  components: {
    // 'view-hall' :viewHall ,
    // 'edit-hall' :editHall ,
    // 'add-hall' :addHall
  },
  created() {
    this.getAll();
  },
  computed: {
    storeTypes() {
      return this.$store.state.storeTypes.storeTypes
    },
    total() {
      return Math.floor(this.$store.state.storeTypes.total);
    }
  },
  data: () => {
    return {
      storeType: {
        storeTypeId:0,
        description: ""
      },
      title: "",
      dialogType: "",
      filter: {
        pageNo: 1,
        pageSize: 10,
        search: ""
      },
      
      dialogFlag: false,
      editFlag: false,
      status: 0,
    }
  },

  methods: {
    getAll() {
      this.$store.dispatch('storeTypes/getAll', this.filter);
    },
    reload() {
      this.filter.pageNo = 1;
      this.getAll();
    },
    PageChanged(pageNo) {
      this.filter.pageNo = pageNo;
      this.getAll();
    },
    addType() {
      this.title = "إضافة نوع";
      this.dialogType = "add";
      this.dialogFlag = true;
    },
    editType(index) {
      this.title = "تعديل نوع";
      this.dialogType = "edit";
      this.dialogFlag = true;
      this.storeType.storeTypeId = this.storeTypes[index].storeTypeId;
      this.storeType.description = this.storeTypes[index].description;
    },
    dialogOperation(){
      this.$store.dispatch(`storeTypes/${this.dialogType}`, this.storeType).then(()=>{
        this.close()
      });
    },
    
    close() {
      this.dialogFlag = false;
      this.storeType.description = "";
    }
  }
}
