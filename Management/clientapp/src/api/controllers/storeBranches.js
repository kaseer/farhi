import axios from 'axios';

const api = "api/storeBranches"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    async getForEdit(id) {
        return axios.get(`${api}/${id}/getForEdit`);
    },
    async edit(data) {
        return axios.put(`${api}`, data);
    },
    lock(id) {
        return axios.put(`${api}/${id}/lock`);
    },
    unlock(id) {
        return axios.put(`${api}/${id}/unlock`);
    },
    async delete(id) {
        return axios.delete(`${api}/${id}`);
    },
    assignMainBranch(id) {
        return axios.put(`${api}/${id}/assignMainBranch`);
    },
    getVideo(id) {
        return axios.get(`${api}/${id}/getVideo`);
    },
    getImages(id) {
        return axios.get(`${api}/${id}/getImages`);
    },
    deleteOldImages(data) {
        return axios.put(`${api}/deleteOldImages`,data);
    },
    async addImages(data) {
        return axios.post(`${api}/addImages`, data);
    },
    async deleteVideo(id) {
        return axios.delete(`${api}/${id}/deleteVideo`);
    },
    async addSubscription(data) {
        return axios.post(`${api}/addSubscription`, data);
    },
}