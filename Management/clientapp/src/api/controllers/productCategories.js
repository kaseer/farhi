import axios from 'axios';

const api = "api/ProductCategories"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async edit(data) {
        return axios.put(`${api}/${data.productCategoryId}`, data);
    },
}