import axios from 'axios';

const api = "api/lookup"
export default {
    async getAddresses() {
        return axios.get(`${api}/getAddresses`);
    },
    async getHalls() {
        return axios.get(`${api}/getHalls`);
    },
    async getHallBranches(id) {
        return axios.get(`${api}/${id}/getHallBranches`);
    },
    async getReservationTypes() {
        return axios.get(`${api}/getReservationTypes`);
    },
    // async getSubscriptionPrices(type) {
    //     return axios.get(`${api}/getSubscriptionPrices?type=${type}`);
    // },
    // async getSubscriptionPricesDetails(subscriptionPriceId) {
    //     return axios.get(`${api}/getSubscriptionPricesDetails?subscriptionPriceId=${subscriptionPriceId}`);
    // },
    async getStoreTypes() {
        return axios.get(`${api}/getStoreTypes`);
    },
    async getStores() {
        return axios.get(`${api}/getStores`);
    },
    async getStoreBranches(id) {
        return axios.get(`${api}/${id}/getStoreBranches`);
    },
    async getCategories() {
        return axios.get(`${api}/getCategories`);
    },
    async getSubscriptionPrices() {
        return axios.get(`${api}/getSubscriptionPrices`);
    },
}