import axios from 'axios';

const api = "api/productSubCategories"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async edit(data) {
        console.log(data)
        return axios.put(`${api}/${data.productSubCategoryId}`, data);
    },
}