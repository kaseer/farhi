import axios from 'axios';

const api = "api/sliderManagement"
export default {
    getAll() {
        return axios.get(`${api}`);
    },
    get(id) {
        return axios.get(`${api}/${id}`);
    },
    add(data) {
        return axios.post(`${api}`, data);
    },
    edit(data) {
        return axios.put(`${api}`, data);
    },
    delete(id) {
        return axios.delete(`${api}/${id}`);
    },
}