import axios from 'axios';

const api = "api/farhiStoreUsers"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    async getForEdit(id) {
        return axios.get(`${api}/${id}/getForEdit`);
    },
    async edit(data) {
        return axios.put(`${api}`, data);
    },
    lock(id) {
        return axios.put(`${api}/${id}/lock`);
    },
    unlock(id) {
        return axios.put(`${api}/${id}/unlock`);
    },
    async delete(id) {
        return axios.delete(`${api}/${id}`);
    },
    resetPassword(id) {
        return axios.put(`${api}/${id}/resetPassword`);
    },
    getFeatures() {
        return axios.get(`${api}/getFeatures`);
    },
}