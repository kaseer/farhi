import lookup from './controllers/lookup.js'
import halls from './controllers/halls.js'
import hallBranches from './controllers/hallBranches.js'
import stores from './controllers/stores.js'
import storeBranches from './controllers/storeBranches.js'
import sliderManagement from './controllers/sliderManagement.js'
import storeTypes from './controllers/storeTypes.js'
import productCategories from './controllers/productCategories.js'
import productSubCategories from './controllers/productSubCategories.js'
import farhiAdmins from './controllers/farhiAdmins.js'
import farhiUsers from './controllers/farhiUsers.js'
// import farhiStoreUsers from './controllers/farhiStoreUsers.js'
import customers from './controllers/customers.js'
import subscriptionPrices from './controllers/subscriptionPrices.js'

//axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

export default {
    lookup,
    halls,
    hallBranches,
    stores,
    storeBranches,
    sliderManagement,
    storeTypes,
    productCategories,
    productSubCategories,
    farhiAdmins,
    farhiUsers,
    // farhiStoreUsers,
    customers,
    subscriptionPrices
}