import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import Dashboard from '../views/dashboard/Dashboard.vue'
import Halls from '../views/halls/Halls.vue'
import HallBranches from '../views/hallBranches/HallBranches.vue'
import Stores from '../views/stores/Stores.vue'
import StoreBranches from '../views/storeBranches/StoreBranches.vue'
import SliderManagement from '../views/sliderManagement/SliderManagement.vue'
import ReservationTypes from '../views/reservationTypes/ReservationTypes.vue'

import ProductCategories from '../views/productCategories/ProductCategories.vue'

import ProductSubCategories from '../views/productSubCategories/ProductSubCategories.vue'

import StoreTypes from '../views/storeTypes/StoreTypes.vue'
// import Reservations from '../views/reservations/Reservations.vue'
import FarhiUsers from '../views/farhiUsers/FarhiUsers.vue'
// import FarhiStoreUsers from '../views/farhiStoreUsers/FarhiStoreUsers.vue'

import FarhiAdmins from '../views/farhiAdmins/FarhiAdmins.vue'
import Customers from '../views/customers/Customers.vue'
import SubscriptionPrices from '../views/subscriptionPrices/Index.vue'
import Reports from '../views/reports/Reports.vue'
import Form from '../views/form/Form.vue'
import Table from '../views/table/Table.vue'

// import Login from '../views/login/Login.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/halls',
    name: 'Halls',
    component: Halls
  },
  {
    path: '/hallBranches',
    name: 'HallBranches',
    component: HallBranches
  },
  {
    path: '/stores',
    name: 'Stores',
    component: Stores
  },
  {
    path: '/StoreBranches',
    name: 'StoreBranches',
    component: StoreBranches
  },
  {
    path: '/SliderManagement',
    name: 'SliderManagement',
    component: SliderManagement
  },
  {
    path: '/ReservationTypes',
    name: 'ReservationTypes',
    component: ReservationTypes
  },
  {
    path: '/StoreTypes',
    name: 'StoreTypes',
    component: StoreTypes
  },
  {
    path: '/ProductCategories',
    name: 'ProductCategories',
    component: ProductCategories
  },
  {
    path: '/ProductSubCategories',
    name: 'ProductSubCategories',
    component: ProductSubCategories
  },
  // {
  //   path: '/Reservations',
  //   name: 'Reservations',
  //   component: Reservations
  // },
  {
    path: '/FarhiUsers',
    name: 'FarhiUsers',
    component: FarhiUsers
  },
  // {
  //   path: '/FarhiStoreUsers',
  //   name: 'FarhiStoreUsers',
  //   component: FarhiStoreUsers
  // },
  {
    path: '/FarhiAdmins',
    name: 'FarhiAdmins',
    component: FarhiAdmins
  },
  {
    path: '/Customers',
    name: 'Customers',
    component: Customers
  },
  {
    path: '/subscriptionPrices',
    name: 'subscriptionPrices',
    component: SubscriptionPrices
  },
  {
    path: '/Reports',
    name: 'Reports',
    component: Reports
  },
  {
    path: '/form',
    name: 'Form',
    component: Form
  }
  ,
  {
    path: '/table',
    name: 'Table',
    component: Table
  }
]

const router = new VueRouter({
  routes
})

export default router
