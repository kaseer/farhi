﻿import Vue from "vue"

Vue.filter('dayName', function (value) {
    if (!value) return ''
    else if (value == 1) return 'السبت'
    else if (value == 2) return 'الاحد'
    else if (value == 3) return 'الاثنين'
    else if (value == 4) return 'الثلاثاء'
    else if (value == 5) return 'الاربعاء'
    else if (value == 6) return 'الخميس'
    else if (value == 7) return 'الجمعة'
})