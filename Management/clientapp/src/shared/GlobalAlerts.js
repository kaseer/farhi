import { Notification } from 'element-ui';
import { Message } from 'element-ui';
import { MessageBox } from 'element-ui';
import loading from '@/shared/loading';
// import globalAlerts from '@/shared/GlobalAlerts';

export default (function () {
    return {
        notify(title,message,type) {
            Notification({
              title: title,
              message: message,
              type: type,
              position: 'bottom-left'
            });
        },
        message(message,type){
            Message({
                message: message,
                type: type
              });
        },
        alert(title,message,type) {
            MessageBox({
                message: message,
                title:title,
                confirmButtonText: 'Ok',
                type: type
            });
        },
        catch(err) {
            
            loading.Stop();
            if (!err.response.data.statusCode) {
                this.message('تعذر الاتصال بالخادم , يرجي المحاولة لاحقا','error');
                return;
            }
            let message = err.response.data.result + "  [" + err.response.data.statusCode + "]";
            this.message(message,'error');
        },
        async confirmation(title){
            return MessageBox.confirm(title, {
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                type: 'warning',
                center: true
              });
        }
    };
})();


// export default {
//     notify(){
//         Notification({
//             type:'success'

//         })
//     }
//     // notify(title,message,type) {
//     //     return this.$notify({
//     //         title: title,
//     //         message: message,
//     //         type: type
//     //     });
//     // },
// }