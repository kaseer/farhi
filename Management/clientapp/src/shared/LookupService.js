﻿import axios from 'axios';
export default {
    //============ LookUps ==================//

    getAddresses() {
        return axios.get(`api/lookup/getAddresses`);
    }, 
    getHalls() {
        return axios.get(`api/lookup/getHalls`);
    },
    getReservationTypes() {
        return axios.get(`api/lookup/getReservationTypes`);
    },
    getSubscriptionPrices(type) {
        return axios.get(`api/lookup/getSubscriptionPrices?type=${type}`);
    },
    getSubscriptionPricesDetails(subscriptionPriceId) {
        return axios.get(`api/lookup/getSubscriptionPricesDetails?subscriptionPriceId=${subscriptionPriceId}`);
    },
    getStoreTypes() {
        return axios.get(`api/lookup/getStoreTypes`);
    },
    getStores() {
        return axios.get(`api/lookup/getStores`);
    },
}