﻿export default {
    //////////////////////////////////////// for All /////////////////////////////////////////////////////
    checkInput(validName,maxLength) {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال ' + validName));
            }
            if (value.length > maxLength) {
                return callback(new Error(`القيمة لايجب ان تكون اكثر من ${maxLength} خانات`));
            }
            else {
                callback();
            }
        }
    },
    checkNullInput(maxLength) {
        return (rule, value, callback) => {
            if (value) {
                if (value > 2) {
                    return callback(new Error(`القيمة لايجب ان تكون اكثر من ${maxLength} خانات`));
                }
                else {
                    callback();
                }
            }
            else {
                callback();
            }
        }
    },
    checkEmail() {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال هذا البريد الالكتروني'));
            } else {
                callback();
            }
        }
    },
    checkSelect(validName) {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال ' + validName));
            } else {
                callback();
            }
        }
    },
    checkDate() {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال حقل التاريخ المطلوب '));
            } else {
                callback();
            }
        }
    },
    checkTime() {
        return (rule, value, callback) => {
            if (!value[0]) 
                return callback(new Error('يجب ادخال حقل الوقت المطلوب '));
            else if (!value[1])
                return callback(new Error('يجب ادخال حقل الوقت المطلوب '));
             else 
                callback();
            
        }
    },
    checkPassword(){
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال هذا الحقل'));
            }
            if (value) {
                const containsUppercase = /[A-Z]/.test(value);
                const containsLowercase = /[a-z]/.test(value);
                const containsNumber = /[0-9]/.test(value);
                const containsSpecial = /[#?!@$%^&*-]/.test(value);
                if (value.length < 8) {
                    return callback(new Error('يجب ادخال 8 خانات علي الاقل'));
                }
                if (!containsUppercase ) {
                    return callback(new Error('يجب ادخال خانات كبيرة'));
                }
                if (!containsLowercase) {
                    return callback(new Error('يجب ادخال خانات صغيرة'));
                }
                if (!containsNumber) {
                    return callback(new Error('يجب ادخال ارقام'));
                }
                if (!containsSpecial) {
                    return callback(new Error('يجب ادخال رموز'));
                }
                callback();
            }
            else {
                callback();
            }
        }
    },
    checkConfirmPassword(data){
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال هذا الحقل'));
            }
            if (value) {
                if (value !== data.password) {
                    return callback(new Error('يرجي التأكد من تطالق الرقم السري'));
                }
                else {
                    callback();
                }
            }
            else {
                callback();
            }
        }
    },
    checkInputNumber(validName, maxLength) {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال ' + validName));
            } else {
                if (Number.isNaN(value))
                    return callback(new Error('يجب ان يكون الحقل عبارة عن رقم'));
                 if (value > maxLength) {
                    return callback(new Error(`القيمة لايجب ان تكون اكثر من ${maxLength} خانات`));
                 }
                 else if (value <= 0) {
                     return callback(new Error(`القيمة لايجب ان تكون اقل من او يساوي 0`));
                 }
                 else {
                    callback();
                 }
            }
        }
    },
    checkLongitude() {
        return (rule, value, callback) => {
            console.log(value)
            if (value) {
                if (Number.isNaN(value))
                    return callback(new Error('يجب ان يكون الحقل عبارة عن رقم'));
                if (value < -90 || value > 90) {
                    return callback(new Error('يجب ادخال الحقل اقل من 180'));
                }
                else {
                    callback();
                }
            }
            else {
                callback();
            }
                
        }
    },
    checkLatitude() {
        return (rule, value, callback) => {
            if (value) {
                if (Number.isNaN(value))
                    return callback(new Error('يجب ان يكون الحقل عبارة عن رقم'));
                if (value < -90 || value > 90) {
                    return callback(new Error('يجب ادخال الحقل اقل من 90'));
                }
                else {
                    callback();
                }
            }
            else {
                callback();
            }
        }
    },
    checkPhoneNumber() {
        return (rule, value, callback) => {
            
            if (!value || value == null) {
                 return callback(new Error('يجب ادخال رقم الهاتف '));
             } else {
                 if (value) {
                     if (value.length != 10 && value.length != 9) {
                         return callback(new Error('حقل الهاتف يتكون من 9 او 10 خانات فقط'));
                     }
                     else if (value.substring(0, 2) != '91' && value.substring(0, 2) != '92'
                         && value.substring(0, 2) != '93' && value.substring(0, 2) != '94'
                         && value.substring(0, 2) != '95') {
                         if (value.substring(0, 3) != '091' && value.substring(0, 3) != '092'
                             && value.substring(0, 3) != '093' && value.substring(0, 3) != '094'
                             && value.substring(0, 3) != '095') {
                             return callback(new Error('يرجي التاكد من صيغة الهاتف'));

                         }
                     }
                     else {
                         callback();
                     }
                 }
                 else {
                     callback();
                 }
            }
        }
    },

    checkNullPhoneNumber() {
        return (rule, value, callback) => {
            if (value) {
                if (value) {
                    if (value.length != 10 && value.length != 9) {
                        return callback(new Error('حقل الهاتف يتكون من 9 او 10 خانات فقط'));
                    }
                    else if (value.substring(0, 2) != '91' && value.substring(0, 2) != '92'
                        && value.substring(0, 2) != '93' && value.substring(0, 2) != '94'
                        && value.substring(0, 2) != '95') {
                        if (value.substring(0, 3) != '091' && value.substring(0, 3) != '092'
                            && value.substring(0, 3) != '093' && value.substring(0, 3) != '094'
                            && value.substring(0, 3) != '095') {
                            return callback(new Error('يرجي التاكد من صيغة الهاتف'));

                        }
                    }
                    else {
                        callback();
                    }
                }
            } else {
                callback();
            }
        }
    },
    checkInputNumberNullable() {
        return (rule, value, callback) => {
            if (!value) {
                return callback();
            } else {
                //if (value == "NaN") {
                 //   return callback(new Error('يجب ادخال الحقل المطلوب بصيغة أرقام'));
                //} else {
                    callback();
                //}
            }
        };
    },
    //////////////////////////////////////// for Arabic Names /////////////////////////////////////////////////////
    // checkArabicName() {
    //     return (rule, value, callback) => {
    //         if (!value) {
    //             return callback(new Error('يجب ادخال الحقل المطلوب'));
    //         } else {
    //             var garbage = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/;
    //             if (garbage.test(value)) {
    //                 return callback(new Error('يجب ادخال الحقل المطلوب بشكل الصحيح'));
    //             } else {
    //                 callback();
    //             }
    //         }

    //     };
    // },
    
    // checkEnglishName() {
    //     return (rule, value, callback) => {
    //         if (value) {
    //             var garbage = /^[a-zA-Z]+$/;
    //             if (!garbage.test(value)) {
    //                 return callback(new Error('يجب ادخال الحقل المطلوب بشكل الصحيح'));
    //             } else {
    //                 callback();
    //             }
    //         } else {
    //             callback();
    //         }

    //     };
    // },
    // checkEnglishNameWithSpace() {
    //     return (rule, value, callback) => {
    //         if (value) {
    //             var garbage = /^[a-zA-Z" "]+$/;
    //             if (!garbage.test(value)) {
    //                 return callback(new Error('يجب ادخال الحقل المطلوب بشكل الصحيح'));
    //             } else {
    //                 callback();
    //             }
    //         } else {
    //             callback();
    //         }

    //     };
    // },
    checkUserName() {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال اسم المستخدم '));
            }
            if (value.length > 40) {
                return callback(new Error('اسم المستخدم لا يزيد عن 40 خانة '));
            }
            if (value) {
                var garbage = /^[a-zA-Z0-9._-]+$/;
                if (!garbage.test(value)) {
                    return callback(new Error('يجب ادخال الحقل المطلوب بشكل الصحيح'));
                } else {
                    callback();
                }
            } else {
                callback();
            }

        };
    },
    
    ////////////////////////////////////////////////// Numbers ///////////////////////////////////////////////////

    checkPositiveNumber() {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال الحقل المطلوب '));
            } else {
                if (value=="NaN") {
                    return callback(new Error('يجب ادخال الحقل المطلوب بصيغة أرقام'));
                } else {
                    if (value < 0) {
                        return callback(new Error('يجب ادخال عدد موجب'));
                    } else {
                        callback();
                    }
                }
            }
        };
    },
    // checkIntigerNumber() {
    //     return (rule, value, callback) => {
    //         if (!value) {
    //             return callback(new Error('يجب ادخال الحقل المطلوب '));
    //         } else {
    //             var Isinteger = /^[0-9]+$/;
    //             if (!Isinteger.test(value)) {
    //                 return callback(new Error('يجب تعبئة الرقم بصيغة ارقام'));
    //             } else {
    //                 callback();
    //             }
    //         };
    //     };
    // },
    checkPercentageNumber() {
        return (rule, value, callback) => {
            if (!value) {
                return callback(new Error('يجب ادخال الحقل المطلوب '));
            } else {
                if (value == "NaN") {
                    return callback(new Error('يجب ادخال الحقل المطلوب بصيغة أرقام'));
                } else {
                    if (value < 0 || value > 100) {
                        return callback(new Error('يجب ادخال الحقل المطلوب بشكل الصحيح'));
                    } else {
                        callback();
                    }
                }
            }
        };
    },
    checkPositiveNumberAllowNULL() {
        return (rule, value, callback) => {
            if (value == "NaN") {
                return callback(new Error('يجب ادخال الحقل المطلوب بصيغة أرقام'));
            } else {
                if (value < 0) {
                    return callback(new Error('يجب ادخال عدد موجب'));
                } else {
                    callback();
                }
            }
        };
    },

    //--------------------------- names --------------------------------//
    // checkName() {
    //     return (rule, value, callback) => {
    //         console.log(rule)
    //         if (!value) {
    //             return callback(new Error('يجب ادخال الاسم بشكل صحيح'));
    //         } else {
    //             if (!value.trim()) {
    //                 return callback(new Error('يجب ادخال الاسم بشكل صحيح'));
    //             } else {
    //                 var garbage = /^[0-9!@#\$%\^\&*\)\(+=._-]+$/;
    //                 if (garbage.test(value)) {
    //                     return callback(new Error('يجب كتابة الحقل المطلوب بشكل الصحيح'));
    //                 } else {
    //                     callback();

    //                 }
    //             }
                
    //         }

    //     };
    // },
}