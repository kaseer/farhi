﻿import { Loading } from 'element-ui';

export default (function () {
    var blockService = null;
    return {
        Start() {
            blockService = Loading.service({
                fullscreen: true,
                text: 'الرجاء الانتظار قليلاً...'
            });
        },
        Stop() {
            blockService.close();
        }
    };
})();



