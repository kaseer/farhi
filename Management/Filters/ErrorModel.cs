﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Filters
{
    public class ErrorModel
    {
        public string StatusCode { get; set; }
        public string Result { get; set; }
    }
}
