﻿using Microsoft.Extensions.Configuration;
using System;

namespace Management.Global
{
    public class ConfigurationItems
    {
        static public string GetStoreImages(IConfiguration configuration)
        {
            return configuration.GetSection("MediaPath").GetSection("Images").GetSection("Stores").Value;
        }
        static public string GetHallImages(IConfiguration configuration)
        {
            return configuration.GetSection("MediaPath").GetSection("Images").GetSection("Halls").Value;
        }
        static public string GetStoreVideos(IConfiguration configuration)
        {
            return configuration.GetSection("MediaPath").GetSection("Videos").GetSection("Stores").Value;
        }
        static public string GetHallVideos(IConfiguration configuration)
        {
            return configuration.GetSection("MediaPath").GetSection("Videos").GetSection("Halls").Value;
        }
        static public string unique()
        {
            return DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
        }
    }
}
