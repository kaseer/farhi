﻿using AutoMapper;
using Common;
using Management.ViewModels;
using Models;
using System;

namespace Management.AutoMapper
{
    public class AdminUserMapper : Profile
    {
        //private readonly IHttpContextAccess _context;
        public AdminUserMapper()
        {
            //_context = context;

            CreateMap<AdminUserData, Adminuser>()
                .ForMember(x => x.Password, opt => opt.Ignore())
                .ForMember(x => x.Salt, opt => opt.Ignore())
                .ForMember(x => x.Permissions, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => { 
                    opt.PreCondition(x => x.AdminId == 0);
                    opt.MapFrom(src => Status.Active);
                })
                .ForMember(x => x.CreatedBy, opt => {
                    opt.PreCondition(x => x.AdminId == 0);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.ModifiedBy, opt => {
                    opt.PreCondition(x => x.AdminId > 0);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.CreatedOn, opt => {
                    opt.PreCondition(x => x.AdminId == 0);
                    opt.MapFrom(src => DateTime.Now);
                })
                .ForMember(x => x.ModifiedOn, opt => {
                    opt.PreCondition(x => x.AdminId > 0);
                    opt.MapFrom(src => DateTime.Now);
                });
        }
    }
}
