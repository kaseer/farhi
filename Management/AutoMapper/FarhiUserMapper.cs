﻿using AutoMapper;
using Common;
using Management.ViewModels;
using Models;
using System;

namespace Management.AutoMapper
{
    public class FarhiUserMapper : Profile
    {
        //private readonly IHttpContextAccess _context;
        public FarhiUserMapper()
        {
            //_context = context;

            CreateMap<FarhiUserData, Farhiuser>()
                .ForMember(x => x.Password, opt => opt.Ignore())
                .ForMember(x => x.Salt, opt => opt.Ignore())
                .ForMember(x => x.FarhiuserpermissionUsers, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => { 
                    opt.PreCondition(x => x.FarhiUserId == 0);
                    opt.MapFrom(src => Status.Active);
                })
                .ForMember(x => x.AdminCreatedBy, opt => {
                    opt.PreCondition(x => x.FarhiUserId == 0);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.AdminModifiedBy, opt => {
                    opt.PreCondition(x => x.FarhiUserId > 0);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.AdminCreatedOn, opt => {
                    opt.PreCondition(x => x.FarhiUserId == 0);
                    opt.MapFrom(src => DateTime.Now);
                })
                .ForMember(x => x.AdminModifiedOn, opt => {
                    opt.PreCondition(x => x.FarhiUserId > 0);
                    opt.MapFrom(src => DateTime.Now);
                });
        }
    }
}
