﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class StoreData
    {
        public string StoreName { get; set; }
        public string OwnerName { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public int StoreTypeId { get; set; }
        //public int? MainBranchId { get; set; }

        public int? StoreId { get; set; }
        //public bool StorePermission { get; set; }
        public StoreBranchData StoreBranch { get; set; }
        public FarhiUser FarhiUser { get; set; }
    }

    //public class StoreBranch
    //{
    //    public int AddressId { get; set; }
    //    public string AddressDescription { get; set; }
    //    public string PhoneNo1 { get; set; }
    //    public string PhoneNo2 { get; set; }
    //    public decimal? Longitude { get; set; }
    //    public decimal? Latitude { get; set; }
    //    public string[] TimeRange { get; set; }
    //}
}
