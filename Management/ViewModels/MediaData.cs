﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class MediaData
    {
        public int HallBranchId { get; set; }
        public List<string> Images { get; set; }
    }
}
