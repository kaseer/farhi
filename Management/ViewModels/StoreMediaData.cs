﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class StoreMediaData
    {
        public int StoreBranchId { get; set; }
        public List<string> Images { get; set; }
    }
}
