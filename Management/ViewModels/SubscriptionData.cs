﻿using System.Collections.Generic;

namespace Management.ViewModels
{
    public class SubscriptionData
    {
        public int SubscriptionPriceId { get; set; }
        public string Description { get; set; }
        public sbyte Type { get; set; }
        public List<Details> SubscriptionDetails { get; set; }
    }

    public class Details
    {
        public int SubscriptionPriceDetailsId { get; set; }
        public sbyte DurationType { get; set; }
        public sbyte DurationTime { get; set; }
        public decimal Price { get; set; }
    }
}
