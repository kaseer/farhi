﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class StoreNewSubscription
    {
        public int StoreBranchId { get; set; }
        public int SubscriptionPriceDetailsId { get; set; }
    }
}
