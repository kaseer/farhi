﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class HallBranchData
    {

        public int? HallId { get; set; }
        public int? HallBranchId { get; set; }
        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public int PeapleCapacity { get; set; }
        public int CarsCapacity { get; set; }
        public string[] TimeRange { get; set; }
        public List<HallAppontments> HallAppointments { get; set; }
        public List<HallServices> HallServices { get; set; }
    }

    public class AppointmentData
    {
        public int? Id { get; set; }
        public int ReservationType { get; set; }
        public string[] TimeRange { get; set; }
        public decimal Price { get; set; }
        public int? reservationStatus { get; set; }
        
    }
    public class HallAppontments
    {
        public sbyte WorkDayId { get; set; }
        public List<AppointmentData> AppointmentData { get; set; }
    }
    public class HallServices
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int? reservationStatus { get; set; }
    }
}
