﻿using FluentValidation;

namespace Management.ViewModels
{
    public class StoreTypesData
    {
        public string Description { get; set; }
    }
    public class StoreTypesDataValidator : AbstractValidator<StoreTypesData>
    {
        public StoreTypesDataValidator()
        {
            RuleFor(x => x.Description).NotEmpty().WithErrorCode("VE06001").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.Description).MaximumLength(30).WithErrorCode("VE06002").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
        }

    }
}
