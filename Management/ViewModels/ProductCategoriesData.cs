﻿using FluentValidation;

namespace Management.ViewModels
{
    public class ProductCategoriesData
    {
        public string Description { get; set; }
    }
    public class ProductCategoriesDataValidator : AbstractValidator<ProductCategoriesData>
    {
        public ProductCategoriesDataValidator()
        {
            RuleFor(x => x.Description).NotEmpty().WithErrorCode("VE07001").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.Description).MaximumLength(30).WithErrorCode("VE07002").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
        }

    }
}
