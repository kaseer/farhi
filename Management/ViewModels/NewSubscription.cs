﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class NewSubscription
    {
        public int HallBranchId { get; set; }
        public int SubscriptionPriceDetailsId { get; set; }
    }
}
