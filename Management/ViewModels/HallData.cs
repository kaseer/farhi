﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class HallData
    {
        public string HallName { get; set; }
        public string OwnerName { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        //public int? MainBranchId { get; set; }
        
        public int? HallId { get; set; }
        //public bool HallPermission { get; set; }
        public HallBranchData HallBranch { get; set; }
        public FarhiUser FarhiUser { get; set; }
    }
    //public class HallBranch
    //{
    //    public int AddressId { get; set; }
    //    public string AddressDescription { get; set; }
    //    public string PhoneNo1 { get; set; }
    //    public string PhoneNo2 { get; set; }
    //    public decimal? Longitude { get; set; }
    //    public decimal? Latitude { get; set; }
    //    public int PeapleCapacity { get; set; }
    //    public int CarsCapacity { get; set; }
    //    public byte WorkDayFrom { get; set; }
    //    public byte WorkDayTo { get; set; }
    //    public string[] TimeRange { get; set; }
    //}
    public class FarhiUser
    {
        public string UserName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string UserEmail { get; set; }
        public string UserPhoneNo1 { get; set; }
        public string UserPhoneNo2 { get; set; }
    }
}
