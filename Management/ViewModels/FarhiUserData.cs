﻿using Common;
using FluentValidation;
using System.Collections.Generic;

namespace Management.ViewModels
{
    public class UserData
    {
        public int? Id { get; set; }
        public List<int> Branches { get; set; }
    }
    
    public class NewFarhiUserData : FarhiUserData
    {
        public string Password { get; set; }
    }
    //public class FarhiStoreUserData : FarhiUserData
    //{
    //    public List<UserData> Stores { get; set; }
    //}
    public class FarhiUserData
    {
        public int FarhiUserId { get; set; }
        public string UserName { get; set; }
        public string LoginName { get; set; }
        public string Email { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public List<int> Permissions { get; set; }
        public List<UserData> Stores { get; set; }
        public List<UserData> Halls { get; set; }
        //public sbyte Status { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }
        public class FarhiUserDataValidator : AbstractValidator<FarhiUserData>
        {
            public FarhiUserDataValidator()
            {
                RuleFor(x => x.UserName).NotEmpty().WithErrorCode("VE11001").WithMessage("يرجي تعبئة الوصف");
                RuleFor(x => x.UserName).MaximumLength(40).WithErrorCode("VE11002").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.LoginName).NotEmpty().WithErrorCode("VE11002").WithMessage("يرجي تعبئة الوصف");
                RuleFor(x => x.LoginName).MaximumLength(40).WithErrorCode("VE11003").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.LoginName).Must(Validation.IsValidLoginName).WithErrorCode("VE11004").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");


                RuleFor(x => x.Email).NotEmpty().WithErrorCode("VE11005").WithMessage("يرجي ادخال البريد الالكتروني");
                RuleFor(x => x.Email).MaximumLength(50).WithErrorCode("VE11006").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Email).Must(Validation.IsValidEmail).WithErrorCode("VE11007").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.PhoneNo1).NotEmpty().WithErrorCode("VE11008").WithMessage("يرجي ادخال رقم الهاتف الاول");
                RuleFor(x => x.PhoneNo1).MaximumLength(15).WithErrorCode("VE11009").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE11010").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.PhoneNo2).MaximumLength(15).WithErrorCode("VE11011").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE11012").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            }
        }

        public class NewFarhiUserDataValidator : AbstractValidator<NewFarhiUserData>
        {
            public NewFarhiUserDataValidator()
            {
                Include(new FarhiUserDataValidator());
                RuleFor(x => x.Password).NotEmpty().WithErrorCode("VE11013").WithMessage("يرجي ادخال كلمة المرور");
                RuleFor(x => x.Password.Length).GreaterThanOrEqualTo(8).WithErrorCode("VE11014").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Password).Must(Validation.IsValidPassword).WithErrorCode("VE11015").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Halls.Count + x.Stores.Count).GreaterThan(0).WithErrorCode("VE11016").WithMessage("يرجي تعبئة صالة أو محل واحد علي الاقل");
                //RuleFor(x => x.HallBranches.Count).GreaterThan(0).Unless(x => x.HallId != null).WithErrorCode("VE1115").WithMessage("يرجي اختيار صاله او فروع");
            }
        }
    }
}
