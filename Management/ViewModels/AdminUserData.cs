﻿using Common;
using FluentValidation;
using System.Collections.Generic;

namespace Management.ViewModels
{
    public class NewAdminUserData : AdminUserData
    {
        public string Password { get; set; }
    }
    public class AdminUserData
    {
        public int AdminId { get; set; }
        public string AdminName { get; set; }
        public string LoginName { get; set; }
        //public string Salt { get; set; }
        public string Email { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public List<int> Permissions { get; set; }
        //public sbyte Status { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }
        public class AdminUserDataValidator : AbstractValidator<AdminUserData>
        {
            public AdminUserDataValidator()
            {
                RuleFor(x => x.AdminName).NotEmpty().WithErrorCode("VE10001").WithMessage("يرجي تعبئة الوصف");
                RuleFor(x => x.AdminName).MaximumLength(40).WithErrorCode("VE10002").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.LoginName).NotEmpty().WithErrorCode("VE10003").WithMessage("يرجي تعبئة الوصف");
                RuleFor(x => x.LoginName).MaximumLength(40).WithErrorCode("VE10004").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.LoginName).Must(Validation.IsValidLoginName).WithErrorCode("VE10005").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");


                RuleFor(x => x.Email).NotEmpty().WithErrorCode("VE10006").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Email).MaximumLength(50).WithErrorCode("VE10007").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Email).Must(Validation.IsValidEmail).WithErrorCode("VE10008").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.PhoneNo1).NotEmpty().WithErrorCode("VE10009").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo1).MaximumLength(15).WithErrorCode("VE10010").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE10011").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.PhoneNo2).MaximumLength(15).WithErrorCode("VE10012").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE10013").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            }
        }

        public class NewAdminUserDataValidator : AbstractValidator<NewAdminUserData>
        {
            public NewAdminUserDataValidator() 
            {
                Include(new AdminUserDataValidator());
                RuleFor(x => x.Password).NotEmpty().WithErrorCode("VE10014").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Password.Length).GreaterThanOrEqualTo(8).WithErrorCode("VE10015").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Password).Must(Validation.IsValidPassword).WithErrorCode("VE10016").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            }
        }
    }
}
