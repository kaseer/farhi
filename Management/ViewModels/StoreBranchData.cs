﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class StoreBranchData
    {

        public int? StoreId { get; set; }
        public int? StoreBranchId { get; set; }
        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public string[] TimeRange { get; set; }
        public List<StoreAppontments> StoreAppointments { get; set; }
    }

    
    public class StoreAppontments
    {
        public sbyte WorkDayId { get; set; }
        public string[] TimeRange { get; set; }
    }
    
}
