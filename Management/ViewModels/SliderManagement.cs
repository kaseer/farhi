﻿using FluentValidation;
using System;

namespace Management.ViewModels
{
    public class SliderManagementData
    {
        public string SampleName { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class SampleDataValidator : AbstractValidator<SliderManagementData>
    {
        public SampleDataValidator()
        {
            RuleFor(x => x.SampleName).NotEmpty().WithMessage("Error SmapleName").WithErrorCode("VE05001");
            RuleFor(x => x.Description).NotEmpty().WithMessage("Error Description").WithErrorCode("VE05002");
        }

    }
}
