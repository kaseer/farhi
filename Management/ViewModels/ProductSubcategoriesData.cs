﻿using FluentValidation;

namespace Management.ViewModels
{
    public class ProductSubcategoriesData
    {
        public string Description { get; set; }
        public int CategoryId { get; set; }
    }
    public class ProductSubcategoriesDataValidator : AbstractValidator<ProductSubcategoriesData>
    {
        public ProductSubcategoriesDataValidator()
        {
            RuleFor(x => x.Description).NotEmpty().WithErrorCode("VE08001").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.Description).MaximumLength(30).WithErrorCode("VE08002").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
        }

    }
}
