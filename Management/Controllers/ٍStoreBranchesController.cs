﻿using Common;
using Management.Global;
using Management.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreBranchesController : RootController
    {
        private IConfiguration configuration;
        private IWebHostEnvironment _env;
        public StoreBranchesController(farhiContext context, IWebHostEnvironment env, IConfiguration configuration) : base(context)
        {
            _env = env;
            this.configuration = configuration;
        }

        //private string imagePath = "/img/";

        List<string> attachmentNames = new List<string>();

        public bool SaveAttachment(string attachmentName, string AttachmentFile)
        {
            try
            {
                byte[] file = Convert.FromBase64String(AttachmentFile.Substring(AttachmentFile.IndexOf(",") + 1));

                var imagepath = ConfigurationItems.GetStoreImages(configuration);
                var path = Path.Combine(imagepath, attachmentName);
                FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
                fileStream.Write(file, 0, file.Length);
                fileStream.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpGet]
        public async Task<IActionResult> getAll(int pageNo, int pageSize, int? storeId, int? storeTypeId, string searchByAddress)
        {
            try
            {
                
                if (storeId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04001", result = "الرجاء التاكد من اختيار المحل " });
                }
                if (pageNo <= 0 || pageSize <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04002", result = "الرجاء التاكد اختيار رقم الصفحة او حجم الصفحة " });
                }

                var StoreBranchesQuery = from h in db.Storebranches
                                 where h.Status != Status.Deleted
                                 && h.StoreId == storeId
                                         select h;

                if (!string.IsNullOrWhiteSpace(searchByAddress))
                {
                    StoreBranchesQuery = from h in StoreBranchesQuery
                                        where h.Address.AddressName.Contains(searchByAddress)
                                        select h;
                }
                if (storeTypeId > 0)
                {
                    StoreBranchesQuery = from h in StoreBranchesQuery
                                         where h.Store.StoreTypeId == storeTypeId
                                         select h;
                }
                var StoreBranches = await (from h in StoreBranchesQuery
                                          orderby h.StoreBranchId
                                   select new
                                   {
                                       h.StoreBranchId,
                                       h.Address.AddressName,
                                       h.PhoneNo1,
                                       h.Store.MainBranchId,
                                       //workDays = Constant.SelectDay(h.WorkDayFrom.Value) + " -  " + Constant.SelectDay(h.WorkDayTo.Value),
                                       workTimes = h.StartTime.ToString(@"hh\:mm\:ss") + " -  " + h.EndTime.ToString(@"hh\:mm\:ss"),
                                       h.AdminCreatedByNavigation.AdminName,
                                       CreatedOn = h.AdminCreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToListAsync();
                var total = await StoreBranchesQuery.CountAsync();
                return Ok(new { StatusCode = 1, StoreBranches, total  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04001", result = errorMsg });
            }
        }

        [HttpPost]
        public async Task<IActionResult> addStoreBranch(StoreBranchData StoreBranchData)
        {
            try
            {
                if(StoreBranchData.StoreId == null)
                {
                    return BadRequest(new { StatusCode = "RE04003", result = "الرجاء التاكد من اختيار المحل " });
                }
                if (StoreBranchData.StoreId.Value <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04004", result = "الرجاء التاكد من اختيار المحل " });
                }
                if (string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE04005", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (StoreBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE04006", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo2))
                {
                    if (StoreBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE04007", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }
                
                
                if (StoreBranchData.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE04008", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(StoreBranchData.AddressDescription))
                {
                    if (StoreBranchData.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode = "RE04009", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE04010", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (StoreBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE04011", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo2))
                {
                    if (StoreBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE04012", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (StoreBranchData.Longitude > 180 || StoreBranchData.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE04013", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (StoreBranchData.Longitude > 90 || StoreBranchData.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE04014", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                



                if (!Validation.IsValidTimeFormat(StoreBranchData.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE04015", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(StoreBranchData.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE04016", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }
                if(StoreBranchData.StoreAppointments.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04017", result = "الرجاء التاكد من  ادخال مواعيد الحجز " });
                }
                foreach (var appontment in StoreBranchData.StoreAppointments)
                {
                    if (appontment.WorkDayId <= 0 || appontment.WorkDayId > 7)
                    {
                        return BadRequest(new { StatusCode = "RE04018", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                    }

                    if (!Validation.IsValidTimeFormat(appontment.TimeRange[0]))
                    {
                        return BadRequest(new { StatusCode = "RE04019", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                    }
                    if (Convert.ToDateTime(appontment.TimeRange[0]) < Convert.ToDateTime(appontment.TimeRange[0])
                        || Convert.ToDateTime(StoreBranchData.TimeRange[0]) > Convert.ToDateTime(StoreBranchData.TimeRange[1]))
                    {
                        return BadRequest(new { StatusCode = "RE04020", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                    }
                    if (!Validation.IsValidTimeFormat(appontment.TimeRange[1]))
                    {
                        return BadRequest(new { StatusCode = "RE04021", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                    }
                    if (Convert.ToDateTime(appontment.TimeRange[1]) > Convert.ToDateTime(appontment.TimeRange[1])
                        || Convert.ToDateTime(StoreBranchData.TimeRange[1]) < Convert.ToDateTime(StoreBranchData.TimeRange[0]))
                    {
                        return BadRequest(new { StatusCode = "RE04022", result = "الرجاء التاكد من ادخال وقت نهاية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                    }
                }


                var checkBranchAddress = await (from h in db.Storebranches
                                           where h.AddressId == StoreBranchData.AddressId
                                           && h.Status != Status.Deleted
                                           && h.StoreId == StoreBranchData.StoreId.Value
                                           select h).CountAsync();
                if (checkBranchAddress > 0)
                {
                    if (string.IsNullOrWhiteSpace(StoreBranchData.AddressDescription))
                    {
                        return BadRequest(new { StatusCode = "RE04023", result = "يوجد فرع لهذا المحل في نفس هذا العنوان ... يرجي كتابة تفصيل العنوان للتفرقة " });
                    }
                }

                Storebranch addStoreBranch = new Storebranch() { 
                    AddressDescription = StoreBranchData.AddressDescription,
                    AddressId = StoreBranchData.AddressId,
                    StoreId = StoreBranchData.StoreId.Value,
                    Latitude = StoreBranchData.Latitude,
                    Longitude = StoreBranchData.Longitude,
                    PhoneNo1 = StoreBranchData.PhoneNo1,
                    PhoneNo2 = StoreBranchData.PhoneNo2,
                    StartTime = TimeOnly.Parse(StoreBranchData.TimeRange[0]),
                    EndTime = TimeOnly.Parse(StoreBranchData.TimeRange[1]),
                    Status = Status.Entry,
                    AdminCreatedBy = UserId(),
                    AdminCreatedOn = DateTime.Now
                };

                await db.Storebranches.AddAsync(addStoreBranch);
                foreach (var appointment in StoreBranchData.StoreAppointments)
                {
                    Storeworkday storeappointments = new Storeworkday()
                    {
                        StoreBranchId = addStoreBranch.StoreBranchId,
                        WorkDayId = appointment.WorkDayId,
                        StartTime = TimeOnly.Parse(StoreBranchData.TimeRange[0]),
                        EndTime = TimeOnly.Parse(StoreBranchData.TimeRange[1]),
                    };
                    addStoreBranch.Storeworkdays.Add(storeappointments);
                }

                await db.SaveChangesAsync();
                var addedBranch = await (from h in db.Storebranches
                                         where h.StoreBranchId == addStoreBranch.StoreBranchId
                                         select new
                                         {
                                             h.StoreBranchId,
                                             h.Address.AddressName,
                                             h.PhoneNo1,
                                             //workDays = Constant.SelectDay(h.WorkDayFrom.Value) + " -  " + Constant.SelectDay(h.WorkDayTo.Value),
                                             workTimes = h.StartTime.ToString(@"hh\:mm\:ss") + " -  " + h.EndTime.ToString(@"hh\:mm\:ss"),
                                             h.AdminCreatedByNavigation.AdminName,
                                             CreatedOn = h.AdminCreatedOn.ToString("yyyy/MM/dd"),
                                             h.Status
                                         }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1,  message = "تم اضافة فرع لهذا المحل بنجاح", addedBranch  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04002", result = errorMsg });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getDetails(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04024", result = "الرجاء التاكد اختيار فرع المحل " });
                }

                var StoreBranchData = await (from h in db.Storebranches
                                      where h.Status != Status.Deleted
                                      && h.StoreBranchId == id
                                      select new
                                      {
                                          h.Address.AddressName,
                                          h.AddressDescription,
                                          h.PhoneNo1,
                                          h.PhoneNo2,
                                          h.Longitude,
                                          h.Latitude,
                                          StartTime = h.StartTime.ToString(@"hh\:mm\:ss"),
                                          EndTime = h.EndTime.ToString(@"hh\:mm\:ss"),
                                          StoreAppointments = h.Storeworkdays
                                            .Select(x => new
                                            {
                                                WorkDay = Constant.SelectDay(x.WorkDayId),
                                                StartTime = x.StartTime.ToString(@"hh\:mm\:ss"),
                                                EndTime = x.EndTime.ToString(@"hh\:mm\:ss"),
                                            }).ToList(),
                                          Subscriptions = 
                                          h.Subscriptions.Select(x=>  
                                                x.Subscriptiondetails.Select(c=>new { 
                                                    c.SubscriptionPriceDetails.SubscriptionPrice.Description,
                                                    StartDate = c.StartDate.ToString("yyyy/MM/dd"),
                                                    EndDate = c.EndDate.ToString("yyyy/MM/dd"),
                                                    c.Price
                                                }).ToList()
                                          ).ToList()
                                      }).SingleOrDefaultAsync();

                if (StoreBranchData == null)
                {
                    return BadRequest(new { StatusCode = "RE04025", result = "لا توجد بيانات لهذا المحل " });
                }

                

                return Ok(new { StatusCode = 1, StoreBranchData });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04003", result = errorMsg });
            }
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> StoreBranchToEdit(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04026", result = "الرجاء التاكد اختيار فرع المحل " });
                }


                
                var StoreBranchData =  await (from h in db.Storebranches
                                            where h.Status != Status.Deleted
                                            && h.StoreBranchId == id
                                            select new
                                            {
                                                h.StoreId,
                                                h.StoreBranchId,
                                                h.AddressId,
                                                h.AddressDescription,
                                                h.PhoneNo1,
                                                h.PhoneNo2,
                                                h.Longitude,
                                                h.Latitude,
                                                storeAppointments = h.Storeworkdays.Select(x=>new { 
                                                    x.WorkDayId,
                                                    TimeRange = new[] { x.StartTime.ToString(@"hh\:mm\:ss"), x.EndTime.ToString(@"hh\:mm\:ss") },

                                                }).ToList(),
                                                TimeRange = new[] { h.StartTime.ToString(@"hh\:mm\:ss"), h.EndTime.ToString(@"hh\:mm\:ss") },
                                                

                                            }).SingleOrDefaultAsync();

                if (StoreBranchData == null)
                {
                    return BadRequest(new { StatusCode = "RE04027", result = "لا توجد بيانات لهذا المحل " });
                }



                return Ok(new { StatusCode = 1, StoreBranch = StoreBranchData } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04004", result = errorMsg });
            }
        }


        [HttpPut]
        public async Task<IActionResult> editStoreBranch([FromBody] StoreBranchData StoreBranchData)
        {
            try
            {
                if (StoreBranchData.StoreId == null)
                {
                    return BadRequest(new { StatusCode = "RE04028", result = "الرجاء التاكد من اختيار المحل " });
                }
                if (StoreBranchData.StoreId.Value <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04029", result = "الرجاء التاكد من اختيار المحل " });
                }
                if (StoreBranchData.StoreBranchId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04030", result = "الرجاء التاكد من اختيار فرع المحل " });
                }
                if (string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE04031", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (StoreBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE04032", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo2))
                {
                    if (StoreBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE04033", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }


                if (StoreBranchData.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE04034", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(StoreBranchData.AddressDescription))
                {
                    if (StoreBranchData.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode = "RE04035", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE04036", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (StoreBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE04037", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(StoreBranchData.PhoneNo2))
                {
                    if (StoreBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE04038", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (StoreBranchData.Longitude > 180 || StoreBranchData.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE04039", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (StoreBranchData.Longitude > 90 || StoreBranchData.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE04040", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }



                if (!Validation.IsValidTimeFormat(StoreBranchData.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE04041", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(StoreBranchData.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE04042", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }
                if (StoreBranchData.StoreAppointments.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04043", result = "الرجاء التاكد من ادخال مواعيد الحجز " });
                }
                foreach (var appontment in StoreBranchData.StoreAppointments)
                {
                    if (appontment.WorkDayId <= 0 || appontment.WorkDayId > 7)
                    {
                        return BadRequest(new { StatusCode = "RE04044", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                    }
                    if (!Validation.IsValidTimeFormat(appontment.TimeRange[0]))
                    {
                        return BadRequest(new { StatusCode = "RE04045", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                    }
                    if (Convert.ToDateTime(appontment.TimeRange[0]) < Convert.ToDateTime(appontment.TimeRange[0])
                        || Convert.ToDateTime(StoreBranchData.TimeRange[0]) > Convert.ToDateTime(StoreBranchData.TimeRange[1]))
                    {
                        return BadRequest(new { StatusCode = "RE04046", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                    }
                    if (!Validation.IsValidTimeFormat(appontment.TimeRange[1]))
                    {
                        return BadRequest(new { StatusCode = "RE04047", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                    }
                    if (Convert.ToDateTime(appontment.TimeRange[1]) > Convert.ToDateTime(appontment.TimeRange[1])
                        || Convert.ToDateTime(StoreBranchData.TimeRange[1]) < Convert.ToDateTime(StoreBranchData.TimeRange[0]))
                    {
                        return BadRequest(new { StatusCode = "RE04048", result = "الرجاء التاكد من ادخال وقت نهاية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                    }
                }


                var checkBranchAddress = await (from h in db.Storebranches
                                                where h.AddressId == StoreBranchData.AddressId
                                                && h.Status != Status.Deleted
                                                && h.StoreId == StoreBranchData.StoreId.Value
                                                select h).CountAsync();

                if (checkBranchAddress > 0)
                {
                    if (string.IsNullOrWhiteSpace(StoreBranchData.AddressDescription))
                    {
                        return BadRequest(new { StatusCode = "RE04049", result = "يوجد فرع لهذا المحل في نفس هذا العنوان ... يرجي كتابة تفصيل العنوان للتفرقة " });
                    }
                }
                var editStoreBranch = await (from h in db.Storebranches
                                            where h.Status != Status.Deleted
                                            && h.StoreBranchId == StoreBranchData.StoreBranchId
                                            select new {
                                                h,
                                                h.Storeworkdays,
                                                h.Address.AddressName
                                            }).SingleOrDefaultAsync();
                if (editStoreBranch == null)
                {
                    return BadRequest(new { StatusCode = "RE04050", result = " لا توجد بيانات لهذا المحل  " });
                }
                if (editStoreBranch.h.StoreId != StoreBranchData.StoreId.Value)
                {
                    return BadRequest(new { StatusCode = "RE04051", result = "لا يوجد بيانات فرع للمحل المختارة" });
                }
                List<int> StoreApponimentsIds = new List<int>();

                editStoreBranch.h.AddressDescription = StoreBranchData.AddressDescription;
                editStoreBranch.h.AddressId = StoreBranchData.AddressId;
                editStoreBranch.h.StoreId = StoreBranchData.StoreId.Value;
                editStoreBranch.h.Latitude = StoreBranchData.Latitude;
                editStoreBranch.h.Longitude = StoreBranchData.Longitude;
                editStoreBranch.h.PhoneNo1 = StoreBranchData.PhoneNo1;
                editStoreBranch.h.PhoneNo2 = StoreBranchData.PhoneNo2;
                editStoreBranch.h.StartTime = TimeOnly.Parse(StoreBranchData.TimeRange[0]);
                editStoreBranch.h.EndTime = TimeOnly.Parse(StoreBranchData.TimeRange[1]);
                editStoreBranch.h.Status = Status.Active;
                editStoreBranch.h.ModifiedBy = UserId();
                editStoreBranch.h.ModifiedOn = DateTime.Now;



                foreach (var appontment in editStoreBranch.Storeworkdays)
                {
                    db.Storeworkdays.Remove(appontment);
                }
                foreach (var appontment in StoreBranchData.StoreAppointments)
                {
                    Storeworkday addAppointment  = new Storeworkday() { 
                        WorkDayId = appontment.WorkDayId,
                        StoreBranchId = editStoreBranch.h.StoreBranchId,
                        StartTime = TimeOnly.Parse(appontment.TimeRange[0]),
                        EndTime = TimeOnly.Parse(appontment.TimeRange[1])
                    };
                    db.Storeworkdays.Add(addAppointment);
                }
                

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم تعديل فرع لهذا المحل بنجاح" ,editStoreBranch.AddressName });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04005", result = errorMsg });
            }
        }


        [HttpPost("addImages")]
        public async Task<IActionResult> addImages(StoreMediaData images)
        {
            try
            {
                if (images == null)
                {
                    return BadRequest(new { StatusCode = "RE04052", result = "يرجي اختيار صور لتحميلها" });
                }
                if (images.StoreBranchId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04053", result = "يرجي التاكد من بيانات الفرع" });
                }
                if (images.Images.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04054", result = "يرجي التاكد من ادخال صور" });
                }
                if (images.Images.Count > 10)
                {
                    return BadRequest(new { StatusCode = "RE04055", result = "لا يمكن ادخال اكثر من 10 صور" });
                }
                var StoreInfo = await (from h in db.Storebranches
                                      where h.StoreBranchId == images.StoreBranchId
                                      select new {
                                          h.Store.StoreName,
                                          MediaCount = h.Media.Count()
                                      }).SingleOrDefaultAsync();
                int checkCount = 10 - StoreInfo.MediaCount;
                if (images.Images.Count > checkCount)
                {
                    return BadRequest(new { StatusCode = "RE04056", result = "لا يمكن ادخال اكثر من "+ checkCount + " صور" });
                }
                int index = 1;
                foreach(var image in images.Images)
                {
                    string attachmentFirstName = StoreInfo.StoreName + "_" + images.StoreBranchId + "_" + index;
                    string attachmentName = "";
                    byte[] file = Convert.FromBase64String(image.Substring(image.IndexOf(",") + 1));

                    attachmentName = attachmentFirstName + attachmentName + ".jpg";
                    attachmentNames.Add(attachmentName);
                    bool check = SaveAttachment(attachmentName, image);
                    if (!check)
                        return BadRequest(new { StatusCode = "RE04057", result = "يوجد خطأ في الصورة المدخلة " });
                    index++;
                }
                
                foreach(var attachment in attachmentNames)
                {
                    Medium addImage = new Medium() {
                        MediaType = 1,
                        Path = attachment,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                        StoreBranchId = images.StoreBranchId,
                        Status = Status.Active,
                    };
                    await db.Media.AddAsync(addImage);
                }

                await db.SaveChangesAsync();

                var imagesQuery = await (from m in db.Media
                                         where m.StoreBranchId == images.StoreBranchId
                                         && m.MediaType == 1
                                         && m.Status != Status.Deleted
                                         select m).ToListAsync();

                var imagepath = ConfigurationItems.GetStoreImages(configuration);
                var oldImages = imagesQuery.Select(x => new {
                    imageId = x.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(imagepath + x.Path))
                });
                return Ok(new { StatusCode = 1,  message = "تم تحميل الصور بنجاح", oldImages  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04006", result = errorMsg });
            }
        }

        [HttpPost("addVideo")]
        public async Task<IActionResult> addVideo(int StoreBranchId, IFormFile file)
        {
            try
            {
                if (StoreBranchId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04058", result = "يرجي التاكد من بيانات الفرع" });
                }
                var StoreInfo = await (from h in db.Storebranches
                                       .Include(m => m.Media.Where(x => x.MediaType == 2))
                                       .Include(m => m.Store)
                                      where h.StoreBranchId == StoreBranchId
                                      && h.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (StoreInfo == null)
                {
                    return NotFound();
                }
                if(StoreInfo.Media.Count > 0)
                {
                    foreach(var item in StoreInfo.Media)
                    {
                        db.Media.Remove(item);
                    }
                }
                string FileName = StoreInfo.Store.StoreName + "_"+ StoreBranchId +".mp4" ;
                var videopath = ConfigurationItems.GetStoreVideos(configuration);
                var filePath = Path.Combine(videopath, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                }
                Medium addVideo = new Medium()
                {
                    MediaType = 2,
                    Path = FileName,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    StoreBranchId = StoreBranchId,
                    Status = Status.Active,
                };
                await db.Media.AddAsync(addVideo);
                await db.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04007", result = errorMsg });
            }
        }

        [HttpDelete("{id}/deleteVideo")]
        public async Task<IActionResult> deleteVideo(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04059", result = "يرجي التاكد من بيانات الفرع" });
                }
                var StoreInfo = await (from h in db.Storebranches
                                      where h.StoreBranchId == id
                                      select h.Store.StoreName).SingleOrDefaultAsync();
                if (StoreInfo == null)
                {
                    return NotFound(new { StatusCode = "RE04060", result = "لم يتم العثور علي بيانات الفرع" });
                }

                

                var removePath = await (from m in db.Media
                                        where m.StoreBranchId == id
                                        && m.MediaType == 2
                                        select m).FirstOrDefaultAsync();
                if (removePath == null)
                {
                    return NotFound(new { StatusCode = "RE04061", result = "لم يتم العثور علي بيانات الفيديو المختار" });
                }
                string FileName = StoreInfo + "_" + id + ".mp4";

                var path = ConfigurationItems.GetStoreVideos(configuration) + FileName;

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    db.Media.Remove(removePath);
                }
                else
                {
                    return NotFound(new { StatusCode = "RE04062", result = "لم يتم العثور علي الفيديو المختار في مستكشف الملفات" });
                }
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم حذف الفيديو بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04008", result = errorMsg });
            }
        }

        [HttpGet("{id}/getVideo")]
        public async Task<IActionResult> getVideo(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04063", result = "يرجي التاكد من بيانات الفرع" });
                }
                var video = await (from m in db.Media
                                   where m.StoreBranchId == id
                                   && m.MediaType == 2
                                   && m.Status != Status.Deleted
                                   select m.Path
                                   ).SingleOrDefaultAsync();


                if (video == null)
                {
                    return Ok(new { StatusCode = 1, video } );
                }
                var videopath = ConfigurationItems.GetStoreVideos(configuration);
                video = "data:video/mp4;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(videopath + video));
                return Ok(new { StatusCode = 1,  data = video } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04009", result = errorMsg });
            }
        }

        [HttpGet("{id}/getImages")]
        public async Task<IActionResult> getImages(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04064", result = "يرجي التاكد من بيانات الفرع" });
                }
                var imagesQuery = await (from m in db.Media
                                   where m.StoreBranchId == id
                                   && m.MediaType == 1
                                   && m.Status != Status.Deleted
                                   select m).ToListAsync();


                if (imagesQuery.Count <= 0)
                {
                    return Ok(new { StatusCode = 1, images = imagesQuery } );
                }
                //for(int i = 0; i < imagesQuery.Count; i++)
                //{
                //    imagesQuery[0].Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + images[0].Path));
                //}
                var imagepath = ConfigurationItems.GetStoreImages(configuration);
                var images = imagesQuery.Select(x => new {
                    imageId = x.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(imagepath + x.Path))
                });
                return Ok(new { StatusCode = 1, images } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04010", result = errorMsg });
            }
        }

        [HttpPut("deleteOldImages")]
        public async Task<IActionResult> deleteOldImages([FromBody]List<int> checkedImages)
        {
            try
            {
                if (checkedImages.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04065", result = "يرجي التاكد من بيانات الفرع" });
                }
                foreach(var image in checkedImages)
                {
                    var deleteImage = await (from m in db.Media
                                             where m.MediaId == image
                                             select m).SingleOrDefaultAsync();
                    if(deleteImage != null)
                    {
                        db.Media.Remove(deleteImage);
                    }
                }
                await db.SaveChangesAsync();
                
                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04011", result = errorMsg });
            }
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> lockStoreBranch(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04066", result = "الرجاء التاكد اختيار فرع المحل " });
                }

                var StoreData = await (from h in db.Storebranches
                                      where h.StoreBranchId == id
                                      && h.Status != Status.Deleted
                                      && h.Store.StoreId != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE04067", result = "لا توجد بيانات لفرع هذا المحل" });
                }

                if (StoreData.Status == Status.Locked)
                {
                    return BadRequest(new { StatusCode = "RE04068", result = "حالة فرع هذا المحل مجمدة مسبقا" });
                }

                var checkStoreSubscription = await (from h in db.Subscriptiondetails
                                                   where
                                                   h.Subscription.StoreBranchId == id &&
                                                   h.EndDate > DateTime.Now
                                                   select h).AnyAsync();
                if (checkStoreSubscription)
                {
                    return BadRequest(new { StatusCode = "RE04069", result = "لا يمكن تجميد فرع المحل و يوجد فروع لها تحت الاشتراك" });
                }

                StoreData.Status = Status.Locked;
                StoreData.ModifiedBy = UserId();
                StoreData.ModifiedOn = DateTime.Now;
                
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم تجميد  فرع المحل بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04012", result = errorMsg });
            }
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> unlockStoreBranch(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04070", result = "الرجاء التاكد اختيار فرع المحل " });
                }

                var StoreData = await (from h in db.Storebranches
                                      where h.StoreBranchId == id
                                      && h.Status != Status.Deleted
                                      && h.Store.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE04071", result = "لا توجد بيانات لهذا المحل" });
                }

                if (StoreData.Status == Status.Entry)
                {
                    return BadRequest(new { StatusCode = "RE04072", result = "حالة فرع هذا المحل غير مجمد" });
                }

                StoreData.Status = Status.Entry;
                StoreData.ModifiedBy = UserId();
                StoreData.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم فك تجميد فرع المحل بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04013", result = errorMsg });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> deleteStoreBranch(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04073", result = "الرجاء التاكد اختيار فرع المحل " });
                }

                var StoreData = await (from h in db.Storebranches
                                      where h.StoreBranchId == id
                                    //   && h.Store.MainBranchId != id
                                      select new { h,h.Store.MainBranchId }).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE04074", result = "لا توجد بيانات لفرع هذا المحل" });
                }
                if (StoreData.MainBranchId == id)
                {
                    return BadRequest(new { StatusCode = "RE04075", result = "لا يمكن حذف الفرع الرئيسي ... لحذف يرجي عمل العملية عند المحل" });
                }
                if (StoreData.h.Status == Status.Deleted)
                {
                    return BadRequest(new { StatusCode = "RE04076", result = "حالة فرع هذا المحل محذوف مسبقا" });
                }

                var checkStoreSubscription = await (from h in db.Subscriptiondetails
                                                   where
                                                   h.Subscription.StoreBranchId == id &&
                                                   h.EndDate > DateTime.Now
                                                   select h).AnyAsync();
                if (checkStoreSubscription)
                {
                    return BadRequest(new { StatusCode = "RE04078", result = "لا يمكن حذف المحل و يوجد فروع لها تحت الاشتراك" });
                }

                StoreData.h.Status = Status.Deleted;
                StoreData.h.ModifiedBy = UserId();
                StoreData.h.ModifiedOn = DateTime.Now;


                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم حذف فرع المحل بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04014", result = errorMsg });
            }
        }

        [HttpPut("{id}/assignMainBranch")]
        public async Task<IActionResult> assignMainBranch(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04079", result = "الرجاء التاكد اختيار فرع المحل " });
                }

                var StoreData = await (from h in db.Storebranches
                                      where h.StoreBranchId == id
                                      && h.Status != Status.Deleted
                                      && h.Store.Status != Status.Deleted
                                      select new { h,h.Store }).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE04080", result = "لا توجد بيانات لفرع هذا المحل" });
                }

                if (StoreData.Store.MainBranchId == id)
                {
                    return BadRequest(new { StatusCode =  "RE04081", result = "حالة فرع هذا المحل معيين كرئيسي مسبقا" });
                }

                StoreData.Store.MainBranchId = id;
                StoreData.h.ModifiedBy = UserId();
                StoreData.h.ModifiedOn = DateTime.Now;
                StoreData.Store.ModifiedBy = UserId();
                StoreData.Store.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم تعيين هذا الفرع كفرع رئيسي بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04015", result = errorMsg });
            }
        }

        [HttpPost("addSubscription")]
        public async Task<IActionResult> addSubscription([FromBody] StoreNewSubscription data)
        {
            try
            {
                if(data.StoreBranchId <= 0)
                {
                    return BadRequest(new { StatusCode =  "RE04082", result = "يرجي التاكد من اختيار فرع المحل" });
                }
                if (data.SubscriptionPriceDetailsId <= 0)
                {
                    return BadRequest(new { StatusCode =  "RE04083", result = "يرجي اختيار نوع الاشتراك" });
                }

                var StoreBranch = await (from h in db.Storebranches
                                        where h.StoreBranchId == data.StoreBranchId
                                        && h.Status != Status.Deleted
                                        && h.Store.Status != Status.Deleted
                                        select h).SingleOrDefaultAsync();
                if(StoreBranch == null)
                {
                    return BadRequest(new { StatusCode =  "RE04084", result = "لا توجد بيانات لفرع هذا المحل" });
                }
                var checkStoreSubscription = await (from h in db.Subscriptiondetails
                                                   where
                                                   h.Subscription.StoreBranchId == data.StoreBranchId &&
                                                //    h.SubscriptionPriceDetailsId == data.SubscriptionPriceDetailsId &&
                                                   h.EndDate > DateTime.Now
                                                   select h).AnyAsync();
                if (checkStoreSubscription)
                {
                    return BadRequest(new { StatusCode =  "RE04085", result = "فرع هذا المحل لديه اشتراك مفعل حاليا" });
                }
                var subscriptionPrices = await (from s in db.Subscriptionpricedetails
                                                where s.SubscriptionPriceDetailsId == data.SubscriptionPriceDetailsId
                                                && s.SubscriptionPrice.Status != Status.Deleted
                                                select s).SingleOrDefaultAsync();
                if (subscriptionPrices == null)
                {
                    return BadRequest(new { StatusCode =  "RE04086", result = "لم يتم العثور علي بيانات لنوع هذا الاشتراك" });
                }
                Subscription newSubscription = new Subscription() { 
                    StoreBranchId = data.StoreBranchId,
                    Status = Status.Active,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now
                };
                db.Subscriptions.Add(newSubscription);

                Subscriptiondetail newSubscriptiondetails = new Subscriptiondetail() {
                    SubscriptionId = newSubscription.SubscriptionId,
                    SubscriptionPriceDetailsId = data.SubscriptionPriceDetailsId,
                    Price = subscriptionPrices.Price,
                    StartDate = DateTime.Now,
                    EndDate = subscriptionPrices.DurationType == 1 ? DateTime.Now.AddYears(subscriptionPrices.DurationTime) :
                             subscriptionPrices.DurationType == 2 ? DateTime.Now.AddMonths(subscriptionPrices.DurationTime) :
                            DateTime.Now.AddDays(subscriptionPrices.DurationTime * 7),
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    Status = Status.Active,
                };
                newSubscription.Subscriptiondetails.Add(newSubscriptiondetails);

                StoreBranch.Status = Status.Active;
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم الاشتراك  بنجاح" } );

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX04016", result = errorMsg });
            }
        }
    }
}
