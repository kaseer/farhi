﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using Newtonsoft.Json;
using Serilog;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class HallsController : RootController
    {
        public IConfiguration configuration;

        private IWebHostEnvironment _env;
        public HallsController(farhiContext context, IWebHostEnvironment env, IConfiguration configuration) : base(context)
        {
            this.configuration = configuration;
            _env = env;
        }

        private string imagePath = "/img/";

        public bool SaveAttachment(string attachmentName, string AttachmentFile)
        {
            try
            {
                byte[] file = Convert.FromBase64String(AttachmentFile.Substring(AttachmentFile.IndexOf(",") + 1));

                var path = Path.Combine(_env.WebRootPath + imagePath, attachmentName);
                FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
                fileStream.Write(file, 0, file.Length);
                fileStream.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        //[AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> getAll(int pageNo, int pageSize, string searchByName)
        {
            try
            {
                if (pageNo <= 0 || pageSize <= 0)
                {
                    return BadRequest(new { StatusCode ="RE01001", result = "الرجاء التاكد اختيار رقم الصفحة او حجم الصفحة " });
                }

                var hallsQuery = from h in db.Halls
                                 where h.Status != Status.Deleted
                                 select h;

                if (!string.IsNullOrWhiteSpace(searchByName))
                {
                    hallsQuery = from h in hallsQuery
                                 where h.HallName.Contains(searchByName)
                                 select h;
                }
                var halls = await (from h in hallsQuery
                                   select new
                                   {
                                       h.HallId,
                                       h.HallName,
                                       h.OwnerName,
                                       h.PhoneNo1,
                                       h.CreatedByNavigation.AdminName,
                                       CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToListAsync();
                var total = await hallsQuery.CountAsync();
                //var xsx = halls[111].CreatedOn;
                //var path = Path.Combine(_env.WebRootPath);
                //var xx = configuration.GetSection("MediaPath").GetSection("Images");
                //List<int> gy = new List<int>();
                return Ok(new { StatusCode = 1, halls, total});
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "The Application failed to start.######333");
                return StatusCode(500, new { StatusCode = "EX01001", result = errorMsg });
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> add([FromBody] HallData hallData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(hallData.HallName))
                {
                    return BadRequest(new { StatusCode = "RE01002", result = "الرجاء التاكد من اسم الصالة " });
                }
                if (hallData.HallName.Length > 30)
                {
                    return BadRequest(new { StatusCode = "RE01003", result = "الرجاء التاكد من طول اسم الصالة " });
                }

                if (string.IsNullOrWhiteSpace(hallData.OwnerName))
                {
                    return BadRequest(new { StatusCode = "RE01004", result = "الرجاء التاكد من ادخال اسم مالك الصالة  " });
                }
                if (hallData.OwnerName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE01005", result = "الرجاء التاكد من طول اسم مالك الصالة " });
                }

                if (string.IsNullOrWhiteSpace(hallData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE01006", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (hallData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE01007", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(hallData.PhoneNo2))
                {
                    if (hallData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE01008", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }
                if (string.IsNullOrWhiteSpace(hallData.Email))
                {
                    return BadRequest(new { StatusCode = "RE01009", result = "الرجاء التاكد من ادخال بريد الكتروني للصالة " });
                }
                if (!Validation.IsValidEmail(hallData.Email))
                {
                    return BadRequest(new { StatusCode = "RE01010", result = "الرجاء التاكد من ادخال البريد الالكتروني للصالة بشكل صحيح " });
                }
                if (hallData.Email.Length > 50)
                {
                    return BadRequest(new { StatusCode = "RE01011", result = " الرجاء التاكد من طول البريد الالكتروني" });
                }
                if (hallData.HallBranch.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE01012", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(hallData.HallBranch.AddressDescription))
                {
                    if (hallData.HallBranch.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode = "RE01013", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(hallData.HallBranch.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE01014", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (hallData.HallBranch.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE01015", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(hallData.HallBranch.PhoneNo2))
                {
                    if (hallData.HallBranch.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE01016", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (hallData.HallBranch.Longitude > 180 || hallData.HallBranch.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE01017", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (hallData.HallBranch.Longitude > 90 || hallData.HallBranch.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE01018", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                if (hallData.HallBranch.PeapleCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01019", result = "الرجاء التاكد من ادخال عدد الاشخاص " });
                }
                if (hallData.HallBranch.CarsCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01020", result = "الرجاء التاكد من ادخال عدد السيارات " });
                }
                
                if (!Validation.IsValidTimeFormat(hallData.HallBranch.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE01021", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(hallData.HallBranch.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE01022", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }


                if (hallData.HallBranch.HallAppointments.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01023", result = "الرجاء التاكد من  ادخال مواعيد الحجز " });
                }
                foreach (var appontment in hallData.HallBranch.HallAppointments)
                {
                    if (appontment.WorkDayId <= 0 || appontment.WorkDayId > 7)
                    {
                        return BadRequest(new { StatusCode = "RE01024", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                    }
                    if (appontment.AppointmentData.Count <= 0)
                    {
                        return BadRequest(new { StatusCode = "RE01025", result = "الرجاء التاكد من ادخال بيانات موعد الحجز " });
                    }
                    foreach (var appointmentDetails in appontment.AppointmentData)
                    {
                        if (appointmentDetails.ReservationType <= 0)
                        {
                            return BadRequest(new { StatusCode = "RE01026", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                        }
                        if (!Validation.IsValidTimeFormat(appointmentDetails.TimeRange[0]))
                        {
                            return BadRequest(new { StatusCode = "RE01027", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                        }
                        if (Convert.ToDateTime(appointmentDetails.TimeRange[0]) < Convert.ToDateTime(appointmentDetails.TimeRange[0])
                            || Convert.ToDateTime(hallData.HallBranch.TimeRange[0]) > Convert.ToDateTime(hallData.HallBranch.TimeRange[1]))
                        {
                            return BadRequest(new { StatusCode = "RE01028", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                        }
                        if (!Validation.IsValidTimeFormat(appointmentDetails.TimeRange[1]))
                        {
                            return BadRequest(new { StatusCode = "RE01029", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                        }
                        if (Convert.ToDateTime(appointmentDetails.TimeRange[1]) > Convert.ToDateTime(appointmentDetails.TimeRange[1])
                            || Convert.ToDateTime(hallData.HallBranch.TimeRange[1]) < Convert.ToDateTime(hallData.HallBranch.TimeRange[0]))
                        {
                            return BadRequest(new { StatusCode = "RE01030"  , result = "الرجاء التاكد من ادخال وقت نهاية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                        }
                        if (appointmentDetails.Price <= 0)
                        {
                            return BadRequest(new { StatusCode = "RE01031", result = "الرجاء التاكد من ادخال سعر موعد الحجز " });
                        }
                    }
                }

                foreach (var service in hallData.HallBranch.HallServices)
                {
                    if (string.IsNullOrWhiteSpace(service.Description))
                    {
                        return BadRequest(new { StatusCode = "RE01032", result = "الرجاء التاكد من ادخال وصف الخدمة " });
                    }

                    if (service.Price <= 0)
                    {
                        return BadRequest(new { StatusCode = "RE01033", result = "الرجاء التاكد من ادخال سعر الخدمة " });
                    }
                }

                if (string.IsNullOrWhiteSpace(hallData.FarhiUser.UserName))
                {
                    return BadRequest(new { StatusCode = "RE01034", result = "الرجاء التاكد من ادخال اسم المستخدم " });
                }
                if (hallData.FarhiUser.UserName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE01035", result = "الرجاء التاكد من ادخال اسم مستحدم لا يتجاوز عن 40 خانة " });
                }
                if (string.IsNullOrWhiteSpace(hallData.FarhiUser.LoginName))
                {
                    return BadRequest(new { StatusCode = "RE01036", result = "الرجاء التاكد من ادخال اسم الدخول " });
                }
                if (hallData.FarhiUser.LoginName.Length > 40)
                {
                    return BadRequest(new { StatusCode = 901029, result = "الرجاء التاكد من ادخال اسم دخول لايتجاوز عن 40 خانة " });
                }
                //==========================Password=============================//
                if (string.IsNullOrWhiteSpace(hallData.FarhiUser.Password))
                {
                    return BadRequest(new { StatusCode = "RE01037", result = "الرجاء التاكد من ادخال كلمة مرور  " });
                }
                if (hallData.FarhiUser.Password.Length > 200)
                {
                    return BadRequest(new { StatusCode = "RE01038", result = "الرجاء التاكد من ادخال كلمة مرور لا تتجاوز عن 200 خانة " });
                }
                if (!Validation.IsValidPassword(hallData.FarhiUser.Password))
                {
                    return BadRequest(new { StatusCode = "RE01039", result = "الرجاء التاكد من ادخال كلمة مرور معقدة " });
                }
                //==========================Password=============================//

                //==========================Logo=============================//

                if (string.IsNullOrWhiteSpace(hallData.Logo))
                {
                    return BadRequest(new { StatusCode = "RE01040", result = "الرجاء التاكد من ادخال شعار الصالة " });
                }
                //==========================Logo=============================//



                if (string.IsNullOrWhiteSpace(hallData.FarhiUser.UserPhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE01041", result = "الرجاء التاكد الرجاء التاكد من ادخال رقم هاتف 1 للمستخدم " });
                }
                if (hallData.FarhiUser.UserPhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE01042", result = "الرجاء التاكد من ادخال رقم هاتف 1 للمستخدم لا يتجاوز 10 خانات " });
                }

                if (!string.IsNullOrWhiteSpace(hallData.FarhiUser.UserPhoneNo2))
                {
                    if (hallData.FarhiUser.UserPhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE01043", result = "الرجاء التاكد من ادخال رقم هاتف 2 للمستخدم " });
                    }
                }

                if (string.IsNullOrWhiteSpace(hallData.FarhiUser.UserEmail))
                {
                    return BadRequest(new { StatusCode = "RE01044", result = "الرجاء التاكد من ادخال بريد الكتروني للمستخدم " });
                }
                if (!Validation.IsValidEmail(hallData.FarhiUser.UserEmail))
                {
                    return BadRequest(new { StatusCode = "RE01045", result = "الرجاء التاكد من من ادخال بريد الكتروني للمستخدم للمستخدم بشكل صحيح " });
                }

                var checkHallName = await (from h in db.Halls
                                           where h.HallName == hallData.HallName
                                           && h.Status != Status.Deleted
                                           select h).CountAsync();
                if (checkHallName > 0)
                {
                    return BadRequest(new { StatusCode = "RE01046", result = "اسم الصالة المدخل مكرر " });
                }

                string attachmentFirstName = hallData.HallName + "_Logo";
                string attachmentName = "";
                byte[] file = Convert.FromBase64String(hallData.Logo.Substring(hallData.Logo.IndexOf(",") + 1));

                attachmentName = attachmentFirstName + attachmentName + ".jpg";
                bool check = SaveAttachment(attachmentName, hallData.Logo);
                if (!check)
                    return BadRequest(new { StatusCode = "RE01047", result = "يوجد خطأ في الصورة المدخلة " });


                var checkLoginName = await (from u in db.Farhiusers
                                            where u.LoginName == hallData.FarhiUser.LoginName
                                            && u.Status != Status.Deleted
                                            select u).CountAsync();

                if (checkLoginName > 0)
                {
                    return BadRequest(new { StatusCode = "RE01048", result = "اسم دخول المستخدم المدخل مكرر " });
                }

                var checkHallEmail = await (from h in db.Halls
                                            where h.Email == hallData.Email
                                            && h.Status != Status.Deleted
                                            select h).CountAsync();
                if (checkHallEmail > 0)
                {
                    return BadRequest(new { StatusCode = "RE01049", result = "البريد الالكتروني للصالة المدخل مكرر " });
                }

                var checkUserEmail = await (from u in db.Farhiusers
                                            where u.Email == hallData.FarhiUser.UserEmail
                                            && u.Status != Status.Deleted
                                            select u).CountAsync();

                if (checkUserEmail > 0)
                {
                    return BadRequest(new { StatusCode = "RE01050", result = "البريد الالكتروني للمستخدم المدخل مكرر " });
                }

                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        Hall newHall = new Hall()
                        {
                            HallName = hallData.HallName,
                            OwnerName = hallData.OwnerName,
                            PhoneNo1 = hallData.PhoneNo1,
                            PhoneNo2 = hallData.PhoneNo2,
                            Email = hallData.Email,
                            Status = Status.Active,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Logo = attachmentName,
                        };
                        await db.Halls.AddAsync(newHall);
                        await db.SaveChangesAsync();
                        Hallbranch newHallBranch = new Hallbranch()
                        {
                            AddressId = hallData.HallBranch.AddressId,
                            AddressDescription = hallData.HallBranch.AddressDescription,
                            CarsCapacity = hallData.HallBranch.CarsCapacity,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Longitude = hallData.HallBranch.Longitude,
                            Latitude = hallData.HallBranch.Latitude,
                            HallId = newHall.HallId,
                            PeapleCapacity = hallData.HallBranch.PeapleCapacity,
                            PhoneNo1 = hallData.HallBranch.PhoneNo1,
                            PhoneNo2 = hallData.HallBranch.PhoneNo2,
                            StartTime = TimeOnly.Parse(hallData.HallBranch.TimeRange[0]),
                            EndTime = TimeOnly.Parse(hallData.HallBranch.TimeRange[1]),
                            Status = Status.Active,
                        };
                        await db.Hallbranches.AddAsync(newHallBranch);

                        foreach (var appontment in hallData.HallBranch.HallAppointments)
                        {
                            foreach (var details in appontment.AppointmentData)
                            {
                                Hallappointment addAppointment = new Hallappointment()
                                {
                                    WorkDayId = appontment.WorkDayId,
                                    HallBranchId = newHallBranch.HallBranchId,
                                    ReservationTypeId = details.ReservationType,
                                    StartTime = TimeOnly.Parse(details.TimeRange[0]),
                                    EndTime = TimeOnly.Parse(details.TimeRange[1]),
                                    Price = details.Price,
                                    Status = Status.Active,
                                    CreatedBy = UserId(),
                                    CreatedOn = DateTime.Now
                                };
                                newHallBranch.Hallappointments.Add(addAppointment);
                            }

                        }
                        foreach (var service in hallData.HallBranch.HallServices)
                        {
                            Hallbranchservice addService = new Hallbranchservice()
                            {
                                HallBranchId = newHallBranch.HallBranchId,
                                Description = service.Description,
                                Price = service.Price,
                                Status = Status.Active,
                                CreatedBy = UserId(),
                                CreatedOn = DateTime.Now
                            };
                            newHallBranch.Hallbranchservices.Add(addService);
                        }
                        var hashing = Security.HashPassword(hallData.FarhiUser.Password, configuration["Secret"]);
                        Farhiuser newFarhiUser = new Farhiuser()
                        {
                            UserName = hallData.FarhiUser.UserName,
                            LoginName = hallData.FarhiUser.LoginName,
                            Password = hashing.Password,
                            Salt = hashing.Salt,
                            Email = hallData.FarhiUser.UserEmail,
                            PhoneNo1 = hallData.FarhiUser.UserPhoneNo1,
                            PhoneNo2 = hallData.FarhiUser.UserPhoneNo2,
                            LoginStatus = 0,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Status = Status.Active
                        };

                        Farhiusershallsstore newhalluser = new Farhiusershallsstore() { 
                            FarhiUserId = newFarhiUser.FarhiUserId,
                            HallId = newHall.HallId
                        };

                        //if (hallData.HallPermission)
                        //{
                        //    newFarhiUser.HalId = newHall.HallId;
                        //}
                        await db.Farhiusers.AddAsync(newFarhiUser);
                        newFarhiUser.Farhiusershallsstores.Add(newhalluser);

                        await db.SaveChangesAsync();
                        newHall.MainBranchId = newHallBranch.HallBranchId;
                        newhalluser.Farhiusershallbranches.Add(new Farhiusershallbranch() { BranchId = newHall.MainBranchId.Value });

                        //if (!hallData.HallPermission)
                        //{
                        //    Farhiuserhallbranches newHallBranchUser = new Farhiuserhallbranches()
                        //    {
                        //        FarhiUserId = newFarhiUser.FarhiUserId,
                        //        HallBranchId = newHallBranch.HallBranchId,
                        //        CreatedBy = UserId(),
                        //        CreatedOn = DateTime.Now,
                        //        Status = Status.Active
                        //    };
                        //    await db.Farhiuserhallbranches.AddAsync(newHallBranchUser);
                        //}
                        await db.SaveChangesAsync();

                        await trans.CommitAsync();

                        var hall = await (from h in db.Halls
                                          where h.HallId == newHall.HallId
                                          select new
                                          {
                                              h.HallId,
                                              h.HallName,
                                              h.OwnerName,
                                              h.PhoneNo1,
                                              h.CreatedByNavigation.AdminName,
                                              CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                              h.Status
                                          }).SingleOrDefaultAsync();
                        return Ok(new { StatusCode = 1,  message = "تم تسجيل الصالة بنجاح", hall } );

                    }
                    catch (Exception ex)
                    {
                        await trans.RollbackAsync();
                        return StatusCode(500, new { StatusCode = "EX01002", result = errorMsg });
                    }
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01003", result = errorMsg });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getDetails(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01051", result = "الرجاء التاكد اختيار الصالة " });
                }

                var hallData = await (from h in db.Halls
                                      where h.Status != Status.Deleted
                                      && h.HallId == id
                                      select new
                                      {
                                          h.HallName,
                                          h.OwnerName,
                                          h.PhoneNo1,
                                          h.PhoneNo2,
                                          h.Email,
                                          h.Logo,
                                          hallBranch = h.Hallbranches
                                                     .Where(x => x.HallBranchId == h.MainBranchId)
                                                     .Select(x => new
                                                     {
                                                         x.Address.AddressName,
                                                         x.AddressDescription,
                                                         x.PhoneNo1,
                                                         x.PhoneNo2,
                                                         x.Longitude,
                                                         x.Latitude,
                                                         x.PeapleCapacity,
                                                         x.CarsCapacity,
                                                         //WorkDayFrom = Constant.SelectDay(x.WorkDayFrom.Value),
                                                         //WorkDayTo = Constant.SelectDay(x.WorkDayTo.Value),
                                                         StartTime = x.StartTime.ToString(@"hh\:mm\:ss"),
                                                         EndTime = x.EndTime.ToString(@"hh\:mm\:ss"),
                                                     }).SingleOrDefault(),
                                          farhiUser = 
                                          new
                                         {
                                              h.Farhiusershallsstores.FirstOrDefault().FarhiUser.UserName,
                                              h.Farhiusershallsstores.FirstOrDefault().FarhiUser.LoginName
                                          },

                                      }).SingleOrDefaultAsync();

                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE01052", result = "لا توجد بيانات لهذه الصالة " });
                }

                if (hallData.farhiUser == null)
                {
                    return BadRequest(new { StatusCode = "RE01053", result = "لا توجد بيانات مستخدم رئيسي لهذه الصالة ... يرجي مراجعة الخطأ " });
                }
                string image = "";
                try {
                    image = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + hallData.Logo));
                }
                catch {
                    return Ok(new { StatusCode = 1, hallData ,image});
                }

                return Ok(new { StatusCode = 1, hallData ,image});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01004", result = errorMsg });
            }
        }


        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01054", result = "الرجاء التاكد اختيار الصالة " });
                }

                var hallData = await (from h in db.Halls
                                      where h.Status != Status.Deleted
                                      && h.HallId == id
                                      select new
                                      {
                                          h.HallName,
                                          h.OwnerName,
                                          h.PhoneNo1,
                                          h.PhoneNo2,
                                          h.Email,
                                          h.Logo,
                                          hallBranch = h.Hallbranches
                                         .Where(x => x.HallBranchId == h.MainBranchId)
                                         .Select(x => new
                                         {
                                             x.Address.AddressId,
                                             x.AddressDescription,
                                             x.PhoneNo1,
                                             x.PhoneNo2,
                                             x.Longitude,
                                             x.Latitude,
                                             x.PeapleCapacity,
                                             x.CarsCapacity,
                                             TimeRange = new[] { x.StartTime.ToString(@"hh\:mm\:ss"), x.EndTime.ToString(@"hh\:mm\:ss") },
                                         }).SingleOrDefault(),
                                      }).SingleOrDefaultAsync();

                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE01055", result = "لا توجد بيانات لهذه الصالة " });
                }
                string image = "";
                try {
                    image = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + hallData.Logo));
                }
                catch {
                    return Ok(new { StatusCode = 1, hallData ,image});
                }
                return Ok(new { StatusCode = 1, hallData,image } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01005", result = errorMsg });
            }
        }

        [HttpPut]
        public async Task<IActionResult> edit([FromBody] HallData hallData)
        {
            try
            {
                if (hallData.HallId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01056", result = "الرجاء التاكد اختيار الصالة " });
                }
                if (string.IsNullOrWhiteSpace(hallData.HallName))
                {
                    return BadRequest(new { StatusCode = "RE01057", result = "الرجاء التاكد من اسم الصالة " });
                }
                if (hallData.HallName.Length > 30)
                {
                    return BadRequest(new { StatusCode = "RE01058", result = "الرجاء التاكد من طول اسم الصالة " });
                }

                if (string.IsNullOrWhiteSpace(hallData.OwnerName))
                {
                    return BadRequest(new { StatusCode = "RE01059", result = "الرجاء التاكد من ادخال اسم مالك الصالة  " });
                }
                if (hallData.OwnerName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE01060", result = "الرجاء التاكد من طول اسم مالك الصالة " });
                }

                if (string.IsNullOrWhiteSpace(hallData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode =  "RE01061", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (hallData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode =  "RE01062", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(hallData.PhoneNo2))
                {
                    if (hallData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode =  "RE01063", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }
                if (string.IsNullOrWhiteSpace(hallData.Email))
                {
                    return BadRequest(new { StatusCode =  "RE01064", result = "الرجاء التاكد من ادخال بريد الكتروني للصالة " });
                }
                if (!Validation.IsValidEmail(hallData.Email))
                {
                    return BadRequest(new { StatusCode =  "RE01065", result = "الرجاء التاكد من ادخال البريد الالكتروني للصالة بشكل صحيح " });
                }
                if (hallData.Email.Length > 50)
                {
                    return BadRequest(new { StatusCode =  "RE01066", result = " الرجاء التاكد من طول البريد الالكتروني" });
                }
                if (hallData.HallBranch.AddressId < 0)
                {
                    return BadRequest(new { StatusCode =  "RE01067", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(hallData.HallBranch.AddressDescription))
                {
                    if (hallData.HallBranch.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode =  "RE01068", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(hallData.HallBranch.PhoneNo1))
                {
                    return BadRequest(new { StatusCode =  "RE01069", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (hallData.HallBranch.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode =  "RE01070", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(hallData.HallBranch.PhoneNo2))
                {
                    if (hallData.HallBranch.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE01071", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (hallData.HallBranch.Longitude > 180 || hallData.HallBranch.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE01072", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (hallData.HallBranch.Longitude > 90 || hallData.HallBranch.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE01073", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                if (hallData.HallBranch.PeapleCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01074", result = "الرجاء التاكد من ادخال عدد الاشخاص " });
                }
                if (hallData.HallBranch.CarsCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01075", result = "الرجاء التاكد من ادخال عدد السيارات " });
                }
                



                if (!Validation.IsValidTimeFormat(hallData.HallBranch.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE01076", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(hallData.HallBranch.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE01077", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }


                
                //==========================Password=============================//

                //==========================Logo=============================//

                if (string.IsNullOrWhiteSpace(hallData.Logo))
                {
                    return BadRequest(new { StatusCode = "RE01078", result = "الرجاء التاكد من ادخال شعار الصالة " });
                }
                //==========================Logo=============================//

                var editHallData = await (from h in db.Halls
                                         where h.HallId == hallData.HallId.Value
                                         && h.Status != Status.Deleted
                                         select h).SingleOrDefaultAsync();

                var editHallBrach = await (from h in db.Hallbranches
                                           where h.Status != Status.Deleted
                                           && h.HallBranchId == editHallData.MainBranchId
                                           select h).SingleOrDefaultAsync();
                if (editHallData == null)
                {
                    return BadRequest(new { StatusCode = "RE01079", result = "هذه الصالة غير مسجلة في المنظمومة " });
                }
                if (editHallBrach == null)
                {
                    return BadRequest(new { StatusCode = "RE01080", result = "هذه الصالة غير مسجلة كفرع و لا يوجد بها اي فروع اخري " });
                }

                var checkHallName = await (from h in db.Halls
                                           where h.HallName == hallData.HallName
                                           && h.Status != Status.Deleted
                                           && h.HallId != hallData.HallId.Value
                                           select h).CountAsync();
                if (checkHallName > 0)
                {
                    return BadRequest(new { StatusCode = "RE01081", result = "اسم الصالة المدخل مكرر " });
                }


                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", editHallData.Logo);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

                string attachmentFirstName = hallData.HallName + "_Logo";
                string attachmentName = "";
                //byte[] file = Convert.FromBase64String(hallData.Logo.Substring(hallData.Logo.IndexOf(",") + 1));

                attachmentName = attachmentFirstName + attachmentName + ".jpg";
                //bool check = SaveAttachment(attachmentName, hallData.Logo);
                //if (!check)
                //{
                //    return BadRequest(new { StatusCode = 901076, result = "يوجد خطأ في الصورة المدخلة " });
                //}



                var checkHallEmail = await (from h in db.Halls
                                            where h.Email == hallData.Email
                                            && h.Status != Status.Deleted
                                            && h.HallId != hallData.HallId
                                            select h).CountAsync();
                if (checkHallEmail > 0)
                {
                    return BadRequest(new { StatusCode = "RE01082", result = "البريد الالكتروني للصالة المدخل مكرر " });
                }

                

                editHallData.HallName = hallData.HallName;
                editHallData.OwnerName = hallData.OwnerName;
                editHallData.PhoneNo1 = hallData.PhoneNo1;
                editHallData.PhoneNo2 = hallData.PhoneNo2;
                editHallData.Email = hallData.Email;
                editHallData.ModifiedBy = UserId();
                editHallData.ModifiedOn = DateTime.Now;
                editHallData.Logo = attachmentName;

                editHallBrach.AddressId = hallData.HallBranch.AddressId;
                editHallBrach.AddressDescription = hallData.HallBranch.AddressDescription;
                editHallBrach.CarsCapacity = hallData.HallBranch.CarsCapacity;
                editHallBrach.ModifiedBy = UserId();
                editHallBrach.ModifiedOn = DateTime.Now;
                editHallBrach.Longitude = hallData.HallBranch.Longitude;
                editHallBrach.Latitude = hallData.HallBranch.Latitude;
                editHallBrach.PeapleCapacity = hallData.HallBranch.PeapleCapacity;
                editHallBrach.PhoneNo1 = hallData.HallBranch.PhoneNo1;
                editHallBrach.PhoneNo2 = hallData.HallBranch.PhoneNo2;
                //editHallBrach.WorkDayFrom = hallData.HallBranch.WorkDayFrom;
                //editHallBrach.WorkDayTo = hallData.HallBranch.WorkDayTo;
                editHallBrach.StartTime = TimeOnly.Parse(hallData.HallBranch.TimeRange[0]);
                editHallBrach.EndTime = TimeOnly.Parse(hallData.HallBranch.TimeRange[1]);

                await db.SaveChangesAsync();

                var hall = await (from h in db.Halls
                                    where h.HallId == hallData.HallId
                                   select new
                                   {
                                       h.HallId,
                                       h.HallName,
                                       h.OwnerName,
                                       h.PhoneNo1,
                                       h.CreatedByNavigation.AdminName,
                                       CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, hall, message = "تم تعديل الصالة بنجاح" } );

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01006", result = errorMsg });
            }
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> locking(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01083", result = "الرجاء التاكد اختيار الصالة " });
                }

                var hallData = await (from h in db.Halls
                                      where h.HallId == id
                                      && h.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE01084", result = "لا توجد بيانات لهذه الصالة" });
                }

                if (hallData.Status == Status.Locked)
                {
                    return BadRequest(new { StatusCode = "RE01085", result = "حالة هذه الصالة مجمدة مسبقا" });
                }
                
                var checkHallSubscription = await (from h in db.Hallbranches
                                                   where
                                                   h.HallId == id &&
                                                   h.Subscriptions
                                                   .Where(x => x.Status == Status.Active
                                                   || x.Status == Status.Locked)
                                                   .Count() > 0
                                                   select h).CountAsync();
                if (checkHallSubscription > 0)
                {
                    return BadRequest(new { StatusCode = "RE01086", result = "لا يمكن تجميد الصالة و يوجد فروع لها تحت الاشتراك" });
                }

                var hallBranchData = await (from h in db.Hallbranches
                                            where h.Status != Status.Deleted
                                            && h.HallId == id
                                            select h).ToListAsync();
                hallData.Status = Status.Locked;
                hallData.ModifiedBy = UserId();
                hallData.ModifiedOn = DateTime.Now;
                foreach (var branch in hallBranchData)
                {
                    branch.Status = Status.Locked;
                    branch.ModifiedBy = UserId();
                    branch.ModifiedOn = DateTime.Now;
                }
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم تجميد الصالة و جميع فروعها بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01007", result = errorMsg });
            }
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> unlock(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01087", result = "الرجاء التاكد اختيار الصالة " });
                }

                var hallData = await (from h in db.Halls
                                      where h.HallId == id
                                      && h.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE01088", result = "لا توجد بيانات لهذه الصالة" });
                }

                if (hallData.Status == Status.Active)
                {
                    return BadRequest(new { StatusCode = "RE01089", result = "حالة هذه الصالة مفعلة مسبقا" });
                }
                

                var hallBranchData = await (from h in db.Hallbranches
                                            where h.Status != Status.Deleted
                                            && h.HallBranchId == hallData.MainBranchId
                                            select h).SingleOrDefaultAsync();
                
                hallData.Status = Status.Active;
                hallData.ModifiedBy = UserId();
                hallData.ModifiedOn = DateTime.Now;

                hallBranchData.Status = Status.Active;
                hallBranchData.ModifiedBy = UserId();
                hallBranchData.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم تفعيل الصالة و فرعها الرئيسي بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01008", result = errorMsg });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> delete(int Id)
        {
            try
            {
                if (Id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE01090", result = "الرجاء التاكد اختيار الصالة " });
                }

                var hallData = await (from h in db.Halls
                                      where h.HallId == Id
                                      select h).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE01091", result = "لا توجد بيانات لهذه الصالة" });
                }

                if (hallData.Status == Status.Deleted)
                {
                    return BadRequest(new { StatusCode = "RE01092", result = "حالة هذه الصالة محذوف مسبقا" });
                }

                var checkHallSubscription = await (from h in db.Hallbranches
                                                   where
                                                   h.HallId == Id &&
                                                   h.Subscriptions
                                                   .Where(x => x.Status == Status.Active
                                                   || x.Status == Status.Locked)
                                                   .Count() > 0
                                                   select h).CountAsync();
                if (checkHallSubscription > 0)
                {
                    return BadRequest(new { StatusCode = "RE01093", result = "لا يمكن حذف الصالة و يوجد فروع لها تحت الاشتراك" });
                }

                hallData.Status = Status.Deleted;
                hallData.ModifiedBy = UserId();
                hallData.ModifiedOn = DateTime.Now;

                var hallBranchData = await (from h in db.Hallbranches
                                            where h.Status != Status.Deleted
                                            && h.HallId == Id
                                            select h).ToListAsync();
                hallData.Status = Status.Deleted;
                hallData.ModifiedBy = UserId();
                hallData.ModifiedOn = DateTime.Now;
                foreach (var branch in hallBranchData)
                {
                    branch.Status = Status.Deleted;
                    branch.ModifiedBy = UserId();
                    branch.ModifiedOn = DateTime.Now;
                }

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم حذف الصالة و جميع فروعها بنجاح"  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX01009", result = errorMsg });
            }
        }

    }
}
