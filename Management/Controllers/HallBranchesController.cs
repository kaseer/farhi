﻿using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HallBranchesController : RootController
    {
        private IWebHostEnvironment _env;
        public HallBranchesController(farhiContext context, IWebHostEnvironment env) : base(context)
        {
            _env = env;
        }

        private string imagePath = "/img/";

        List<string> attachmentNames = new List<string>();

        public bool SaveAttachment(string attachmentName, string AttachmentFile)
        {
            try
            {
                byte[] file = Convert.FromBase64String(AttachmentFile.Substring(AttachmentFile.IndexOf(",") + 1));

                var path = Path.Combine(_env.WebRootPath + imagePath, attachmentName);
                FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
                fileStream.Write(file, 0, file.Length);
                fileStream.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpGet]
        public async Task<IActionResult> getAll(int pageNo, int pageSize, int? hallId, string searchByAddress)
        {
            try
            {
                
                if (hallId <= 0)
                {
                    return BadRequest(new { StatusCode ="RE02001"   , result = "الرجاء التاكد من اختيار الصالة " });
                }
                if (pageNo <= 0 || pageSize <= 0)
                {
                    return BadRequest(new { StatusCode =  "RE02002", result = "الرجاء التاكد اختيار رقم الصفحة او حجم الصفحة " });
                }

                var hallBranchesQuery = from h in db.Hallbranches
                                 where h.Status != Status.Deleted
                                 && h.HallId == hallId
                                 select h;

                if (!string.IsNullOrWhiteSpace(searchByAddress))
                {
                    hallBranchesQuery = from h in hallBranchesQuery
                                        where h.Address.AddressName.Contains(searchByAddress)
                                        select h;
                }
                var hallBranches = await (from h in hallBranchesQuery
                                          orderby h.HallBranchId
                                   select new
                                   {
                                       h.HallBranchId,
                                       AddressName = h.Address.AddressName + " " + h.AddressDescription,
                                       h.PhoneNo1,
                                       h.Hall.MainBranchId,
                                       //workDays = Constant.SelectDay(h.WorkDayFrom.Value) + " -  " + Constant.SelectDay(h.WorkDayTo.Value),
                                       workTimes = h.StartTime.ToString(@"hh\:mm\:ss") + " -  " + h.EndTime.ToString(@"hh\:mm\:ss"),
                                       h.CreatedByNavigation.AdminName,
                                       CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToListAsync();
                var total = await hallBranchesQuery.CountAsync();
                return Ok(new { StatusCode = 1, hallBranches, total  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02001", result = errorMsg });
            }
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] HallBranchData hallBranchData)
        {
            try
            {
                if(hallBranchData.HallId == null)
                {
                    return BadRequest(new { StatusCode = "RE02003", result = "الرجاء التاكد من اختيار الصالة " });
                }
                if (hallBranchData.HallId.Value <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02004", result = "الرجاء التاكد من اختيار الصالة " });
                }
                if (string.IsNullOrWhiteSpace(hallBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE02005", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (hallBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE02006", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(hallBranchData.PhoneNo2))
                {
                    if (hallBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE02007", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }
                
                
                if (hallBranchData.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE02008", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(hallBranchData.AddressDescription))
                {
                    if (hallBranchData.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode =  "RE02009", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(hallBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE02010", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (hallBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE02011", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(hallBranchData.PhoneNo2))
                {
                    if (hallBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE02012", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (hallBranchData.Longitude > 180 || hallBranchData.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE02013", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (hallBranchData.Longitude > 90 || hallBranchData.Latitude < -90)
                {
                    return BadRequest(new { StatusCode =  "RE02014", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                if (hallBranchData.PeapleCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02015", result = "الرجاء التاكد من ادخال عدد الاشخاص " });
                }
                if (hallBranchData.CarsCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02016", result = "الرجاء التاكد من ادخال عدد السيارات " });
                }



                if (!Validation.IsValidTimeFormat(hallBranchData.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE02017", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(hallBranchData.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE02018", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }
                if(hallBranchData.HallAppointments.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02019", result = "الرجاء التاكد من  ادخال مواعيد الحجز " });
                }
                foreach (var appontment in hallBranchData.HallAppointments)
                {
                    if (appontment.WorkDayId <= 0 || appontment.WorkDayId > 7)
                    {
                        return BadRequest(new { StatusCode = "RE02020", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                    }
                    if (appontment.AppointmentData.Count <= 0)
                    {
                        return BadRequest(new { StatusCode = "RE02021", result = "الرجاء التاكد من ادخال بيانات موعد الحجز " });
                    }
                    foreach (var appointmentDetails in appontment.AppointmentData)
                    {
                        if (appointmentDetails.ReservationType <= 0)
                        {
                            return BadRequest(new { StatusCode ="RE02022", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                        }
                        if (!Validation.IsValidTimeFormat(appointmentDetails.TimeRange[0]))
                        {
                            return BadRequest(new { StatusCode = "RE02023", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                        }
                        if (Convert.ToDateTime(appointmentDetails.TimeRange[0]) < Convert.ToDateTime(appointmentDetails.TimeRange[0])
                            || Convert.ToDateTime(hallBranchData.TimeRange[0]) > Convert.ToDateTime(hallBranchData.TimeRange[1]))
                        {
                            return BadRequest(new { StatusCode = "RE02024", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                        }
                        if (!Validation.IsValidTimeFormat(appointmentDetails.TimeRange[1]))
                        {
                            return BadRequest(new { StatusCode = "RE02025", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                        }
                        if (Convert.ToDateTime(appointmentDetails.TimeRange[1]) > Convert.ToDateTime(appointmentDetails.TimeRange[1])
                            || Convert.ToDateTime(hallBranchData.TimeRange[1]) < Convert.ToDateTime(hallBranchData.TimeRange[0]))
                        {
                            return BadRequest(new { StatusCode = "RE02026", result = "الرجاء التاكد من ادخال وقت نهاية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                        }
                        if (appointmentDetails.Price <= 0)
                        {
                            return BadRequest(new { StatusCode = "RE02027", result = "الرجاء التاكد من ادخال سعر موعد الحجز " });
                        }
                    }
                }

                foreach (var service in hallBranchData.HallServices)
                {
                    if (string.IsNullOrWhiteSpace(service.Description))
                    {
                        return BadRequest(new { StatusCode = "RE02028", result = "الرجاء التاكد من ادخال وصف الخدمة " });
                    }
                    
                    if (service.Price <= 0)
                    {
                        return BadRequest(new { StatusCode = "RE02029", result = "الرجاء التاكد من ادخال سعر الخدمة " });
                    }
                }

                var checkBranchAddress = await (from h in db.Hallbranches
                                           where h.AddressId == hallBranchData.AddressId
                                           && h.Status != Status.Deleted
                                           && h.HallId == hallBranchData.HallId.Value
                                           select h).CountAsync();
                if (checkBranchAddress > 0)
                {
                    if (string.IsNullOrWhiteSpace(hallBranchData.AddressDescription))
                    {
                        return BadRequest(new { StatusCode = "RE02030", result = "يوجد فرع لهذه الصالة في نفس هذا العنوان ... يرجي كتابة تفصيل العنوان للتفرقة " });
                    }
                }

                Hallbranch addHallBranch = new Hallbranch() { 
                    AddressDescription = hallBranchData.AddressDescription,
                    AddressId = hallBranchData.AddressId,
                    CarsCapacity = hallBranchData.CarsCapacity,
                    HallId = hallBranchData.HallId.Value,
                    Latitude = hallBranchData.Latitude,
                    Longitude = hallBranchData.Longitude,
                    PeapleCapacity = hallBranchData.PeapleCapacity,
                    PhoneNo1 = hallBranchData.PhoneNo1,
                    PhoneNo2 = hallBranchData.PhoneNo2,
                    StartTime = TimeOnly.Parse(hallBranchData.TimeRange[0]),
                    EndTime = TimeOnly.Parse(hallBranchData.TimeRange[1]),
                    //WorkDayFrom = hallBranchData.WorkDayFrom,
                    //WorkDayTo = hallBranchData.WorkDayTo,
                    Status = Status.Entry,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now
                };

                await db.Hallbranches.AddAsync(addHallBranch);

                foreach(var appontment in hallBranchData.HallAppointments)
                {
                    foreach(var details in appontment.AppointmentData)
                    {
                        Hallappointment addAppointment = new Hallappointment()
                        {
                            WorkDayId = appontment.WorkDayId,
                            HallBranchId = addHallBranch.HallBranchId,
                            ReservationTypeId = details.ReservationType,
                            StartTime = TimeOnly.Parse(details.TimeRange[0]),
                            EndTime = TimeOnly.Parse(details.TimeRange[1]),
                            Price = details.Price,
                            Status = Status.Active,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now
                        };
                        addHallBranch.Hallappointments.Add(addAppointment);
                    }
                    
                }
                foreach (var service in hallBranchData.HallServices)
                {
                    Hallbranchservice addService = new Hallbranchservice()
                    {
                        HallBranchId = addHallBranch.HallBranchId,
                        Description = service.Description,
                        Price = service.Price,
                        Status = Status.Active,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now
                    };
                    addHallBranch.Hallbranchservices.Add(addService);
                }

                await db.SaveChangesAsync();
                var addedBranch = await (from h in db.Hallbranches
                                         where h.HallBranchId == addHallBranch.HallBranchId
                                         select new
                                         {
                                             h.HallBranchId,
                                             AddressName = h.Address.AddressName + " " + h.AddressDescription,
                                             h.PhoneNo1,
                                             //workDays = Constant.SelectDay(h.WorkDayFrom.Value) + " -  " + Constant.SelectDay(h.WorkDayTo.Value),
                                             workTimes = h.StartTime.ToString(@"hh\:mm\:ss") + " -  " + h.EndTime.ToString(@"hh\:mm\:ss"),
                                             h.CreatedByNavigation.AdminName,
                                             CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                             h.Status
                                         }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1,  message = "تم اضافة فرع لهذه الصالة بنجاح", addedBranch } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02002", result = errorMsg });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> viewBranchHall(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02031", result = "الرجاء التاكد من اختيار فرع الصالة " });
                }

                var hallBranchData = await (from h in db.Hallbranches
                                      where h.Status != Status.Deleted
                                      && h.HallBranchId == id
                                      select new
                                      {
                                          h.Address.AddressName,
                                          h.AddressDescription,
                                          h.PhoneNo1,
                                          h.PhoneNo2,
                                          h.Longitude,
                                          h.Latitude,
                                          h.PeapleCapacity,
                                          h.CarsCapacity,
                                          //WorkDayFrom = Constant.SelectDay(h.WorkDayFrom.Value),
                                          //WorkDayTo = Constant.SelectDay(h.WorkDayTo.Value),
                                          StartTime = h.StartTime.ToString(@"hh\:mm\:ss"),
                                          EndTime = h.EndTime.ToString(@"hh\:mm\:ss"),
                                          HallAppointments = h.Hallappointments
                                            .Where(x=>x.Status != Status.Deleted)
                                            .Select(x=> new {
                                                WorkDay = Constant.SelectDay(x.WorkDayId),
                                                x.ReservationType.Description,
                                                StartTime = x.StartTime.ToString(@"hh\:mm\:ss"),
                                                EndTime = x.EndTime.ToString(@"hh\:mm\:ss"),
                                                x.Price,
                                            }).ToList(),
                                          HallServices = h.Hallbranchservices
                                            .Where(x => x.Status != Status.Deleted)
                                            .Select(x => new {
                                                x.Description,
                                                x.Price,
                                            }).ToList(),
                                          Subscriptions = 
                                          h.Subscriptions.Select(x=>  
                                                x.Subscriptiondetails.Select(c=>new { 
                                                    c.SubscriptionPriceDetails.SubscriptionPrice.Description,
                                                    StartDate = c.StartDate.ToString("yyyy/MM/dd"),
                                                    EndDate = c.EndDate.ToString("yyyy/MM/dd"),
                                                    c.Price
                                                }).ToList()
                                          ).ToList()
                                          //(from s in db.Subscriptiondetails
                                          // where s.Subscription.HallBranchId == hallBranchId
                                          // && s.Status != Status.Deleted && s.Subscription.Status != Status.Deleted
                                          // select new
                                          // {
                                          //     //s.SubscriptionPriceDetails.SubscriptionPrices.Description,
                                          //     //s.StartDate,
                                          //     //s.EndDate,
                                          //     //s.Price,
                                          //     s.CreatedByNavigation.AdminName
                                          // }).ToList()
                                      }).SingleOrDefaultAsync();

                if (hallBranchData == null)
                {
                    return BadRequest(new { StatusCode = "RE02032", result = "لا توجد بيانات لهذه الصالة " });
                }

                

                return Ok(new { StatusCode = 1,  hallBranchData  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02003", result = errorMsg });
            }
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> hallBranchToEdit(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02033", result = "الرجاء التاكد اختيار فرع الصالة " });
                }


                var HallAppontmentsQuery = await (from p in db.Hallappointments
                                where p.HallBranchId == id
                                select new
                                {
                                    p.HallAppointmentId,
                                    p.WorkDayId,
                                    p.ReservationTypeId,
                                    TimeRange = new[] { p.StartTime.ToString(@"hh\:mm\:ss"), p.EndTime.ToString(@"hh\:mm\:ss") },
                                    p.Price,
                                    reservationStatus = p.Hallreservationdetails
                                    .Select(f => f).Count(),
                                }).ToListAsync();

                var hallAppointments = HallAppontmentsQuery
                    .GroupBy(u => u.WorkDayId)
                    .OrderBy(u=>u.Key)
                    .Select(x => new {
                        workDayId = x.Key,
                        AppointmentData = x.Select(c=>new {
                            id = c.HallAppointmentId,
                            ReservationType = c.ReservationTypeId,
                            c.TimeRange,
                            c.Price,
                            c.reservationStatus
                        }).ToList()
                    }).ToList();
                
                var hallBranchData =  await (from h in db.Hallbranches
                                            where h.Status != Status.Deleted
                                            && h.HallBranchId == id
                                            select new
                                            {
                                                h.AddressId,
                                                h.AddressDescription,
                                                h.PhoneNo1,
                                                h.PhoneNo2,
                                                h.Longitude,
                                                h.Latitude,
                                                h.PeapleCapacity,
                                                h.CarsCapacity,
                                                hallAppointments,
                                                //h.WorkDayFrom,
                                                //h.WorkDayTo,
                                                TimeRange = new[] { h.StartTime.ToString(@"hh\:mm\:ss"), h.EndTime.ToString(@"hh\:mm\:ss") },
                                                HallServices = h.Hallbranchservices
                                                  .Where(x => x.Status != Status.Deleted)
                                                  .Select(x => new
                                                  {
                                                      id = x.HallBranchServiceId,
                                                      x.Description,
                                                      x.Price,
                                                      reservationStatus = x.Hallservicereservations
                                                        .Where(f => f.Status == Status.Active)
                                                        .Select(f => f).Count(),
                                                  }).ToList(),

                                            }).SingleOrDefaultAsync();

                if (hallBranchData == null)
                {
                    return BadRequest(new { StatusCode = "RE02034", result = "لا توجد بيانات لهذه الصالة " });
                }



                return Ok(new { StatusCode = 1, hallBranchData  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02004", result = errorMsg });
            }
        }


        [HttpPut]
        public async Task<IActionResult> editHallBranch([FromBody] HallBranchData hallBranchData)
        {
            try
            {
                if (hallBranchData.HallId == null)
                {
                    return BadRequest(new { StatusCode = "RE02035", result = "الرجاء التاكد من اختيار الصالة " });
                }
                if (hallBranchData.HallId.Value <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02036", result = "الرجاء التاكد من اختيار الصالة " });
                }
                if (hallBranchData.HallBranchId == null)
                {
                    return BadRequest(new { StatusCode = "RE02037", result = "الرجاء التاكد من اختيار فرع الصالة " });
                }
                if (hallBranchData.HallBranchId.Value <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02038", result = "الرجاء التاكد من اختيار فرع الصالة " });
                }
                if (string.IsNullOrWhiteSpace(hallBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE02039", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (hallBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE02040", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(hallBranchData.PhoneNo2))
                {
                    if (hallBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE02041", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }


                if (hallBranchData.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE02042", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(hallBranchData.AddressDescription))
                {
                    if (hallBranchData.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode = "RE02043", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(hallBranchData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE02044", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (hallBranchData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE02045", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(hallBranchData.PhoneNo2))
                {
                    if (hallBranchData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE02046", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (hallBranchData.Longitude > 180 || hallBranchData.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE02047", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (hallBranchData.Longitude > 90 || hallBranchData.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE02048", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                if (hallBranchData.PeapleCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02049", result = "الرجاء التاكد من ادخال عدد الاشخاص " });
                }
                if (hallBranchData.CarsCapacity <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02050", result = "الرجاء التاكد من ادخال عدد السيارات " });
                }



                if (!Validation.IsValidTimeFormat(hallBranchData.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE02051", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(hallBranchData.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE02052", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }
                if (hallBranchData.HallAppointments.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02053", result = "الرجاء التاكد من ادخال مواعيد الحجز " });
                }
                foreach (var appontment in hallBranchData.HallAppointments)
                {
                    if (appontment.WorkDayId <= 0 || appontment.WorkDayId > 7)
                    {
                        return BadRequest(new { StatusCode = "RE02054", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                    }
                    if (appontment.AppointmentData.Count <= 0)
                    {
                        return BadRequest(new { StatusCode = "RE02055", result = "الرجاء التاكد من ادخال بيانات موعد الحجز " });
                    }
                    foreach (var appointmentDetails in appontment.AppointmentData)
                    {
                        if (appointmentDetails.ReservationType <= 0)
                        {
                            return BadRequest(new { StatusCode ="RE02056", result = "الرجاء التاكد من ادخال نوع موعد الحجز " });
                        }
                        if (!Validation.IsValidTimeFormat(appointmentDetails.TimeRange[0]))
                        {
                            return BadRequest(new { StatusCode ="RE02057", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                        }
                        if (Convert.ToDateTime(appointmentDetails.TimeRange[0]) < Convert.ToDateTime(appointmentDetails.TimeRange[0])
                            || Convert.ToDateTime(hallBranchData.TimeRange[0]) > Convert.ToDateTime(hallBranchData.TimeRange[1]))
                        {
                            return BadRequest(new { StatusCode = "RE02058", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                        }
                        if (!Validation.IsValidTimeFormat(appointmentDetails.TimeRange[1]))
                        {
                            return BadRequest(new { StatusCode = "RE02059", result = "الرجاء التاكد من ادخال وقت بداية الموعد بشكل صحيح " });
                        }
                        if (Convert.ToDateTime(appointmentDetails.TimeRange[1]) > Convert.ToDateTime(appointmentDetails.TimeRange[1])
                            || Convert.ToDateTime(hallBranchData.TimeRange[1]) < Convert.ToDateTime(hallBranchData.TimeRange[0]))
                        {
                            return BadRequest(new { StatusCode ="RE02060", result = "الرجاء التاكد من ادخال وقت نهاية الموعد بشكل صحيح ( لا يكون اصغر من وقت بداية العمل و لا اكبر من وقت نهاية العمل) " });

                        }
                        if (appointmentDetails.Price <= 0)
                        {
                            return BadRequest(new { StatusCode = "RE02061", result = "الرجاء التاكد من ادخال سعر موعد الحجز " });
                        }
                    }
                }

                foreach (var service in hallBranchData.HallServices)
                {
                    if (string.IsNullOrWhiteSpace(service.Description))
                    {
                        return BadRequest(new { StatusCode = "RE02062", result = "الرجاء التاكد من ادخال وصف الخدمة " });
                    }

                    if (service.Price <= 0)
                    {
                        return BadRequest(new { StatusCode = "RE02063", result = "الرجاء التاكد من ادخال سعر الخدمة " });
                    }
                }

                var checkBranchAddress = await (from h in db.Hallbranches
                                                where h.AddressId == hallBranchData.AddressId
                                                && h.Status != Status.Deleted
                                                && h.HallId == hallBranchData.HallId.Value
                                                select h).CountAsync();

                if (checkBranchAddress > 0)
                {
                    if (string.IsNullOrWhiteSpace(hallBranchData.AddressDescription))
                    {
                        return BadRequest(new { StatusCode = "RE02064", result = "يوجد فرع لهذه الصالة في نفس هذا العنوان ... يرجي كتابة تفصيل العنوان للتفرقة " });
                    }
                }
                var editHallBranch = await (from h in db.Hallbranches
                                            where h.Status != Status.Deleted
                                            && h.HallBranchId == hallBranchData.HallBranchId.Value
                                            select new {
                                                h,
                                                h.Address.AddressName
                                            }).SingleOrDefaultAsync();
                if (editHallBranch == null)
                {
                    return BadRequest(new { StatusCode = "RE02065", result = " لا توجد بيانات لهذه الصالة  " });
                }
                if (editHallBranch.h.HallId != hallBranchData.HallId.Value)
                {
                    return BadRequest(new { StatusCode = "RE02066", result = "لا يوجد بيانات فرع للصالة المختارة" });
                }
                List<int> hallApponimentsIds = new List<int>();
                List<int> hallServiceIds = new List<int>();

                editHallBranch.h.AddressDescription = hallBranchData.AddressDescription;
                editHallBranch.h.AddressId = hallBranchData.AddressId;
                editHallBranch.h.CarsCapacity = hallBranchData.CarsCapacity;
                editHallBranch.h.HallId = hallBranchData.HallId.Value;
                editHallBranch.h.Latitude = hallBranchData.Latitude;
                editHallBranch.h.Longitude = hallBranchData.Longitude;
                editHallBranch.h.PeapleCapacity = hallBranchData.PeapleCapacity;
                editHallBranch.h.PhoneNo1 = hallBranchData.PhoneNo1;
                editHallBranch.h.PhoneNo2 = hallBranchData.PhoneNo2;
                editHallBranch.h.StartTime = TimeOnly.Parse(hallBranchData.TimeRange[0]);
                editHallBranch.h.EndTime = TimeOnly.Parse(hallBranchData.TimeRange[1]);
                //editHallBranch.h.WorkDayFrom = hallBranchData.WorkDayFrom;
                //editHallBranch.h.WorkDayTo = hallBranchData.WorkDayTo;
                editHallBranch.h.Status = Status.Active;
                editHallBranch.h.ModifiedBy = UserId();
                editHallBranch.h.ModifiedOn = DateTime.Now;



                foreach (var appontment in hallBranchData.HallAppointments)
                {
                    
                    foreach (var appointmentDetails in appontment.AppointmentData)
                    {
                        var checkAppointmentReservation = await (from p in db.Hallappointments
                                                                 where p.HallAppointmentId == appointmentDetails.Id
                                                                 && p.Status != Status.Deleted
                                                                 select p.Hallreservationdetails
                                                                 .Select(x => x).Count()).SingleOrDefaultAsync();
                        if (checkAppointmentReservation > 0)
                        {
                            return BadRequest(new { StatusCode = "RE02067", result = "يوجد حجز مسبق لهذا الموعد" });
                        }
                        if(appointmentDetails.Id != null)
                        {
                            var editAppointment = await (from p in db.Hallappointments
                                                         where p.HallAppointmentId == appointmentDetails.Id
                                                         select p).SingleOrDefaultAsync();
                            editAppointment.ReservationTypeId = appointmentDetails.ReservationType;
                            editAppointment.StartTime = TimeOnly.Parse(appointmentDetails.TimeRange[0]);
                            editAppointment.EndTime = TimeOnly.Parse(appointmentDetails.TimeRange[1]);
                            editAppointment.Price = appointmentDetails.Price;
                            hallApponimentsIds.Add(appointmentDetails.Id.Value);
                        }
                        else
                        {
                            Hallappointment addAppointment = new Hallappointment()
                            {
                                WorkDayId = appontment.WorkDayId,
                                HallBranchId = hallBranchData.HallBranchId.Value,
                                ReservationTypeId = appointmentDetails.ReservationType,
                                StartTime = TimeOnly.Parse(appointmentDetails.TimeRange[0]),
                                EndTime = TimeOnly.Parse(appointmentDetails.TimeRange[1]),
                                Price = appointmentDetails.Price,
                                Status = Status.Active,
                                CreatedBy = UserId(),
                                CreatedOn = DateTime.Now
                            };
                            await db.Hallappointments.AddAsync(addAppointment);
                        }
                        

                    }
                }

                foreach (var service in hallBranchData.HallServices)
                {
                    var checkServiceReservation = await (from p in db.Hallbranchservices
                                                             where p.HallBranchServiceId == service.Id
                                                             && p.Status != Status.Deleted
                                                             select p.Hallservicereservations
                                                             .Where(x => x.Status == Status.Active)
                                                             .Select(x => x).Count()).SingleOrDefaultAsync();
                    if (checkServiceReservation > 0)
                    {
                        return BadRequest(new { StatusCode =  "RE02068", result = "يوجد حجز مسبق لهذه الخدمة" });
                    }

                    if (service.Id != null)
                    {
                        var editService = await (from p in db.Hallbranchservices
                                                     where p.HallBranchServiceId == service.Id
                                                     select p).SingleOrDefaultAsync();
                        editService.Description = service.Description;
                        editService.Price = service.Price;
                        hallServiceIds.Add(service.Id.Value);
                    }
                    else
                    {
                        Hallbranchservice addService = new Hallbranchservice()
                        {
                            HallBranchId = hallBranchData.HallBranchId.Value,
                            Description = service.Description,
                            Price = service.Price,
                            Status = Status.Active,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now
                        };
                        await db.Hallbranchservices.AddAsync(addService);
                    }
                }

                var removeAppoitment = await (from p in db.Hallappointments
                                              where p.HallBranchId == hallBranchData.HallBranchId.Value
                                              select p).ToListAsync();
                foreach(var remove in removeAppoitment)
                {
                    int check = hallApponimentsIds.Find(x => x == remove.HallAppointmentId);
                    if (check == 0)
                    {
                        db.Hallappointments.Remove(remove);
                    }
                }
                var removeService = await (from p in db.Hallbranchservices
                                              where p.HallBranchId == hallBranchData.HallBranchId.Value
                                              select p).ToListAsync();
                foreach (var remove in removeService)
                {
                    int check = hallServiceIds.Find(x => x == remove.HallBranchServiceId);
                    if (check == 0)
                    {
                        db.Hallbranchservices.Remove(remove);
                    }
                }

                await db.SaveChangesAsync();

                var hallBranches = await (from h in db.Hallbranches
                where h.HallBranchId == hallBranchData.HallBranchId
                                          orderby h.HallBranchId
                                   select new
                                   {
                                       h.HallBranchId,
                                       AddressName = h.Address.AddressName + " " + h.AddressDescription,
                                       h.PhoneNo1,
                                       h.Hall.MainBranchId,
                                       //workDays = Constant.SelectDay(h.WorkDayFrom.Value) + " -  " + Constant.SelectDay(h.WorkDayTo.Value),
                                       workTimes = h.StartTime.ToString(@"hh\:mm\:ss") + " -  " + h.EndTime.ToString(@"hh\:mm\:ss"),
                                       h.CreatedByNavigation.AdminName,
                                       CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).SingleOrDefaultAsync();
                //var addressName = await (from a in db.Addresses 
                //                         where a.AddressId == hallBranchData.AddressId
                //                         select a.AddressName).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1,  message = $"تم تعديل فرع{editHallBranch.AddressName}  لهذه الصالة بنجاح"  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02005", result = errorMsg });
            }
        }


        [HttpPost("addImages")]
        public async Task<IActionResult> addImages([FromBody] MediaData images)
        {
            try
            {
                if (images == null)
                {
                    return BadRequest(new { StatusCode =  "RE02070", result = "يرجي اختيار صور لتحميلها" });
                }
                if (images.HallBranchId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02071", result = "يرجي التاكد من بيانات الفرع" });
                }
                if (images.Images.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02072", result = "يرجي التاكد من ادخال صور" });
                }
                if (images.Images.Count > 10)
                {
                    return BadRequest(new { StatusCode = "RE02073", result = "لا يمكن ادخال اكثر من 10 صور" });
                }
                var hallInfo = await (from h in db.Hallbranches
                                      where h.HallBranchId == images.HallBranchId
                                      select new {
                                          h.Hall.HallName,
                                          MediaCount = h.Media.Count()
                                      }).SingleOrDefaultAsync();
                int checkCount = 10 - hallInfo.MediaCount;
                if (images.Images.Count > checkCount)
                {
                    return BadRequest(new { StatusCode = "RE02074", result = "لا يمكن ادخال اكثر من "+ checkCount + " صور" });
                }
                int index = 1;
                foreach(var image in images.Images)
                {
                    string attachmentFirstName = hallInfo.HallName + "_" + images.HallBranchId + "_" + index;
                    string attachmentName = "";
                    byte[] file = Convert.FromBase64String(image.Substring(image.IndexOf(",") + 1));

                    attachmentName = attachmentFirstName + attachmentName + ".jpg";
                    attachmentNames.Add(attachmentName);
                    bool check = SaveAttachment(attachmentName, image);
                    if (!check)
                        return BadRequest(new { StatusCode = "RE02075", result = "يوجد خطأ في الصورة المدخلة " });
                    index++;
                }
                
                foreach(var attachment in attachmentNames)
                {
                    Medium addImage = new Medium() {
                        MediaType = 1,
                        Path = attachment,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                        HallBranchId = images.HallBranchId,
                        Status = Status.Active,
                    };
                    await db.Media.AddAsync(addImage);
                }

                await db.SaveChangesAsync();

                var imagesQuery = await (from m in db.Media
                                         where m.HallBranchId == images.HallBranchId
                                         && m.MediaType == 1
                                         && m.Status != Status.Deleted
                                         select m).ToListAsync();


                var oldImages = imagesQuery.Select(x => new {
                    imageId = x.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + x.Path))
                });
                return Ok(new { StatusCode = 1, message = "تم تحميل الصور بنجاح", oldImages });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02006", result = errorMsg });
            }
        }

        [HttpPost("addVideo")]
        public async Task<IActionResult> addVideo(int hallBranchId, IFormFile file)
        {
            try
            {
                if (hallBranchId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02069", result = "يرجي التاكد من بيانات الفرع" });
                }
                var hallInfo = await (from h in db.Hallbranches
                                      where h.HallBranchId == hallBranchId
                                      && h.Status != Status.Deleted
                                      select new { 
                                        h.Hall.HallName,
                                        videoCount = h.Media.Where(x=>x.MediaType == 2)
                                        .Select(x=>x).Count()
                                      }).SingleOrDefaultAsync();
                if (hallInfo == null)
                {
                    return NotFound();
                }
                if(hallInfo.videoCount > 1)
                {
                    return BadRequest();
                }
                string FileName = hallInfo.HallName + "_"+ hallBranchId +".mp4" ;
                var filePath = Path.Combine(_env.WebRootPath + imagePath, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                }
                Medium addVideo = new Medium()
                {
                    MediaType = 2,
                    Path = FileName,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    HallBranchId = hallBranchId,
                    Status = Status.Active,
                };
                await db.Media.AddAsync(addVideo);
                await db.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02007", result = errorMsg });
            }
        }

        [HttpDelete("{id}/deleteVideo")]
        public async Task<IActionResult> deleteVideo(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02076", result = "يرجي التاكد من بيانات الفرع" });
                }
                var hallInfo = await (from h in db.Hallbranches
                                      where h.HallBranchId == id
                                      select h.Hall.HallName).SingleOrDefaultAsync();
                if (hallInfo == null)
                {
                    return NotFound(new { StatusCode = "RE02077", result = "لم يتم العثور علي بيانات الفرع" });
                }

                

                var removePath = await (from m in db.Media
                                        where m.HallBranchId == id
                                        && m.MediaType == 2
                                        select m).FirstOrDefaultAsync();
                if (removePath == null)
                {
                    return NotFound(new { StatusCode ="RE02078", result = "لم يتم العثور علي بيانات الفيديو المختار" });
                }
                string FileName = hallInfo + "_" + id + ".mp4";

                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", FileName);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    db.Media.Remove(removePath);
                }
                else
                {
                    return NotFound(new { StatusCode = "RE02079", result = "لم يتم العثور علي الفيديو المختار في مستكشف الملفات" });
                }
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم حذف الفيديو بنجاح"  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "RE02080", result = errorMsg });
            }
        }

        [HttpGet("{id}/getVideo")]
        public async Task<IActionResult> getVideo(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02081", result = "يرجي التاكد من بيانات الفرع" });
                }
                var video = await (from m in db.Media
                                   where m.HallBranchId == id
                                   && m.MediaType == 2
                                   && m.Status != Status.Deleted
                                   select m.Path
                                   ).SingleOrDefaultAsync();


                if (video == null)
                {
                    return Ok(new { StatusCode = 1,  video });
                }
                video = "data:video/mp4;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + video));
                return Ok(new { StatusCode = 1, video } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02009", result = errorMsg });
            }
        }

        [HttpGet("{id}/getImages")]
        public async Task<IActionResult> getImages(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode ="RE02082", result = "يرجي التاكد من بيانات الفرع" });
                }
                var imagesQuery = await (from m in db.Media
                                   where m.HallBranchId == id
                                   && m.MediaType == 1
                                   && m.Status != Status.Deleted
                                   select m).ToListAsync();


                if (imagesQuery.Count <= 0)
                {
                    return Ok(new { StatusCode = 1, images = imagesQuery  });
                }
                //for(int i = 0; i < imagesQuery.Count; i++)
                //{
                //    imagesQuery[0].Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + images[0].Path));
                //}
                var images = imagesQuery.Select(x => new {
                    imageId = x.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + x.Path))
                });
                return Ok(new { StatusCode = 1, images } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02008", result = errorMsg });
            }
        }

        [HttpPut("deleteOldImages")]
        public async Task<IActionResult> deleteOldImages([FromBody]List<int> checkedImages)
        {
            try
            {
                if (checkedImages.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02083", result = "يرجي التاكد من بيانات الفرع" });
                }
                foreach(var image in checkedImages)
                {
                    var deleteImage = await (from m in db.Media
                                             where m.MediaId == image
                                             select m).SingleOrDefaultAsync();
                    if(deleteImage != null)
                    {
                        db.Media.Remove(deleteImage);
                    }
                }
                await db.SaveChangesAsync();
                
                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode ="EX02010", result = errorMsg });
            }
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> locking(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = 902097, result = "الرجاء التاكد اختيار فرع الصالة " });
                }

                var hallData = await (from h in db.Hallbranches
                                      where h.HallBranchId == id
                                      && h.Status != Status.Deleted
                                      && h.Hall.HallId != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE02084", result = "لا توجد بيانات لفرع هذه الصالة" });
                }

                if (hallData.Status == Status.Locked)
                {
                    return BadRequest(new { StatusCode = "RE02085", result = "حالة فرع هذه الصالة مجمدة مسبقا" });
                }

                var checkHallSubscription = await (from h in db.Subscriptiondetails
                                                   where
                                                   h.Subscription.HallBranchId == id &&
                                                   h.EndDate > DateTime.Now
                                                   select h).AnyAsync();
                if (checkHallSubscription)
                {
                    return BadRequest(new { StatusCode = "RE02086", result = "لا يمكن تجميد فرع الصالة و يوجد فروع لها تحت الاشتراك" });
                }

                hallData.Status = Status.Locked;
                hallData.ModifiedBy = UserId();
                hallData.ModifiedOn = DateTime.Now;
                
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم تجميد  فرع الصالة بنجاح"  });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02011", result = errorMsg });
            }
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> unlockHallBranch(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode =  "RE02087", result = "الرجاء التاكد اختيار فرع الصالة " });
                }

                var hallData = await (from h in db.Hallbranches
                                      where h.HallBranchId == id
                                      && h.Status != Status.Deleted
                                      && h.Hall.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode =  "RE02088", result = "لا توجد بيانات لهذه الصالة" });
                }

                if (hallData.Status == Status.Entry)
                {
                    return BadRequest(new { StatusCode =  "RE02089", result = "حالة فرع هذه الصالة غير مجمد" });
                }

                hallData.Status = Status.Entry;
                hallData.ModifiedBy = UserId();
                hallData.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم فك تجميد فرع الصالة بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02012", result = errorMsg });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02090", result = "الرجاء التاكد اختيار فرع الصالة " });
                }

                var hallData = await (from h in db.Hallbranches
                                      where h.HallBranchId == id
                                      && h.Hall.MainBranchId != id
                                      select new { h,h.Hall.MainBranchId }).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE02091", result = "لا توجد بيانات لفرع هذه الصالة" });
                }
                if (hallData.MainBranchId == id)
                {
                    return BadRequest(new { StatusCode = "RE02092", result = "لا يمكن حذف الفرع الرئيسي ... لحذف يرجي عمل العملية عند الصالة" });
                }
                if (hallData.h.Status == Status.Deleted)
                {
                    return BadRequest(new { StatusCode = "RE02093", result = "حالة فرع هذه الصالة محذوف مسبقا" });
                }

                var checkHallSubscription = await (from h in db.Subscriptiondetails
                                                   where
                                                   h.Subscription.HallBranchId == id &&
                                                   h.EndDate > DateTime.Now
                                                   select h).AnyAsync();
                if (checkHallSubscription)
                {
                    return BadRequest(new { StatusCode = "RE02094", result = "لا يمكن حذف الصالة و يوجد فروع لها تحت الاشتراك" });
                }

                hallData.h.Status = Status.Deleted;
                hallData.h.ModifiedBy = UserId();
                hallData.h.ModifiedOn = DateTime.Now;


                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم حذف فرع الصالة بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02013", result = errorMsg });
            }
        }

        [HttpPut("{id}/assignMainBranch")]
        public async Task<IActionResult> assignMainBranch(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02095", result = "الرجاء التاكد اختيار فرع الصالة " });
                }

                var hallData = await (from h in db.Hallbranches
                                      where h.HallBranchId == id
                                      && h.Status != Status.Deleted
                                      && h.Hall.Status != Status.Deleted
                                      select new { h,h.Hall }).SingleOrDefaultAsync();
                if (hallData == null)
                {
                    return BadRequest(new { StatusCode = "RE02096", result = "لا توجد بيانات لفرع هذه الصالة" });
                }

                if (hallData.Hall.MainBranchId == id)
                {
                    return BadRequest(new { StatusCode = "RE02097", result = "حالة فرع هذه الصالة معيين كرئيسي مسبقا" });
                }

                hallData.Hall.MainBranchId = id;
                hallData.h.ModifiedBy = UserId();
                hallData.h.ModifiedOn = DateTime.Now;
                hallData.Hall.ModifiedBy = UserId();
                hallData.Hall.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم تعيين هذا الفرع كفرع رئيسي بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX02014", result = errorMsg });
            }
        }

        [HttpPost("addSubscription")]
        public async Task<IActionResult> addSubscription([FromBody] NewSubscription data)
        {
            try
            {
                if(data.HallBranchId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02098", result = "يرجي التاكد من اختيار فرع الصالة" });
                }
                if (data.SubscriptionPriceDetailsId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02099", result = "يرجي اختيار نوع الاشتراك" });
                }

                var hallBranch = await (from h in db.Hallbranches
                                        where h.HallBranchId == data.HallBranchId
                                        && h.Status != Status.Deleted
                                        && h.Hall.Status != Status.Deleted
                                        select h).SingleOrDefaultAsync();
                if(hallBranch == null)
                {
                    return BadRequest(new { StatusCode = "RE02100", result = "لا توجد بيانات لفرع هذه الصالة" });
                }
                var checkHallSubscription = await (from h in db.Subscriptiondetails
                                                   where
                                                   h.Subscription.HallBranchId == data.HallBranchId 
                                                   &&
                                                   //h.SubscriptionPriceDetailsId == data.SubscriptionPriceDetailsId &&
                                                   h.EndDate > DateTime.Now
                                                   select h).AnyAsync();
                if (checkHallSubscription)
                {
                    return BadRequest(new { StatusCode = "RE02101", result = "فرع هذه الصالة لديه اشتراك مفعل حاليا" });
                }
                var subscriptionPrices = await (from s in db.Subscriptionpricedetails
                                                where s.SubscriptionPriceDetailsId == data.SubscriptionPriceDetailsId
                                                && s.SubscriptionPrice.Status != Status.Deleted
                                                select s).SingleOrDefaultAsync();
                if (subscriptionPrices == null)
                {
                    return BadRequest(new { StatusCode = "RE02102", result = "لم يتم العثور علي بيانات لنوع هذا الاشتراك" });
                }
                Subscription newSubscription = new Subscription() { 
                    HallBranchId = data.HallBranchId,
                    Status = Status.Active,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now
                };
                db.Subscriptions.Add(newSubscription);

                Subscriptiondetail newSubscriptiondetails = new Subscriptiondetail() {
                    SubscriptionId = newSubscription.SubscriptionId,
                    SubscriptionPriceDetailsId = data.SubscriptionPriceDetailsId,
                    Price = subscriptionPrices.Price,
                    StartDate = DateTime.Now,
                    EndDate = subscriptionPrices.DurationType == 1 ? DateTime.Now.AddYears(subscriptionPrices.DurationTime) :
                             subscriptionPrices.DurationType == 2 ? DateTime.Now.AddMonths(subscriptionPrices.DurationTime) :
                            DateTime.Now.AddDays(subscriptionPrices.DurationTime * 7),
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    Status = Status.Active,
                };
                newSubscription.Subscriptiondetails.Add(newSubscriptiondetails);

                hallBranch.Status = Status.Active;
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم الاشتراك  بنجاح" });

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX02015", result = errorMsg });
            }
        }
    }
}
