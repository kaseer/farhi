﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreTypesController : RootController
    {
        public StoreTypesController(farhiContext context) : base(context) {  }
        // GET: api/<StoreTypesController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] Pagination pagination, string search)
        {
            try
            {
                var query = from c in db.Storetypes
                            where c.Status != Status.Deleted
                            select new { 
                                c.StoreTypeId,
                                c.Description,
                                c.Status,
                                CreatedBy = c.CreatedByNavigation.AdminName,
                                CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                            };
                if(search is not null) query = from c in query where c.Description.Contains(search) select c;

                var storeTypes = await query.Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new {StatusCode = 1, storeTypes ,total});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX06001", result = errorMsg });
            }
        }


        // POST api/<StoreTypesController>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] StoreTypesData dataVM)
        {
            try
            {
                bool check = await (from c in db.Storetypes
                                    where c.Description == dataVM.Description
                                    select c).AnyAsync();

                if (check) return BadRequest(new { StatusCode = "RE06001", Message = "هذا النوع موجود مسبقا" });
                Storetype storetype = new Storetype() { 
                    Description = dataVM.Description,
                    Status = 1,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                };
                db.Storetypes.Add(storetype);
                await db.SaveChangesAsync();
                var data = await (from c in db.Storetypes
                                  where storetype.StoreTypeId == c.StoreTypeId
                                  select new
                                  {
                                      c.StoreTypeId,
                                      c.Description,
                                      c.Status,
                                      CreatedBy = c.CreatedByNavigation.AdminName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                  }).SingleOrDefaultAsync();
                return Ok(new {StatusCode = 1, message = "تم اضافة نوع محل بنجاح", data });
            }
            catch(Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX06002", result = errorMsg });
            }
        }

        // PUT api/<StoreTypesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] StoreTypesData dataVM)
        {
            try
            {
                bool check = await(from c in db.Storetypes
                                   where c.Description == dataVM.Description
                                   && c.StoreTypeId != id
                                   select c).AnyAsync();

                if (check) return BadRequest(new { StatusCode = "RE06002", Message = "هذا النوع موجود مسبقا" });
                var data = await (from c in db.Storetypes where c.StoreTypeId == id select c).SingleOrDefaultAsync();
                data.Description = dataVM.Description;
                data.ModifiedBy = UserId();
                data.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم تعديل نوع محل بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX06003", result = errorMsg });
            }
        }
    }
}
