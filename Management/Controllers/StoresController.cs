﻿using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : RootController
    {
        private IWebHostEnvironment _env;
        public IConfiguration configuration;

        public StoresController(farhiContext context, IWebHostEnvironment env, IConfiguration configuration) : base(context)
        {
            _env = env;
            this.configuration = configuration;
        }
        private string imagePath = "/img/";
        public bool SaveAttachment(string attachmentName, string AttachmentFile)
        {
            try
            {
                byte[] file = Convert.FromBase64String(AttachmentFile.Substring(AttachmentFile.IndexOf(",") + 1));

                var path = Path.Combine(_env.WebRootPath + imagePath, attachmentName);
                FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
                fileStream.Write(file, 0, file.Length);
                fileStream.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpGet]
        public async Task<IActionResult> getAll(int pageNo, int pageSize, string searchByName)
        {
            try
            {
                if (pageNo <= 0 || pageSize <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03001", result = "الرجاء التاكد اختيار رقم الصفحة او حجم الصفحة " });
                }

                var storesQuery = from h in db.Stores
                                 where h.Status != Status.Deleted
                                 select h;

                if (!string.IsNullOrWhiteSpace(searchByName))
                {
                    storesQuery = from h in storesQuery
                                  where h.StoreName.Contains(searchByName)
                                 select h;
                }
                var stores = await (from h in storesQuery
                                    select new
                                   {
                                       h.StoreId,
                                       h.StoreName,
                                       h.OwnerName,
                                       h.PhoneNo1,
                                        StoreType = h.StoreType.Description,
                                       h.CreatedByNavigation.AdminName,
                                       CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).Skip((pageNo - 1) * pageSize).Take(pageSize).ToListAsync();
                var total = await storesQuery.CountAsync();
                return Ok(new { StatusCode = 1, stores, total } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03001", result = errorMsg });
            }
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] StoreData StoreData)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(StoreData.StoreName))
                {
                    return BadRequest(new { StatusCode = "RE03002", result = "الرجاء التاكد من اسم المحل " });
                }
                if (StoreData.StoreName.Length > 30)
                {
                    return BadRequest(new { StatusCode = "RE03003", result = "الرجاء التاكد من طول اسم المحل " });
                }

                if (StoreData.StoreTypeId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03004", result = "الرجاء التاكد من ادخال نوع المحل  " });
                }
                if (string.IsNullOrWhiteSpace(StoreData.OwnerName))
                {
                    return BadRequest(new { StatusCode = "RE03005", result = "الرجاء التاكد من ادخال اسم مالك المحل  " });
                }
                if (StoreData.OwnerName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE03006", result = "الرجاء التاكد من طول اسم مالك المحل " });
                }

                if (string.IsNullOrWhiteSpace(StoreData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE03007", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (StoreData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE03008", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.PhoneNo2))
                {
                    if (StoreData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE03009", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }
                if (string.IsNullOrWhiteSpace(StoreData.Email))
                {
                    return BadRequest(new { StatusCode = "RE03010", result = "الرجاء التاكد من ادخال بريد الكتروني للمحل " });
                }
                if (!Validation.IsValidEmail(StoreData.Email))
                {
                    return BadRequest(new { StatusCode = "RE03011", result = "الرجاء التاكد من ادخال البريد الالكتروني للمحل بشكل صحيح " });
                }
                if (StoreData.Email.Length > 50)
                {
                    return BadRequest(new { StatusCode = "RE03012", result = " الرجاء التاكد من طول البريد الالكتروني" });
                }
                if (StoreData.StoreBranch.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE03013", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.StoreBranch.AddressDescription))
                {
                    if (StoreData.StoreBranch.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode = "RE03014", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(StoreData.StoreBranch.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE03015", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (StoreData.StoreBranch.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE03016", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.StoreBranch.PhoneNo2))
                {
                    if (StoreData.StoreBranch.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE03017", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (StoreData.StoreBranch.Longitude > 180 || StoreData.StoreBranch.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE03018", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (StoreData.StoreBranch.Longitude > 90 || StoreData.StoreBranch.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE03019", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                
                



                if (!Validation.IsValidTimeFormat(StoreData.StoreBranch.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE03020", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(StoreData.StoreBranch.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE03021", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }

                if (StoreData.StoreBranch.StoreAppointments.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03022", result = "الرجاء التاكد من موعد عمل واحد علي الاقل " });
                }
                foreach(var appointment in StoreData.StoreBranch.StoreAppointments)
                {
                    if (!Validation.IsValidTimeFormat(appointment.TimeRange[0]))
                    {
                        return BadRequest(new { StatusCode = "RE03023", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                    }

                    if (!Validation.IsValidTimeFormat(appointment.TimeRange[1]))
                    {
                        return BadRequest(new { StatusCode = "RE03024", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                    }
                }

                if (string.IsNullOrWhiteSpace(StoreData.FarhiUser.UserName))
                {
                    return BadRequest(new { StatusCode = "RE03025", result = "الرجاء التاكد من ادخال اسم المستخدم " });
                }
                if (StoreData.FarhiUser.UserName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE03026", result = "الرجاء التاكد من ادخال اسم مستحدم لا يتجاوز عن 40 خانة " });
                }
                if (string.IsNullOrWhiteSpace(StoreData.FarhiUser.LoginName))
                {
                    return BadRequest(new { StatusCode = "RE03027", result = "الرجاء التاكد من ادخال اسم الدخول " });
                }
                if (StoreData.FarhiUser.LoginName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE03028", result = "الرجاء التاكد من ادخال اسم دخول لايتجاوز عن 40 خانة " });
                }
                //==========================Password=============================//
                if (string.IsNullOrWhiteSpace(StoreData.FarhiUser.Password))
                {
                    return BadRequest(new { StatusCode = "RE03029", result = "الرجاء التاكد من ادخال كلمة مرور  " });
                }
                if (StoreData.FarhiUser.Password.Length > 200)
                {
                    return BadRequest(new { StatusCode = "RE03030", result = "الرجاء التاكد من ادخال كلمة مرور لا تتجاوز عن 200 خانة " });
                }
                if (!Validation.IsValidPassword(StoreData.FarhiUser.Password))
                {
                    return BadRequest(new { StatusCode = "RE03031", result = "الرجاء التاكد من ادخال كلمة مرور معقدة " });
                }
                //==========================Password=============================//

                //==========================Logo=============================//

                if (string.IsNullOrWhiteSpace(StoreData.Logo))
                {
                    return BadRequest(new { StatusCode = "RE03032", result = "الرجاء التاكد من ادخال شعار المحل " });
                }
                //==========================Logo=============================//



                if (string.IsNullOrWhiteSpace(StoreData.FarhiUser.UserPhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE03033", result = "الرجاء التاكد الرجاء التاكد من ادخال رقم هاتف 1 للمستخدم " });
                }
                if (StoreData.FarhiUser.UserPhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE03034", result = "الرجاء التاكد من ادخال رقم هاتف 1 للمستخدم لا يتجاوز 10 خانات " });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.FarhiUser.UserPhoneNo2))
                {
                    if (StoreData.FarhiUser.UserPhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE03035", result = "الرجاء التاكد من ادخال رقم هاتف 2 للمستخدم " });
                    }
                }

                if (string.IsNullOrWhiteSpace(StoreData.FarhiUser.UserEmail))
                {
                    return BadRequest(new { StatusCode = "RE03036", result = "الرجاء التاكد من ادخال بريد الكتروني للمستخدم " });
                }
                if (!Validation.IsValidEmail(StoreData.FarhiUser.UserEmail))
                {
                    return BadRequest(new { StatusCode = "RE03037", result = "الرجاء التاكد من من ادخال بريد الكتروني للمستخدم للمستخدم بشكل صحيح " });
                }

                var checkStoreName = await (from h in db.Stores
                                           where h.StoreName == StoreData.StoreName
                                           && h.Status != Status.Deleted
                                           select h).CountAsync();
                if (checkStoreName > 0)
                {
                    return BadRequest(new { StatusCode = "RE03038", result = "اسم المحل المدخل مكرر " });
                }

                string attachmentFirstName = StoreData.StoreName + "_S_Logo";
                string attachmentName = "";
                byte[] file = Convert.FromBase64String(StoreData.Logo.Substring(StoreData.Logo.IndexOf(",") + 1));

                attachmentName = attachmentFirstName + attachmentName + ".jpg";
                bool check = SaveAttachment(attachmentName, StoreData.Logo);
                if (!check)
                    return BadRequest(new { StatusCode = "RE03039", result = "يوجد خطأ في الصورة المدخلة " });


                var checkLoginName = await (from u in db.Farhiusers
                                            where u.LoginName == StoreData.FarhiUser.LoginName
                                            && u.Status != Status.Deleted
                                            select u).CountAsync();

                if (checkLoginName > 0)
                {
                    return BadRequest(new { StatusCode = "RE03040", result = "اسم دخول المستخدم المدخل مكرر " });
                }

                var checkStoreEmail = await (from h in db.Stores
                                            where h.Email == StoreData.Email
                                            && h.Status != Status.Deleted
                                            select h).CountAsync();
                if (checkStoreEmail > 0)
                {
                    return BadRequest(new { StatusCode = "RE03041", result = "البريد الالكتروني للمحل المدخل مكرر " });
                }

                var checkUserEmail = await (from u in db.Farhiusers
                                            where u.Email == StoreData.FarhiUser.UserEmail
                                            && u.Status != Status.Deleted
                                            select u).CountAsync();

                if (checkUserEmail > 0)
                {
                    return BadRequest(new { StatusCode = "RE03042", result = "البريد الالكتروني للمستخدم المدخل مكرر " });
                }

                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        Store newStore = new Store()
                        {
                            StoreName = StoreData.StoreName,
                            StoreTypeId = StoreData.StoreTypeId,
                            OwnerName = StoreData.OwnerName,
                            PhoneNo1 = StoreData.PhoneNo1,
                            PhoneNo2 = StoreData.PhoneNo2,
                            Email = StoreData.Email,
                            Status = Status.Active,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Logo = attachmentName,
                        };
                        await db.Stores.AddAsync(newStore);
                        await db.SaveChangesAsync();
                        Storebranch newStoreBranch = new Storebranch()
                        {
                            AddressId = StoreData.StoreBranch.AddressId,
                            AddressDescription = StoreData.StoreBranch.AddressDescription,
                            AdminCreatedBy = UserId(),
                            AdminCreatedOn = DateTime.Now,
                            Longitude = StoreData.StoreBranch.Longitude,
                            Latitude = StoreData.StoreBranch.Latitude,
                            StoreId = newStore.StoreId,
                            PhoneNo1 = StoreData.StoreBranch.PhoneNo1,
                            PhoneNo2 = StoreData.StoreBranch.PhoneNo2,
                            StartTime = TimeOnly.Parse(StoreData.StoreBranch.TimeRange[0]),
                            EndTime = TimeOnly.Parse(StoreData.StoreBranch.TimeRange[1]),
                            Status = Status.Active,
                        };

                        await db.Storebranches.AddAsync(newStoreBranch);
                        var hashing = Security.HashPassword(StoreData.FarhiUser.Password, configuration["Secret"]);

                        Farhiuser newFarhiUser = new Farhiuser()
                        {
                            UserName = StoreData.FarhiUser.UserName,
                            LoginName = StoreData.FarhiUser.LoginName,
                            Password = hashing.Password,
                            Salt = hashing.Salt,
                            Email = StoreData.FarhiUser.UserEmail,
                            PhoneNo1 = StoreData.FarhiUser.UserPhoneNo1,
                            PhoneNo2 = StoreData.FarhiUser.UserPhoneNo2,
                            LoginStatus = 0,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Status = Status.Active
                        };

                        Farhiusershallsstore newstoreuser = new Farhiusershallsstore()
                        {
                            FarhiUserId = newFarhiUser.FarhiUserId,
                            HallId = newStore.StoreId
                        };
                        //if (StoreData.StorePermission)
                        //{
                        //    newFarhiUser.StoreId = newStore.StoreId;
                        //}
                        await db.Farhiusers.AddAsync(newFarhiUser);
                        newFarhiUser.Farhiusershallsstores.Add(newstoreuser);
                        //newstoreuser.
                        await db.SaveChangesAsync();
                        newStore.MainBranchId = newStoreBranch.StoreBranchId;
                        newstoreuser.Farhiusersstorebranches.Add(new Farhiusersstorebranch() { BranchId = newStore.MainBranchId.Value });

                        foreach (var appointment in StoreData.StoreBranch.StoreAppointments)
                        {
                            Storeworkday Storeworkdays = new Storeworkday()
                            {
                                StoreBranchId = newStoreBranch.StoreBranchId,
                                WorkDayId = appointment.WorkDayId,
                                StartTime = TimeOnly.Parse(StoreData.StoreBranch.TimeRange[0]),
                                EndTime = TimeOnly.Parse(StoreData.StoreBranch.TimeRange[1]),
                            };
                            await db.Storeworkdays.AddAsync(Storeworkdays);
                        }
                        await db.SaveChangesAsync();

                        await trans.CommitAsync();

                        var Store = await (from h in db.Stores
                                          where h.StoreId == newStore.StoreId
                                          select new
                                          {
                                              h.StoreId,
                                              StoreType = h.StoreType.Description,
                                              h.StoreName,
                                              h.OwnerName,
                                              h.PhoneNo1,
                                              h.CreatedByNavigation.AdminName,
                                              CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                              h.Status
                                          }).SingleOrDefaultAsync();
                        return Ok(new { StatusCode = 1,  message = "تم تسجيل المحل بنجاح", Store });

                    }
                    catch (Exception ex)
                    {
                        await trans.RollbackAsync();
                        return StatusCode(500, new { StatusCode = "EX03002", result = errorMsg });
                    }
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03003", result = errorMsg });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> getDatails(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03043", result = "الرجاء التاكد اختيار المحل " });
                }

                var StoreData = await (from h in db.Stores
                                      where h.Status != Status.Deleted
                                      && h.StoreId == id
                                      select new
                                      {
                                          h.StoreName,
                                          StoreType = h.StoreType.Description,
                                          h.OwnerName,
                                          h.PhoneNo1,
                                          h.PhoneNo2,
                                          h.Email,
                                          h.Logo,
                                          StoreBranch = h.Storebranches
                                         .Where(x => x.StoreBranchId == h.MainBranchId)
                                         .Select(x => new
                                         {
                                             x.Address.AddressName,
                                             x.AddressDescription,
                                             x.PhoneNo1,
                                             x.PhoneNo2,
                                             x.Longitude,
                                             x.Latitude,
                                             StartTime = x.StartTime.ToString(@"hh\:mm\:ss"),
                                             EndTime = x.EndTime.ToString(@"hh\:mm\:ss"),
                                         }).SingleOrDefault(),
                                          farhiUser =
                                          new
                                          {
                                              h.Farhiusershallsstores.FirstOrDefault().FarhiUser.UserName,
                                              h.Farhiusershallsstores.FirstOrDefault().FarhiUser.LoginName
                                          },

                                      }).SingleOrDefaultAsync();

                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE03044", result = "لا توجد بيانات لهذه المحل " });
                }

                if (StoreData.farhiUser == null)
                {
                    return BadRequest(new { StatusCode = "RE03045", result = "لا توجد بيانات مستخدم رئيسي لهذه المحل ... يرجي مراجعة الخطأ " });
                }
                var image = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + StoreData.Logo));

                return Ok(new { StatusCode = 1,  StoreData, image } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03004", result = errorMsg });
            }
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03046", result = "الرجاء التاكد اختيار المحل " });
                }

                var StoreData = await (from h in db.Stores
                                      where h.Status != Status.Deleted
                                      && h.StoreId == id
                                      select new
                                      {
                                          h.StoreName,
                                          h.StoreTypeId,
                                          h.OwnerName,
                                          h.PhoneNo1,
                                          h.PhoneNo2,
                                          h.Email,
                                          h.Logo,
                                          Description = h.StoreType.Description,
                                          StoreBranch = h.Storebranches
                                         .Where(x => x.StoreBranchId == h.MainBranchId)
                                         .Select(x => new
                                         {
                                             x.Address.AddressId,
                                             x.AddressDescription,
                                             x.PhoneNo1,
                                             x.PhoneNo2,
                                             x.Longitude,
                                             x.Latitude,
                                             TimeRange = new[] { x.StartTime.ToString(@"hh\:mm\:ss"), x.EndTime.ToString(@"hh\:mm\:ss") },
                                         }).SingleOrDefault(),
                                      }).SingleOrDefaultAsync();

                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE03047", result = "لا توجد بيانات لهذه المحل " });
                }
                var image = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + StoreData.Logo));

                return Ok(new { StatusCode = 1, StoreData, image } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03005", result = errorMsg });
            }
        }

        [HttpPut]
        public async Task<IActionResult> edit([FromBody] StoreData StoreData)
        {
            try
            {
                if (StoreData.StoreId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03048", result = "الرجاء التاكد اختيار المحل " });
                }
                if (StoreData.StoreTypeId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03049", result = "الرجاء التاكد اختيار المحل " });
                }
                if (string.IsNullOrWhiteSpace(StoreData.StoreName))
                {
                    return BadRequest(new { StatusCode = "RE03050", result = "الرجاء التاكد من اسم المحل " });
                }
                if (StoreData.StoreName.Length > 30)
                {
                    return BadRequest(new { StatusCode = "RE03051", result = "الرجاء التاكد من طول اسم المحل " });
                }

                if (string.IsNullOrWhiteSpace(StoreData.OwnerName))
                {
                    return BadRequest(new { StatusCode = "RE03052", result = "الرجاء التاكد من ادخال اسم مالك المحل  " });
                }
                if (StoreData.OwnerName.Length > 40)
                {
                    return BadRequest(new { StatusCode = "RE03053", result = "الرجاء التاكد من طول اسم مالك المحل " });
                }

                if (string.IsNullOrWhiteSpace(StoreData.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE03054", result = "الرجاء التاكد من ادخال رقم الهاتف 1 " });
                }
                if (StoreData.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE03055", result = "الرجاء التاكد من طول رقم الهاتف 1 " });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.PhoneNo2))
                {
                    if (StoreData.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE03056", result = "الرجاء التاكد من طول رقم الهاتف 2 " });
                    }
                }
                if (string.IsNullOrWhiteSpace(StoreData.Email))
                {
                    return BadRequest(new { StatusCode = "RE03057", result = "الرجاء التاكد من ادخال بريد الكتروني للمحل " });
                }
                if (!Validation.IsValidEmail(StoreData.Email))
                {
                    return BadRequest(new { StatusCode = "RE03058", result = "الرجاء التاكد من ادخال البريد الالكتروني للمحل بشكل صحيح " });
                }
                if (StoreData.Email.Length > 50)
                {
                    return BadRequest(new { StatusCode = "RE03059", result = " الرجاء التاكد من طول البريد الالكتروني" });
                }
                if (StoreData.StoreBranch.AddressId < 0)
                {
                    return BadRequest(new { StatusCode = "RE03060", result = "الرجاء التاكد من ادخال العنوان" });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.StoreBranch.AddressDescription))
                {
                    if (StoreData.StoreBranch.AddressDescription.Length > 50)
                    {
                        return BadRequest(new { StatusCode = "RE03061", result = "الرجاء التاكد من طول وصف العنوان لايتجاوز 50 خانة" });
                    }
                }


                if (string.IsNullOrWhiteSpace(StoreData.StoreBranch.PhoneNo1))
                {
                    return BadRequest(new { StatusCode = "RE03061", result = "الرجاء التاكد من ادخال رقم هاتف 1 للفرع " });
                }
                if (StoreData.StoreBranch.PhoneNo1.Length > 10)
                {
                    return BadRequest(new { StatusCode = "RE03062", result = "الرجاء التاكد من ادخال رقم الهاتف 1 للفرع لايتجاوز عن 10 خانات" });
                }

                if (!string.IsNullOrWhiteSpace(StoreData.StoreBranch.PhoneNo2))
                {
                    if (StoreData.StoreBranch.PhoneNo2.Length > 10)
                    {
                        return BadRequest(new { StatusCode = "RE03063", result = "الرجاء التاكد ادخال رقم الهاتف 2 للفرع لايتجاوز عن 10 خانات " });
                    }
                }

                if (StoreData.StoreBranch.Longitude > 180 || StoreData.StoreBranch.Latitude < -180)
                {
                    return BadRequest(new { StatusCode = "RE03064", result = "الرجاء التاكد ادخال خط الطول لا يقل عن -180 و لايزيد عن 180 " });
                }

                if (StoreData.StoreBranch.Longitude > 90 || StoreData.StoreBranch.Latitude < -90)
                {
                    return BadRequest(new { StatusCode = "RE03065", result = "الرجاء التاكد من ادخال خط الطول لا يقل عن -90 و لايزيد عن 90 " });
                }

                



                if (!Validation.IsValidTimeFormat(StoreData.StoreBranch.TimeRange[0]))
                {
                    return BadRequest(new { StatusCode = "RE03066", result = "الرجاء التاكد من ادخال وقت البداية بشكل صحيح " });
                }

                if (!Validation.IsValidTimeFormat(StoreData.StoreBranch.TimeRange[1]))
                {
                    return BadRequest(new { StatusCode = "RE03067", result = "الرجاء التاكد من ادخال وقت النهاية بشكل صحيح " });
                }



                //==========================Password=============================//

                //==========================Logo=============================//

                if (string.IsNullOrWhiteSpace(StoreData.Logo))
                {
                    return BadRequest(new { StatusCode = "RE03068", result = "الرجاء التاكد من ادخال شعار المحل " });
                }
                //==========================Logo=============================//

                var editStoreData = await (from h in db.Stores
                                          where h.StoreId == StoreData.StoreId.Value
                                          && h.Status != Status.Deleted
                                          select h).SingleOrDefaultAsync();

                var editStoreBrach = await (from h in db.Storebranches
                                           where h.Status != Status.Deleted
                                           && h.StoreBranchId == editStoreData.MainBranchId
                                           select h).SingleOrDefaultAsync();
                if (editStoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE03069", result = "هذه المحل غير مسجلة في المنظمومة " });
                }
                if (editStoreBrach == null)
                {
                    return BadRequest(new { StatusCode = "RE03070", result = "هذه المحل غير مسجلة كفرع و لا يوجد بها اي فروع اخري " });
                }

                var checkStoreName = await (from h in db.Stores
                                           where h.StoreName == StoreData.StoreName
                                           && h.Status != Status.Deleted
                                           && h.StoreId != StoreData.StoreId.Value
                                           select h).CountAsync();
                if (checkStoreName > 0)
                {
                    return BadRequest(new { StatusCode = "RE03071", result = "اسم المحل المدخل مكرر " });
                }


                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\img", editStoreData.Logo);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }

                string attachmentFirstName = StoreData.StoreName + "_Logo";
                string attachmentName = "";
                //byte[] file = Convert.FromBase64String(StoreData.Logo.Substring(StoreData.Logo.IndexOf(",") + 1));

                attachmentName = attachmentFirstName + attachmentName + ".jpg";
                bool check = SaveAttachment(attachmentName, StoreData.Logo);
                if (!check)
                {
                    return BadRequest(new { StatusCode = "RE03072", result = "يوجد خطأ في الصورة المدخلة " });
                }



                var checkStoreEmail = await (from h in db.Stores
                                            where h.Email == StoreData.Email
                                            && h.Status != Status.Deleted
                                            && h.StoreId != StoreData.StoreId
                                            select h).CountAsync();
                if (checkStoreEmail > 0)
                {
                    return BadRequest(new { StatusCode = "RE03073", result = "البريد الالكتروني للمحل المدخل مكرر " });
                }



                editStoreData.StoreName = StoreData.StoreName;
                editStoreData.StoreTypeId = StoreData.StoreTypeId;
                editStoreData.OwnerName = StoreData.OwnerName;
                editStoreData.PhoneNo1 = StoreData.PhoneNo1;
                editStoreData.PhoneNo2 = StoreData.PhoneNo2;
                editStoreData.Email = StoreData.Email;
                editStoreData.ModifiedBy = UserId();
                editStoreData.ModifiedOn = DateTime.Now;
                editStoreData.Logo = attachmentName;

                editStoreBrach.AddressId = StoreData.StoreBranch.AddressId;
                editStoreBrach.AddressDescription = StoreData.StoreBranch.AddressDescription;
                editStoreBrach.ModifiedBy = UserId();
                editStoreBrach.ModifiedOn = DateTime.Now;
                editStoreBrach.Longitude = StoreData.StoreBranch.Longitude;
                editStoreBrach.Latitude = StoreData.StoreBranch.Latitude;
                editStoreBrach.PhoneNo1 = StoreData.StoreBranch.PhoneNo1;
                editStoreBrach.PhoneNo2 = StoreData.StoreBranch.PhoneNo2;
                editStoreBrach.StartTime = TimeOnly.Parse(StoreData.StoreBranch.TimeRange[0]);
                editStoreBrach.EndTime = TimeOnly.Parse(StoreData.StoreBranch.TimeRange[1]);

                await db.SaveChangesAsync();
                
                var store = await (from h in db.Stores
                                    where h.StoreId == StoreData.StoreId.Value
                                    select new
                                   {
                                       h.StoreId,
                                       h.StoreName,
                                       h.OwnerName,
                                       h.PhoneNo1,
                                        StoreType = h.StoreType.Description,
                                       h.CreatedByNavigation.AdminName,
                                       CreatedOn = h.CreatedOn.ToString("yyyy/MM/dd"),
                                       h.Status
                                   }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1,  message = "تم تعديل المحل بنجاح", store } );

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03006", result = errorMsg });
            }
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> lockStore(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03074", result = "الرجاء التاكد اختيار المحل " });
                }

                var StoreData = await (from h in db.Stores
                                      where h.StoreId == id
                                      && h.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE03075", result = "لا توجد بيانات لهذه المحل" });
                }

                if (StoreData.Status == Status.Locked)
                {
                    return BadRequest(new { StatusCode = "RE03076", result = "حالة هذه المحل مجمدة مسبقا" });
                }

                var checkStoreSubscription = await (from h in db.Storebranches
                                                   where
                                                   h.StoreId == id &&
                                                   h.Subscriptions
                                                   .Where(x => x.Status == Status.Active
                                                   || x.Status == Status.Locked)
                                                   .Count() > 0
                                                   select h).CountAsync();
                if (checkStoreSubscription > 0)
                {
                    return BadRequest(new { StatusCode = "RE03077", result = "لا يمكن تجميد المحل و يوجد فروع لها تحت الاشتراك" });
                }

                var StoreBranchData = await (from h in db.Storebranches
                                            where h.Status != Status.Deleted
                                            && h.StoreId == id
                                            select h).ToListAsync();
                StoreData.Status = Status.Locked;
                StoreData.ModifiedBy = UserId();
                StoreData.ModifiedOn = DateTime.Now;
                foreach (var branch in StoreBranchData)
                {
                    branch.Status = Status.Locked;
                    branch.ModifiedBy = UserId();
                    branch.ModifiedOn = DateTime.Now;
                }
                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم تجميد المحل و جميع فروعها بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03007", result = errorMsg });
            }
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> unlock(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03078", result = "الرجاء التاكد اختيار المحل " });
                }

                var StoreData = await (from h in db.Stores
                                      where h.StoreId == id
                                      && h.Status != Status.Deleted
                                      select h).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE03079", result = "لا توجد بيانات لهذه المحل" });
                }

                if (StoreData.Status == Status.Active)
                {
                    return BadRequest(new { StatusCode = "RE03080", result = "حالة هذه المحل مفعلة مسبقا" });
                }


                var StoreBranchData = await (from h in db.Storebranches
                                            where h.Status != Status.Deleted
                                            && h.StoreBranchId == StoreData.MainBranchId
                                            select h).SingleOrDefaultAsync();

                StoreData.Status = Status.Active;
                StoreData.ModifiedBy = UserId();
                StoreData.ModifiedOn = DateTime.Now;

                StoreBranchData.Status = Status.Active;
                StoreBranchData.ModifiedBy = UserId();
                StoreBranchData.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1,  message = "تم تفعيل المحل و فرعها الرئيسي بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03008", result = errorMsg });
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> deleteStore(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE03081", result = "الرجاء التاكد اختيار المحل " });
                }

                var StoreData = await (from h in db.Stores
                                      where h.StoreId == id
                                      select h).SingleOrDefaultAsync();
                if (StoreData == null)
                {
                    return BadRequest(new { StatusCode = "RE03082", result = "لا توجد بيانات لهذه المحل" });
                }

                if (StoreData.Status == Status.Deleted)
                {
                    return BadRequest(new { StatusCode = "RE03083", result = "حالة هذه المحل محذوف مسبقا" });
                }

                var checkStoreSubscription = await (from h in db.Storebranches
                                                   where
                                                   h.StoreId == id &&
                                                   h.Subscriptions
                                                   .Where(x => x.Status == Status.Active
                                                   || x.Status == Status.Locked)
                                                   .Count() > 0
                                                   select h).CountAsync();
                if (checkStoreSubscription > 0)
                {
                    return BadRequest(new { StatusCode = "RE03084", result = "لا يمكن حذف المحل و يوجد فروع لها تحت الاشتراك" });
                }

                StoreData.Status = Status.Deleted;
                StoreData.ModifiedBy = UserId();
                StoreData.ModifiedOn = DateTime.Now;

                var StoreBranchData = await (from h in db.Storebranches
                                            where h.Status != Status.Deleted
                                            && h.StoreId == id
                                            select h).ToListAsync();
                StoreData.Status = Status.Deleted;
                StoreData.ModifiedBy = UserId();
                StoreData.ModifiedOn = DateTime.Now;
                foreach (var branch in StoreBranchData)
                {
                    branch.Status = Status.Deleted;
                    branch.ModifiedBy = UserId();
                    branch.ModifiedOn = DateTime.Now;
                }

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم حذف المحل و جميع فروعها بنجاح" } );
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX03009", result = errorMsg });
            }
        }


    }
}
