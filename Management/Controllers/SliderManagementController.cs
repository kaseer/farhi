﻿using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class SliderManagement : RootController
    {
        public SliderManagement(farhiContext context) : base(context)
        {
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var query = from c in db.Slidermanagements
                where c.Status == Status.Active
                            select new
                            {
                                c.SliderManagementId,
                                c.Description,
                                c.Media.Path,
                                c.Media.MediaType
                            };
                var list = await query.ToListAsync();
                return Ok(new { StatusCode = 1, list });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX05001", message = AppMessages.ErrorMessages.ServerError });
            }
        }
    }
}
