﻿using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class FarhiAdminsController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public FarhiAdminsController(farhiContext context,  IConfiguration configuration, IMapper mapper) :base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
        {
            try
            {
                var query = from c in db.Adminusers
                            where c.Status != Status.Deleted
                            select c;
                if(search is not null)
                    query = query.Where(x => x.AdminName.Contains(search) || x.LoginName.Contains(search));

                var data = await query.Select(c => new { 
                    c.AdminId,
                    c.AdminName,
                    c.LoginName,
                    c.PhoneNo1,
                    CreatedBy = c.CreatedByNavigation.AdminName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1 , data, total});
            }
            catch(Exception ex)
            {
                return StatusCode(500, new {StatusCode="EX10001", result = errorMsg });
            }
        }

        // GET api/<FarhiAdminsController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetails(int id)
        {
            try
            {
                var features = (from p in db.Features
                                where p.Status != Status.Deleted
                                //&& db.Permissions.Where(x => x.Adminpermissions.Where(x => x.AdminId == id).Count() > 0).Count() > 0
                                select new
                                {
                                    p.FeatureName,
                                    Permissions = p.Permissions
                                    .Where(p => p.Adminpermissions.Select(x => x.AdminId).Contains(id))
                                    .Select(x => x.PermissionName)
                                }).Where(x => x.Permissions.Count() > 0).ToList();

                var data = await (from c in db.Adminusers
                                  where c.Status != Status.Deleted
                                  && c.AdminId == id
                                  select new { 
                                        c.AdminName,
                                        c.LoginName,
                                        c.Email,
                                        c.PhoneNo1,
                                        c.PhoneNo2,
                                        features
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE10001", result = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10002", result = errorMsg });
            }
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> GetForEdit(int id)
        {
            try
            {
                var data = await (from c in db.Adminusers
                                  where c.Status != Status.Deleted
                                  && c.AdminId == id
                                  select new
                                  {
                                      c.AdminId,
                                      c.AdminName,
                                      c.LoginName,
                                      c.Email,
                                      c.PhoneNo1,
                                      c.PhoneNo2,
                                      Permissions = c.AdminpermissionAdmins.Select(x => x.PermissionId).ToList()
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE10002", result = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10003", result = errorMsg });
            }
        }

        // POST api/<FarhiAdminsController>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] NewAdminUserData dataVM)
        {
            try
            {
                var check = await (from c in db.Adminusers where c.LoginName == dataVM.LoginName && c.Status != Status.Deleted select c).AnyAsync();
                if (check) return BadRequest(new { StatusCode = "RE10003", result = "اسم الدخول المدخل موجود مسبقا" });

                var user = mapper.Map<Adminuser>(dataVM, opt => opt.Items["UserId"] = UserId());
                var hashing = Security.HashPassword(dataVM.Password, configuration["Secret"]);
                user.Password = hashing.Password;
                user.Salt = hashing.Salt;

                foreach(var per in dataVM.Permissions)
                {
                    Adminpermission adminpermission = new Adminpermission() { 
                        PermissionId = per,
                        AdminId = user.AdminId,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                    };
                    user.AdminpermissionAdmins.Add(adminpermission);
                }
                db.Add(user);
                await db.SaveChangesAsync();

                var data = await (from c in db.Adminusers
                            where c.AdminId == user.AdminId
                            select new {
                                c.AdminId,
                                c.AdminName,
                                c.LoginName,
                                c.PhoneNo1,
                                CreatedBy = c.CreatedByNavigation.AdminName,
                                CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                c.Status
                            }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1 , data, message = "تمت إضافة المستخدم بنجاح"});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10004", result = errorMsg });
            }
        }

        // PUT api/<FarhiAdminsController>/5
        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] AdminUserData dataVM)
        {
            try
            {
                var check = await (from c in db.Adminusers 
                                   where c.LoginName == dataVM.LoginName 
                                   && c.Status != Status.Deleted 
                                   && c.AdminId != dataVM.AdminId
                                   select c).AnyAsync();
                if (check) return BadRequest(new { StatusCode = "RE10004", result = "اسم الدخول المدخل موجود مسبقا" });

                var user = await (from c in db.Adminusers.Include(x => x.AdminpermissionAdmins)
                            where c.Status != Status.Deleted
                            && c.AdminId == dataVM.AdminId
                            select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new {StatusCode = "RE10005", result = "لا توجد يبانات لهذا المستخدم"});
                var edit = mapper.Map(dataVM, user, opt => opt.Items["UserId"] = UserId());
                //var user = mapper.Map<Adminuser>(dataVM, opt => opt.Items["UserId"] = UserId());

                foreach (var per in user.AdminpermissionAdmins)
                    db.Adminpermissions.Remove(per);

                foreach (var per in dataVM.Permissions)
                {
                    Adminpermission adminpermission = new Adminpermission()
                    {
                        PermissionId = per,
                        AdminId = user.AdminId,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                    };
                    user.AdminpermissionAdmins.Add(adminpermission);
                }
                await db.SaveChangesAsync();

                var data = await (from c in db.Adminusers
                                  where c.AdminId == user.AdminId
                                  select new
                                  {
                                      c.AdminId,
                                      c.AdminName,
                                      c.LoginName,
                                      c.PhoneNo1,
                                      CreatedBy = c.CreatedByNavigation.AdminName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                      c.Status
                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data, message = "تمت تعديل المستخدم بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10006", result = errorMsg });
            }
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> Lock(int id)
        {
            try
            {
                var user = await (from c in db.Adminusers
                                   where c.AdminId == id
                                   && c.Status != Status.Deleted
                                   select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE10006", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
                if (user.Status == Status.Locked) return BadRequest(new { StatusCode = "RE10007", result = "حالة هذا المستخدم مقفلة مسبقا" });

                user.Status = Status.Locked;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قفل حالة المستخدم بنجاح"});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10007", result = errorMsg });
            }
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> Unlock(int id)
        {
            try
            {
                var user = await (from c in db.Adminusers
                                  where c.AdminId == id
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE10008", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
                if (user.Status == Status.Active) return BadRequest(new { StatusCode = "RE10009", result = "حالة هذا المستخدم فعالة مسبقا" });

                user.Status = Status.Active;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم تفعيل حالة المستخدم بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10008", result = errorMsg });
            }
        }
        // DELETE api/<FarhiAdminsController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                //TODO -- check ability to delete user 

                if (UserId() == id) return BadRequest(new { StatusCode = "RE10010", result = "لا يمكن حذف المستخدم مستخدم من قبل النظام حاليا" });
                var user = await (from c in db.Adminusers.Include(x => x.AdminpermissionAdmins)
                                  where c.AdminId == id
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE10011", result = " لم يتم العثور علي بيانات لهذا المستخدم" });

                user.Status = Status.Deleted;
                foreach(var c in user.AdminpermissionAdmins)
                {
                    db.Adminpermissions.Remove(c);
                }
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم حذف المستخدم بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10009", result = errorMsg });
            }
        }

        [HttpPut("{id}/resetPassword")]
        public async Task<IActionResult> ResetPassword(int id)
        {
            try
            {
                var user = await (from c in db.Adminusers
                                  where c.AdminId == id
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE10012", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
                var password = Security.GeneratePassword();

                var hashing = Security.HashPassword(password, configuration["Secret"]);
                user.Password = hashing.Password;
                user.Salt = hashing.Salt;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم إعادة تعيين كلمة مرور المستخدم بنجاح", data =  password});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10010", result = errorMsg });
            }
        }
        [HttpGet("getFeatures")]
        public async Task<IActionResult> GetFeatures()
        {
            try
            {
                var data = await (from c in db.Features
                                  where c.Permissions.Where(x => x.Adminpermissions.Where(x => x.AdminId == UserId()).Count() > 0).Count() > 0
                                  && c.Status != Status.Deleted
                                  && c.FeatureType == 1
                                  select new {
                                        c.FeatureId,
                                        c.FeatureName,
                                        checkAll = false,
                                        Permissions = c.Permissions
                                            .Where(p => p.Adminpermissions.Select(x => x.AdminId).Contains(UserId()))
                                            .Select(p => new
                                            {
                                                p.PermissionId,
                                                p.PermissionName
                                            }).ToList()
                                    }).ToListAsync();
                return Ok(new { StatusCode = 1, data});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX10011", result = errorMsg });
            }
        }
    }
}
