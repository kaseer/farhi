﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductSubcategoriesController : RootController
    {
        public ProductSubcategoriesController(farhiContext context) : base(context) {  }
        // GET: api/<ProductSubcategoriesController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] Pagination pagination, int categoryId, string search)
        {
            try
            {
                //if (categoryId < 0) return BadRequest(new { StatusCode = "65555", result = "string>" });
                var query = from c in db.Subcategories
                            where c.Status != Status.Deleted
                            //&& c.CategoryId == categoryId
                            select new {
                                ProductSubCategoryId = c.SubCategoryId,
                                c.CategoryId,
                                c.Description,
                                Category = c.Category.Description,
                                c.Status,
                                CreatedBy = c.CreatedByNavigation.AdminName,
                                CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                            };
                if(search is not null) query = from c in query where c.Description.Contains(search) select c;
                if(categoryId > 0) query = from c in query where c.CategoryId == categoryId select c;

                var ProductSubCategories = await query.Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new {StatusCode = 1, ProductSubCategories ,total});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX08001", result = errorMsg });
            }
        }


        // POST api/<ProductSubcategoriesController>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] ProductSubcategoriesData dataVM)
        {
            try
            {
                bool check = await (from c in db.Subcategories
                                    where c.Description == dataVM.Description
                                    && c.CategoryId == dataVM.CategoryId
                                    select c).AnyAsync();

                if (check) return BadRequest(new { StatusCode = "RE08001", result = "هذا النوع موجود مسبقا" });
                Subcategory productSubcategory = new Subcategory() { 
                    Description = dataVM.Description,
                    CategoryId = dataVM.CategoryId,
                    Status = 1,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                };
                db.Subcategories.Add(productSubcategory);
                await db.SaveChangesAsync();
                var data = await (from c in db.Subcategories
                                  where productSubcategory.SubCategoryId == c.SubCategoryId
                                  select new
                                  {
                                      ProductSubcategoryId = c.SubCategoryId,
                                      c.CategoryId,
                                      c.Description,
                                      Category = c.Category.Description,
                                      c.Status,
                                      CreatedBy = c.CreatedByNavigation.AdminName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                  }).SingleOrDefaultAsync();
                return Ok(new {StatusCode = 1, message = "تم اضافة نوع المنتج بنجاح", data });
            }
            catch(Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX08002", result = errorMsg });
            }
        }

        // PUT api/<ProductSubcategoriesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] ProductSubcategoriesData dataVM)
        {
            try
            {
                bool check = await(from c in db.Subcategories
                                   where c.Description == dataVM.Description
                                   && c.SubCategoryId != id
                                   && c.CategoryId == dataVM.CategoryId
                                   select c).AnyAsync();

                if (check) return BadRequest(new { StatusCode = "RE08002", result = "هذا النوع موجود مسبقا" });
                var data = await (from c in db.Subcategories where c.SubCategoryId == id select c).SingleOrDefaultAsync();
                data.Description = dataVM.Description;
                data.CategoryId = dataVM.CategoryId;
                data.ModifiedBy = UserId();
                data.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم تعديل نوع المنتج بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX08003", result = errorMsg });
            }
        }
    }
}
