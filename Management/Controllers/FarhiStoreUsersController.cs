﻿//using Common;
//using Management.ViewModels;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Configuration;
//using Models;
//using System;
//using System.Threading.Tasks;
//using Microsoft.EntityFrameworkCore;
//using System.Linq;
//using AutoMapper;


//// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

//namespace Management.Controllers
//{
//    [Route("api/[controller]")]
//    public class FarhiStoreUsersController : RootController
//    {
//        public IConfiguration configuration;
//        private readonly IMapper mapper;
//        public FarhiStoreUsersController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
//        {
//            this.configuration = configuration;
//            this.mapper = mapper;
//        }
//        // GET: api/<FarhiUsersController>
//        [HttpGet]
//        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
//        {
//            try
//            {
//                var query = from c in db.Farhiusers
//                            where c.Status != Status.Deleted
//                            select c;
//                if (search is not null)
//                    query = query.Where(x => x.UserName.Contains(search) || x.LoginName.Contains(search));

//                var data = await query.Select(c => new
//                {
//                    c.FarhiUserId,
//                    c.UserName,
//                    c.LoginName,
//                    c.PhoneNo1,
//                    CreatedBy = c.CreatedBy != null ? c.CreatedByNavigation.UserName : "-",
//                    CreatedOn = c.CreatedOn != null ? c.CreatedOn.Value.ToString("yyyy/MM/dd") : "-",
//                    AdminCreatedBy = c.AdminCreatedBy != null ? c.AdminCreatedByNavigation.AdminName : "-",
//                    AdminCreatedOn = c.AdminCreatedOn != null ? c.AdminCreatedOn.Value.ToString("yyyy/MM/dd") : "-",
//                    c.Status
//                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
//                var total = await query.CountAsync();

//                return Ok(new { StatusCode = 1, data, total });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        // GET api/<FarhiUsersController>/5
//        [HttpGet("{id}")]
//        public async Task<IActionResult> GetDetails(int id)
//        {
//            try
//            {
//                var features = (from p in db.Features
//                                where p.Status != Status.Deleted
//                                //&& db.Permissions.Where(x => x.Adminpermissions.Where(x => x.AdminId == id).Count() > 0).Count() > 0
//                                select new
//                                {
//                                    p.FeatureName,
//                                    Permissions = p.Permissions
//                                    .Where(p => p.Farhiuserpermissions.Select(x => x.UserId).Contains(id))
//                                    .Select(x => x.PermissionName)
//                                }).Where(x => x.Permissions.Count() > 0).ToList();

//                var data = await (from c in db.Farhiusers
//                                  where c.Status != Status.Deleted
//                                  && c.FarhiUserId == id
//                                  select new
//                                  {
//                                      c.UserName,
//                                      c.LoginName,
//                                      c.Email,
//                                      c.PhoneNo1,
//                                      c.PhoneNo2,
//                                      stores = c.Farhiusershallsstores.Select(x => new
//                                      {
//                                          x.Store.StoreName,
//                                          branches = x.Farhiusersstorebranches.Select(z => "فرع " + z.Branch.Address.AddressName).ToList()
//                                      }).ToList(),
//                                      features
//                                  }).SingleOrDefaultAsync();
//                if (data is null) return BadRequest(new { StatusCode = "RE0000", result = "لا توجد يبانات لهذا المستخدم" });
//                return Ok(new { StatusCode = 1, data });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        [HttpGet("{id}/getForEdit")]
//        public async Task<IActionResult> GetForEdit(int id)
//        {
//            try
//            {
//                var data = await (from c in db.Farhiusers
//                                  where c.Status != Status.Deleted
//                                  && c.FarhiUserId == id
//                                  select new
//                                  {
//                                      c.FarhiUserId,
//                                      c.UserName,
//                                      c.LoginName,
//                                      c.Email,
//                                      c.PhoneNo1,
//                                      c.PhoneNo2,
//                                      stores = c.Farhiusershallsstores.Select(x => new UserData()
//                                      {
//                                          Id = x.StoreId,
//                                          Branches = x.Farhiusersstorebranches.Select(z => z.BranchId).ToList()
//                                      }).ToList(),
//                                      Permissions = c.Farhiuserpermissions.Select(x => x.PermissionId).ToList(),
//                                  }).SingleOrDefaultAsync();
//                if (data is null) return BadRequest(new { StatusCode = "RE0000", result = "لا توجد يبانات لهذا المستخدم" });
//                return Ok(new { StatusCode = 1, data });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        // POST api/<FarhiUsersController>
//        [HttpPost]
//        public async Task<IActionResult> Add([FromBody] NewFarhiStoreUserData dataVM)
//        {
//            try
//            {
//                var check = await (from c in db.Farhiusers where c.LoginName == dataVM.LoginName && c.Status != Status.Deleted select c).AnyAsync();
//                if (check) return BadRequest(new { StatusCode = "RE2222", result = "اسم الدخول المدخل موجود مسبقا" });

//                var user = mapper.Map<Farhiuser>(dataVM, opt => opt.Items["UserId"] = UserId());
//                var hashing = Security.HashPassword(dataVM.Password, configuration["Secret"]);
//                user.Password = hashing.Password;
//                user.Salt = hashing.Salt;

//                foreach (var store in dataVM.Stores)
//                {
//                    Farhiusershallsstore userstores = new Farhiusershallsstore()
//                    {
//                        FarhiUserId = user.FarhiUserId,
//                        StoreId = store.Id,
//                        CreatedOn = DateTime.Now,
//                    };
//                    user.Farhiusershallsstores.Add(userstores);

//                    if (store.Branches.Count == 0)
//                    {
//                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
//                        {
//                            BranchId = (from c in db.Stores where c.StoreId == store.Id select c.MainBranchId.Value).SingleOrDefault(),
//                            Id = userstores.Id,
//                            CreatedOn = DateTime.Now
//                        };
//                        userstores.Farhiusersstorebranches.Add(userbranches);
//                    }

//                    foreach (var branch in store.Branches)
//                    {
//                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
//                        {
//                            BranchId = branch,
//                            Id = userstores.Id,
//                            CreatedOn = DateTime.Now
//                        };
//                        userstores.Farhiusersstorebranches.Add(userbranches);
//                    }
//                }
//                //}
//                foreach (var per in dataVM.Permissions)
//                {
//                    Farhiuserpermission adminpermission = new Farhiuserpermission()
//                    {
//                        PermissionId = per,
//                        UserId = user.FarhiUserId,
//                        CreatedBy = UserId(),
//                        CreatedOn = DateTime.Now,
//                    };
//                    user.Farhiuserpermissions.Add(adminpermission);
//                }
//                db.Add(user);
//                await db.SaveChangesAsync();

//                var data = await (from c in db.Farhiusers
//                                  where c.FarhiUserId == user.FarhiUserId
//                                  select new
//                                  {
//                                      c.FarhiUserId,
//                                      c.UserName,
//                                      c.LoginName,
//                                      c.PhoneNo1,
//                                      CreatedBy = c.CreatedBy != null ? c.CreatedByNavigation.UserName : "-",
//                                      CreatedOn = c.CreatedOn != null ? c.CreatedOn.Value.ToString("yyyy/MM/dd") : "-",
//                                      AdminCreatedBy = c.AdminCreatedBy != null ? c.AdminCreatedByNavigation.AdminName : "-",
//                                      AdminCreatedOn = c.AdminCreatedOn != null ? c.AdminCreatedOn.Value.ToString("yyyy/MM/dd") : "-",
//                                      c.Status
//                                  }).SingleOrDefaultAsync();
//                return Ok(new { StatusCode = 1, data, message = "تمت إضافة المستخدم بنجاح" });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        // PUT api/<FarhiUsersController>/5
//        [HttpPut]
//        public async Task<IActionResult> Edit([FromBody] FarhiStoreUserData dataVM)
//        {
//            try
//            {
//                var check = await (from c in db.Farhiusers
//                                   where c.LoginName == dataVM.LoginName
//                                   && c.Status != Status.Deleted
//                                   && c.FarhiUserId != dataVM.FarhiUserId
//                                   select c).AnyAsync();
//                if (check) return BadRequest(new { StatusCode = "RE2222", result = "اسم الدخول المدخل موجود مسبقا" });

//                var user = await (from c in db.Farhiusers
//                                  .Include(x => x.Farhiuserpermissions)
//                                  .Include(x => x.Farhiusershallsstores)
//                                  where c.Status != Status.Deleted
//                                  && c.FarhiUserId == dataVM.FarhiUserId
//                                  select c).SingleOrDefaultAsync();
//                if (user is null) return BadRequest(new { StatusCode = "RE0000", result = "لا توجد يبانات لهذا المستخدم" });
//                var edit = mapper.Map(dataVM, user, opt => opt.Items["UserId"] = UserId());
//                //var user = mapper.Map<Adminuser>(dataVM, opt => opt.Items["UserId"] = UserId());

//                foreach (var store in user.Farhiusershallsstores)
//                {
//                    var remove = await (from c in db.Farhiusersstorebranches where c.Id == store.Id select c).ToListAsync();
//                    foreach (var item in remove)
//                    {
//                        db.Farhiusersstorebranches.Remove(item);
//                    }
//                    db.Farhiusershallsstores.Remove(store);
//                }

//                foreach (var store in dataVM.Stores)
//                {
//                    Farhiusershallsstore userstores = new Farhiusershallsstore()
//                    {
//                        FarhiUserId = user.FarhiUserId,
//                        StoreId = store.Id,
//                        CreatedOn = DateTime.Now,
//                    };
//                    user.Farhiusershallsstores.Add(userstores);

//                    if (store.Branches.Count == 0)
//                    {
//                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
//                        {
//                            BranchId = (from c in db.Stores where c.StoreId == store.Id select c.MainBranchId.Value).SingleOrDefault(),
//                            Id = userstores.Id,
//                            CreatedOn = DateTime.Now
//                        };
//                        userstores.Farhiusersstorebranches.Add(userbranches);
//                    }

//                    foreach (var branch in store.Branches)
//                    {
//                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
//                        {
//                            BranchId = branch,
//                            Id = userstores.Id,
//                            CreatedOn = DateTime.Now
//                        };
//                        userstores.Farhiusersstorebranches.Add(userbranches);
//                    }
//                }


//                foreach (var per in user.Farhiuserpermissions)
//                    db.Farhiuserpermissions.Remove(per);

//                foreach (var per in dataVM.Permissions)
//                {
//                    Farhiuserpermission userpermission = new Farhiuserpermission()
//                    {
//                        PermissionId = per,
//                        UserId = user.FarhiUserId,
//                        CreatedBy = UserId(),
//                        CreatedOn = DateTime.Now,
//                    };
//                    user.Farhiuserpermissions.Add(userpermission);
//                }
//                await db.SaveChangesAsync();

//                var data = await (from c in db.Farhiusers
//                                  where c.FarhiUserId == user.FarhiUserId
//                                  select new
//                                  {
//                                      c.FarhiUserId,
//                                      c.UserName,
//                                      c.LoginName,
//                                      c.PhoneNo1,
//                                      CreatedBy = c.CreatedBy != null ? c.CreatedByNavigation.UserName : "-",
//                                      CreatedOn = c.CreatedOn != null ? c.CreatedOn.Value.ToString("yyyy/MM/dd") : "-",
//                                      AdminCreatedBy = c.AdminCreatedBy != null ? c.AdminCreatedByNavigation.AdminName : "-",
//                                      AdminCreatedOn = c.AdminCreatedOn != null ? c.AdminCreatedOn.Value.ToString("yyyy/MM/dd") : "-",
//                                      c.Status
//                                  }).SingleOrDefaultAsync();
//                return Ok(new { StatusCode = 1, data, message = "تم تعديل المستخدم بنجاح" });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        [HttpPut("{id}/lock")]
//        public async Task<IActionResult> Lock(int id)
//        {
//            try
//            {
//                var user = await (from c in db.Farhiusers
//                                  where c.FarhiUserId == id
//                                  && c.Status != Status.Deleted
//                                  select c).SingleOrDefaultAsync();
//                if (user is null) return BadRequest(new { StatusCode = "RE2222", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
//                if (user.Status == Status.Locked) return BadRequest(new { StatusCode = "RE2222", result = "حالة هذا المستخدم مقفلة مسبقا" });

//                user.Status = Status.Locked;
//                await db.SaveChangesAsync();

//                return Ok(new { StatusCode = 1, message = "تم قفل حالة المستخدم بنجاح" });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        [HttpPut("{id}/unlock")]
//        public async Task<IActionResult> Unlock(int id)
//        {
//            try
//            {
//                var user = await (from c in db.Farhiusers
//                                  where c.FarhiUserId == id
//                                  && c.Status != Status.Deleted
//                                  select c).SingleOrDefaultAsync();
//                if (user is null) return BadRequest(new { StatusCode = "RE2222", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
//                if (user.Status == Status.Active) return BadRequest(new { StatusCode = "RE2222", result = "حالة هذا المستخدم فعالة مسبقا" });

//                user.Status = Status.Active;
//                await db.SaveChangesAsync();

//                return Ok(new { StatusCode = 1, message = "تم تفعيل حالة المستخدم بنجاح" });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }
//        // DELETE api/<FarhiUsersController>/5
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> Delete(int id)
//        {
//            try
//            {
//                //TODO -- check ability to delete user 
//                //if(UserId() == id) return BadRequest(new { StatusCode = "RE0000", result = "لا يمكن حذف المستخدم مستخدم من قبل النظام حاليا" });
//                var user = await (from c in db.Farhiusers
//                                  .Include(x => x.Farhiuserpermissions)
//                                  .Include(x => x.Farhiusershallsstores)
//                                  where c.FarhiUserId == id
//                                  && c.Status != Status.Deleted
//                                  select c).SingleOrDefaultAsync();
//                if (user is null) return BadRequest(new { StatusCode = "RE2222", result = " لم يتم العثور علي بيانات لهذا المستخدم" });

//                user.Status = Status.Deleted;
//                foreach (var c in user.Farhiuserpermissions)
//                {
//                    db.Farhiuserpermissions.Remove(c);
//                }
//                foreach (var store in user.Farhiusershallsstores)
//                {
//                    var remove = await (from c in db.Farhiusersstorebranches where c.Id == store.Id select c).ToListAsync();
//                    foreach (var item in remove)
//                    {
//                        db.Farhiusersstorebranches.Remove(item);
//                    }
//                    db.Farhiusershallsstores.Remove(store);
//                }
//                await db.SaveChangesAsync();

//                return Ok(new { StatusCode = 1, message = "تم حذف المستخدم بنجاح" });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }

//        [HttpPut("{id}/resetPassword")]
//        public async Task<IActionResult> ResetPassword(int id)
//        {
//            try
//            {
//                var user = await (from c in db.Farhiusers
//                                  where c.FarhiUserId == id
//                                  && c.Status != Status.Deleted
//                                  select c).SingleOrDefaultAsync();
//                if (user is null) return BadRequest(new { StatusCode = "RE2222", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
//                var password = Security.GeneratePassword();

//                var hashing = Security.HashPassword(password, configuration["Secret"]);
//                user.Password = hashing.Password;
//                user.Salt = hashing.Salt;

//                await db.SaveChangesAsync();
//                return Ok(new { StatusCode = 1, message = "تم إعادة تعيين كلمة مرور المستخدم بنجاح", data = password });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }
//        [HttpGet("getFeatures")]
//        public async Task<IActionResult> GetFeatures()
//        {
//            try
//            {
//                var data = await (from c in db.Features
//                                  where c.Status != Status.Deleted
//                                  && c.FeatureType == 2
//                                  select new
//                                  {
//                                      c.FeatureId,
//                                      c.FeatureName,
//                                      checkAll = false,
//                                      Permissions = c.Permissions
//                                          .Select(p => new
//                                          {
//                                              p.PermissionId,
//                                              p.PermissionName
//                                          }).ToList()
//                                  }).ToListAsync();
//                return Ok(new { StatusCode = 1, data });
//            }
//            catch (Exception ex)
//            {
//                return StatusCode(500, new { StatusCode = "EX0000", result = errorMsg });
//            }
//        }
//    }
//}
