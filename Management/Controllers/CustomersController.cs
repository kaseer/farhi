﻿using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : RootController
    {
        public IConfiguration configuration;
        public CustomersController(farhiContext context,  IConfiguration configuration) :base(context)
        {
            this.configuration = configuration;
        }
        // GET: api/<CustomersController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
        {
            try
            {
                var query = from c in db.Customers
                            where c.Status != Status.Deleted
                            select c;
                if(search is not null)
                    query = query.Where(x => x.CustomerName.Contains(search) || x.LoginName.Contains(search));

                var data = await query.Select(c => new { 
                    c.CustomerId,
                    c.CustomerName,
                    c.LoginName,
                    c.PhoneNo1,
                    CreatedBy = c.CreatedByNavigation.UserName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1 , data, total});
            }
            catch(Exception ex)
            {
                return StatusCode(500, new {StatusCode="EX12001", result = errorMsg });
            }
        }

        // GET api/<CustomersController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetails(int id)
        {
            try
            {

                var data = await (from c in db.Customers
                                  where c.Status != Status.Deleted
                                  && c.CustomerId == id
                                  select new { 
                                        c.CustomerName,
                                        c.LoginName,
                                        c.Email,
                                        c.PhoneNo1,
                                        c.PhoneNo2,
                                        c.Status,
                                        c.Address.AddressName,
                                        c.AddressDescription,
                                        HallReservations = c.Hallreservations.Select(x => new { 
                                            x.HallReservationId,
                                            x.ReservationDate,
                                            Hall = x.HallBranch.Hall.HallName + " -  فرع " + x.HallBranch.Address.AddressName + " - " + x.HallBranch.AddressDescription,
                                            x.Status,
                                            total = x.Hallreservationdetails.Select(p => p.Price).Count()
                                        }),
                                      ProductReservations = c.Productreservations.Select(x => new {
                                          x.ProductReservationId,
                                          Product = x.Product.Description,
                                          x.Status,
                                          total = x.Price
                                      }),
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE12001", result = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX12002", result = errorMsg });
            }
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> Lock(int id)
        {
            try
            {
                var customer = await (from c in db.Customers
                                   where c.CustomerId == id
                                   && c.Status != Status.Deleted
                                   select c).SingleOrDefaultAsync();
                if (customer is null) return BadRequest(new { StatusCode = "RE12002", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
                if (customer.Status == Status.Locked) return BadRequest(new { StatusCode = "RE12003", result = "حالة هذا المستخدم مقفلة مسبقا" });

                customer.Status = Status.Locked;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قفل حالة المستخدم بنجاح"});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX12003", result = errorMsg });
            }
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> Unlock(int id)
        {
            try
            {
                var customer = await (from c in db.Customers
                                      where c.CustomerId == id
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (customer is null) return BadRequest(new { StatusCode = "RE12004", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
                if (customer.Status == Status.Active) return BadRequest(new { StatusCode = "RE12005", result = "حالة هذا المستخدم فعالة مسبقا" });

                customer.Status = Status.Active;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم تفعيل حالة المستخدم بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX12004", result = errorMsg });
            }
        }
        [HttpPut("{id}/resetPassword")]
        public async Task<IActionResult> ResetPassword(int id)
        {
            try
            {
                var customer = await (from c in db.Customers
                                      where c.CustomerId == id
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (customer is null) return BadRequest(new { StatusCode = "RE12006", result = " لم يتم العثور علي بيانات لهذا المستخدم" });
                var password = Security.GeneratePassword();

                var hashing = Security.HashPassword(password, configuration["Secret"]);
                customer.Password = hashing.Password;
                customer.Salt = hashing.Salt;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم إعادة تعيين كلمة مرور المستخدم بنجاح", data =  password});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX12005", result = errorMsg });
            }
        }
    }
}
