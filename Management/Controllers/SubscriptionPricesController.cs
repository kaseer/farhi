﻿using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class SubscriptionPricesController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public SubscriptionPricesController(farhiContext context,  IConfiguration configuration, IMapper mapper) :base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<SubscriptionPricesController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
        {
            try
            {
                var query = from c in db.Subscriptionprices
                            where c.Status != Status.Deleted
                            select c;
                if(search is not null)
                    query = query.Where(x => x.Description.Contains(search));

                var data = await query.Select(c => new { 
                    c.SubscriptionPriceId,
                    c.Description,
                    Type = c.Type == 1 ? "إشتراك صالات" : c.Type == 2 ? "إشتراك محلات" : "نوع اخر",
                    CreatedBy = c.CreatedByNavigation.AdminName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1 , data, total});
            }
            catch(Exception ex)
            {
                return StatusCode(500, new {StatusCode="EX13001", result = errorMsg });
            }
        }

        // GET api/<SubscriptionPricesController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetails(int id)
        {
            try
            {

                var data = await (from c in db.Subscriptionprices
                                  where c.Status != Status.Deleted
                                  && c.SubscriptionPriceId == id
                                  select new { 
                                        c.Description,
                                        Type = c.Type == 1 ? "إشتراك صالات" : c.Type == 2 ? "إشتراك محلات" : "نوع اخر",
                                        subscriptionDetails = c.Subscriptionpricedetails.Select(x => new { 
                                            x.DurationTime,
                                            DurationType = x.DurationTypeNavigation.DurationTypeName,
                                            x.Price,
                                            CreatedBy = x.CreatedByNavigation.AdminName,
                                            CreatedOn = x.CreatedOn.ToString("yyyy/MM/dd")
                                        }).ToList()
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE13001", result = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX13002", result = errorMsg });
            }
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> GetForEdit(int id)
        {
            try
            {
                var data = await (from c in db.Subscriptionprices
                                  where c.Status != Status.Deleted
                                  && c.SubscriptionPriceId == id
                                  select new
                                  {
                                      c.SubscriptionPriceId,
                                      c.Description,
                                      c.Type,
                                      subscriptionDetails = c.Subscriptionpricedetails
                                            .Select(x => new { 
                                                x.SubscriptionPriceDetailsId,
                                                x.DurationTime,
                                                x.DurationType,
                                                x.Price,
                                            }).ToList()
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE13002", result = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX13003", result = errorMsg });
            }
        }

        // POST api/<SubscriptionPricesController>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] SubscriptionData dataVM)
        {
            try
            {
                var check = await (from c in db.Subscriptionprices where c.Description == dataVM.Description && c.Status != Status.Deleted select c).AnyAsync();
                if (check) return BadRequest(new { StatusCode = "RE13003", result = "وصف الاشتراك المدخل موجود مسبقا" });

                var subscription = new Subscriptionprice() { 
                    Description = dataVM.Description,
                    Type = dataVM.Type,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    Status = Status.Active,
                };
                foreach(var per in dataVM.SubscriptionDetails)
                {
                    Subscriptionpricedetail details = new Subscriptionpricedetail() { 
                        DurationTime = per.DurationTime,
                        DurationType = per.DurationType,
                        Price = per.Price,
                        SubscriptionPriceId = subscription.SubscriptionPriceId,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                    };
                    subscription.Subscriptionpricedetails.Add(details);
                }
                db.Subscriptionprices.Add(subscription);
                await db.SaveChangesAsync();

                var data = await (from c in db.Subscriptionprices
                            where c.SubscriptionPriceId == subscription.SubscriptionPriceId
                            select new {
                                c.SubscriptionPriceId,
                                c.Description,
                                Type = c.Type == 1 ? "إشتراك صالات" : c.Type == 2 ? "إشتراك محلات" : "نوع اخر",
                                CreatedBy = c.CreatedByNavigation.AdminName,
                                CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                c.Status
                            }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1 , data, message = "تمت إضافة المستخدم بنجاح"});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX13004", result = errorMsg });
            }
        }

        // PUT api/<SubscriptionPricesController>/5
        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] SubscriptionData dataVM)
        {
            try
            {
                var check = await (from c in db.Subscriptionprices 
                                   where c.Description == dataVM.Description 
                                   && c.Status != Status.Deleted 
                                   && c.SubscriptionPriceId != dataVM.SubscriptionPriceId
                                   select c).AnyAsync();
                if (check) return BadRequest(new { StatusCode = "RE13004", result = "هذا النوع المدخل موجود مسبقا" });

                var subscription = await (from c in db.Subscriptionprices.Include(x => x.Subscriptionpricedetails)
                            where c.Status != Status.Deleted
                            && c.SubscriptionPriceId == dataVM.SubscriptionPriceId
                            select c).SingleOrDefaultAsync();
                if (subscription is null) return BadRequest(new {StatusCode = "RE13005", result = "لا توجد يبانات لهذا الاشتراك"});

                subscription.Description = dataVM.Description;
                subscription.Type = dataVM.Type;

                foreach (var per in dataVM.SubscriptionDetails)
                {
                    var checkdetails = subscription.Subscriptionpricedetails.Select(x => x.SubscriptionPriceDetailsId).Contains(per.SubscriptionPriceDetailsId);
                    if (!checkdetails)
                    {
                        Subscriptionpricedetail details = new Subscriptionpricedetail()
                        {
                            SubscriptionPriceId = dataVM.SubscriptionPriceId,
                            DurationTime = per.DurationTime,
                            DurationType = per.DurationType,
                            Price = per.Price,
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                        };
                        subscription.Subscriptionpricedetails.Add(details);
                    }
                }

                foreach (var per in subscription.Subscriptionpricedetails)
                {
                    var checkdetails = dataVM.SubscriptionDetails.Select(x => x.SubscriptionPriceDetailsId).Contains(per.SubscriptionPriceDetailsId);
                    if (!checkdetails)
                    {
                        checkdetails = await (from c in db.Subscriptiondetails
                                                  where c.SubscriptionDetailsId == per.SubscriptionPriceDetailsId
                                                  select c).AnyAsync();
                        if(checkdetails) return BadRequest(new { StatusCode = "RE13006", result = "لا يمكن حذف بيانات اشتراك لديها تسجيل مسبق لأحد الزبائن" });

                        db.Subscriptionpricedetails.Remove(per);
                    }
                }
                    

                
                await db.SaveChangesAsync();

                var data = await (from c in db.Subscriptionprices
                                  where c.SubscriptionPriceId == subscription.SubscriptionPriceId
                                  select new
                                  {
                                      c.SubscriptionPriceId,
                                      c.Description,
                                      Type = c.Type == 1 ? "إشتراك صالات" : c.Type == 2 ? "إشتراك محلات" : "نوع اخر",
                                      CreatedBy = c.CreatedByNavigation.AdminName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                      c.Status
                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data, message = "تمت تعديل الاشتراك بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX13005", result = errorMsg });
            }
        }

        // DELETE api/<SubscriptionPricesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                //TODO -- check ability to delete user 

                if (UserId() == id) return BadRequest(new { StatusCode = "RE13007", result = "لا يمكن حذف اشتراك مستخدم من قبل النظام حاليا" });
                var subscription = await (from c in db.Subscriptionprices.Include(x => x.Subscriptionpricedetails)
                                  where c.SubscriptionPriceId == id
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (subscription is null) return BadRequest(new { StatusCode = "RE13008", result = " لم يتم العثور علي بيانات لهذا المستخدم" });

                subscription.Status = Status.Deleted;
                foreach(var s in subscription.Subscriptionpricedetails)
                {
                    var check = await (from c in db.Subscriptiondetails
                                       where c.SubscriptionPriceDetailsId == s.SubscriptionPriceDetailsId
                                       && c.Status != Status.Deleted
                                       select c).AnyAsync();
                    if (check) return BadRequest(new { StatusCode = "", result = "لا يمكن حذف اشتراك مستخدم من قبل النظام حاليا" });
                    db.Subscriptionpricedetails.Remove(s);
                }
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم حذف الاشتراك بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = "EX13006", result = errorMsg });
            }
        }
    }
}
