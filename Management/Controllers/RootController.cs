﻿using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Linq;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class RootController : Controller
    {
        public farhiContext db;

        public RootController(farhiContext context)
        {
            db = context;
        }

        public int UserId()
        {
            var claims = HttpContext.User.Claims.ToList();
            int userId = Convert.ToInt32(claims.Where(p => p.Type == "UserId").Select(p => p.Value).SingleOrDefault());
            return 1;
        }

        public string errorMsg = "خطا في الخادم , يرجي التاكد من حالة الخادم!";
    }
}
