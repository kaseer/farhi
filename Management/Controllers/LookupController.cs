﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Management.Controllers
{
    [Route("api/[controller]")]
    public class LookupController : RootController
    {

        public LookupController(farhiContext context) : base(context)
        {
        }
        [HttpGet("getAddresses")]
        public async Task<IActionResult> getAddresses()
        {
            try
            {
                var addresses = await (from a in db.Addresses
                                       where a.Status != Status.Deleted
                                       select new { 
                                            a.AddressId,
                                            a.AddressName
                                       }).ToListAsync();
                return Ok(new { StatusCode = 1,  addresses });
            }
            catch(Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("getHalls")]
        public async Task<IActionResult> getHalls()
        {
            try
            {
                var halls = await (from a in db.Halls
                                       where a.Status != Status.Deleted
                                       select new
                                       {
                                           a.HallId,
                                           a.HallName
                                       }).ToListAsync();
                return Ok(new { StatusCode = 1,  halls }) ;
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }
        [HttpGet("getReservationTypes")]
        public async Task<IActionResult> getReservationTypes()
        {
            try
            {
                var ReservationTypes = await (from a in db.Reservationtypes
                                   where a.Status != Status.Deleted
                                   select new
                                   {
                                       a.ReservationTypeId,
                                       a.Description
                                   }).ToListAsync();
                return Ok(new { StatusCode = 1,  ReservationTypes });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }
        [HttpGet("getSubscriptionPrices")]
        public async Task<IActionResult> getSubscriptionPrices(int type)
        {
            try
            {
                if (type <= 0)
                {
                    return BadRequest(new { StatusCode = 999999, result = errorMsg });
                }
                var SubscriptionPrices = await (from s in db.Subscriptionprices
                                                where s.Status == Status.Active
                                                && (s.Type == type || s.Type == 3)
                                                select new { 
                                                    s.SubscriptionPriceId,
                                                    s.Description
                                                }).ToListAsync();
                return Ok(new { StatusCode = 1,  SubscriptionPrices });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("getSubscriptionPricesDetails")]
        public async Task<IActionResult> getSubscriptionPricesDetails(int subscriptionPriceId)
        {
            try
            {
                if (subscriptionPriceId <= 0)
                {
                    return BadRequest(new { StatusCode = 999999, result = errorMsg });
                }
                var SubscriptionPricesDetails = await (from s in db.Subscriptionpricedetails
                                                where s.SubscriptionPrice.Status == Status.Active
                                                && s.SubscriptionPriceId == subscriptionPriceId
                                                       select new
                                                {
                                                    s.SubscriptionPriceDetailsId,
                                                    s.DurationTime,
                                                    s.DurationTypeNavigation.DurationTypeName,
                                                    s.Price
                                                }).ToListAsync();
                return Ok(new { StatusCode = 1,  SubscriptionPricesDetails });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("getStoreTypes")]
        public async Task<IActionResult> getStoreTypes()
        {
            try
            {
                var StoreTypes = await (from a in db.Storetypes
                                       where a.Status != Status.Deleted
                                       select new
                                       {
                                           a.StoreTypeId,
                                           a.Description
                                       }).ToListAsync();
                return Ok(new { StatusCode = 1,  StoreTypes });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("getStores")]
        public async Task<IActionResult> getStores()
        {
            try
            {
                var stores = await (from a in db.Stores
                                   where a.Status != Status.Deleted
                                   select new
                                   {
                                       a.StoreId,
                                       a.StoreName
                                   }).ToListAsync();
                return Ok(new { StatusCode = 1,  stores });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("getCategories")]
        public async Task<IActionResult> getCategories()
        {
            try
            {
                //List<Password> pp= new List<Password>();
                //List<bool> pps= new List<bool>();
                var password = Security.HashPassword("mahmood2017", "==+N@qrA_Xpx==}2@0?==");
                var cc = Security.DecryptPassword(password.Password, givenPassword: "mahmood2017", key: "==+N@qrA_Xpx==}2@0?==", salt: "trhfghf");
                var dd = password.Password.Length;
                var dsd = password.Salt.Length;
                var dsdd = password.Salt.Length;
                //var password2 = Security.HashPassword("mahmood2017", "==+N@qrA_Xpx==}2@0?==");
                //var password3 = Security.HashPassword("mahmood2017", "==+N@qrA_Xpx==}2@0?==");
                //pp.Add(password);
                //pp.Add(password2);
                //pp.Add(password3);

                //foreach (var x in pp)
                //{
                //    var cc = Security.DecryptPassword(x.Hash, givenPassword: "mahmood2017", key: "==+N@qrA_Xpx==}2@0?==", salt: x.Salt);
                //}
                var categories = await (from a in db.Categories
                                   where a.Status != Status.Deleted
                                   select new
                                   {
                                       value = a.CategoryId,
                                       label = a.Description
                                   }).ToListAsync();
                return Ok(new { StatusCode = 1, categories });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("{id}/getHallBranches")]
        public async Task<IActionResult> getHallBranches(int id)
        {
            try
            {
                var HallBranches = await (from a in db.Hallbranches
                                          where a.HallId == id
                                          where a.Status != Status.Deleted
                                          select new
                                          {
                                              Value = a.HallBranchId,
                                              Label = "فرع " + a.Address.AddressName + " - " + a.AddressDescription,
                                              a.HallId
                                          }).ToListAsync();
                return Ok(new { StatusCode = 1, HallBranches });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }
        [HttpGet("{id}/getStoreBranches")]
        public async Task<IActionResult> getStoreBranches(int id)
        {
            try
            {
                var storeBranches = await (from a in db.Storebranches
                                           where a.StoreId == id
                                           where a.Status != Status.Deleted
                                           select new
                                           {
                                               Value = a.StoreBranchId,
                                               Label = "فرع " + a.Address.AddressName + " - " + a.AddressDescription
                                           }).ToListAsync();
                return Ok(new { StatusCode = 1, storeBranches });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }

        [HttpGet("getSubscriptionPrices")]
        public async Task<IActionResult> getSubscriptionPrices()
        {
            try
            {
                var subscriptionPrices = await (from a in db.Subscriptionprices
                                           where a.Status != Status.Deleted
                                           select new
                                           {
                                               Value = a.SubscriptionPriceId,
                                               Label = a.Description
                                           }).ToListAsync();
                return Ok(new { StatusCode = 1, subscriptionPrices });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode = 999999, result = errorMsg });
            }
        }
    }
}
