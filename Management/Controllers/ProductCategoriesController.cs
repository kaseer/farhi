﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCategoriesController : RootController
    {
        public ProductCategoriesController(farhiContext context) : base(context) {  }
        // GET: api/<ProductCategoriesController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] Pagination pagination, string search)
        {
            try
            {
                var query = from c in db.Categories
                            where c.Status != Status.Deleted
                            select new {
                                ProductCategoryId = c.CategoryId,
                                c.Description,
                                c.Status,
                                CreatedBy = c.CreatedByNavigation.AdminName,
                                CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                            };
                if(search is not null) query = from c in query where c.Description.Contains(search) select c;

                var ProductCategories = await query.Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new {StatusCode = 1, ProductCategories ,total});
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX07001", result = errorMsg });
            }
        }


        // POST api/<ProductCategoriesController>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] ProductCategoriesData dataVM)
        {
            try
            {
                bool check = await (from c in db.Categories
                                    where c.Description == dataVM.Description
                                    select c).AnyAsync();

                if (check) return BadRequest(new { StatusCode =  "RE07001", Message = "هذا النوع موجود مسبقا" });
                Category productCategory = new Category() { 
                    Description = dataVM.Description,
                    Status = 1,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                };
                db.Categories.Add(productCategory);
                await db.SaveChangesAsync();
                var data = await (from c in db.Categories
                                  where productCategory.CategoryId == c.CategoryId
                                  select new
                                  {
                                      c.CategoryId,
                                      c.Description,
                                      c.Status,
                                      CreatedBy = c.CreatedByNavigation.AdminName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                  }).SingleOrDefaultAsync();
                return Ok(new {StatusCode = 1, message = "تم اضافة نوع المنتج بنجاح", data });
            }
            catch(Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX07002", result = errorMsg });
            }
        }

        // PUT api/<ProductCategoriesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] ProductCategoriesData dataVM)
        {
            try
            {
                bool check = await(from c in db.Categories
                                   where c.Description == dataVM.Description
                                   && c.CategoryId != id
                                   select c).AnyAsync();

                if (check) return BadRequest(new { StatusCode = "RE07002", Message = "هذا النوع موجود مسبقا" });
                var data = await (from c in db.Categories where c.CategoryId == id select c).SingleOrDefaultAsync();
                data.Description = dataVM.Description;
                data.ModifiedBy = UserId();
                data.ModifiedOn = DateTime.Now;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم تعديل نوع المنتج بنجاح" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { StatusCode =  "EX07003", result = errorMsg });
            }
        }
    }
}
