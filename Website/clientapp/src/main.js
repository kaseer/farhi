import Vue from 'vue';
import App from './App.vue';
// import ElementUI from 'element-ui';
// import locale from 'element-ui/lib/index.js';
// import Quasar from 'quasar';
// import locale from 'quasar/lang/ar.js';
import Vuesax from 'vuesax'

import 'vuesax/dist/vuesax.css' //Vuesax styles
import 'material-icons/iconfont/material-icons.css';

Vue.use(Vuesax, {
  // options here
})
Vue.use(Vuesax, {
  theme:{
    colors:{
      primary:'#5b3cc4',
      success:'rgb(23, 201, 100)',
      danger:'rgb(242, 19, 93)',
      warning:'rgb(255, 130, 0)',
      dark:'rgb(36, 33, 69)'
    }
  }
})
import router from './router.js';
import dataService from './shared/DataService';
// import BlockUIService from './shared/BlockUIService.js';
import './styles/Site.scss';
// Vue.use(Quasar, { locale });

// this.$vs.theme({
//   primary:'rgb(5, 173, 88)' // my new color
// })

Vue.prototype.$http = dataService;
// Vue.prototype.$blockUI = BlockUIService;

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
