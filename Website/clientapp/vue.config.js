module.exports = {
    outputDir: "../wwwroot",
    filenameHashing: false,
    devServer:{
        port:8080,
        https:false,
        // proxy:{
        //     '^api':{
        //         target:'https://localhost:5003/'
        //     }
        // },
        
    },
    chainWebpack: config => {
        config.module
          .rule('images')
            .use('url-loader')
              .loader('url-loader')
              .tap(options => Object.assign(options, { limit: 10240 }))
    }
}