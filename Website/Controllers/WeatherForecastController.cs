﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Website.Controllers
{
    [EnableCors("_myAllowSpecificOrigins")]

    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public IEnumerable<WeatherForecast> Get(IFormFile file)
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();

        //     if(formData != null && ModelState.IsValid) {
        //     client.BaseAddress = new Uri("http://example.com:8000");
        //     client.DefaultRequestHeaders.Accept.Clear();

        //     var multiContent = new MultipartFormDataContent();

        //     var file = formData.fileToUpload;
        //     if(file != null) {
        //         var fileStreamContent = new StreamContent(file.OpenReadStream());
        //         multiContent.Add(fileStreamContent, "fileToUpload", file.FileName);
        //     }

        //     multiContent.Add(new StringContent(formData.id.ToString()), "id");

        //     var response = await client.PostAsync("/document/upload", multiContent);
        //     if (response.IsSuccessStatusCode) {
        //        var retValue = await response.Content.ReadAsAsync<DocumentUploadResult>();
        //        return Ok(reyValue);
        //     }
        // }
        // //if we get this far something Failed.
        // return BadRequest();
        }
    }
}
