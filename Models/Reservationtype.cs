﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Reservationtype
    {
        public Reservationtype()
        {
            Hallappointments = new HashSet<Hallappointment>();
        }

        public int ReservationTypeId { get; set; }
        public string Description { get; set; }
        public sbyte Status { get; set; }

        public virtual ICollection<Hallappointment> Hallappointments { get; set; }
    }
}
