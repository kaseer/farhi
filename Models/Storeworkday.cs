﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Storeworkday
    {
        public sbyte WorkDayId { get; set; }
        public int StoreBranchId { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }

        public virtual Storebranch StoreBranch { get; set; }
    }
}
