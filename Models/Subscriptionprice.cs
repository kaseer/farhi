﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Subscriptionprice
    {
        public Subscriptionprice()
        {
            Subscriptionpricedetails = new HashSet<Subscriptionpricedetail>();
        }

        public int SubscriptionPriceId { get; set; }
        public string Description { get; set; }
        public sbyte Type { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Subscriptionpricedetail> Subscriptionpricedetails { get; set; }
    }
}
