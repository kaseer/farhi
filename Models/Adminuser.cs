﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Adminuser
    {
        public Adminuser()
        {
            AdminpermissionAdmins = new HashSet<Adminpermission>();
            AdminpermissionCreatedByNavigations = new HashSet<Adminpermission>();
            CategoryCreatedByNavigations = new HashSet<Category>();
            CategoryModifiedByNavigations = new HashSet<Category>();
            FarhiuserAdminCreatedByNavigations = new HashSet<Farhiuser>();
            FarhiuserAdminModifiedByNavigations = new HashSet<Farhiuser>();
            Farhiuserpermissions = new HashSet<Farhiuserpermission>();
            HallCreatedByNavigations = new HashSet<Hall>();
            HallModifiedByNavigations = new HashSet<Hall>();
            HallappointmentAdminCreatedByNavigations = new HashSet<Hallappointment>();
            HallappointmentAdminModifiedByNavigations = new HashSet<Hallappointment>();
            HallbranchCreatedByNavigations = new HashSet<Hallbranch>();
            HallbranchModifiedByNavigations = new HashSet<Hallbranch>();
            Hallbranchconditions = new HashSet<Hallbranchcondition>();
            HallbranchserviceAdminCreatedByNavigations = new HashSet<Hallbranchservice>();
            HallbranchserviceAdminModifiedByNavigations = new HashSet<Hallbranchservice>();
            HallreservationAdminCreatedByNavigations = new HashSet<Hallreservation>();
            HallreservationAdminModifiedByNavigations = new HashSet<Hallreservation>();
            InverseCreatedByNavigation = new HashSet<Adminuser>();
            InverseModifiedByNavigation = new HashSet<Adminuser>();
            Media = new HashSet<Medium>();
            Permissions = new HashSet<Permission>();
            Productimages = new HashSet<Productimage>();
            ProductreservationAdminCreatedByNavigations = new HashSet<Productreservation>();
            ProductreservationAdminModifiedByNavigations = new HashSet<Productreservation>();
            Products = new HashSet<Product>();
            SlidermanagementCreatedByNavigations = new HashSet<Slidermanagement>();
            SlidermanagementModifiedByNavigations = new HashSet<Slidermanagement>();
            StoreCreatedByNavigations = new HashSet<Store>();
            StoreModifiedByNavigations = new HashSet<Store>();
            StorebranchAdminCreatedByNavigations = new HashSet<Storebranch>();
            StorebranchAdminModifiedByNavigations = new HashSet<Storebranch>();
            Storeservices = new HashSet<Storeservice>();
            StoretypeCreatedByNavigations = new HashSet<Storetype>();
            StoretypeModifiedByNavigations = new HashSet<Storetype>();
            SubcategoryCreatedByNavigations = new HashSet<Subcategory>();
            SubcategoryModifiedByNavigations = new HashSet<Subcategory>();
            SubscriptionCreatedByNavigations = new HashSet<Subscription>();
            SubscriptionModifiedByNavigations = new HashSet<Subscription>();
            SubscriptiondetailCreatedByNavigations = new HashSet<Subscriptiondetail>();
            SubscriptiondetailModifiedByNavigations = new HashSet<Subscriptiondetail>();
            SubscriptionpriceCreatedByNavigations = new HashSet<Subscriptionprice>();
            SubscriptionpriceModifiedByNavigations = new HashSet<Subscriptionprice>();
            Subscriptionpricedetails = new HashSet<Subscriptionpricedetail>();
        }

        public int AdminId { get; set; }
        public string AdminName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Adminpermission> AdminpermissionAdmins { get; set; }
        public virtual ICollection<Adminpermission> AdminpermissionCreatedByNavigations { get; set; }
        public virtual ICollection<Category> CategoryCreatedByNavigations { get; set; }
        public virtual ICollection<Category> CategoryModifiedByNavigations { get; set; }
        public virtual ICollection<Farhiuser> FarhiuserAdminCreatedByNavigations { get; set; }
        public virtual ICollection<Farhiuser> FarhiuserAdminModifiedByNavigations { get; set; }
        public virtual ICollection<Farhiuserpermission> Farhiuserpermissions { get; set; }
        public virtual ICollection<Hall> HallCreatedByNavigations { get; set; }
        public virtual ICollection<Hall> HallModifiedByNavigations { get; set; }
        public virtual ICollection<Hallappointment> HallappointmentAdminCreatedByNavigations { get; set; }
        public virtual ICollection<Hallappointment> HallappointmentAdminModifiedByNavigations { get; set; }
        public virtual ICollection<Hallbranch> HallbranchCreatedByNavigations { get; set; }
        public virtual ICollection<Hallbranch> HallbranchModifiedByNavigations { get; set; }
        public virtual ICollection<Hallbranchcondition> Hallbranchconditions { get; set; }
        public virtual ICollection<Hallbranchservice> HallbranchserviceAdminCreatedByNavigations { get; set; }
        public virtual ICollection<Hallbranchservice> HallbranchserviceAdminModifiedByNavigations { get; set; }
        public virtual ICollection<Hallreservation> HallreservationAdminCreatedByNavigations { get; set; }
        public virtual ICollection<Hallreservation> HallreservationAdminModifiedByNavigations { get; set; }
        public virtual ICollection<Adminuser> InverseCreatedByNavigation { get; set; }
        public virtual ICollection<Adminuser> InverseModifiedByNavigation { get; set; }
        public virtual ICollection<Medium> Media { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
        public virtual ICollection<Productimage> Productimages { get; set; }
        public virtual ICollection<Productreservation> ProductreservationAdminCreatedByNavigations { get; set; }
        public virtual ICollection<Productreservation> ProductreservationAdminModifiedByNavigations { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Slidermanagement> SlidermanagementCreatedByNavigations { get; set; }
        public virtual ICollection<Slidermanagement> SlidermanagementModifiedByNavigations { get; set; }
        public virtual ICollection<Store> StoreCreatedByNavigations { get; set; }
        public virtual ICollection<Store> StoreModifiedByNavigations { get; set; }
        public virtual ICollection<Storebranch> StorebranchAdminCreatedByNavigations { get; set; }
        public virtual ICollection<Storebranch> StorebranchAdminModifiedByNavigations { get; set; }
        public virtual ICollection<Storeservice> Storeservices { get; set; }
        public virtual ICollection<Storetype> StoretypeCreatedByNavigations { get; set; }
        public virtual ICollection<Storetype> StoretypeModifiedByNavigations { get; set; }
        public virtual ICollection<Subcategory> SubcategoryCreatedByNavigations { get; set; }
        public virtual ICollection<Subcategory> SubcategoryModifiedByNavigations { get; set; }
        public virtual ICollection<Subscription> SubscriptionCreatedByNavigations { get; set; }
        public virtual ICollection<Subscription> SubscriptionModifiedByNavigations { get; set; }
        public virtual ICollection<Subscriptiondetail> SubscriptiondetailCreatedByNavigations { get; set; }
        public virtual ICollection<Subscriptiondetail> SubscriptiondetailModifiedByNavigations { get; set; }
        public virtual ICollection<Subscriptionprice> SubscriptionpriceCreatedByNavigations { get; set; }
        public virtual ICollection<Subscriptionprice> SubscriptionpriceModifiedByNavigations { get; set; }
        public virtual ICollection<Subscriptionpricedetail> Subscriptionpricedetails { get; set; }
    }
}
