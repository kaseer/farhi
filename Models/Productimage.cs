﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Productimage
    {
        public int ProductImageId { get; set; }
        public string Path { get; set; }
        public int ProductId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
