﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Productreservation
    {
        public int ProductReservationId { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? AdminCreatedBy { get; set; }
        public int? AdminModifiedBy { get; set; }
        public DateTime? AdminModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? CustomerModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Adminuser AdminModifiedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Product Product { get; set; }
    }
}
