﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Hall
    {
        public Hall()
        {
            Farhiusershallsstores = new HashSet<Farhiusershallsstore>();
            Hallbranches = new HashSet<Hallbranch>();
        }

        public int HallId { get; set; }
        public string HallName { get; set; }
        public string OwnerName { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public int? MainBranchId { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Farhiusershallsstore> Farhiusershallsstores { get; set; }
        public virtual ICollection<Hallbranch> Hallbranches { get; set; }
    }
}
