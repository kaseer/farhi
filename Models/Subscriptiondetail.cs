﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Subscriptiondetail
    {
        public int SubscriptionDetailsId { get; set; }
        public int SubscriptionId { get; set; }
        public int SubscriptionPriceDetailsId { get; set; }
        public decimal Price { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual Subscription Subscription { get; set; }
        public virtual Subscriptionpricedetail SubscriptionPriceDetails { get; set; }
    }
}
