﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Hallservicereservation
    {
        public int HallBranchServiceId { get; set; }
        public int HallReservationDetailsId { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? CustomerModifiedOn { get; set; }

        public virtual Hallbranchservice HallBranchService { get; set; }
        public virtual Hallreservationdetail HallReservationDetails { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
    }
}
