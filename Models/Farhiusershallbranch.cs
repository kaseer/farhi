﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Farhiusershallbranch
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Hallbranch Branch { get; set; }
        public virtual Farhiusershallsstore IdNavigation { get; set; }
    }
}
