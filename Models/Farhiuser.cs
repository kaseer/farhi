﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Farhiuser
    {
        public Farhiuser()
        {
            CustomerCreatedByNavigations = new HashSet<Customer>();
            CustomerModifiedByNavigations = new HashSet<Customer>();
            FarhiuserpermissionCreatedByNavigations = new HashSet<Farhiuserpermission>();
            FarhiuserpermissionUsers = new HashSet<Farhiuserpermission>();
            Farhiusershallsstores = new HashSet<Farhiusershallsstore>();
            HallappointmentCreatedByNavigations = new HashSet<Hallappointment>();
            HallappointmentModifiedByNavigations = new HashSet<Hallappointment>();
            HallbranchconditionCreatedByNavigations = new HashSet<Hallbranchcondition>();
            HallbranchconditionModifiedByNavigations = new HashSet<Hallbranchcondition>();
            Hallbranches = new HashSet<Hallbranch>();
            HallbranchserviceCreatedByNavigations = new HashSet<Hallbranchservice>();
            HallbranchserviceModifiedByNavigations = new HashSet<Hallbranchservice>();
            HallreservationCreatedByNavigations = new HashSet<Hallreservation>();
            HallreservationModifiedByNavigations = new HashSet<Hallreservation>();
            Hallservicereservations = new HashSet<Hallservicereservation>();
            InverseCreatedByNavigation = new HashSet<Farhiuser>();
            InverseModifiedByNavigation = new HashSet<Farhiuser>();
            MediumCreatedByNavigations = new HashSet<Medium>();
            MediumModifiedByNavigations = new HashSet<Medium>();
            ProductCreatedByNavigations = new HashSet<Product>();
            ProductModifiedByNavigations = new HashSet<Product>();
            Productimages = new HashSet<Productimage>();
            ProductreservationCreatedByNavigations = new HashSet<Productreservation>();
            ProductreservationModifiedByNavigations = new HashSet<Productreservation>();
            Storebranches = new HashSet<Storebranch>();
            StoreserviceCreatedByNavigations = new HashSet<Storeservice>();
            StoreserviceModifiedByNavigations = new HashSet<Storeservice>();
            StoreservicereservationCreatedByNavigations = new HashSet<Storeservicereservation>();
            StoreservicereservationModifiedByNavigations = new HashSet<Storeservicereservation>();
        }

        public int FarhiUserId { get; set; }
        public string UserName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public sbyte LoginStatus { get; set; }
        public DateTime LastLogin { get; set; }
        public sbyte LoginTimeAttempts { get; set; }
        public sbyte Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }
        public int? AdminModifiedBy { get; set; }
        public DateTime? AdminModifiedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Adminuser AdminModifiedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Customer> CustomerCreatedByNavigations { get; set; }
        public virtual ICollection<Customer> CustomerModifiedByNavigations { get; set; }
        public virtual ICollection<Farhiuserpermission> FarhiuserpermissionCreatedByNavigations { get; set; }
        public virtual ICollection<Farhiuserpermission> FarhiuserpermissionUsers { get; set; }
        public virtual ICollection<Farhiusershallsstore> Farhiusershallsstores { get; set; }
        public virtual ICollection<Hallappointment> HallappointmentCreatedByNavigations { get; set; }
        public virtual ICollection<Hallappointment> HallappointmentModifiedByNavigations { get; set; }
        public virtual ICollection<Hallbranchcondition> HallbranchconditionCreatedByNavigations { get; set; }
        public virtual ICollection<Hallbranchcondition> HallbranchconditionModifiedByNavigations { get; set; }
        public virtual ICollection<Hallbranch> Hallbranches { get; set; }
        public virtual ICollection<Hallbranchservice> HallbranchserviceCreatedByNavigations { get; set; }
        public virtual ICollection<Hallbranchservice> HallbranchserviceModifiedByNavigations { get; set; }
        public virtual ICollection<Hallreservation> HallreservationCreatedByNavigations { get; set; }
        public virtual ICollection<Hallreservation> HallreservationModifiedByNavigations { get; set; }
        public virtual ICollection<Hallservicereservation> Hallservicereservations { get; set; }
        public virtual ICollection<Farhiuser> InverseCreatedByNavigation { get; set; }
        public virtual ICollection<Farhiuser> InverseModifiedByNavigation { get; set; }
        public virtual ICollection<Medium> MediumCreatedByNavigations { get; set; }
        public virtual ICollection<Medium> MediumModifiedByNavigations { get; set; }
        public virtual ICollection<Product> ProductCreatedByNavigations { get; set; }
        public virtual ICollection<Product> ProductModifiedByNavigations { get; set; }
        public virtual ICollection<Productimage> Productimages { get; set; }
        public virtual ICollection<Productreservation> ProductreservationCreatedByNavigations { get; set; }
        public virtual ICollection<Productreservation> ProductreservationModifiedByNavigations { get; set; }
        public virtual ICollection<Storebranch> Storebranches { get; set; }
        public virtual ICollection<Storeservice> StoreserviceCreatedByNavigations { get; set; }
        public virtual ICollection<Storeservice> StoreserviceModifiedByNavigations { get; set; }
        public virtual ICollection<Storeservicereservation> StoreservicereservationCreatedByNavigations { get; set; }
        public virtual ICollection<Storeservicereservation> StoreservicereservationModifiedByNavigations { get; set; }
    }
}
