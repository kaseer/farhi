﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Customer
    {
        public Customer()
        {
            Hallreservations = new HashSet<Hallreservation>();
            Productreservations = new HashSet<Productreservation>();
            Storeservicereservations = new HashSet<Storeservicereservation>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public string Email { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public DateTime LastLogin { get; set; }
        public int LoginTimeAttempt { get; set; }
        public sbyte LoginStatus { get; set; }
        public sbyte Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Address Address { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Hallreservation> Hallreservations { get; set; }
        public virtual ICollection<Productreservation> Productreservations { get; set; }
        public virtual ICollection<Storeservicereservation> Storeservicereservations { get; set; }
    }
}
