﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Hallappointment
    {
        public Hallappointment()
        {
            Hallreservationdetails = new HashSet<Hallreservationdetail>();
        }

        public int HallAppointmentId { get; set; }
        public int ReservationTypeId { get; set; }
        public sbyte WorkDayId { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public int HallBranchId { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }
        public int? AdminModifiedBy { get; set; }
        public DateTime? AdminModifiedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Adminuser AdminModifiedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Hallbranch HallBranch { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Reservationtype ReservationType { get; set; }
        public virtual ICollection<Hallreservationdetail> Hallreservationdetails { get; set; }
    }
}
