﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Durationtype
    {
        public Durationtype()
        {
            Subscriptionpricedetails = new HashSet<Subscriptionpricedetail>();
        }

        public sbyte DurationTypeId { get; set; }
        public string DurationTypeName { get; set; }

        public virtual ICollection<Subscriptionpricedetail> Subscriptionpricedetails { get; set; }
    }
}
