﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Hallreservationdetail
    {
        public Hallreservationdetail()
        {
            Hallservicereservations = new HashSet<Hallservicereservation>();
        }

        public int HallReservationDetailsId { get; set; }
        public int HallReservationId { get; set; }
        public int HallAppointmentId { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Hallappointment HallAppointment { get; set; }
        public virtual Hallreservation HallReservation { get; set; }
        public virtual ICollection<Hallservicereservation> Hallservicereservations { get; set; }
    }
}
