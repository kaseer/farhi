﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Subscriptionpricedetail
    {
        public Subscriptionpricedetail()
        {
            Subscriptiondetails = new HashSet<Subscriptiondetail>();
        }

        public int SubscriptionPriceDetailsId { get; set; }
        public int SubscriptionPriceId { get; set; }
        public sbyte DurationTime { get; set; }
        public sbyte DurationType { get; set; }
        public decimal Price { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Durationtype DurationTypeNavigation { get; set; }
        public virtual Subscriptionprice SubscriptionPrice { get; set; }
        public virtual ICollection<Subscriptiondetail> Subscriptiondetails { get; set; }
    }
}
