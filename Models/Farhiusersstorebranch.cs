﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Farhiusersstorebranch
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Storebranch Branch { get; set; }
        public virtual Farhiusershallsstore IdNavigation { get; set; }
    }
}
