﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Medium
    {
        public Medium()
        {
            Slidermanagements = new HashSet<Slidermanagement>();
        }

        public int MediaId { get; set; }
        public int MediaType { get; set; }
        public string Path { get; set; }
        public int? HallBranchId { get; set; }
        public int? StoreBranchId { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Hallbranch HallBranch { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Storebranch StoreBranch { get; set; }
        public virtual ICollection<Slidermanagement> Slidermanagements { get; set; }
    }
}
