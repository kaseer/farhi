﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Storebranch
    {
        public Storebranch()
        {
            Farhiusersstorebranches = new HashSet<Farhiusersstorebranch>();
            Media = new HashSet<Medium>();
            Products = new HashSet<Product>();
            Stores = new HashSet<Store>();
            Storeservices = new HashSet<Storeservice>();
            Storeworkdays = new HashSet<Storeworkday>();
            Subscriptions = new HashSet<Subscription>();
        }

        public int StoreBranchId { get; set; }
        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public int StoreId { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public sbyte Status { get; set; }
        public int AdminCreatedBy { get; set; }
        public DateTime AdminCreatedOn { get; set; }
        public int? AdminModifiedBy { get; set; }
        public DateTime? AdminModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Address Address { get; set; }
        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Adminuser AdminModifiedByNavigation { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Store Store { get; set; }
        public virtual ICollection<Farhiusersstorebranch> Farhiusersstorebranches { get; set; }
        public virtual ICollection<Medium> Media { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Store> Stores { get; set; }
        public virtual ICollection<Storeservice> Storeservices { get; set; }
        public virtual ICollection<Storeworkday> Storeworkdays { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}
