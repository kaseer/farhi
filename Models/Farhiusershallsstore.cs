﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Farhiusershallsstore
    {
        public Farhiusershallsstore()
        {
            Farhiusershallbranches = new HashSet<Farhiusershallbranch>();
            Farhiusersstorebranches = new HashSet<Farhiusersstorebranch>();
        }

        public int Id { get; set; }
        public int FarhiUserId { get; set; }
        public int? HallId { get; set; }
        public int? StoreId { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Farhiuser FarhiUser { get; set; }
        public virtual Hall Hall { get; set; }
        public virtual Store Store { get; set; }
        public virtual ICollection<Farhiusershallbranch> Farhiusershallbranches { get; set; }
        public virtual ICollection<Farhiusersstorebranch> Farhiusersstorebranches { get; set; }
    }
}
