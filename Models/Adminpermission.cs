﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Adminpermission
    {
        public int AdminId { get; set; }
        public int PermissionId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Adminuser Admin { get; set; }
        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Permission Permission { get; set; }
    }
}
