﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Category
    {
        public Category()
        {
            Subcategories = new HashSet<Subcategory>();
        }

        public int CategoryId { get; set; }
        public string Description { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Subcategory> Subcategories { get; set; }
    }
}
