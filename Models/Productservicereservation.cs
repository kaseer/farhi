﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Productservicereservation
    {
        public int ProductServiceReservationId { get; set; }
        public int ProductReservationDetailsId { get; set; }
        public int ProductServiceId { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? AdminModifiedBy { get; set; }
        public DateTime? AdminModifiedOn { get; set; }
        public bool? CustomerIsModified { get; set; }
        public DateTime? CustomerModifiedOn { get; set; }

        public virtual Farhiuser AdminModifiedByNavigation { get; set; }
        public virtual Productreservationdetail ProductReservationDetails { get; set; }
        public virtual Productservice ProductService { get; set; }
    }
}
