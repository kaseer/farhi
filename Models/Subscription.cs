﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Subscription
    {
        public Subscription()
        {
            Subscriptiondetails = new HashSet<Subscriptiondetail>();
        }

        public int SubscriptionId { get; set; }
        public int? HallBranchId { get; set; }
        public int? StoreBranchId { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Hallbranch HallBranch { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual Storebranch StoreBranch { get; set; }
        public virtual ICollection<Subscriptiondetail> Subscriptiondetails { get; set; }
    }
}
