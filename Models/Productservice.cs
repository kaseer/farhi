﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Productservice
    {
        public Productservice()
        {
            Productservicereservations = new HashSet<Productservicereservation>();
        }

        public int ProductServiceId { get; set; }
        public int ProductId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Product Product { get; set; }
        public virtual ICollection<Productservicereservation> Productservicereservations { get; set; }
    }
}
