﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Permission
    {
        public Permission()
        {
            Adminpermissions = new HashSet<Adminpermission>();
            Farhiuserpermissions = new HashSet<Farhiuserpermission>();
        }

        public int PermissionId { get; set; }
        public string PermissionName { get; set; }
        public string Code { get; set; }
        public int FeatureId { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Feature Feature { get; set; }
        public virtual ICollection<Adminpermission> Adminpermissions { get; set; }
        public virtual ICollection<Farhiuserpermission> Farhiuserpermissions { get; set; }
    }
}
