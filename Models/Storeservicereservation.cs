﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Storeservicereservation
    {
        public int ServiceReservationId { get; set; }
        public int ServiceId { get; set; }
        public int CustomerId { get; set; }
        public decimal Price { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public sbyte WorkDayId { get; set; }
        public sbyte Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? CustomerModifiedOn { get; set; }

        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Storeservice Service { get; set; }
    }
}
