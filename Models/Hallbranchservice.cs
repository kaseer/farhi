﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Hallbranchservice
    {
        public Hallbranchservice()
        {
            Hallservicereservations = new HashSet<Hallservicereservation>();
        }

        public int HallBranchServiceId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int HallBranchId { get; set; }
        public sbyte Status { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }
        public int? AdminModifiedBy { get; set; }
        public DateTime? AdminModifiedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Adminuser AdminModifiedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Hallbranch HallBranch { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual ICollection<Hallservicereservation> Hallservicereservations { get; set; }
    }
}
