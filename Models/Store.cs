﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Store
    {
        public Store()
        {
            Farhiusershallsstores = new HashSet<Farhiusershallsstore>();
            Storebranches = new HashSet<Storebranch>();
        }

        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public string OwnerName { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public int? MainBranchId { get; set; }
        public int StoreTypeId { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Storebranch MainBranch { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual Storetype StoreType { get; set; }
        public virtual ICollection<Farhiusershallsstore> Farhiusershallsstores { get; set; }
        public virtual ICollection<Storebranch> Storebranches { get; set; }
    }
}
