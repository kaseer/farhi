﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Address
    {
        public Address()
        {
            Customers = new HashSet<Customer>();
            Hallbranches = new HashSet<Hallbranch>();
            Storebranches = new HashSet<Storebranch>();
        }

        public int AddressId { get; set; }
        public string AddressName { get; set; }
        public sbyte Status { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Hallbranch> Hallbranches { get; set; }
        public virtual ICollection<Storebranch> Storebranches { get; set; }
    }
}
