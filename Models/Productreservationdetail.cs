﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Productreservationdetail
    {
        public Productreservationdetail()
        {
            Productservicereservations = new HashSet<Productservicereservation>();
        }

        public int ProductReservationDetailsId { get; set; }
        public int ProductReservationId { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedOn { get; set; }

        public virtual Productreservation ProductReservation { get; set; }
        public virtual ICollection<Productservicereservation> Productservicereservations { get; set; }
    }
}
