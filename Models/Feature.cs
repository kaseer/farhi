﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Feature
    {
        public Feature()
        {
            Permissions = new HashSet<Permission>();
        }

        public int FeatureId { get; set; }
        public string FeatureName { get; set; }
        public int FeatureType { get; set; }
        public sbyte Status { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
