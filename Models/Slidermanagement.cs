﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Slidermanagement
    {
        public int SliderManagementId { get; set; }
        public int MediaId { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Medium Media { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
    }
}
