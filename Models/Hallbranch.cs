﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Hallbranch
    {
        public Hallbranch()
        {
            Farhiusershallbranches = new HashSet<Farhiusershallbranch>();
            Hallappointments = new HashSet<Hallappointment>();
            Hallbranchconditions = new HashSet<Hallbranchcondition>();
            Hallbranchservices = new HashSet<Hallbranchservice>();
            Hallreservations = new HashSet<Hallreservation>();
            Media = new HashSet<Medium>();
            Subscriptions = new HashSet<Subscription>();
        }

        public int HallBranchId { get; set; }
        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public int HallId { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public int PeapleCapacity { get; set; }
        public int CarsCapacity { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly EndTime { get; set; }
        public decimal? StandardPrice { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? UserModifiedBy { get; set; }
        public DateTime? UserModifiedOn { get; set; }

        public virtual Address Address { get; set; }
        public virtual Adminuser CreatedByNavigation { get; set; }
        public virtual Hall Hall { get; set; }
        public virtual Adminuser ModifiedByNavigation { get; set; }
        public virtual Farhiuser UserModifiedByNavigation { get; set; }
        public virtual ICollection<Farhiusershallbranch> Farhiusershallbranches { get; set; }
        public virtual ICollection<Hallappointment> Hallappointments { get; set; }
        public virtual ICollection<Hallbranchcondition> Hallbranchconditions { get; set; }
        public virtual ICollection<Hallbranchservice> Hallbranchservices { get; set; }
        public virtual ICollection<Hallreservation> Hallreservations { get; set; }
        public virtual ICollection<Medium> Media { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}
