﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Product
    {
        public Product()
        {
            Productimages = new HashSet<Productimage>();
            Productreservations = new HashSet<Productreservation>();
        }

        public int ProductId { get; set; }
        public int StoreBranchId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SubCategoryId { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public sbyte Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Farhiuser ModifiedByNavigation { get; set; }
        public virtual Storebranch StoreBranch { get; set; }
        public virtual Subcategory SubCategory { get; set; }
        public virtual ICollection<Productimage> Productimages { get; set; }
        public virtual ICollection<Productreservation> Productreservations { get; set; }
    }
}
