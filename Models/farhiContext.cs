﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Models
{
    public partial class farhiContext : DbContext
    {
        public farhiContext(DbContextOptions<farhiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Adminpermission> Adminpermissions { get; set; }
        public virtual DbSet<Adminuser> Adminusers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Durationtype> Durationtypes { get; set; }
        public virtual DbSet<Farhiuser> Farhiusers { get; set; }
        public virtual DbSet<Farhiuserpermission> Farhiuserpermissions { get; set; }
        public virtual DbSet<Farhiusershallbranch> Farhiusershallbranches { get; set; }
        public virtual DbSet<Farhiusershallsstore> Farhiusershallsstores { get; set; }
        public virtual DbSet<Farhiusersstorebranch> Farhiusersstorebranches { get; set; }
        public virtual DbSet<Feature> Features { get; set; }
        public virtual DbSet<Hall> Halls { get; set; }
        public virtual DbSet<Hallappointment> Hallappointments { get; set; }
        public virtual DbSet<Hallbranch> Hallbranches { get; set; }
        public virtual DbSet<Hallbranchcondition> Hallbranchconditions { get; set; }
        public virtual DbSet<Hallbranchservice> Hallbranchservices { get; set; }
        public virtual DbSet<Hallreservation> Hallreservations { get; set; }
        public virtual DbSet<Hallreservationdetail> Hallreservationdetails { get; set; }
        public virtual DbSet<Hallservicereservation> Hallservicereservations { get; set; }
        public virtual DbSet<Medium> Media { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Productimage> Productimages { get; set; }
        public virtual DbSet<Productreservation> Productreservations { get; set; }
        public virtual DbSet<Reservationtype> Reservationtypes { get; set; }
        public virtual DbSet<Slidermanagement> Slidermanagements { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Storebranch> Storebranches { get; set; }
        public virtual DbSet<Storeservice> Storeservices { get; set; }
        public virtual DbSet<Storeservicereservation> Storeservicereservations { get; set; }
        public virtual DbSet<Storetype> Storetypes { get; set; }
        public virtual DbSet<Storeworkday> Storeworkdays { get; set; }
        public virtual DbSet<Subcategory> Subcategories { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<Subscriptiondetail> Subscriptiondetails { get; set; }
        public virtual DbSet<Subscriptionprice> Subscriptionprices { get; set; }
        public virtual DbSet<Subscriptionpricedetail> Subscriptionpricedetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=95.216.93.102;port=3306;user=naqraly;password=N@qrA2O20;database=farhi", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.3.35-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8_general_ci")
                .HasCharSet("utf8");

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("addresses");

                entity.Property(e => e.AddressId).HasColumnType("int(11)");

                entity.Property(e => e.AddressName)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");
            });

            modelBuilder.Entity<Adminpermission>(entity =>
            {
                entity.HasKey(e => new { e.AdminId, e.PermissionId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("adminpermissions");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.PermissionId, "PermissionId");

                entity.Property(e => e.AdminId).HasColumnType("int(11)");

                entity.Property(e => e.PermissionId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.AdminpermissionAdmins)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("adminpermissions_ibfk_1");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.AdminpermissionCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("adminpermissions_ibfk_2");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.Adminpermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .HasConstraintName("adminpermissions_ibfk_3");
            });

            modelBuilder.Entity<Adminuser>(entity =>
            {
                entity.HasKey(e => e.AdminId)
                    .HasName("PRIMARY");

                entity.ToTable("adminusers");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.AdminId).HasColumnType("int(11)");

                entity.Property(e => e.AdminName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(44);

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(24);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.InverseCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("adminusers_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.InverseModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("adminusers_ibfk_2");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("categories");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.CategoryId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.CategoryCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("categories_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.CategoryModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("categories_ibfk_2");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("customers");

                entity.HasIndex(e => e.AddressId, "AddressId");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.CustomerId).HasColumnType("int(11)");

                entity.Property(e => e.AddressDescription)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.AddressId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.LoginStatus).HasColumnType("tinyint(4)");

                entity.Property(e => e.LoginTimeAttempt).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(44);

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(24);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("customers_ibfk_3");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.CustomerCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("customers_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.CustomerModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("customers_ibfk_2");
            });

            modelBuilder.Entity<Durationtype>(entity =>
            {
                entity.ToTable("durationtypes");

                entity.HasIndex(e => e.DurationTypeId, "DurationTypeId");

                entity.Property(e => e.DurationTypeId)
                    .HasColumnType("tinyint(4)")
                    .ValueGeneratedNever();

                entity.Property(e => e.DurationTypeName)
                    .IsRequired()
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<Farhiuser>(entity =>
            {
                entity.ToTable("farhiusers");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.AdminModifiedBy, "AdminModifiedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.LoginName, "LoginName")
                    .IsUnique();

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.FarhiUserId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.AdminModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastLogin).HasColumnType("datetime");

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.LoginStatus).HasColumnType("tinyint(4)");

                entity.Property(e => e.LoginTimeAttempts).HasColumnType("tinyint(4)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(44);

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(24);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.FarhiuserAdminCreatedByNavigations)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiusers_ibfk_5");

                entity.HasOne(d => d.AdminModifiedByNavigation)
                    .WithMany(p => p.FarhiuserAdminModifiedByNavigations)
                    .HasForeignKey(d => d.AdminModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiusers_ibfk_6");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.InverseCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiusers_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.InverseModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiusers_ibfk_2");
            });

            modelBuilder.Entity<Farhiuserpermission>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.PermissionId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("farhiuserpermissions");

                entity.HasIndex(e => e.AdminCreatedBy, "CreatedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy_2");

                entity.HasIndex(e => e.PermissionId, "PermissionId");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.PermissionId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.Farhiuserpermissions)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiuserpermissions_ibfk_1");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.FarhiuserpermissionCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiuserpermissions_ibfk_4");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.Farhiuserpermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .HasConstraintName("farhiuserpermissions_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.FarhiuserpermissionUsers)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("farhiuserpermissions_ibfk_3");
            });

            modelBuilder.Entity<Farhiusershallbranch>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BranchId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("farhiusershallbranches");

                entity.HasIndex(e => e.BranchId, "farhiusershallbranches_ibfk_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BranchId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Farhiusershallbranches)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("farhiusershallbranches_ibfk_2");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.Farhiusershallbranches)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("farhiusershallbranches_ibfk_1");
            });

            modelBuilder.Entity<Farhiusershallsstore>(entity =>
            {
                entity.ToTable("farhiusershallsstores");

                entity.HasIndex(e => e.FarhiUserId, "FarhiUserId");

                entity.HasIndex(e => e.HallId, "HallId");

                entity.HasIndex(e => e.StoreId, "StoreId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FarhiUserId).HasColumnType("int(11)");

                entity.Property(e => e.HallId).HasColumnType("int(11)");

                entity.Property(e => e.StoreId).HasColumnType("int(11)");

                entity.HasOne(d => d.FarhiUser)
                    .WithMany(p => p.Farhiusershallsstores)
                    .HasForeignKey(d => d.FarhiUserId)
                    .HasConstraintName("farhiusershallsstores_ibfk_1");

                entity.HasOne(d => d.Hall)
                    .WithMany(p => p.Farhiusershallsstores)
                    .HasForeignKey(d => d.HallId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiusershallsstores_ibfk_2");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Farhiusershallsstores)
                    .HasForeignKey(d => d.StoreId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("farhiusershallsstores_ibfk_3");
            });

            modelBuilder.Entity<Farhiusersstorebranch>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.BranchId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("farhiusersstorebranches");

                entity.HasIndex(e => e.BranchId, "farhiusersstorebranches_ibfk_2");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BranchId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Farhiusersstorebranches)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("farhiusersstorebranches_ibfk_2");

                entity.HasOne(d => d.IdNavigation)
                    .WithMany(p => p.Farhiusersstorebranches)
                    .HasForeignKey(d => d.Id)
                    .HasConstraintName("farhiusersstorebranches_ibfk_1");
            });

            modelBuilder.Entity<Feature>(entity =>
            {
                entity.ToTable("features");

                entity.Property(e => e.FeatureId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.FeatureName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FeatureType).HasColumnType("int(11)");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");
            });

            modelBuilder.Entity<Hall>(entity =>
            {
                entity.ToTable("halls");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.Email, "Email")
                    .IsUnique();

                entity.HasIndex(e => e.HallName, "HallName")
                    .IsUnique();

                entity.HasIndex(e => e.MainBranchId, "MainBranchId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.HallId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.HallName)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Logo)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.MainBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OwnerName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HallCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("halls_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.HallModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("halls_ibfk_2");
            });

            modelBuilder.Entity<Hallappointment>(entity =>
            {
                entity.ToTable("hallappointments");

                entity.HasIndex(e => e.AdminCreatedBy, "CreatedBy");

                entity.HasIndex(e => e.HallBranchId, "HallBranchId");

                entity.HasIndex(e => e.AdminModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.ReservationTypeId, "ReservationTypeId");

                entity.HasIndex(e => e.CreatedBy, "UserCreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "UserModifiedBy");

                entity.Property(e => e.HallAppointmentId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.AdminModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("time");

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.ReservationTypeId).HasColumnType("int(11)");

                entity.Property(e => e.StartTime).HasColumnType("time");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.WorkDayId).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.HallappointmentAdminCreatedByNavigations)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallappointments_ibfk_1");

                entity.HasOne(d => d.AdminModifiedByNavigation)
                    .WithMany(p => p.HallappointmentAdminModifiedByNavigations)
                    .HasForeignKey(d => d.AdminModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallappointments_ibfk_2");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HallappointmentCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallappointments_ibfk_5");

                entity.HasOne(d => d.HallBranch)
                    .WithMany(p => p.Hallappointments)
                    .HasForeignKey(d => d.HallBranchId)
                    .HasConstraintName("hallappointments_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.HallappointmentModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallappointments_ibfk_6");

                entity.HasOne(d => d.ReservationType)
                    .WithMany(p => p.Hallappointments)
                    .HasForeignKey(d => d.ReservationTypeId)
                    .HasConstraintName("hallappointments_ibfk_4");
            });

            modelBuilder.Entity<Hallbranch>(entity =>
            {
                entity.ToTable("hallbranches");

                entity.HasIndex(e => e.AddressId, "AddressId");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.HallId, "HallId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.UserModifiedBy, "UserModifiedBy");

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.AddressDescription).HasMaxLength(50);

                entity.Property(e => e.AddressId).HasColumnType("int(11)");

                entity.Property(e => e.CarsCapacity).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("time");

                entity.Property(e => e.HallId).HasColumnType("int(11)");

                entity.Property(e => e.Latitude).HasPrecision(10);

                entity.Property(e => e.Longitude).HasPrecision(10);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PeapleCapacity).HasColumnType("int(11)");

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.StandardPrice).HasPrecision(10);

                entity.Property(e => e.StartTime).HasColumnType("time");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.UserModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.UserModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Hallbranches)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("hallbranches_ibfk_4");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HallbranchCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("hallbranches_ibfk_2");

                entity.HasOne(d => d.Hall)
                    .WithMany(p => p.Hallbranches)
                    .HasForeignKey(d => d.HallId)
                    .HasConstraintName("hallbranches_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.HallbranchModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranches_ibfk_3");

                entity.HasOne(d => d.UserModifiedByNavigation)
                    .WithMany(p => p.Hallbranches)
                    .HasForeignKey(d => d.UserModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranches_ibfk_5");
            });

            modelBuilder.Entity<Hallbranchcondition>(entity =>
            {
                entity.HasKey(e => e.ConditionId)
                    .HasName("PRIMARY");

                entity.ToTable("hallbranchconditions");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.HallBranchId, "HallBranchId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.ConditionId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.Hallbranchconditions)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranchconditions_ibfk_4");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HallbranchconditionCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("hallbranchconditions_ibfk_1");

                entity.HasOne(d => d.HallBranch)
                    .WithMany(p => p.Hallbranchconditions)
                    .HasForeignKey(d => d.HallBranchId)
                    .HasConstraintName("hallbranchconditions_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.HallbranchconditionModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranchconditions_ibfk_2");
            });

            modelBuilder.Entity<Hallbranchservice>(entity =>
            {
                entity.ToTable("hallbranchservices");

                entity.HasIndex(e => e.CreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.AdminCreatedBy, "CreatedBy");

                entity.HasIndex(e => e.HallBranchId, "HallBranchId");

                entity.HasIndex(e => e.AdminModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.ModifiedBy, "UserModifiedBy");

                entity.Property(e => e.HallBranchServiceId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.AdminModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.HallbranchserviceAdminCreatedByNavigations)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranchservices_ibfk_1");

                entity.HasOne(d => d.AdminModifiedByNavigation)
                    .WithMany(p => p.HallbranchserviceAdminModifiedByNavigations)
                    .HasForeignKey(d => d.AdminModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranchservices_ibfk_2");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HallbranchserviceCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranchservices_ibfk_4");

                entity.HasOne(d => d.HallBranch)
                    .WithMany(p => p.Hallbranchservices)
                    .HasForeignKey(d => d.HallBranchId)
                    .HasConstraintName("hallbranchservices_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.HallbranchserviceModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallbranchservices_ibfk_5");
            });

            modelBuilder.Entity<Hallreservation>(entity =>
            {
                entity.ToTable("hallreservations");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.AdminModifiedBy, "AdminModifiedBy");

                entity.HasIndex(e => e.CustomerId, "CustomerId");

                entity.HasIndex(e => e.HallBranchId, "HallBranchId");

                entity.HasIndex(e => e.CreatedBy, "UserCreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "UserModifiedBy");

                entity.Property(e => e.HallReservationId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ReservationDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.HallreservationAdminCreatedByNavigations)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallreservations_ibfk_1");

                entity.HasOne(d => d.AdminModifiedByNavigation)
                    .WithMany(p => p.HallreservationAdminModifiedByNavigations)
                    .HasForeignKey(d => d.AdminModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallreservations_ibfk_2");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.HallreservationCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallreservations_ibfk_5");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Hallreservations)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("hallreservations_ibfk_3");

                entity.HasOne(d => d.HallBranch)
                    .WithMany(p => p.Hallreservations)
                    .HasForeignKey(d => d.HallBranchId)
                    .HasConstraintName("hallreservations_ibfk_4");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.HallreservationModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallreservations_ibfk_6");
            });

            modelBuilder.Entity<Hallreservationdetail>(entity =>
            {
                entity.HasKey(e => e.HallReservationDetailsId)
                    .HasName("PRIMARY");

                entity.ToTable("hallreservationdetails");

                entity.HasIndex(e => e.HallAppointmentId, "HallAppointmentId");

                entity.HasIndex(e => e.HallReservationId, "HallReservationId");

                entity.Property(e => e.HallReservationDetailsId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HallAppointmentId).HasColumnType("int(11)");

                entity.Property(e => e.HallReservationId).HasColumnType("int(11)");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.HallAppointment)
                    .WithMany(p => p.Hallreservationdetails)
                    .HasForeignKey(d => d.HallAppointmentId)
                    .HasConstraintName("hallreservationdetails_ibfk_2");

                entity.HasOne(d => d.HallReservation)
                    .WithMany(p => p.Hallreservationdetails)
                    .HasForeignKey(d => d.HallReservationId)
                    .HasConstraintName("hallreservationdetails_ibfk_3");
            });

            modelBuilder.Entity<Hallservicereservation>(entity =>
            {
                entity.HasKey(e => new { e.HallBranchServiceId, e.HallReservationDetailsId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("hallservicereservation");

                entity.HasIndex(e => e.ModifiedBy, "AdminModifiedBy");

                entity.HasIndex(e => e.HallReservationDetailsId, "HallReservationDetailsId");

                entity.Property(e => e.HallBranchServiceId).HasColumnType("int(11)");

                entity.Property(e => e.HallReservationDetailsId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.HallBranchService)
                    .WithMany(p => p.Hallservicereservations)
                    .HasForeignKey(d => d.HallBranchServiceId)
                    .HasConstraintName("hallservicereservation_ibfk_2");

                entity.HasOne(d => d.HallReservationDetails)
                    .WithMany(p => p.Hallservicereservations)
                    .HasForeignKey(d => d.HallReservationDetailsId)
                    .HasConstraintName("hallservicereservation_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.Hallservicereservations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("hallservicereservation_ibfk_1");
            });

            modelBuilder.Entity<Medium>(entity =>
            {
                entity.HasKey(e => e.MediaId)
                    .HasName("PRIMARY");

                entity.ToTable("media");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.HallBranchId, "HallBranchId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.StoreBranchId, "StoreBranchId");

                entity.Property(e => e.MediaId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.MediaType).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(255)
                    .UseCollation("utf8_unicode_ci");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreBranchId).HasColumnType("int(11)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.Media)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("media_ibfk_5");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.MediumCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("media_ibfk_1");

                entity.HasOne(d => d.HallBranch)
                    .WithMany(p => p.Media)
                    .HasForeignKey(d => d.HallBranchId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("media_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.MediumModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("media_ibfk_2");

                entity.HasOne(d => d.StoreBranch)
                    .WithMany(p => p.Media)
                    .HasForeignKey(d => d.StoreBranchId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("media_ibfk_4");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.ToTable("permissions");

                entity.HasIndex(e => e.Code, "Code")
                    .IsUnique();

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.FeatureId, "FeatureId");

                entity.Property(e => e.PermissionId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FeatureId).HasColumnType("int(11)");

                entity.Property(e => e.PermissionName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.Permissions)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("permissions_ibfk_2");

                entity.HasOne(d => d.Feature)
                    .WithMany(p => p.Permissions)
                    .HasForeignKey(d => d.FeatureId)
                    .HasConstraintName("permissions_ibfk_1");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.StoreBranchId, "StoreBranchId");

                entity.HasIndex(e => e.SubCategoryId, "SubCategoryId");

                entity.Property(e => e.ProductId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.Qty).HasColumnType("int(11)");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreBranchId).HasColumnType("int(11)");

                entity.Property(e => e.SubCategoryId).HasColumnType("int(11)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("products_ibfk_1");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ProductCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("products_ibfk_2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ProductModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("products_ibfk_3");

                entity.HasOne(d => d.StoreBranch)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.StoreBranchId)
                    .HasConstraintName("products_ibfk_6");

                entity.HasOne(d => d.SubCategory)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.SubCategoryId)
                    .HasConstraintName("products_ibfk_5");
            });

            modelBuilder.Entity<Productimage>(entity =>
            {
                entity.ToTable("productimages");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ProductId, "ProductId");

                entity.Property(e => e.ProductImageId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.ProductId).HasColumnType("int(11)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.Productimages)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("productimages_ibfk_1");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.Productimages)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("productimages_ibfk_2");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Productimages)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("productimages_ibfk_4");
            });

            modelBuilder.Entity<Productreservation>(entity =>
            {
                entity.ToTable("productreservations");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.AdminModifiedBy, "AdminModifiedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.CustomerId, "CustomerId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.ProductId, "ProductId");

                entity.Property(e => e.ProductReservationId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.ProductId).HasColumnType("int(11)");

                entity.Property(e => e.Qty).HasColumnType("int(11)");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.ProductreservationAdminCreatedByNavigations)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("productreservations_ibfk_1");

                entity.HasOne(d => d.AdminModifiedByNavigation)
                    .WithMany(p => p.ProductreservationAdminModifiedByNavigations)
                    .HasForeignKey(d => d.AdminModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("productreservations_ibfk_2");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ProductreservationCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("productreservations_ibfk_5");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Productreservations)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("productreservations_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.ProductreservationModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("productreservations_ibfk_6");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Productreservations)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("productreservations_ibfk_4");
            });

            modelBuilder.Entity<Reservationtype>(entity =>
            {
                entity.ToTable("reservationtypes");

                entity.Property(e => e.ReservationTypeId).HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");
            });

            modelBuilder.Entity<Slidermanagement>(entity =>
            {
                entity.ToTable("slidermanagement");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.MediaId, "MediaId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.SliderManagementId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.MediaId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.SlidermanagementCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("slidermanagement_ibfk_1");

                entity.HasOne(d => d.Media)
                    .WithMany(p => p.Slidermanagements)
                    .HasForeignKey(d => d.MediaId)
                    .HasConstraintName("slidermanagement_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.SlidermanagementModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("slidermanagement_ibfk_2");
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.ToTable("stores");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.MainBranchId, "MainBranchId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.StoreTypeId, "StoreTypeId");

                entity.Property(e => e.StoreId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Logo)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.MainBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.OwnerName)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreName)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.StoreTypeId).HasColumnType("int(11)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.StoreCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("stores_ibfk_1");

                entity.HasOne(d => d.MainBranch)
                    .WithMany(p => p.Stores)
                    .HasForeignKey(d => d.MainBranchId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("stores_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.StoreModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("stores_ibfk_2");

                entity.HasOne(d => d.StoreType)
                    .WithMany(p => p.Stores)
                    .HasForeignKey(d => d.StoreTypeId)
                    .HasConstraintName("stores_ibfk_4");
            });

            modelBuilder.Entity<Storebranch>(entity =>
            {
                entity.ToTable("storebranches");

                entity.HasIndex(e => e.AddressId, "AddressId");

                entity.HasIndex(e => e.AdminCreatedBy, "CreatedBy");

                entity.HasIndex(e => e.AdminModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.StoreId, "StoreId");

                entity.HasIndex(e => e.ModifiedBy, "UserModifiedBy");

                entity.Property(e => e.StoreBranchId).HasColumnType("int(11)");

                entity.Property(e => e.AddressDescription).HasMaxLength(50);

                entity.Property(e => e.AddressId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.AdminModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("time");

                entity.Property(e => e.Latitude).HasPrecision(10);

                entity.Property(e => e.Longitude).HasPrecision(10);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.PhoneNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.PhoneNo2).HasMaxLength(15);

                entity.Property(e => e.StartTime).HasColumnType("time");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreId).HasColumnType("int(11)");

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.Storebranches)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("storebranches_ibfk_1");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.StorebranchAdminCreatedByNavigations)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .HasConstraintName("storebranches_ibfk_2");

                entity.HasOne(d => d.AdminModifiedByNavigation)
                    .WithMany(p => p.StorebranchAdminModifiedByNavigations)
                    .HasForeignKey(d => d.AdminModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storebranches_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.Storebranches)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storebranches_ibfk_5");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Storebranches)
                    .HasForeignKey(d => d.StoreId)
                    .HasConstraintName("storebranches_ibfk_4");
            });

            modelBuilder.Entity<Storeservice>(entity =>
            {
                entity.HasKey(e => e.ServiceId)
                    .HasName("PRIMARY");

                entity.ToTable("storeservices");

                entity.HasIndex(e => e.AdminCreatedBy, "AdminCreatedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.StoreBranchId, "StoreBranchId");

                entity.Property(e => e.ServiceId).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.AdminCreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreBranchId).HasColumnType("int(11)");

                entity.HasOne(d => d.AdminCreatedByNavigation)
                    .WithMany(p => p.Storeservices)
                    .HasForeignKey(d => d.AdminCreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storeservices_ibfk_1");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.StoreserviceCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("storeservices_ibfk_2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.StoreserviceModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storeservices_ibfk_3");

                entity.HasOne(d => d.StoreBranch)
                    .WithMany(p => p.Storeservices)
                    .HasForeignKey(d => d.StoreBranchId)
                    .HasConstraintName("storeservices_ibfk_4");
            });

            modelBuilder.Entity<Storeservicereservation>(entity =>
            {
                entity.HasKey(e => e.ServiceReservationId)
                    .HasName("PRIMARY");

                entity.ToTable("storeservicereservations");

                entity.HasIndex(e => e.ModifiedBy, "AdminModifiedBy");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.CustomerId, "CustomerId");

                entity.HasIndex(e => e.ServiceId, "ProductServiceId");

                entity.Property(e => e.ServiceReservationId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnType("int(11)");

                entity.Property(e => e.CustomerModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("time");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.ServiceId).HasColumnType("int(11)");

                entity.Property(e => e.StartTime).HasColumnType("time");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.WorkDayId).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.StoreservicereservationCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storeservicereservations_ibfk_5");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Storeservicereservations)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("storeservicereservations_ibfk_4");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.StoreservicereservationModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storeservicereservations_ibfk_2");

                entity.HasOne(d => d.Service)
                    .WithMany(p => p.Storeservicereservations)
                    .HasForeignKey(d => d.ServiceId)
                    .HasConstraintName("storeservicereservations_ibfk_3");
            });

            modelBuilder.Entity<Storetype>(entity =>
            {
                entity.ToTable("storetypes");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.StoreTypeId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.StoretypeCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("storetypes_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.StoretypeModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("storetypes_ibfk_2");
            });

            modelBuilder.Entity<Storeworkday>(entity =>
            {
                entity.HasKey(e => new { e.WorkDayId, e.StoreBranchId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("storeworkdays");

                entity.HasIndex(e => e.StoreBranchId, "StoreBranchId");

                entity.Property(e => e.WorkDayId).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreBranchId).HasColumnType("int(11)");

                entity.Property(e => e.EndTime).HasColumnType("time");

                entity.Property(e => e.StartTime).HasColumnType("time");

                entity.HasOne(d => d.StoreBranch)
                    .WithMany(p => p.Storeworkdays)
                    .HasForeignKey(d => d.StoreBranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("storeworkdays_ibfk_1");
            });

            modelBuilder.Entity<Subcategory>(entity =>
            {
                entity.ToTable("subcategories");

                entity.HasIndex(e => e.CategoryId, "CategoryId");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.SubCategoryId).HasColumnType("int(11)");

                entity.Property(e => e.CategoryId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Subcategories)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("subcategories_ibfk_1");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.SubcategoryCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("subcategories_ibfk_2");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.SubcategoryModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("subcategories_ibfk_3");
            });

            modelBuilder.Entity<Subscription>(entity =>
            {
                entity.ToTable("subscriptions");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.HallBranchId, "FarhiUserId");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.StoreBranchId, "StoreBranchId");

                entity.Property(e => e.SubscriptionId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.HallBranchId).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.StoreBranchId).HasColumnType("int(11)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.SubscriptionCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("subscriptions_ibfk_1");

                entity.HasOne(d => d.HallBranch)
                    .WithMany(p => p.Subscriptions)
                    .HasForeignKey(d => d.HallBranchId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("subscriptions_ibfk_3");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.SubscriptionModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("subscriptions_ibfk_2");

                entity.HasOne(d => d.StoreBranch)
                    .WithMany(p => p.Subscriptions)
                    .HasForeignKey(d => d.StoreBranchId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("subscriptions_ibfk_4");
            });

            modelBuilder.Entity<Subscriptiondetail>(entity =>
            {
                entity.HasKey(e => e.SubscriptionDetailsId)
                    .HasName("PRIMARY");

                entity.ToTable("subscriptiondetails");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.HasIndex(e => e.SubscriptionId, "SubscriptionId");

                entity.HasIndex(e => e.SubscriptionPriceDetailsId, "SubscriptionPriceDetailsId");

                entity.Property(e => e.SubscriptionDetailsId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.SubscriptionId).HasColumnType("int(11)");

                entity.Property(e => e.SubscriptionPriceDetailsId).HasColumnType("int(11)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.SubscriptiondetailCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("subscriptiondetails_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.SubscriptiondetailModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("subscriptiondetails_ibfk_2");

                entity.HasOne(d => d.Subscription)
                    .WithMany(p => p.Subscriptiondetails)
                    .HasForeignKey(d => d.SubscriptionId)
                    .HasConstraintName("subscriptiondetails_ibfk_3");

                entity.HasOne(d => d.SubscriptionPriceDetails)
                    .WithMany(p => p.Subscriptiondetails)
                    .HasForeignKey(d => d.SubscriptionPriceDetailsId)
                    .HasConstraintName("subscriptiondetails_ibfk_4");
            });

            modelBuilder.Entity<Subscriptionprice>(entity =>
            {
                entity.ToTable("subscriptionprices");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.ModifiedBy, "ModifiedBy");

                entity.Property(e => e.SubscriptionPriceId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(70);

                entity.Property(e => e.ModifiedBy).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnType("tinyint(4)");

                entity.Property(e => e.Type).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.SubscriptionpriceCreatedByNavigations)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("subscriptionprices_ibfk_1");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.SubscriptionpriceModifiedByNavigations)
                    .HasForeignKey(d => d.ModifiedBy)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("subscriptionprices_ibfk_2");
            });

            modelBuilder.Entity<Subscriptionpricedetail>(entity =>
            {
                entity.HasKey(e => e.SubscriptionPriceDetailsId)
                    .HasName("PRIMARY");

                entity.ToTable("subscriptionpricedetails");

                entity.HasIndex(e => e.CreatedBy, "CreatedBy");

                entity.HasIndex(e => e.DurationType, "DurationType");

                entity.HasIndex(e => e.SubscriptionPriceId, "SubscriptionPricesId");

                entity.Property(e => e.SubscriptionPriceDetailsId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DurationTime).HasColumnType("tinyint(4)");

                entity.Property(e => e.DurationType).HasColumnType("tinyint(4)");

                entity.Property(e => e.Price).HasPrecision(10);

                entity.Property(e => e.SubscriptionPriceId).HasColumnType("int(11)");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.Subscriptionpricedetails)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("subscriptionpricedetails_ibfk_1");

                entity.HasOne(d => d.DurationTypeNavigation)
                    .WithMany(p => p.Subscriptionpricedetails)
                    .HasForeignKey(d => d.DurationType)
                    .HasConstraintName("subscriptionpricedetails_ibfk_4");

                entity.HasOne(d => d.SubscriptionPrice)
                    .WithMany(p => p.Subscriptionpricedetails)
                    .HasForeignKey(d => d.SubscriptionPriceId)
                    .HasConstraintName("subscriptionpricedetails_ibfk_3");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
