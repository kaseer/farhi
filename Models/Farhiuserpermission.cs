﻿using System;
using System.Collections.Generic;

namespace Models
{
    public partial class Farhiuserpermission
    {
        public int UserId { get; set; }
        public int PermissionId { get; set; }
        public int? AdminCreatedBy { get; set; }
        public DateTime? AdminCreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }

        public virtual Adminuser AdminCreatedByNavigation { get; set; }
        public virtual Farhiuser CreatedByNavigation { get; set; }
        public virtual Permission Permission { get; set; }
        public virtual Farhiuser User { get; set; }
    }
}
