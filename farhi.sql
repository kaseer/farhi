-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 28, 2022 at 06:50 PM
-- Server version: 10.3.34-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `farhi`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `AddressId` int(11) NOT NULL,
  `AddressName` varchar(30) NOT NULL,
  `Status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`AddressId`, `AddressName`, `Status`) VALUES
(1, 'بن عاشور', 1),
(2, 'الظهرة', 1);

-- --------------------------------------------------------

--
-- Table structure for table `adminpermissions`
--

CREATE TABLE `adminpermissions` (
  `AdminId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `adminpermissions`
--

INSERT INTO `adminpermissions` (`AdminId`, `PermissionId`, `CreatedBy`, `CreatedOn`) VALUES
(1, 1, 1, '2022-04-21 16:26:12'),
(1, 2, 1, '2022-04-21 16:26:12'),
(1, 3, 1, '2022-04-21 16:26:12'),
(1, 4, 1, '2022-04-21 16:26:12'),
(1, 5, 1, '2022-04-21 16:26:12'),
(1, 6, 1, '2022-04-21 16:26:12'),
(1, 7, 1, '2022-04-21 16:26:12'),
(1, 8, 1, '2022-04-21 16:26:12'),
(1, 9, 1, '2022-04-21 16:26:12'),
(1, 10, 1, '2022-04-21 16:26:12'),
(1, 11, 1, '2022-04-21 16:26:12'),
(1, 12, 1, '2022-04-21 16:26:12'),
(1, 13, 1, '2022-04-21 16:26:12'),
(1, 14, 1, '2022-04-21 16:26:12'),
(1, 15, 1, '2022-04-21 16:26:12'),
(1, 16, 1, '2022-04-21 16:26:12'),
(1, 17, 1, '2022-04-21 16:26:12'),
(1, 18, 1, '2022-04-21 16:26:12'),
(1, 19, 1, '2022-04-21 16:26:12'),
(1, 20, 1, '2022-04-21 16:26:12'),
(1, 21, 1, '2022-04-21 16:26:12'),
(1, 22, 1, '2022-04-21 16:26:12'),
(1, 23, 1, '2022-04-21 16:26:12'),
(1, 24, 1, '2022-04-21 16:26:12'),
(1, 25, 1, '2022-04-21 16:26:12'),
(1, 26, 1, '2022-04-21 16:26:12'),
(1, 27, 1, '2022-04-21 16:26:12'),
(1, 28, 1, '2022-04-21 16:26:12'),
(1, 29, 1, '2022-04-21 16:26:12'),
(1, 30, 1, '2022-04-21 16:26:12'),
(1, 31, 1, '2022-04-21 16:26:12'),
(1, 32, 1, '2022-04-21 16:26:12'),
(1, 33, 1, '2022-04-21 16:26:12'),
(1, 34, 1, '2022-04-21 16:26:12'),
(1, 35, 1, '2022-04-21 16:26:12'),
(1, 36, 1, '2022-04-21 16:26:12'),
(1, 37, 1, '2022-04-21 16:26:12'),
(1, 38, 1, '2022-04-21 16:26:12'),
(1, 39, 1, '2022-04-21 16:26:12'),
(1, 40, 1, '2022-04-21 16:26:12'),
(1, 41, 1, '2022-04-21 16:26:12'),
(1, 42, 1, '2022-04-21 16:26:12'),
(1, 43, 1, '2022-04-21 16:26:12'),
(1, 44, 1, '2022-04-21 16:26:12'),
(1, 45, 1, '2022-04-21 16:26:12'),
(1, 46, 1, '2022-04-21 16:26:12'),
(1, 47, 1, '2022-04-21 16:26:12'),
(1, 48, 1, '2022-04-21 16:26:12'),
(1, 49, 1, '2022-04-21 16:26:12'),
(1, 50, 1, '2022-04-21 16:26:12'),
(1, 51, 1, '2022-04-21 16:26:12'),
(1, 52, 1, '2022-04-21 16:26:12'),
(1, 53, 1, '2022-04-21 16:26:12'),
(1, 54, 1, '2022-04-21 16:26:12'),
(1, 55, 1, '2022-04-21 16:26:12'),
(1, 56, 1, '2022-04-21 16:26:12'),
(1, 57, 1, '2022-04-21 16:26:12'),
(1, 58, 1, '2022-04-21 16:26:12'),
(1, 59, 1, '2022-04-21 16:26:12'),
(1, 60, 1, '2022-04-21 16:26:12'),
(1, 61, 1, '2022-04-21 16:26:12'),
(1, 62, 1, '2022-04-21 16:26:12'),
(1, 63, 1, '2022-04-21 16:26:12'),
(4, 1, 1, '2022-04-21 16:32:16'),
(4, 2, 1, '2022-04-21 16:32:16'),
(4, 3, 1, '2022-04-21 16:32:16'),
(4, 4, 1, '2022-04-21 16:32:16'),
(4, 5, 1, '2022-04-21 16:32:16'),
(4, 6, 1, '2022-04-21 16:32:16'),
(4, 12, 1, '2022-04-21 16:32:16'),
(4, 13, 1, '2022-04-21 16:32:16'),
(4, 14, 1, '2022-04-21 16:32:16'),
(4, 15, 1, '2022-04-21 16:32:16'),
(4, 16, 1, '2022-04-21 16:32:16'),
(4, 17, 1, '2022-04-21 16:32:16'),
(4, 18, 1, '2022-04-21 16:32:16'),
(4, 19, 1, '2022-04-21 16:32:16'),
(4, 20, 1, '2022-04-21 16:32:16'),
(4, 21, 1, '2022-04-21 16:32:16'),
(4, 22, 1, '2022-04-21 16:32:16'),
(4, 23, 1, '2022-04-21 16:32:16'),
(4, 24, 1, '2022-04-21 16:32:16'),
(4, 25, 1, '2022-04-21 16:32:16'),
(5, 26, 1, '2022-04-21 16:35:47'),
(5, 27, 1, '2022-04-21 16:35:47'),
(5, 28, 1, '2022-04-21 16:35:47'),
(5, 29, 1, '2022-04-21 16:35:47'),
(5, 30, 1, '2022-04-21 16:35:47'),
(5, 31, 1, '2022-04-21 16:35:47'),
(5, 32, 1, '2022-04-21 16:35:47'),
(6, 7, 1, '2022-04-21 16:39:23'),
(6, 8, 1, '2022-04-21 16:39:23'),
(6, 9, 1, '2022-04-21 16:39:23'),
(6, 10, 1, '2022-04-21 16:39:23'),
(6, 11, 1, '2022-04-21 16:39:23');

-- --------------------------------------------------------

--
-- Table structure for table `adminusers`
--

CREATE TABLE `adminusers` (
  `AdminId` int(11) NOT NULL,
  `AdminName` varchar(40) NOT NULL,
  `LoginName` varchar(40) NOT NULL,
  `Password` varchar(44) NOT NULL,
  `Salt` varchar(24) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `adminusers`
--

INSERT INTO `adminusers` (`AdminId`, `AdminName`, `LoginName`, `Password`, `Salt`, `Email`, `PhoneNo1`, `PhoneNo2`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'محمد سليم', 'mohammed', 'eCLb7ZNMIVtJ68maYt4xGFKFEi3AGCP2t4UDvqYd29I=', 'af6K+MYjTDImxSUkPn7m2w==', 'm@farhi.ly', '913333333', NULL, 1, 1, '2020-06-24 21:25:02', 1, '2022-04-21 16:25:40'),
(2, 'fsdfds', 'sdfsdf', 'c7dWTQb1fFH8qIcIE/PhIHiXydx/eW/XzT4V/mbSX88=', 'dTuyv+1cTQ2EZt2NJLLjTw==', 'fff@dd.dd', '913333333', NULL, 9, 1, '2022-04-21 15:58:21', 1, '2022-04-21 16:29:37'),
(3, 'xzccc', 'xcxc', '6ewYoqq5V++9ZlbDGjUOpdEcwZzoeM7N16De0CMAodo=', 'eOvdBriFFDXHKzlPCnDcDQ==', 'cxcx@edd.dd', '921111111', NULL, 9, 1, '2022-04-21 16:03:02', NULL, NULL),
(4, 'sdfsdf', 'sdfsdf', 'kX6oUxGwm74fNP8ya2S3wAUUGw9xYA7bZ7aofU7RaPs=', 'e9Jhne9YswEeEtMueNXKGA==', 'dsfds@dd.dd', '911111111', NULL, 1, 1, '2022-04-21 16:32:16', NULL, NULL),
(5, 'fdgdf', 'gdfg', '7+A7U55iGPvi1O0l66p5kJArfwXGQTS5umMGwdSVraY=', 'zCBHdFv8ikLqeNddLnUwQA==', 'dfgdfg@rr.rr', '911111111', NULL, 1, 1, '2022-04-21 16:35:47', NULL, NULL),
(6, 'hhhgfhg', 'hghg', 'UbIJ83RDUPQFl+z0+NAXHP4o9U2PbdyuNCullnGQnuc=', 'HtdppJz5wnObmJmBXQqNfg==', 'dd@dd.dd', '925555555', NULL, 2, 1, '2022-04-21 16:39:23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryId` int(11) NOT NULL,
  `Description` varchar(30) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryId`, `Description`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'مستلزمات افراح', 1, 1, '2020-06-09 21:26:58', NULL, NULL),
(2, 'فثفثفثف', 1, 1, '2022-04-18 13:11:53', 1, '2022-04-18 13:15:04'),
(3, 'بسيبسيبس', 1, 1, '2022-04-18 13:17:11', NULL, NULL),
(4, 'dfdsfsd', 1, 1, '2022-04-27 11:30:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `CustomerId` int(11) NOT NULL,
  `CustomerName` varchar(40) NOT NULL,
  `LoginName` varchar(40) NOT NULL,
  `Password` varchar(44) NOT NULL,
  `Salt` varchar(24) NOT NULL,
  `AddressId` int(11) NOT NULL,
  `AddressDescription` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `LastLogin` datetime NOT NULL,
  `LoginTimeAttempt` int(11) NOT NULL DEFAULT 0,
  `LoginStatus` tinyint(4) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `durationtypes`
--

CREATE TABLE `durationtypes` (
  `DurationTypeId` tinyint(4) NOT NULL,
  `DurationTypeName` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `durationtypes`
--

INSERT INTO `durationtypes` (`DurationTypeId`, `DurationTypeName`) VALUES
(1, 'سنة'),
(2, 'شهر'),
(3, 'اسبوع');

-- --------------------------------------------------------

--
-- Table structure for table `farhiuserhallbranches`
--

CREATE TABLE `farhiuserhallbranches` (
  `FarhiUserId` int(11) NOT NULL,
  `HallBranchId` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farhiuserhallbranches`
--

INSERT INTO `farhiuserhallbranches` (`FarhiUserId`, `HallBranchId`, `CreatedOn`) VALUES
(2, 21, '2022-04-25 14:33:26'),
(2, 23, '2022-04-25 14:33:26'),
(2, 24, '2022-04-25 14:33:26'),
(2, 25, '2022-04-25 14:33:26'),
(2, 26, '2022-04-25 14:33:26'),
(5, 5, '0000-00-00 00:00:00'),
(11, 8, '0000-00-00 00:00:00'),
(22, 22, '0000-00-00 00:00:00'),
(22, 23, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `farhiuserpermissions`
--

CREATE TABLE `farhiuserpermissions` (
  `UserId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `farhiusers`
--

CREATE TABLE `farhiusers` (
  `FarhiUserId` int(11) NOT NULL,
  `UserName` varchar(40) NOT NULL,
  `LoginName` varchar(40) NOT NULL,
  `Password` varchar(44) NOT NULL,
  `Salt` varchar(24) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `HallId` int(11) DEFAULT NULL,
  `StoreId` int(11) DEFAULT NULL,
  `LoginStatus` tinyint(4) NOT NULL,
  `LastLogin` datetime NOT NULL,
  `LoginTimeAttempts` tinyint(4) NOT NULL DEFAULT 0,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL,
  `AdminModifiedBy` int(11) DEFAULT NULL,
  `AdminModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farhiusers`
--

INSERT INTO `farhiusers` (`FarhiUserId`, `UserName`, `LoginName`, `Password`, `Salt`, `Email`, `PhoneNo1`, `PhoneNo2`, `HallId`, `StoreId`, `LoginStatus`, `LastLogin`, `LoginTimeAttempts`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`, `AdminCreatedBy`, `AdminCreatedOn`, `AdminModifiedBy`, `AdminModifiedOn`) VALUES
(1, 'محد سليم', 'mohammed', 'FVe+Tm59j+Buc3q1KJcLVGulZBw0S/3IfLftjat7Hyo=', 'iKBCyU/p7yPX6ujCIEnq8A==', 'm@farhi.ly', '913333333', NULL, 1, NULL, 0, '2020-06-09 22:03:45', 0, 1, 1, '2020-06-09 22:03:45', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'erwerw', 'werwe', 'IT4IyOixd7kjNY+7G3CSxrMsxWjPaAQBqnSWcWWuv/Y=', 'JfrWrHGU0OiXdcMRiUY1MQ==', 'w2@ss.ss', '911111111', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-16 19:38:47', 1, '2022-04-25 14:33:26', NULL, NULL, NULL, NULL),
(3, 'fdg', 'gfhg', 'm+9CnfVALTbm69FNFz1L3tGthPhekax4u6e+JKw8PQU=', 'tH5CqoycbVwY+JiRlMuQuA==', 'sddfs@fdf.hj', '911111111', NULL, 3, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-16 22:01:16', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'sdfsdf', 'sdfsdfsd', 'dsfsdfsd', '', 'sds@dsd.hgh', '911111111', '911111111', 4, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-17 19:53:49', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'sdfrsdf', 'sdfsdrfsd', 'dsfsdfsd', '', 'sds@dsd.hrgh', '911111111', '911111111', NULL, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-17 19:57:23', NULL, NULL, NULL, NULL, NULL, NULL),
(6, '4sdfrsdf', 'sdfsdrf4sd', 'dsfsdfsd', '', 'sds@dsd.hrrgh', '911111111', '911111111', 6, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-17 20:10:23', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'ewrwer', 'werwer', 'werwerwer', '', 'fsdfs@wsww.ee', '911111111', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-17 20:37:52', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'ewrywer', 'werwyer', 'werwerwer', '', 'fsdfs@wsyww.ee', '911111111', NULL, 12, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-17 20:38:48', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'sdfsf', 'fsdfsdfds', 'P@$$w0rd', '', 'ff@ff.ff', '911111111', NULL, 20, NULL, 0, '0001-01-01 00:00:00', 0, 2, 1, '2020-11-20 16:43:03', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'erter', 'tertert', 'M@dd10215', '', 'ff@ee.ee', '911111111', NULL, 21, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-20 18:12:12', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'jhjhjh', 'bgdgfsd', 'M@hh6789b', '', 'hhh@jjj.lj', '911111111', NULL, 29, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-11-24 21:13:08', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'hussam', 'hhh', 'h?H**&23Dddnh', '', 'dd@dd.dd', '911111111', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2020-12-30 22:10:37', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'asdasd', 'asdasdasd', 'M@hm0od2019', '', 'sss@ss.ss', '911111111', NULL, 30, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2021-03-20 14:11:43', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'gdfgdf', 'dgdfgdf', '@wrfseM3442', '', 'ff@see.ee', '911111111', NULL, 31, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2021-03-20 16:57:32', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'eterter', 'ertert', 'ert@#4234rfdsfgbv3453', '', 'ddd@ww.ww', '911111111', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 9, 1, '2021-03-21 17:03:54', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'asdas', 'sdaasda', 'dasd342%$Sf', '', 'ds@des.dsd', '911111111', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 9, 1, '2021-03-22 20:42:28', NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'dd', 'dsd', 'M@hm0od2O10', '', 'mm@mm.nn', '911111111', NULL, 32, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2021-08-08 20:05:05', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'ddsf', 'sdfsdfs', 'FF@s345234fr', '', 'ss@ee.ee', '911111111', NULL, 33, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2021-08-10 19:10:44', NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'dsfsf', 'sdfsdf', 'Pk9Fd62ruvaomY510lC1Hr5PLZ+R2LRRfjPYeJLwzys=', '1QtpdEdcoAkuKt3jTt6dfw==', 'sdd@ee.ee', '911111111', NULL, 34, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2021-08-10 19:14:35', NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'fsdf', 'fsdfsdfsd', 'M2WW#3442', '', 'mm@dd.dd', '912222222', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 9, 1, '2021-09-23 17:12:41', NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'cxzc', 'asdasds', 'w/B01SyMHSjfXmbTpaWZpVBEZYjG+45EwLYegKz8OPg=', 'jdV43cyUjgPyCQPo0jtOMQ==', 'asds@dd.dd', '912111111', NULL, 6, NULL, 0, '0001-01-01 00:00:00', 0, 1, 1, '2022-04-25 12:45:13', 1, '2022-04-25 14:23:01', NULL, NULL, NULL, NULL),
(35, 'werew', 'werew', 'DU4HKKrVIzAa603fgpZIMsRVH/jvqET08q7G3m1o9Kk=', '4ELB56QXMCjQMljVnk0uNA==', 'dfs@dss.ds', '912222222', NULL, NULL, NULL, 0, '0001-01-01 00:00:00', 0, 2, 1, '2022-04-25 16:05:31', NULL, NULL, NULL, NULL, 1, '2022-04-25 16:15:58'),
(36, 'adas', 'dasd', 'CmiKIZtNWqcnFC4lCCzAz69XV/dlXaAFmCNgrDxlyZQ=', 'NpdZAZuf17pi7VHaoxa8ew==', 'ss@ss.dd', '915555555', NULL, NULL, 2, 0, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, NULL, 1, '2022-04-25 16:11:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `farhiuserstorebranches`
--

CREATE TABLE `farhiuserstorebranches` (
  `FarhiUserId` int(11) NOT NULL,
  `StoreBranchId` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `farhiuserstorebranches`
--

INSERT INTO `farhiuserstorebranches` (`FarhiUserId`, `StoreBranchId`, `CreatedOn`) VALUES
(35, 1, '2022-04-25 16:15:58');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `FeatureId` int(11) NOT NULL,
  `FeatureName` varchar(50) NOT NULL,
  `FeatureType` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`FeatureId`, `FeatureName`, `FeatureType`, `Status`, `CreatedBy`) VALUES
(1, 'إدارة الصالات', 1, 1, 1),
(2, 'فروع الصالات', 1, 1, 1),
(3, 'المحلات', 1, 1, 1),
(4, 'فروع المحلات', 1, 1, 1),
(5, 'Slider Management', 1, 1, 1),
(6, 'انواع المحلات', 1, 1, 1),
(7, 'انواع المنتجات', 1, 1, 1),
(8, 'انواع المنتجات الفرعية', 1, 1, 1),
(9, 'الحجوزات', 1, 1, 1),
(10, 'مستخدمي النظام', 1, 1, 1),
(11, 'زبائن فرحي(صالات)', 1, 1, 1),
(12, 'زبائن فرحي(محلات)', 1, 1, 1),
(13, 'الزبائن', 1, 1, 1),
(14, 'الاشتراكات', 1, 1, 1),
(15, 'التقارير', 1, 1, 1),
(16, 'لوحة البيانات', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hallappointments`
--

CREATE TABLE `hallappointments` (
  `HallAppointmentId` int(11) NOT NULL,
  `ReservationTypeId` int(11) NOT NULL,
  `WorkDayId` tinyint(4) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `HallBranchId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hallappointments`
--

INSERT INTO `hallappointments` (`HallAppointmentId`, `ReservationTypeId`, `WorkDayId`, `StartTime`, `EndTime`, `HallBranchId`, `Price`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 1, 1, '01:00:00', '05:00:00', 18, '3', 1, 1, '2020-11-24 20:58:14', NULL, NULL),
(2, 1, 1, '02:00:00', '04:00:00', 18, '3', 1, 1, '2020-11-24 20:58:14', NULL, NULL),
(3, 1, 1, '01:00:00', '05:00:00', 19, '3', 1, 1, '2020-11-24 20:58:48', NULL, NULL),
(4, 1, 1, '02:00:00', '04:00:00', 19, '3', 1, 1, '2020-11-24 20:58:48', NULL, NULL),
(5, 1, 1, '01:00:00', '05:00:00', 21, '3', 1, 1, '2020-11-25 19:26:01', NULL, NULL),
(6, 1, 1, '02:00:00', '05:00:00', 21, '7', 1, 1, '2020-11-25 19:26:01', NULL, NULL),
(7, 1, 1, '01:00:00', '07:00:00', 22, '5', 1, 1, '2020-11-28 18:13:35', NULL, NULL),
(8, 1, 2, '00:00:00', '08:00:00', 22, '5', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(9, 1, 2, '01:00:00', '05:00:00', 22, '5', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(10, 1, 3, '01:00:00', '05:00:00', 22, '3', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(11, 1, 4, '01:00:00', '05:00:00', 22, '3', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(12, 1, 5, '01:00:00', '04:00:00', 22, '3', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(13, 1, 6, '01:00:00', '04:00:00', 22, '4', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(14, 1, 7, '01:00:00', '05:00:00', 22, '3', 1, 1, '2020-11-28 18:13:46', NULL, NULL),
(15, 1, 1, '01:00:00', '07:00:00', 23, '5', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(16, 1, 2, '00:00:00', '08:00:00', 23, '5', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(17, 1, 2, '01:00:00', '05:00:00', 23, '5', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(18, 1, 3, '01:00:00', '05:00:00', 23, '3', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(19, 1, 4, '01:00:00', '05:00:00', 23, '3', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(21, 1, 6, '01:00:00', '04:00:00', 23, '4', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(22, 1, 7, '01:00:00', '05:00:00', 23, '3', 1, 1, '2020-11-28 18:15:50', NULL, NULL),
(27, 1, 7, '01:00:00', '04:00:00', 25, '7', 1, 1, '2020-11-30 23:04:29', NULL, NULL),
(31, 1, 1, '01:00:00', '05:00:00', 26, '6', 1, 1, '2020-12-23 13:53:25', NULL, NULL),
(32, 1, 1, '00:00:00', '05:00:00', 26, '13', 1, 1, '2020-12-23 13:53:25', NULL, NULL),
(33, 1, 1, '01:00:00', '05:00:00', 26, '8', 1, 1, '2020-12-23 13:53:25', NULL, NULL),
(34, 1, 3, '01:00:00', '04:00:00', 26, '6', 1, 1, '2020-12-23 13:53:25', NULL, NULL),
(35, 1, 3, '01:00:00', '04:00:00', 26, '13', 1, 1, '2020-12-23 13:53:25', NULL, NULL),
(36, 1, 1, '01:00:00', '05:00:00', 27, '5', 1, 1, '2020-12-24 17:52:59', NULL, NULL),
(37, 1, 1, '01:00:00', '05:00:00', 27, '3', 1, 1, '2020-12-24 17:52:59', NULL, NULL),
(38, 1, 1, '01:00:00', '05:00:00', 27, '11', 1, 1, '2020-12-24 17:52:59', NULL, NULL),
(39, 1, 3, '01:00:00', '05:00:00', 27, '5', 1, 1, '2020-12-24 17:52:59', NULL, NULL),
(40, 1, 1, '01:01:00', '09:00:00', 29, '10', 1, 1, '2021-03-20 14:19:53', NULL, NULL),
(41, 1, 1, '00:00:00', '07:00:00', 29, '8', 1, 1, '2021-03-20 14:19:53', NULL, NULL),
(42, 1, 5, '01:00:00', '07:00:00', 29, '7', 1, 1, '2021-03-20 14:19:53', NULL, NULL),
(43, 1, 1, '00:00:00', '05:00:00', 28, '8', 1, 1, '2021-03-20 15:35:25', NULL, NULL),
(44, 1, 5, '01:00:00', '15:00:00', 30, '5', 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(45, 1, 5, '01:00:00', '04:00:00', 30, '5', 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(46, 1, 5, '01:00:00', '05:00:00', 30, '6', 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(47, 1, 7, '01:00:00', '05:00:00', 30, '5', 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(48, 1, 7, '00:00:00', '05:00:00', 30, '6', 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(49, 1, 1, '02:00:00', '02:00:00', 31, '6', 1, 1, '2021-08-08 20:05:05', NULL, NULL),
(50, 1, 1, '01:00:00', '02:00:00', 32, '7', 1, 1, '2021-08-10 19:10:44', NULL, NULL),
(51, 1, 1, '02:00:00', '02:00:00', 33, '4', 1, 1, '2021-08-10 19:14:35', NULL, NULL),
(52, 1, 1, '02:00:00', '02:00:00', 34, '3', 1, 1, '2021-08-10 23:36:04', NULL, NULL),
(53, 1, 1, '02:00:00', '02:00:00', 35, '7', 1, 1, '2021-08-28 20:27:04', NULL, NULL),
(54, 1, 1, '02:00:00', '02:00:00', 36, '3', 1, 1, '2021-08-28 20:33:59', NULL, NULL),
(57, 1, 1, '02:00:00', '02:00:00', 1, '3', 1, 1, '2021-08-28 21:59:06', NULL, NULL),
(58, 1, 1, '02:00:00', '02:00:00', 37, '3', 1, 1, '2021-09-21 12:52:29', NULL, NULL),
(59, 1, 1, '02:00:00', '02:00:00', 37, '2', 1, 1, '2021-09-21 12:52:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hallbranchconditions`
--

CREATE TABLE `hallbranchconditions` (
  `ConditionId` int(11) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `HallBranchId` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hallbranches`
--

CREATE TABLE `hallbranches` (
  `HallBranchId` int(11) NOT NULL,
  `AddressId` int(11) NOT NULL,
  `AddressDescription` varchar(50) DEFAULT NULL,
  `HallId` int(11) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `Longitude` decimal(10,0) DEFAULT NULL,
  `Latitude` decimal(10,0) DEFAULT NULL,
  `PeapleCapacity` int(11) NOT NULL,
  `CarsCapacity` int(11) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `StandardPrice` decimal(10,0) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hallbranches`
--

INSERT INTO `hallbranches` (`HallBranchId`, `AddressId`, `AddressDescription`, `HallId`, `PhoneNo1`, `PhoneNo2`, `Longitude`, `Latitude`, `PeapleCapacity`, `CarsCapacity`, `StartTime`, `EndTime`, `StandardPrice`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 1, 'dfsdfsdf', 1, '913333333', NULL, '0', '0', 100, 20, '21:08:03', '21:42:03', NULL, 1, 1, '2020-06-24 21:25:02', 1, '2021-09-23 14:24:18'),
(2, 1, 'fewfrer', 2, '912222222', NULL, '4', '3', 3, 4, '01:00:00', '07:00:00', NULL, 1, 1, '2020-11-16 19:38:45', 1, '2021-09-23 15:42:38'),
(3, 1, 'vfdfg', 3, '91111111', NULL, '3', '3', 6, 6, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-16 22:01:16', 1, '2020-11-21 15:17:08'),
(4, 1, 'dasdasdas', 4, '911111111', '911111111', '10', '15', 9, 12, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-17 19:53:47', NULL, NULL),
(5, 1, 'dasdasdas', 5, '911111111', '911111111', '10', '15', 9, 12, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-17 19:57:23', 1, '2021-08-08 17:29:54'),
(6, 1, 'dasdasdas', 6, '911111111', '911111111', '10', '15', 9, 12, '01:00:00', '05:00:00', NULL, 2, 1, '2020-11-17 20:10:23', 1, '2021-08-08 17:39:37'),
(8, 1, 'dfsdfsd', 11, '911111111', NULL, '32', '32', 3, 3, '01:00:00', '11:00:00', NULL, 1, 1, '2020-11-17 20:37:49', 1, '2021-08-08 17:46:10'),
(9, 1, 'dfysdfsd', 12, '911111111', NULL, '32', '32', 3, 3, '01:00:00', '11:00:00', NULL, 2, 1, '2020-11-17 20:38:47', 1, '2021-08-08 17:27:11'),
(16, 1, NULL, 20, '911111111', NULL, '6', '7', 30, 30, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-20 16:43:03', 1, '2021-08-08 17:45:09'),
(17, 1, NULL, 21, '911111111', '911111111', '43', '34', 44, 44, '01:00:00', '04:00:00', NULL, 9, 1, '2020-11-20 18:12:12', 1, '2021-02-27 15:17:32'),
(18, 1, '2ddds', 1, '911111111', NULL, '0', '0', 12, 21, '00:00:00', '05:00:00', NULL, 1, 1, '2020-11-24 20:58:13', 1, '2021-09-22 18:02:39'),
(19, 2, '2d23dds', 1, '911111111', NULL, NULL, NULL, 12, 21, '00:00:00', '05:00:00', NULL, 9, 1, '2020-11-24 20:58:48', 1, '2020-12-12 20:52:51'),
(20, 1, NULL, 29, '911111111', NULL, '0', '0', 3, 3, '01:00:00', '12:00:00', NULL, 1, 1, '2020-11-24 21:13:08', 1, '2021-09-23 17:15:24'),
(21, 1, 'sdfsdf', 1, '911111111', NULL, '0', '0', 2, 4, '00:00:00', '07:00:00', NULL, 1, 1, '2020-11-25 19:26:01', 1, '2021-08-10 23:24:46'),
(22, 1, 'dcfsfd', 1, '911111111', NULL, '3', '0', 4, 4, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-28 18:13:17', NULL, NULL),
(23, 1, 'dcfsfd', 1, '911111111', NULL, '3', '0', 4, 4, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-28 18:15:50', 1, '2021-08-10 23:17:52'),
(24, 1, '????????', 1, '911111111', NULL, '0', '0', 4, 4, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-28 18:42:55', 1, '2021-08-10 23:13:40'),
(25, 2, '??? ??????', 1, '911111111', NULL, '1', '5', 6, 5, '01:00:00', '05:00:00', NULL, 1, 1, '2020-11-30 23:04:28', 1, '2021-08-10 23:11:00'),
(26, 1, 'sfsdfsdfsdfsd', 1, '911111111', NULL, '7', '7', 6, 5, '00:00:00', '23:00:00', NULL, 1, 1, '2020-12-23 13:53:25', 1, '2021-08-21 14:41:14'),
(27, 1, 'uyrttujyfkjg', 1, '911111111', NULL, '7', '6', 5, 5, '00:00:00', '05:00:00', NULL, 9, 1, '2020-12-24 17:52:58', 1, '2021-08-10 23:09:00'),
(28, 1, 'asdfasdfasdf', 30, '911111111', NULL, '5', '6', 5, 6, '02:00:00', '09:00:00', NULL, 1, 1, '2021-03-20 14:11:43', 1, '2021-03-20 15:50:43'),
(29, 1, 'rtuyrt', 30, '9111111111', NULL, '3', '-2', 6, 6, '01:00:00', '05:00:00', NULL, 1, 1, '2021-03-20 14:19:53', 1, '2021-03-20 15:30:47'),
(30, 1, NULL, 31, '9111111111', NULL, '3', '3', 3, 4, '01:00:00', '07:00:00', NULL, 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(31, 1, 'sdfsdf', 32, '911111111', NULL, '3', '4', 4, 4, '00:01:00', '04:02:00', NULL, 1, 1, '2021-08-08 20:05:05', NULL, NULL),
(32, 1, '911111111', 33, '911111111', NULL, '3', '0', 3, 5, '01:01:00', '02:00:00', NULL, 1, 1, '2021-08-10 19:10:44', NULL, NULL),
(33, 1, 'wqdsqw', 34, '911111111', NULL, '0', '0', 3, 3, '02:00:00', '02:00:00', NULL, 9, 1, '2021-08-10 19:14:35', 1, '2021-08-10 19:52:17'),
(34, 1, 'dsfsaf', 2, '912222222', NULL, '0', '3', 3, 30, '02:00:00', '02:00:00', NULL, 1, 1, '2021-08-10 23:36:04', 1, '2021-09-23 15:42:22'),
(35, 1, 'sdfsfsd', 4, '912222222', NULL, '0', '0', 1, 2, '02:00:00', '02:00:00', NULL, 0, 1, '2021-08-28 20:27:04', NULL, NULL),
(36, 1, 'werwerwe', 4, '911333333', NULL, '0', '0', 1, 1, '02:00:00', '02:00:00', NULL, 0, 1, '2021-08-28 20:33:21', NULL, NULL),
(37, 1, 'sdfsdf', 1, '911111111', NULL, '2', '3', 5, 3, '02:00:00', '02:00:00', NULL, 9, 1, '2021-09-21 12:52:29', 1, '2021-09-25 14:30:05');

-- --------------------------------------------------------

--
-- Table structure for table `hallbranchservices`
--

CREATE TABLE `hallbranchservices` (
  `HallBranchServiceId` int(11) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `HallBranchId` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hallbranchservices`
--

INSERT INTO `hallbranchservices` (`HallBranchServiceId`, `Description`, `Price`, `HallBranchId`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`, `AdminCreatedBy`, `AdminCreatedOn`) VALUES
(1, 'اكل', '50', 1, 1, 1, '2020-06-16 21:46:27', NULL, NULL, NULL, NULL),
(2, 'sdfsdf', '2', 19, 1, 1, '2020-11-24 20:58:48', NULL, NULL, NULL, NULL),
(3, 'vdvvf', '1', 19, 1, 1, '2020-11-24 20:58:48', NULL, NULL, NULL, NULL),
(4, 'vfdvdfvv', '4', 19, 1, 1, '2020-11-24 20:58:48', NULL, NULL, NULL, NULL),
(5, 'vgbdfgdf', '3', 22, 1, 1, '2020-11-28 18:13:53', NULL, NULL, NULL, NULL),
(6, 'vgbdfgdf', '3', 23, 1, 1, '2020-11-28 18:15:50', NULL, NULL, NULL, NULL),
(9, 'asdasdasd', '6', 26, 1, 1, '2020-12-23 13:53:25', NULL, NULL, NULL, NULL),
(10, 'asdasdasdas', '11', 26, 1, 1, '2020-12-23 13:53:25', NULL, NULL, NULL, NULL),
(11, 'gdfgdfgdf', '4', 30, 1, 1, '2021-03-20 16:57:32', NULL, NULL, NULL, NULL),
(12, 'dfgfdgdf', '6', 30, 1, 1, '2021-03-20 16:57:32', NULL, NULL, NULL, NULL),
(13, 'rwerw', '3', 35, 1, 1, '2021-08-28 20:27:04', NULL, NULL, NULL, NULL),
(14, 'ewerew', '1', 36, 1, 1, '2021-08-28 20:33:59', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hallreservationdetails`
--

CREATE TABLE `hallreservationdetails` (
  `HallReservationDetailsId` int(11) NOT NULL,
  `HallReservationId` int(11) NOT NULL,
  `HallAppointmentId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UserModifiedBy` int(11) DEFAULT NULL,
  `UserModifiedOn` datetime DEFAULT NULL,
  `CustomerIsModified` tinyint(1) DEFAULT NULL,
  `CustomerModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hallreservations`
--

CREATE TABLE `hallreservations` (
  `HallReservationId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `HallBranchId` int(11) NOT NULL,
  `ReservationDate` date NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `AdminModifiedBy` int(11) DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `CustomerIsModified` tinyint(1) DEFAULT NULL,
  `CustomerModifiedOn` datetime DEFAULT NULL,
  `AdminModifiedOn` datetime DEFAULT NULL,
  `UserCreatedBy` int(11) DEFAULT NULL,
  `UserCreatedOn` datetime DEFAULT NULL,
  `UserModifiedBy` int(11) DEFAULT NULL,
  `UserModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--

CREATE TABLE `halls` (
  `HallId` int(11) NOT NULL,
  `HallName` varchar(30) NOT NULL,
  `OwnerName` varchar(40) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `MainBranchId` int(11) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `halls`
--

INSERT INTO `halls` (`HallId`, `HallName`, `OwnerName`, `PhoneNo1`, `PhoneNo2`, `Email`, `Logo`, `MainBranchId`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'الهناء2121', 'سليم', '913333333', NULL, 'm@farhi.ly', 'الهناء2121_Logo.jpg', 1, 1, 1, '2020-06-09 21:30:24', 1, '2021-08-21 14:48:40'),
(2, 'erewr', 'werwer', '912222222', NULL, 'eee@ee.ee', 'erewr_Logo.jpg', 2, 1, 1, '2020-11-16 19:38:34', 1, '2021-09-23 15:42:38'),
(3, 'hgghg', 'hghgh', '911111111', NULL, 'ghh@ff.jj', 'foto4.png', 3, 1, 1, '2020-11-16 22:01:16', 1, '2020-11-21 15:17:08'),
(4, 'daasdas', 'dasdsa', '911111111', '911111111', 'saas@eded.dd', 'foto3.png', 4, 1, 1, '2020-11-17 19:53:45', NULL, NULL),
(5, 'daasdars', 'darsdsa', '911111111', '911111111', 'esaas@eded.dd', 'foto2.png', 5, 2, 1, '2020-11-17 19:57:23', 1, '2021-08-08 17:29:54'),
(6, 'daasd5ars', 'dars5dsa', '911111111', '911111111', 'esaars@eded.dd', 'foto1.png', 6, 2, 1, '2020-11-17 20:10:23', 1, '2021-08-08 17:39:37'),
(11, 'retet', 'ertert', '911111111', NULL, 'fdgdfg@ee.ee', 'foto5.png', 8, 1, 1, '2020-11-17 20:37:47', 1, '2021-08-08 17:46:10'),
(12, 'reyytet', 'ertyyert', '911111111', NULL, 'fdgydfg@ee.ee', 'foto5.png', 9, 2, 1, '2020-11-17 20:38:45', 1, '2021-08-08 17:27:11'),
(20, 'dfgdfgd', 'dfgdfgdf', '911111111', NULL, 'ddd@dd.dd', 'dfgdfgd_Logo.jpg', 16, 1, 1, '2020-11-20 16:43:03', 1, '2021-08-08 17:45:09'),
(21, 'gdfgdf', 'dfgdfgd', '911111111', NULL, 'fff@dd.dd', 'gdfgdf_Logo.jpg', 17, 9, 1, '2020-11-20 18:12:11', 1, '2021-02-27 15:17:32'),
(29, 'jkghj', 'ngfghfgh', '911111111', NULL, 'mm@hhh.kk', 'jkghj_Logo.jpg', 20, 1, 1, '2020-11-24 21:13:06', 1, '2021-09-23 17:15:24'),
(30, 'asdasd', 'sadasd', '911111111', NULL, 'ss@ss.ss', 'asdasd_Logo.jpg', 28, 1, 1, '2021-03-20 14:11:42', NULL, NULL),
(31, 'dsfgdg', 'dfgdfgdf', '911111111', NULL, 'ss@dd.ff', 'dsfgdg_Logo.jpg', 30, 1, 1, '2021-03-20 16:57:32', NULL, NULL),
(32, 'fthyyfghfg', 'rtyrtyrt', '911111111', NULL, 'gg@rr.rr', 'fthyyfghfg_Logo.jpg', 31, 1, 1, '2021-08-08 20:05:03', NULL, NULL),
(33, 'tess', 'tesss', '911111111', NULL, 'dd@dd.dd', 'tess_Logo.jpg', 32, 1, 1, '2021-08-10 19:10:40', NULL, NULL),
(34, 'sadrasd', 'asdasd', '911111111', NULL, 'ss@2ss.ss', 'sadrasd_Logo.jpg', 33, 9, 1, '2021-08-10 19:14:33', 1, '2021-08-10 19:52:17');

-- --------------------------------------------------------

--
-- Table structure for table `hallservicereservation`
--

CREATE TABLE `hallservicereservation` (
  `HallBranchServiceId` int(11) NOT NULL,
  `HallReservationDetailsId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UserModifiedBy` int(11) DEFAULT NULL,
  `UserModifiedOn` datetime DEFAULT NULL,
  `CustomerIsModified` tinyint(1) DEFAULT NULL,
  `CustomerModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `MediaId` int(11) NOT NULL,
  `MediaType` int(11) NOT NULL,
  `Path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `HallBranchId` int(11) DEFAULT NULL,
  `StoreBranchId` int(11) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`MediaId`, `MediaType`, `Path`, `HallBranchId`, `StoreBranchId`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`, `AdminCreatedBy`, `AdminCreatedOn`) VALUES
(64, 1, 'محل1_1_1.jpg', NULL, 1, 1, 1, '2021-04-05 22:14:12', NULL, NULL, NULL, NULL),
(67, 1, 'محل1_1_1.jpg', NULL, 1, 1, 1, '2021-04-05 22:16:00', NULL, NULL, NULL, NULL),
(70, 1, 'محل1_1_4.jpg', NULL, 1, 1, 1, '2021-04-05 22:16:00', NULL, NULL, NULL, NULL),
(73, 1, 'erewr_2_2.jpg', 2, NULL, 1, 1, '2021-09-22 19:21:47', NULL, NULL, NULL, NULL),
(78, 1, 'erewr_2_3.jpg', 2, NULL, 1, 1, '2021-09-22 19:32:41', NULL, NULL, NULL, NULL),
(83, 1, 'الهناء2121_1_5.jpg', 1, NULL, 1, 1, '2021-09-22 20:41:30', NULL, NULL, NULL, NULL),
(84, 1, 'الهناء2121_1_1.jpg', 1, NULL, 1, 1, '2021-09-22 20:43:27', NULL, NULL, NULL, NULL),
(85, 1, 'الهناء2121_1_2.jpg', 1, NULL, 1, 1, '2021-09-22 20:43:27', NULL, NULL, NULL, NULL),
(88, 1, 'الهناء2121_1_2.jpg', 1, NULL, 1, 1, '2021-09-22 20:47:30', NULL, NULL, NULL, NULL),
(89, 1, 'الهناء2121_1_1.jpg', 1, NULL, 1, 1, '2021-09-22 20:49:53', NULL, NULL, NULL, NULL),
(90, 1, 'الهناء2121_1_2.jpg', 1, NULL, 1, 1, '2021-09-22 20:49:53', NULL, NULL, NULL, NULL),
(92, 1, 'الهناء2121_1_1.jpg', 1, NULL, 1, 1, '2021-09-22 20:51:42', NULL, NULL, NULL, NULL),
(95, 2, 'الهناء2121_18.mp4', 18, NULL, 1, 1, '2021-09-23 14:14:38', NULL, NULL, NULL, NULL),
(96, 2, 'الهناء2121_1.mp4', 1, NULL, 1, 1, '2021-09-23 14:22:54', NULL, NULL, NULL, NULL),
(97, 1, 'محل1_1_1.jpg', NULL, 1, 1, 1, '2021-09-25 17:46:30', NULL, NULL, NULL, NULL),
(98, 1, 'محل1_7_1.jpg', NULL, 7, 1, 1, '2022-04-26 16:56:13', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `PermissionId` int(11) NOT NULL,
  `PermissionName` varchar(50) NOT NULL,
  `Code` varchar(5) NOT NULL,
  `FeatureId` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`PermissionId`, `PermissionName`, `Code`, `FeatureId`, `Status`, `CreatedBy`, `CreatedOn`) VALUES
(1, 'عرض الصالات', '01000', 1, 1, 1, '2020-11-04 22:08:27'),
(2, 'اضافة صالة', '01001', 1, 1, 1, '2020-11-01 22:08:27'),
(3, 'عرض بيانات الصالة', '01002', 1, 1, 1, '2020-11-17 22:08:27'),
(4, 'تعديل بيانات الصالة', '01003', 1, 1, 1, '2020-11-04 22:08:27'),
(5, 'تجميد و فك تجميد الصالة', '01004', 1, 1, 1, '2020-11-11 23:03:20'),
(6, 'حذف الصالة', '01005', 1, 1, 1, '2020-11-04 22:12:03'),
(7, 'عرض الزبائن', '13000', 13, 1, 1, '2020-11-04 23:10:48'),
(8, 'عرض بيانات الزبون', '13002', 13, 1, 1, '2020-11-04 23:23:05'),
(9, 'اضافة زبون', '13001', 13, 1, 1, '2020-11-04 23:25:25'),
(10, 'تعديل بيانات الزبون', '13003', 13, 1, 1, '2020-11-04 23:26:38'),
(11, 'حذف زبون', '13005', 13, 1, 1, '2020-11-04 23:28:20'),
(12, 'عرض الفروع', '02000', 2, 1, 1, '2020-11-05 18:46:25'),
(13, 'اضافة فرع', '02001', 2, 1, 1, '2020-11-05 18:55:13'),
(14, 'عرض بيانات الفرع', '02002', 2, 1, 1, '2020-11-05 18:56:17'),
(15, 'تعديل بيانات الفرع', '02003', 2, 1, 1, '2020-11-05 18:57:15'),
(16, 'تجميد وفك تجميد فرع', '02004', 2, 1, 1, '2020-11-05 18:59:16'),
(17, 'حذف الفرع', '02005', 2, 1, 1, '2020-11-05 19:01:21'),
(18, 'اضافة ميديا', '02006', 2, 1, 1, '2020-11-05 19:02:22'),
(19, 'اشتراك', '02007', 2, 1, 1, '2020-11-05 19:03:36'),
(20, 'عرض المحلات', '03000', 3, 1, 1, '2020-11-05 19:04:19'),
(21, 'اضافة محل', '03001', 3, 1, 1, '2020-11-05 19:11:13'),
(22, 'عرض بيانات المحل', '03002', 3, 1, 1, '2020-11-05 19:12:16'),
(23, 'تعديل بيانات المحل', '03003', 3, 1, 1, '2020-11-05 19:18:37'),
(24, 'تجميد وفك تجميد محل', '03004', 3, 1, 1, '2020-11-05 19:20:54'),
(25, 'حذف محل', '03005', 3, 1, 1, '2020-11-05 19:21:37'),
(26, 'عرض فروع المحلات', '04000', 4, 1, 1, '2020-11-05 19:34:49'),
(27, 'اضافة فرع لمحل', '04001', 4, 1, 1, '2020-11-05 20:07:21'),
(28, 'عرض بيانات الفرع', '04002', 4, 1, 1, '2020-11-05 20:20:45'),
(29, 'تعديل بيانات الفرع', '04003', 4, 1, 1, '2020-11-05 20:22:01'),
(30, 'حذف فرع محل', '04005', 4, 1, 1, '2020-11-05 20:25:32'),
(31, 'اضافة ميديا', '04006', 4, 1, 1, '2020-11-05 20:27:28'),
(32, 'اشتراك', '04007', 4, 1, 1, '2020-11-05 20:28:42'),
(33, 'عرض Slider Management', '05000', 5, 1, 1, '2020-11-05 20:30:39'),
(34, 'اضافة Slider Management', '05001', 5, 1, 1, '2020-11-05 20:32:17'),
(35, 'تعديل Slider Management', '05003', 5, 1, 1, '2020-11-05 20:33:12'),
(36, 'حذف Slider Management', '05005', 5, 1, 1, '2020-11-05 20:33:59'),
(37, 'عرض انواع المحلات', '06000', 6, 1, 1, '2020-11-05 20:35:34'),
(38, 'اضافة نوع', '06001', 6, 1, 1, '2020-11-05 20:37:04'),
(39, 'تعديل نوع ', '06003', 6, 1, 1, '2020-11-05 20:38:07'),
(40, 'حذف نوع', '06005', 6, 1, 1, '2020-11-05 20:38:56'),
(41, 'عرض انواع المنتجات', '07000', 7, 1, 1, '2020-11-05 20:40:00'),
(42, 'اضافة نوع منتج', '07001', 7, 1, 1, '2020-11-05 20:41:51'),
(43, 'تعديل نوع منتج', '07003', 7, 1, 1, '2020-11-05 20:42:23'),
(44, 'حذف نوع منتج', '07005', 7, 1, 1, '2020-11-05 20:43:05'),
(45, 'عرض المنتجات الفرعية', '08000', 8, 1, 1, '2020-11-05 20:46:57'),
(46, 'اضافة منتج فرعي', '08001', 8, 1, 1, '2020-11-05 20:48:12'),
(47, 'تعديل منتج فرعي', '08003', 8, 1, 1, '2020-11-05 20:49:45'),
(48, 'حذف منتج فرعي', '08005', 8, 1, 1, '2020-11-05 20:50:45'),
(49, 'عرض الحجوزات', '09000', 9, 1, 1, '2020-11-05 20:51:53'),
(50, 'اضافة حجز جديد', '09001', 9, 1, 1, '2020-11-05 20:54:12'),
(51, 'عرض حجز', '09002', 9, 1, 1, '2020-11-05 20:54:53'),
(52, 'تعديل حجز', '09003', 9, 1, 1, '2020-11-05 20:55:29'),
(53, 'حذف حجز', '09005', 9, 1, 1, '2020-11-05 20:56:09'),
(54, 'عرض مستخدمي النظام', '10000', 10, 1, 1, '2020-11-05 20:57:04'),
(55, 'اضافة مستخدم', '10001', 10, 1, 1, '2020-11-05 20:59:13'),
(56, 'عرض بيانات مستخدم', '10002', 10, 1, 1, '2020-11-05 21:00:06'),
(57, 'تعديل بيانات مستخدم', '10003', 10, 1, 1, '2020-11-05 21:01:42'),
(58, 'حذف مستخدم', '10005', 10, 1, 1, '2020-11-05 21:04:26'),
(59, 'عرض زبائن فرحي(صالات)', '11000', 11, 1, 1, '2020-11-06 14:03:31'),
(60, 'اضافة زبون', '11001', 11, 1, 1, '2020-11-06 14:16:33'),
(61, 'عرض بيانات زبون', '11002', 11, 1, 1, '2020-11-06 14:18:04'),
(62, 'تعديل بيانات زبون', '11003', 11, 1, 1, '2020-11-06 14:19:27'),
(63, 'حذف بيانات زبون', '11005', 11, 1, 1, '2020-11-06 14:20:14');

-- --------------------------------------------------------

--
-- Table structure for table `productimages`
--

CREATE TABLE `productimages` (
  `ProductImageId` int(11) NOT NULL,
  `Path` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `productreservationdetails`
--

CREATE TABLE `productreservationdetails` (
  `ProductReservationDetailsId` int(11) NOT NULL,
  `ProductReservationId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `AdminModifiedBy` int(11) DEFAULT NULL,
  `AdminModifiedOn` datetime DEFAULT NULL,
  `CustomerIsModified` tinyint(1) DEFAULT NULL,
  `CustomerModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `productreservations`
--

CREATE TABLE `productreservations` (
  `ProductReservationId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `AdminModifiedBy` int(11) DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `CustomerIsModified` tinyint(1) DEFAULT NULL,
  `CustomerModifiedOn` datetime DEFAULT NULL,
  `AdminModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductId` int(11) NOT NULL,
  `StoreBranchId` int(11) NOT NULL,
  `Description` varchar(70) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `SubCategoryId` int(11) DEFAULT NULL,
  `Qty` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `productservicereservations`
--

CREATE TABLE `productservicereservations` (
  `ProductServiceReservationId` int(11) NOT NULL,
  `ProductReservationDetailsId` int(11) NOT NULL,
  `ProductServiceId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `AdminModifiedBy` int(11) DEFAULT NULL,
  `AdminModifiedOn` datetime DEFAULT NULL,
  `CustomerIsModified` tinyint(1) DEFAULT NULL,
  `CustomerModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `productservices`
--

CREATE TABLE `productservices` (
  `ProductServiceId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `Description` varchar(70) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL,
  `AdminCreatedBy` int(11) DEFAULT NULL,
  `AdminCreatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reservationtypes`
--

CREATE TABLE `reservationtypes` (
  `ReservationTypeId` int(11) NOT NULL,
  `Description` varchar(50) NOT NULL,
  `Status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservationtypes`
--

INSERT INTO `reservationtypes` (`ReservationTypeId`, `Description`, `Status`) VALUES
(1, 'افراح', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slidermanagement`
--

CREATE TABLE `slidermanagement` (
  `SliderManagementId` int(11) NOT NULL,
  `MediaId` int(11) NOT NULL,
  `Description` varchar(70) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `storeappointments`
--

CREATE TABLE `storeappointments` (
  `WorkDayId` tinyint(4) NOT NULL,
  `StoreBranchId` int(11) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storeappointments`
--

INSERT INTO `storeappointments` (`WorkDayId`, `StoreBranchId`, `StartTime`, `EndTime`) VALUES
(1, 1, '02:00:00', '02:00:00'),
(1, 2, '01:00:00', '05:00:00'),
(1, 6, '01:00:00', '05:00:00'),
(1, 8, '02:00:00', '02:00:00'),
(1, 9, '02:00:00', '02:00:00'),
(2, 1, '02:00:00', '02:00:00'),
(2, 2, '01:00:00', '05:00:00'),
(2, 6, '01:00:00', '05:00:00'),
(2, 7, '01:00:00', '05:00:00'),
(2, 8, '02:00:00', '02:00:00'),
(2, 9, '02:00:00', '02:00:00'),
(3, 2, '01:00:00', '05:00:00'),
(3, 6, '01:00:00', '05:00:00'),
(3, 8, '02:00:00', '02:00:00'),
(3, 9, '02:00:00', '02:00:00'),
(4, 2, '01:00:00', '05:00:00'),
(4, 6, '01:00:00', '05:00:00'),
(4, 7, '01:00:00', '05:00:00'),
(4, 8, '02:00:00', '02:00:00'),
(4, 9, '02:00:00', '02:00:00'),
(5, 2, '01:00:00', '05:00:00'),
(5, 6, '01:00:00', '05:00:00'),
(5, 7, '01:00:00', '05:00:00'),
(5, 8, '02:00:00', '02:00:00'),
(5, 9, '02:00:00', '02:00:00'),
(6, 2, '01:00:00', '05:00:00'),
(6, 6, '01:00:00', '05:00:00'),
(6, 7, '01:00:00', '05:00:00'),
(6, 8, '02:00:00', '02:00:00'),
(6, 9, '02:00:00', '02:00:00'),
(7, 2, '01:00:00', '05:00:00'),
(7, 6, '01:00:00', '05:00:00'),
(7, 7, '01:00:00', '05:00:00'),
(7, 8, '02:00:00', '02:00:00'),
(7, 9, '02:00:00', '02:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `storebranches`
--

CREATE TABLE `storebranches` (
  `StoreBranchId` int(11) NOT NULL,
  `AddressId` int(11) NOT NULL,
  `AddressDescription` varchar(50) DEFAULT NULL,
  `StoreId` int(11) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `Longitude` decimal(10,0) DEFAULT NULL,
  `Latitude` decimal(10,0) DEFAULT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storebranches`
--

INSERT INTO `storebranches` (`StoreBranchId`, `AddressId`, `AddressDescription`, `StoreId`, `PhoneNo1`, `PhoneNo2`, `Longitude`, `Latitude`, `StartTime`, `EndTime`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 1, 'vbvb', 1, '911111111', NULL, '0', '0', '00:00:00', '07:00:00', 1, 1, '2020-12-30 22:10:37', 1, '2021-09-25 17:49:00'),
(2, 1, NULL, 2, '911111111', NULL, '1', '0', '01:00:00', '05:00:00', 1, 1, '2021-03-21 17:03:54', NULL, NULL),
(6, 1, NULL, 6, '911111111', NULL, '2', '-2', '01:00:00', '05:00:00', 1, 1, '2021-03-22 20:42:28', NULL, NULL),
(7, 1, 'qweqwe', 1, '911111111', NULL, '1', '0', '02:00:00', '07:00:00', 1, 1, '2021-03-22 22:52:59', 1, '2021-09-25 17:44:55'),
(8, 1, 'erwerw', 7, '912222222', NULL, '1', '1', '02:00:00', '02:00:00', 9, 1, '2021-09-23 17:12:41', 1, '2021-09-23 17:24:45'),
(9, 1, 'sadasdas', 1, '912223222', NULL, '1', '1', '02:00:00', '02:00:00', 9, 1, '2021-09-25 17:53:53', 1, '2021-09-25 17:57:37');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `StoreId` int(11) NOT NULL,
  `StoreName` varchar(30) NOT NULL,
  `OwnerName` varchar(40) NOT NULL,
  `PhoneNo1` varchar(15) NOT NULL,
  `PhoneNo2` varchar(15) DEFAULT NULL,
  `Email` varchar(50) NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `MainBranchId` int(11) DEFAULT NULL,
  `StoreTypeId` int(11) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`StoreId`, `StoreName`, `OwnerName`, `PhoneNo1`, `PhoneNo2`, `Email`, `Logo`, `MainBranchId`, `StoreTypeId`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'محل1', 'hushuhs', '911111111', NULL, 'ff@dd.dd', 'محل1_Logo.jpg', 1, 1, 2, 1, '2020-12-30 22:10:37', 1, '2021-09-25 16:53:41'),
(2, 'yrete', 'ertyery', '911111111', NULL, 'mm@mm.ff', 'yrete_S_Logo.jpg', 2, 3, 1, 1, '2021-03-21 17:03:54', NULL, NULL),
(6, 'sadasd', 'asdasd', '911111111', NULL, 'dd@ss.tt', 'sadasd_S_Logo.jpg', 6, 1, 1, 1, '2021-03-22 20:42:28', NULL, NULL),
(7, 'dfgdfg', 'dfgdfg', '912222222', NULL, 'dd@dd.dd', 'dfgdfg_Logo.jpg', 8, 1, 9, 1, '2021-09-23 17:12:40', 1, '2021-09-23 17:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `storetypes`
--

CREATE TABLE `storetypes` (
  `StoreTypeId` int(11) NOT NULL,
  `Description` varchar(30) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `storetypes`
--

INSERT INTO `storetypes` (`StoreTypeId`, `Description`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'مستلزمات افراح', 1, 1, '2020-06-09 21:30:24', NULL, NULL),
(2, 'ازياء', 1, 1, '2020-12-21 17:15:15', NULL, NULL),
(3, 'مزين', 1, 1, '2020-12-14 17:18:32', 1, '2022-04-15 17:30:17'),
(4, 'اكسسوارات و مواد تجميل', 1, 1, '2020-12-30 17:19:00', 1, '2022-04-15 17:32:16'),
(5, 'sasasa', 1, 1, '2022-04-15 17:29:31', 1, '2022-04-18 12:56:07');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `SubCategoryId` int(11) NOT NULL,
  `CategoryId` int(11) NOT NULL,
  `Description` varchar(30) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`SubCategoryId`, `CategoryId`, `Description`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 3, 'sadasd', 1, 1, '2022-04-18 16:59:57', 1, '2022-04-18 17:24:54'),
(2, 3, 'czxczxczx', 1, 1, '2022-04-18 17:07:12', 1, '2022-04-18 17:23:53');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptiondetails`
--

CREATE TABLE `subscriptiondetails` (
  `SubscriptionDetailsId` int(11) NOT NULL,
  `SubscriptionId` int(11) NOT NULL,
  `SubscriptionPriceDetailsId` int(11) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptiondetails`
--

INSERT INTO `subscriptiondetails` (`SubscriptionDetailsId`, `SubscriptionId`, `SubscriptionPriceDetailsId`, `Price`, `StartDate`, `EndDate`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 2, 5, '100', '2020-12-23 13:53:45', '2021-12-23 13:53:45', 1, 1, '2020-12-23 13:53:45', NULL, NULL),
(2, 3, 2, '40', '2021-04-07 21:07:16', '2021-05-07 21:07:16', 1, 1, '2021-04-07 21:07:16', NULL, NULL),
(3, 4, 5, '100', '2021-04-07 21:07:40', '2022-04-07 21:07:40', 1, 1, '2021-04-07 21:07:40', NULL, NULL),
(4, 5, 4, '85', '2021-09-23 15:47:57', '2022-03-23 15:47:57', 1, 1, '2021-09-23 15:47:57', NULL, NULL),
(5, 6, 5, '100', '2021-09-23 15:49:33', '2022-09-23 15:49:33', 1, 1, '2021-09-23 15:49:33', NULL, NULL),
(6, 7, 2, '40', '2021-09-23 15:50:05', '2021-10-23 15:50:05', 1, 1, '2021-09-23 15:50:05', NULL, NULL),
(7, 8, 1, '10', '2021-09-23 15:54:16', '2021-10-07 15:54:16', 1, 1, '2021-09-23 15:54:16', NULL, NULL),
(8, 9, 2, '40', '2021-09-23 15:57:41', '2021-10-23 15:57:41', 1, 1, '2021-09-23 15:57:41', NULL, NULL),
(9, 10, 3, '70', '2021-09-23 15:59:46', '2021-12-23 15:59:46', 1, 1, '2021-09-23 15:59:46', NULL, NULL),
(10, 11, 4, '85', '2021-09-25 17:47:46', '2022-03-25 17:47:46', 1, 1, '2021-09-25 17:47:46', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptionpricedetails`
--

CREATE TABLE `subscriptionpricedetails` (
  `SubscriptionPriceDetailsId` int(11) NOT NULL,
  `SubscriptionPricesId` int(11) NOT NULL,
  `DurationTime` tinyint(4) NOT NULL,
  `DurationType` tinyint(4) NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptionpricedetails`
--

INSERT INTO `subscriptionpricedetails` (`SubscriptionPriceDetailsId`, `SubscriptionPricesId`, `DurationTime`, `DurationType`, `Price`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 1, 2, 3, '10', 1, 1, '2020-06-09 21:30:24', NULL, NULL),
(2, 1, 1, 2, '40', 1, 1, '2020-12-21 17:15:15', NULL, NULL),
(3, 1, 3, 2, '70', 1, 1, '2020-12-14 17:18:32', NULL, NULL),
(4, 1, 6, 2, '85', 1, 1, '2020-12-30 17:19:00', NULL, NULL),
(5, 1, 1, 1, '100', 1, 1, '2020-12-29 17:19:36', NULL, NULL),
(6, 3, 1, 2, '10', 1, 1, '2021-09-26 17:10:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptionprices`
--

CREATE TABLE `subscriptionprices` (
  `SubscriptionPricesId` int(11) NOT NULL,
  `Description` varchar(70) NOT NULL,
  `Type` tinyint(4) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptionprices`
--

INSERT INTO `subscriptionprices` (`SubscriptionPricesId`, `Description`, `Type`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 'اشتراك صالة افراح', 1, 1, 1, '2020-06-09 21:30:24', NULL, NULL),
(2, 'اشتراك محل ملابس', 2, 1, 1, '2020-12-21 17:15:15', NULL, NULL),
(3, 'إشتراك Slider', 3, 1, 1, '2021-09-26 17:09:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `SubscriptionId` int(11) NOT NULL,
  `HallBranchId` int(11) DEFAULT NULL,
  `StoreBranchId` int(11) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`SubscriptionId`, `HallBranchId`, `StoreBranchId`, `Status`, `CreatedBy`, `CreatedOn`, `ModifiedBy`, `ModifiedOn`) VALUES
(1, 1, NULL, 1, 1, '2020-06-09 21:30:24', NULL, NULL),
(2, 26, NULL, 1, 1, '2020-12-23 13:53:45', NULL, NULL),
(3, NULL, 7, 1, 1, '2021-04-07 21:07:16', NULL, NULL),
(4, NULL, 7, 1, 1, '2021-04-07 21:07:40', NULL, NULL),
(5, 18, NULL, 1, 1, '2021-09-23 15:47:57', NULL, NULL),
(6, 34, NULL, 1, 1, '2021-09-23 15:49:33', NULL, NULL),
(7, 34, NULL, 1, 1, '2021-09-23 15:50:05', NULL, NULL),
(8, 3, NULL, 1, 1, '2021-09-23 15:54:15', NULL, NULL),
(9, 5, NULL, 1, 1, '2021-09-23 15:57:41', NULL, NULL),
(10, 22, NULL, 1, 1, '2021-09-23 15:59:46', NULL, NULL),
(11, NULL, 1, 1, 1, '2021-09-25 17:47:46', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`AddressId`);

--
-- Indexes for table `adminpermissions`
--
ALTER TABLE `adminpermissions`
  ADD PRIMARY KEY (`AdminId`,`PermissionId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `PermissionId` (`PermissionId`);

--
-- Indexes for table `adminusers`
--
ALTER TABLE `adminusers`
  ADD PRIMARY KEY (`AdminId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`CustomerId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `AddressId` (`AddressId`);

--
-- Indexes for table `durationtypes`
--
ALTER TABLE `durationtypes`
  ADD PRIMARY KEY (`DurationTypeId`),
  ADD KEY `DurationTypeId` (`DurationTypeId`);

--
-- Indexes for table `farhiuserhallbranches`
--
ALTER TABLE `farhiuserhallbranches`
  ADD PRIMARY KEY (`FarhiUserId`,`HallBranchId`);

--
-- Indexes for table `farhiuserpermissions`
--
ALTER TABLE `farhiuserpermissions`
  ADD PRIMARY KEY (`UserId`,`PermissionId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `PermissionId` (`PermissionId`);

--
-- Indexes for table `farhiusers`
--
ALTER TABLE `farhiusers`
  ADD PRIMARY KEY (`FarhiUserId`),
  ADD UNIQUE KEY `LoginName` (`LoginName`),
  ADD KEY `HalId` (`HallId`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`),
  ADD KEY `AdminModifiedBy` (`AdminModifiedBy`);

--
-- Indexes for table `farhiuserstorebranches`
--
ALTER TABLE `farhiuserstorebranches`
  ADD PRIMARY KEY (`FarhiUserId`,`StoreBranchId`),
  ADD KEY `StoreBranchId` (`StoreBranchId`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`FeatureId`),
  ADD KEY `CreatedBy` (`CreatedBy`);

--
-- Indexes for table `hallappointments`
--
ALTER TABLE `hallappointments`
  ADD PRIMARY KEY (`HallAppointmentId`),
  ADD KEY `ReservationTypeId` (`ReservationTypeId`),
  ADD KEY `HallBranchId` (`HallBranchId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `hallbranchconditions`
--
ALTER TABLE `hallbranchconditions`
  ADD PRIMARY KEY (`ConditionId`),
  ADD KEY `HallBranchId` (`HallBranchId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `hallbranches`
--
ALTER TABLE `hallbranches`
  ADD PRIMARY KEY (`HallBranchId`),
  ADD KEY `AddressId` (`AddressId`),
  ADD KEY `HallId` (`HallId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `hallbranchservices`
--
ALTER TABLE `hallbranchservices`
  ADD PRIMARY KEY (`HallBranchServiceId`),
  ADD KEY `HallBranchId` (`HallBranchId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `hallreservationdetails`
--
ALTER TABLE `hallreservationdetails`
  ADD PRIMARY KEY (`HallReservationDetailsId`),
  ADD KEY `HallReservationId` (`HallReservationId`),
  ADD KEY `HallAppointmentId` (`HallAppointmentId`),
  ADD KEY `AdminModifiedBy` (`UserModifiedBy`);

--
-- Indexes for table `hallreservations`
--
ALTER TABLE `hallreservations`
  ADD PRIMARY KEY (`HallReservationId`),
  ADD KEY `CustomerId` (`CustomerId`),
  ADD KEY `HallBranchId` (`HallBranchId`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`),
  ADD KEY `AdminModifiedBy` (`AdminModifiedBy`),
  ADD KEY `UserCreatedBy` (`UserCreatedBy`),
  ADD KEY `UserModifiedBy` (`UserModifiedBy`);

--
-- Indexes for table `halls`
--
ALTER TABLE `halls`
  ADD PRIMARY KEY (`HallId`),
  ADD UNIQUE KEY `HallName` (`HallName`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `MainBranchId` (`MainBranchId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`);

--
-- Indexes for table `hallservicereservation`
--
ALTER TABLE `hallservicereservation`
  ADD PRIMARY KEY (`HallBranchServiceId`,`HallReservationDetailsId`),
  ADD KEY `AdminModifiedBy` (`UserModifiedBy`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`MediaId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `HallBranchId` (`HallBranchId`),
  ADD KEY `StoreBranchId` (`StoreBranchId`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`PermissionId`),
  ADD UNIQUE KEY `Code` (`Code`),
  ADD KEY `FeatureId` (`FeatureId`),
  ADD KEY `CreatedBy` (`CreatedBy`);

--
-- Indexes for table `productimages`
--
ALTER TABLE `productimages`
  ADD PRIMARY KEY (`ProductImageId`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `productreservationdetails`
--
ALTER TABLE `productreservationdetails`
  ADD PRIMARY KEY (`ProductReservationDetailsId`),
  ADD KEY `ProductReservationId` (`ProductReservationId`),
  ADD KEY `AdminModifiedBy` (`AdminModifiedBy`);

--
-- Indexes for table `productreservations`
--
ALTER TABLE `productreservations`
  ADD PRIMARY KEY (`ProductReservationId`),
  ADD KEY `CustomerId` (`CustomerId`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `AdminModifiedBy` (`AdminModifiedBy`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductId`),
  ADD KEY `StoreBranchId` (`StoreBranchId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `SubCategoryId` (`SubCategoryId`),
  ADD KEY `CategoryId` (`CategoryId`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `productservicereservations`
--
ALTER TABLE `productservicereservations`
  ADD PRIMARY KEY (`ProductServiceReservationId`),
  ADD KEY `ProductReservationDetailsId` (`ProductReservationDetailsId`),
  ADD KEY `ProductServiceId` (`ProductServiceId`),
  ADD KEY `AdminModifiedBy` (`AdminModifiedBy`);

--
-- Indexes for table `productservices`
--
ALTER TABLE `productservices`
  ADD PRIMARY KEY (`ProductServiceId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `ProductId` (`ProductId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `AdminCreatedBy` (`AdminCreatedBy`);

--
-- Indexes for table `reservationtypes`
--
ALTER TABLE `reservationtypes`
  ADD PRIMARY KEY (`ReservationTypeId`);

--
-- Indexes for table `slidermanagement`
--
ALTER TABLE `slidermanagement`
  ADD PRIMARY KEY (`SliderManagementId`),
  ADD KEY `MediaId` (`MediaId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `storeappointments`
--
ALTER TABLE `storeappointments`
  ADD PRIMARY KEY (`WorkDayId`,`StoreBranchId`),
  ADD KEY `StoreBranchId` (`StoreBranchId`);

--
-- Indexes for table `storebranches`
--
ALTER TABLE `storebranches`
  ADD PRIMARY KEY (`StoreBranchId`),
  ADD KEY `AddressId` (`AddressId`),
  ADD KEY `StoreId` (`StoreId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`StoreId`),
  ADD KEY `MainBranchId` (`MainBranchId`),
  ADD KEY `StoreTypeId` (`StoreTypeId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `storetypes`
--
ALTER TABLE `storetypes`
  ADD PRIMARY KEY (`StoreTypeId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`SubCategoryId`),
  ADD KEY `CategoryId` (`CategoryId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`);

--
-- Indexes for table `subscriptiondetails`
--
ALTER TABLE `subscriptiondetails`
  ADD PRIMARY KEY (`SubscriptionDetailsId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `SubscriptionId` (`SubscriptionId`) USING BTREE,
  ADD KEY `SubscriptionPriceDetailsId` (`SubscriptionPriceDetailsId`);

--
-- Indexes for table `subscriptionpricedetails`
--
ALTER TABLE `subscriptionpricedetails`
  ADD PRIMARY KEY (`SubscriptionPriceDetailsId`),
  ADD KEY `SubscriptionPricesId` (`SubscriptionPricesId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `DurationType` (`DurationType`);

--
-- Indexes for table `subscriptionprices`
--
ALTER TABLE `subscriptionprices`
  ADD PRIMARY KEY (`SubscriptionPricesId`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `CreatedBy` (`CreatedBy`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`SubscriptionId`),
  ADD KEY `FarhiUserId` (`HallBranchId`),
  ADD KEY `CreatedBy` (`CreatedBy`),
  ADD KEY `ModifiedBy` (`ModifiedBy`),
  ADD KEY `StoreBranchId` (`StoreBranchId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `AddressId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `adminusers`
--
ALTER TABLE `adminusers`
  MODIFY `AdminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `CustomerId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `farhiusers`
--
ALTER TABLE `farhiusers`
  MODIFY `FarhiUserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `hallappointments`
--
ALTER TABLE `hallappointments`
  MODIFY `HallAppointmentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `hallbranchconditions`
--
ALTER TABLE `hallbranchconditions`
  MODIFY `ConditionId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hallbranches`
--
ALTER TABLE `hallbranches`
  MODIFY `HallBranchId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `hallbranchservices`
--
ALTER TABLE `hallbranchservices`
  MODIFY `HallBranchServiceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `hallreservationdetails`
--
ALTER TABLE `hallreservationdetails`
  MODIFY `HallReservationDetailsId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hallreservations`
--
ALTER TABLE `hallreservations`
  MODIFY `HallReservationId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halls`
--
ALTER TABLE `halls`
  MODIFY `HallId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `MediaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `productimages`
--
ALTER TABLE `productimages`
  MODIFY `ProductImageId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productreservationdetails`
--
ALTER TABLE `productreservationdetails`
  MODIFY `ProductReservationDetailsId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productreservations`
--
ALTER TABLE `productreservations`
  MODIFY `ProductReservationId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productservicereservations`
--
ALTER TABLE `productservicereservations`
  MODIFY `ProductServiceReservationId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productservices`
--
ALTER TABLE `productservices`
  MODIFY `ProductServiceId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservationtypes`
--
ALTER TABLE `reservationtypes`
  MODIFY `ReservationTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slidermanagement`
--
ALTER TABLE `slidermanagement`
  MODIFY `SliderManagementId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `storebranches`
--
ALTER TABLE `storebranches`
  MODIFY `StoreBranchId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `StoreId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `storetypes`
--
ALTER TABLE `storetypes`
  MODIFY `StoreTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `SubCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscriptiondetails`
--
ALTER TABLE `subscriptiondetails`
  MODIFY `SubscriptionDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `subscriptionpricedetails`
--
ALTER TABLE `subscriptionpricedetails`
  MODIFY `SubscriptionPriceDetailsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subscriptionprices`
--
ALTER TABLE `subscriptionprices`
  MODIFY `SubscriptionPricesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `SubscriptionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adminpermissions`
--
ALTER TABLE `adminpermissions`
  ADD CONSTRAINT `adminpermissions_ibfk_1` FOREIGN KEY (`AdminId`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adminpermissions_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adminpermissions_ibfk_3` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `adminusers`
--
ALTER TABLE `adminusers`
  ADD CONSTRAINT `adminusers_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adminusers_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_ibfk_3` FOREIGN KEY (`AddressId`) REFERENCES `addresses` (`AddressId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `farhiuserhallbranches`
--
ALTER TABLE `farhiuserhallbranches`
  ADD CONSTRAINT `farhiuserhallbranches_ibfk_3` FOREIGN KEY (`FarhiUserId`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiuserhallbranches_ibfk_4` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `farhiuserpermissions`
--
ALTER TABLE `farhiuserpermissions`
  ADD CONSTRAINT `farhiuserpermissions_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiuserpermissions_ibfk_2` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiuserpermissions_ibfk_3` FOREIGN KEY (`UserId`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `farhiusers`
--
ALTER TABLE `farhiusers`
  ADD CONSTRAINT `farhiusers_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiusers_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiusers_ibfk_3` FOREIGN KEY (`StoreId`) REFERENCES `stores` (`StoreId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiusers_ibfk_4` FOREIGN KEY (`HallId`) REFERENCES `halls` (`HallId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiusers_ibfk_5` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiusers_ibfk_6` FOREIGN KEY (`AdminModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `farhiuserstorebranches`
--
ALTER TABLE `farhiuserstorebranches`
  ADD CONSTRAINT `farhiuserstorebranches_ibfk_1` FOREIGN KEY (`FarhiUserId`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `farhiuserstorebranches_ibfk_2` FOREIGN KEY (`StoreBranchId`) REFERENCES `storebranches` (`StoreBranchId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `features_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallappointments`
--
ALTER TABLE `hallappointments`
  ADD CONSTRAINT `hallappointments_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallappointments_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallappointments_ibfk_3` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallappointments_ibfk_4` FOREIGN KEY (`ReservationTypeId`) REFERENCES `reservationtypes` (`ReservationTypeId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallbranchconditions`
--
ALTER TABLE `hallbranchconditions`
  ADD CONSTRAINT `hallbranchconditions_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranchconditions_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranchconditions_ibfk_3` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranchconditions_ibfk_4` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallbranches`
--
ALTER TABLE `hallbranches`
  ADD CONSTRAINT `hallbranches_ibfk_1` FOREIGN KEY (`HallId`) REFERENCES `halls` (`HallId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranches_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranches_ibfk_3` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranches_ibfk_4` FOREIGN KEY (`AddressId`) REFERENCES `addresses` (`AddressId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallbranchservices`
--
ALTER TABLE `hallbranchservices`
  ADD CONSTRAINT `hallbranchservices_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranchservices_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranchservices_ibfk_3` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallbranchservices_ibfk_4` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallreservationdetails`
--
ALTER TABLE `hallreservationdetails`
  ADD CONSTRAINT `hallreservationdetails_ibfk_1` FOREIGN KEY (`UserModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservationdetails_ibfk_2` FOREIGN KEY (`HallAppointmentId`) REFERENCES `hallappointments` (`HallAppointmentId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservationdetails_ibfk_3` FOREIGN KEY (`HallReservationId`) REFERENCES `hallreservations` (`HallReservationId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallreservations`
--
ALTER TABLE `hallreservations`
  ADD CONSTRAINT `hallreservations_ibfk_1` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservations_ibfk_2` FOREIGN KEY (`AdminModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservations_ibfk_3` FOREIGN KEY (`CustomerId`) REFERENCES `customers` (`CustomerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservations_ibfk_4` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservations_ibfk_5` FOREIGN KEY (`UserCreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallreservations_ibfk_6` FOREIGN KEY (`UserModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `halls`
--
ALTER TABLE `halls`
  ADD CONSTRAINT `halls_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `halls_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hallservicereservation`
--
ALTER TABLE `hallservicereservation`
  ADD CONSTRAINT `hallservicereservation_ibfk_1` FOREIGN KEY (`UserModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hallservicereservation_ibfk_2` FOREIGN KEY (`HallBranchServiceId`) REFERENCES `hallbranchservices` (`HallBranchServiceId`) ON UPDATE CASCADE;

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_ibfk_3` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_ibfk_4` FOREIGN KEY (`StoreBranchId`) REFERENCES `storebranches` (`StoreBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `media_ibfk_5` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`FeatureId`) REFERENCES `features` (`FeatureId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productimages`
--
ALTER TABLE `productimages`
  ADD CONSTRAINT `productimages_ibfk_1` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productimages_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productimages_ibfk_3` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productimages_ibfk_4` FOREIGN KEY (`ProductId`) REFERENCES `products` (`ProductId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productreservationdetails`
--
ALTER TABLE `productreservationdetails`
  ADD CONSTRAINT `productreservationdetails_ibfk_1` FOREIGN KEY (`AdminModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productreservationdetails_ibfk_2` FOREIGN KEY (`ProductReservationId`) REFERENCES `productreservations` (`ProductReservationId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productreservations`
--
ALTER TABLE `productreservations`
  ADD CONSTRAINT `productreservations_ibfk_1` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productreservations_ibfk_2` FOREIGN KEY (`AdminModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productreservations_ibfk_3` FOREIGN KEY (`CustomerId`) REFERENCES `customers` (`CustomerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productreservations_ibfk_4` FOREIGN KEY (`ProductId`) REFERENCES `products` (`ProductId`) ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_3` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_4` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`CategoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_5` FOREIGN KEY (`SubCategoryId`) REFERENCES `subcategories` (`SubCategoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_6` FOREIGN KEY (`StoreBranchId`) REFERENCES `storebranches` (`StoreBranchId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productservicereservations`
--
ALTER TABLE `productservicereservations`
  ADD CONSTRAINT `productservicereservations_ibfk_1` FOREIGN KEY (`ProductReservationDetailsId`) REFERENCES `productreservationdetails` (`ProductReservationDetailsId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productservicereservations_ibfk_2` FOREIGN KEY (`AdminModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productservicereservations_ibfk_3` FOREIGN KEY (`ProductServiceId`) REFERENCES `productservices` (`ProductServiceId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productservices`
--
ALTER TABLE `productservices`
  ADD CONSTRAINT `productservices_ibfk_1` FOREIGN KEY (`AdminCreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productservices_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productservices_ibfk_3` FOREIGN KEY (`ModifiedBy`) REFERENCES `farhiusers` (`FarhiUserId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productservices_ibfk_4` FOREIGN KEY (`ProductId`) REFERENCES `products` (`ProductId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slidermanagement`
--
ALTER TABLE `slidermanagement`
  ADD CONSTRAINT `slidermanagement_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slidermanagement_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `slidermanagement_ibfk_3` FOREIGN KEY (`MediaId`) REFERENCES `media` (`MediaId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storeappointments`
--
ALTER TABLE `storeappointments`
  ADD CONSTRAINT `storeappointments_ibfk_1` FOREIGN KEY (`StoreBranchId`) REFERENCES `storebranches` (`StoreBranchId`);

--
-- Constraints for table `storebranches`
--
ALTER TABLE `storebranches`
  ADD CONSTRAINT `storebranches_ibfk_1` FOREIGN KEY (`AddressId`) REFERENCES `addresses` (`AddressId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storebranches_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storebranches_ibfk_3` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storebranches_ibfk_4` FOREIGN KEY (`StoreId`) REFERENCES `stores` (`StoreId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `stores_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stores_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stores_ibfk_3` FOREIGN KEY (`MainBranchId`) REFERENCES `storebranches` (`StoreBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stores_ibfk_4` FOREIGN KEY (`StoreTypeId`) REFERENCES `storetypes` (`StoreTypeId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `storetypes`
--
ALTER TABLE `storetypes`
  ADD CONSTRAINT `storetypes_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `storetypes_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_ibfk_1` FOREIGN KEY (`CategoryId`) REFERENCES `categories` (`CategoryId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subcategories_ibfk_2` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subcategories_ibfk_3` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscriptiondetails`
--
ALTER TABLE `subscriptiondetails`
  ADD CONSTRAINT `subscriptiondetails_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptiondetails_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptiondetails_ibfk_3` FOREIGN KEY (`SubscriptionId`) REFERENCES `subscriptions` (`SubscriptionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptiondetails_ibfk_4` FOREIGN KEY (`SubscriptionPriceDetailsId`) REFERENCES `subscriptionpricedetails` (`SubscriptionPriceDetailsId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscriptionpricedetails`
--
ALTER TABLE `subscriptionpricedetails`
  ADD CONSTRAINT `subscriptionpricedetails_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptionpricedetails_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptionpricedetails_ibfk_3` FOREIGN KEY (`SubscriptionPricesId`) REFERENCES `subscriptionprices` (`SubscriptionPricesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptionpricedetails_ibfk_4` FOREIGN KEY (`DurationType`) REFERENCES `durationtypes` (`DurationTypeId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscriptionprices`
--
ALTER TABLE `subscriptionprices`
  ADD CONSTRAINT `subscriptionprices_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptionprices_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD CONSTRAINT `subscriptions_ibfk_1` FOREIGN KEY (`CreatedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptions_ibfk_2` FOREIGN KEY (`ModifiedBy`) REFERENCES `adminusers` (`AdminId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptions_ibfk_3` FOREIGN KEY (`HallBranchId`) REFERENCES `hallbranches` (`HallBranchId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscriptions_ibfk_4` FOREIGN KEY (`StoreBranchId`) REFERENCES `storebranches` (`StoreBranchId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
