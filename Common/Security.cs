﻿using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;


namespace Common
{
    public class Hash
    {
        public string Password { get; set; }
        public string Salt { get; set; }
    }
    public class Security
    {
        static public Hash HashPassword(string password,string key)
        {

            // generate a 128-bit salt using a cryptographically strong random sequence of nonzero values
            password = password + key;
            byte[] salt = new byte[128 / 8];
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetNonZeroBytes(salt);
            }
            //Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            // derive a 256-bit subkey (use HMACSHA256 with 100,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));
            return new Hash()
            {
                Password = hashed,
                Salt = Convert.ToBase64String(salt)
            };
        }

        static public bool DecryptPassword(string password, string givenPassword, string key, string salt)
        {

            givenPassword = givenPassword + key;
            if(!Validation.IsBase64String(salt)) return false;

            byte[] saltByte = Convert.FromBase64String(salt);
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: givenPassword,
                salt: saltByte,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            if(hashed == password) return true;
            return false;
        }

        static public string GeneratePassword()
        {
            var result = "";
            Random rnd = new Random();
            var lowercases = "abcdefghijklmnopqrstuvwxyz";
            var numbers = "0123456789";
            var uppercases = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var sympols = "#?!@$%^&*-";
            //int card = rnd.Next(52);     // creates a number between 0 and 51
            result += lowercases.Substring(rnd.Next(1, lowercases.Length-1),2);
            result += numbers.Substring(rnd.Next(1, numbers.Length - 1),2);
            result += uppercases.Substring(rnd.Next(1, uppercases.Length - 1),2);
            result += sympols.Substring(rnd.Next(1, sympols.Length - 1),2);
            return result;
        }
    }
}
