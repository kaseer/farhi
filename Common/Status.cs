﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public static class Status
    {
        public static readonly sbyte Entry = 0;
        public static readonly sbyte Active = 1;
        public static readonly sbyte Locked = 2;
        public static readonly sbyte Expired = 3;
        public static readonly sbyte Accept = 4;
        public static readonly sbyte Reject = 5;
        public static readonly sbyte Deleted = 9;
    }
}
