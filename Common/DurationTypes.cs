﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public static class DurationTypes
    {
        public static readonly sbyte Year = 1;
        public static readonly sbyte Month = 2;
        public static readonly sbyte Week = 3;
    }
}
