﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Common
{
    public static class Validation
    {
        public static bool IsValidPassword(string passWord)
        {
            //int validConditions = 0;
            if(!passWord.Any(char.IsUpper)) return false;
            if(!passWord.Any(char.IsUpper)) return false;
            if(!passWord.Any(char.IsNumber)) return false;
            //if(!passWord.Any(char.IsSymbol)) return false;
            //foreach (char c in passWord)
            //{
            //    if (c >= 'a' && c <= 'z')
            //    {
            //        validConditions++;
            //        break;
            //    }
            //}

            //foreach (char c in passWord)

            //{

            //    if (c >= 'A' && c <= 'Z')

            //    {

            //        validConditions++;

            //        break;

            //    }

            //}

            //if (validConditions == 0) return false;

            //foreach (char c in passWord)

            //{

            //    if (c >= '0' && c <= '9')

            //    {

            //        validConditions++;

            //        break;

            //    }

            //}

            //if (validConditions == 1) return false;

            //if (validConditions == 2)

            //{

            //    char[] special = { '@', '#', '$', '%', '^', '&', '+', '=' }; // or whatever

            //    if (passWord.IndexOfAny(special) == -1) return false;

            //}
            return true;
        }
        

        
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsValidTimeFormat(string input)
        {
            TimeSpan dummyOutput;
            return TimeSpan.TryParse(input, out dummyOutput);
        }

        public static bool IsValidLoginName(string s)
        {
            s = s.Trim();
            bool valid = Regex.IsMatch(s, @"^[a-zA-Z0-9._-]", RegexOptions.None);
            return valid;

        }

        public static bool IsValidNumber(string s)
        {
            if (s is null) return true;
            s = s.Trim();
            bool valid = Regex.IsMatch(s, @"^[0-9]*$", RegexOptions.None);
            return valid;
        }

        public static bool IsBase64String(string s)
        {
            if(s is null) return true;
            s = s.Trim();
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);

        }
    }
}
