﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public static class Constant
    {
        

        public static string SelectDay(int dayId)
        {
            List<Day> days = new List<Day>() {
                new Day() { DayId = 1, DayName = "الاثنين"},
                new Day() { DayId = 2, DayName = "الثلاثاء"},
                new Day() { DayId = 3, DayName = "الاربعاء"},
                new Day() { DayId = 4, DayName = "الخميس"},
                new Day() { DayId = 5, DayName = "الجمعة"},
                new Day() { DayId = 6, DayName = "السبت"},
                new Day() { DayId = 7, DayName = "الاحد"},
            };

            return days.Find(x => x.DayId == dayId).DayName;
        }

        class Day
        {
            public int DayId { get; set; }
            public string DayName { get; set; }
        }
    }
}
