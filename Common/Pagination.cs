﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common
{
    public class Pagination
    {
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }
}
