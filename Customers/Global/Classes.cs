﻿namespace Customers.Global
{
    public class Classes
    {
        public class Images
        {
            public int ImageId { get; set; }
            public string Path { get; set; }
        }
    }
}
