module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  outputDir: "../wwwroot",
  filenameHashing: false,
  devServer:{
      port:8080,
      https:false,
        proxy:{
            '^/api':{
                //target:"http://localhost:39626",
               //target:"https://localhost:44319",
              target: "https://Farhi-Customers.naqra.ly"
            }
        },
      
  },
  chainWebpack: config => {
      config.module
        .rule('images')
          .use('url-loader')
            .loader('url-loader')
            .tap(options => Object.assign(options, { limit: 100240 }))
  }
}
