﻿import { store }  from '@/store'

export default {
    startNotify(message,color) {
        store.dispatch('notify/openNotify',{
            snackbar:true,
            message: message,
            color: color
        });
    },
    startLoading() {
        store.dispatch('loading/startLoading');
    },
    stopLoading() {
        store.dispatch('loading/stopLoading');
    },
    openAlert(message) {
        store.dispatch('notify/openAlert',{
            dialog:true,
            message: message,
        });
    },
    catch(err) {
        this.stopLoading();
        console.log(err.response.data.StatusCode)

        if (!err.response.data.StatusCode || err.response.data.StatusCode == undefined) {
            this.openAlert('تعذر الاتصال بالخادم , يرجي المحاولة لاحقا');
            return;
        }
        let message = err.response.data.Message + "  [" + err.response.data.StatusCode + "]";
        this.openAlert(message);
    }
}
