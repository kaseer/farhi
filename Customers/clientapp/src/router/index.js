import Vue from 'vue'
import VueRouter from 'vue-router'
// import Table from '../views/table/Table.vue'

Vue.use(VueRouter)

const routes = [
  
  {
    path: '/',
    name: 'لوحة البيانات',
    icon: 'mdi-view-dashboard',
    component:  () => import('../views/dashboard/Dashboard.vue')
  },
  {
    path: '/Sample',
    name: 'عينة',
    icon: 'mdi-cellphone-wireless',
    component: () => import('../views/sample/Index.vue')
  },
  {
    path: '/hallBranches',
    name: 'صالاتي',
    icon: 'mdi-domain',
    component: () => import('../views/hallBranches/Index.vue')
  },
  {
    path: '/storeBranches',
    name: 'محلاتي',
    icon: 'mdi-watermark',
    component: () => import('../views/storeBranches/Index.vue')
  },
  {
    path: '/products',
    name: 'المنتجات',
    icon: 'mdi-credit-card',
    component: () => import('../views/products/Index.vue')
  },
  {
    path: '/storeReservations',
    name: 'حجوزات خدمات المحلات',
    icon: 'mdi-bag-suitcase-outline',
    component: () => import('../views/storeReservations/Index.vue')
  },
  {
    path: '/productReservations',
    name: 'حجوزات المنتجات',
    icon: 'mdi-bag-suitcase-outline',
    component: () => import('../views/productReservations/Index.vue')
  },
  {
    path: '/hallReservations',
    name: 'حجوزات الصالات',
    icon: 'mdi-offer',
    component: () => import('../views/hallReservations/Index.vue')
  },
  {
    path: '/customers',
    name: 'الزبائن',
    icon: 'mdi-account-box',
    component: () => import('../views/customers/Index.vue')
  },
  
  {
    path: '/farhiUsers',
    name: 'مستخدمي النظام',
    icon: 'mdi-account',
    component: () => import('../views/farhiUsers/Index.vue')
  },
  
  {
    path: '/Support',
    name: 'الدعم',
    icon: 'mdi-face-agent',
    component: () => import('../views/support/Index.vue')
  },
  
  // {
  //   name: 'تقارير',
  //   path: '',
  //   icon: 'mdi-chart-box',
  //   component: null,
  //   children: [
  //     {
  //       path: '/specieltable',
  //       name: 'specieltable-Nested',
  //       icon: 'mdi-calendar',
  //       component: () => import('../views/specielTable/SpecielTable.vue')
  //     }
  //   ]
  // },
]

const router = new VueRouter({
  routes
})

export default router
