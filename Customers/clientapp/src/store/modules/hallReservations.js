﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import hallReservation from '@/api/controllers/hallReservation'
const state = {
    hallReservations: [],
    hallReservation: {},
    hallReservationForEdit: {},
    total: 0,
    price: 0,
    availibleAppointments: []
}
const mutations = {
    setHallReservations(state, payload) {
        state.hallReservations = payload
    },
    setHallReservation(state, payload) {
        state.hallReservation = payload
    },
    setHallReservationForEdit(state, payload) {
        state.hallReservationForEdit = payload
    },
    add(state, payload) {
        state.hallReservations.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.hallReservations.splice(index, 1, payload);
    },
    accept(state, index) {
        state.hallReservations[index].status = 4;
    },
    reject(state, index) {
        state.hallReservations[index].status = 5;
    },
    total(state, payload) {
        state.total = payload
    },
    availibleAppointments(state, payload){
        state.availibleAppointments = payload
    }
}
const getters = {
    index(id) {
        return state.hallReservations.findIndex(x => x.hallReservationId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.hallReservations.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            let total = res.data.total;
            commit('setHallReservations', data);
            commit('total', total);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.hallReservations.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setHallReservation', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.hallReservations.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            helper.startLoading();
            const res = await api.hallReservations.add(payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.hallReservations.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            //return Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async accept({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.hallReservations.accept(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            console.log(index)
            commit('accept', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            console.log("sdfdsfsdfsd")
            helper.catch(err)
        }
    },
    async reject({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.hallReservations.reject(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('reject', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },

    
    async appointmentPrice(_, id) {

        try {
            helper.startLoading();
            const res = await api.hallReservations.appointmentPrice(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },

    async servicePrice(_, id) {

        try {
            helper.startLoading();
            const res = await api.hallReservations.servicePrice(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    
    
    async getAvailibleAppointments(_,{branchId, reservationDate}) {
        try {
            helper.startLoading();
            const res = await api.hallReservations.getAvailibleAppointments(branchId, reservationDate);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
        }
    },

    
    async checkAvailibility(_,{branchId, reservationDate, appointmentId}) {

        try {
            helper.startLoading();
            const res = await api.hallReservations.checkAvailibility(branchId, reservationDate, appointmentId);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data;
            if (!data) {
                helper.openAlert('هذا الموعد غير متوفر');
                return;
            }
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },

}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};