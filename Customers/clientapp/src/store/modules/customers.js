﻿import api from '@/api/index';
import helper from '@/shared/helper';


const state = {
    customers: [],
    customer: {},
    total: 0,
}
const mutations = {
    setCustomers(state, payload) {
        state.customers = payload
    },
    setCustomer(state, payload) {
        state.customer = payload
    },
    lock(state, index ) {
        state.customers[index].status = 2;
    },
    unlock(state, index) {
        state.customers[index].status = 1;
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.customers.findIndex(x => x.customerId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.customers.getAll(query);
            helper.stopLoading();

            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let list = res.data.data;
            let total = res.data.total;
            commit('setCustomers', list);
            commit('total', total);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.customers.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setCustomer', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async lock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.customers.lock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('lock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async unlock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.customers.unlock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('unlock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    
    async resetPassword(_, id) {
        try {
            helper.startLoading();
            const res = await api.customers.resetPassword(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            helper.openAlert(message + ", كلمة المرور الجديدة :" + res.data.data, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};