﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import product from '@/api/controllers/product'
const state = {
    products: [],
    product: {},
    productForEdit: {},
    total: 0,
}
const mutations = {
    setProducts(state, payload) {
        state.products = payload
    },
    setProduct(state, payload) {
        state.product = payload
    },
    setProductForEdit(state, payload) {
        state.productForEdit = payload
    },
    add(state, payload) {
        state.products.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.products.splice(index, 1, payload);
    },
    delete(state, index) {
        state.products.splice(index, 1);
    },
    lock(state, index) {
        state.products[index].status = 2;
    },
    unlock(state, index) {
        state.products[index].status = 1;
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.products.findIndex(x => x.productId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.products.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            let total = res.data.total;
            commit('setProducts', data);
            commit('total', total);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.products.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setProduct', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.products.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            helper.startLoading();
            const res = await api.products.add(payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.products.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            //return Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async lock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.products.lock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('lock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            console.log("sdfdsfsdfsd")
            helper.catch(err)
        }
    },
    async unlock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.products.unlock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('unlock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async delete({ commit, getters }, id) {
        try {
            helper.startLoading();
            const res = await api.products.delete(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('delete', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getImages(_, id) {
        try {
            helper.startLoading();
            const res = await api.products.getImages(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async addImages(_, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.products.addImages(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let data = res.data.data;
            helper.startNotify(message, "success");
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async deleteImages(_, checkedImages) {
        try {
            helper.startLoading();
            const res = await api.products.deleteImages(checkedImages);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            // let data = res.data.data;
            helper.startNotify(message, "success");
            // return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};