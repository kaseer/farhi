﻿import api from '@/api/index';
import helper from '@/shared/helper';
// import loading from '@/shared/loading';

const state = {
    addresses: [],
    allHalls: [],
    // hallBranches: [],
    reservationTypes: [],
    // subscriptionPrices: [],
    // subscriptionPricesDetails: [],
    storeTypes: [],
    // stores: [],
    allHallBranches: [],
    allStoreBranches: [],
    categories: [],
    subCategories: [],
    customers: [],
    storeServices: [],
    hallServices: [],
    storeProducts:[],
    loading: false,
    days: [
        {value:1, text:"الاثنين"},
        {value:2, text:"الثلاثاء"},
        {value:3, text:"الاربعاء"},
        {value:4, text:"الخميس"},
        {value:5, text:"الجمعة"},
        {value:6, text:"السبت"},
        {value:7, text:"الاحد"},
    ],

}

const mutations = {
    startLoad(state) {
        state.loading = true
    },
    stopLoad(state) {
        state.loading = false
    },
    setAddresses(state, payload) {
        state.addresses = payload
    },
    setHalls(state, payload) {
        state.allHalls = payload
    },
    // setHallBranches(state, payload) {
    //     state.hallBranches = payload
    // },
    setReservationTypes(state, payload) {
        state.reservationTypes = payload
    },
    // setSubscriptionPrices(state, payload) {
    //     state.subscriptionPrices = payload
    // },
    // setSubscriptionPricesDetails(state, payload) {
    //     state.subscriptionPricesDetails = payload
    // },
    setStoreTypes(state, payload) {
        state.storeTypes = payload
    },
    setCustomers(state, payload) {
        state.customers = payload
    },
    setStoreServices(state, payload) {
        state.storeServices = payload
    },
    setHallServices(state, payload) {
        state.hallServices = payload
    },
    setStoreProducts(state, payload) {
        state.storeProducts = payload
    },
    // setStores(state, payload) {
    //     state.stores = payload
    // },
    setAllStoreBranches(state, payload) {
        state.allStoreBranches = payload
    },
    setAllHallBranches(state, payload) {
        state.allHallBranches = payload
    },
    setCategories(state, payload) {
        state.categories = payload
    },
    setSubCategories(state, payload) {
        state.subCategories = payload
    },
    // setSubscriptionPrices(state, payload) {
    //     state.subscriptionPrices = payload
    // },
}
const getters = {
    allStoreBranches() {
        let branches = state.allStoreBranches;
        return branches.filter(x => x.value !== 0);
    },
    allHallBranches() {
        let branches = state.allHallBranches;
        return branches.filter(x => x.value !== 0);
    }
}
const actions = {
    async getAddresses({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getAddresses();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.addresses;
            commit('setAddresses', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getHalls({ commit }) {
        try {
            helper.startLoading();
            const res = await api.lookup.getHalls();
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let list = res.data.halls;
            commit('setHalls', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    // async getHallBranches({ commit }, id) {
    //     try {
    //         commit('startLoad');
    //         const res = await api.lookup.getHallBranches(id);
    //         commit('stopLoad');
    //         if (!res.data.statusCode) {
    //             helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.hallBranches;
    //         commit('setHallBranches', list);
    //     }
    //     catch (err) {
    //         helper.catch(err)
    //     }
    // },
    async getReservationTypes({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getReservationTypes();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.reservationTypes;
            commit('setReservationTypes', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    // async getSubscriptionPrices({ commit },type) {
    //     try {
    //         commit('startLoad');
    //         const res = await api.lookup.getSubscriptionPrices(type);
    //         commit('stopLoad');
    //         if (!res.data.statusCode) {
    //             helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.subscriptionPrices;
    //         commit('setSubscriptionPrices', list);
    //     }
    //     catch (err) {
    //         helper.catch(err)
    //     }
    // },
    // async getSubscriptionPricesDetails({ commit },subscriptionPriceId) {
    //     try {
    //         loading.Start();
    //         const res = await api.lookup.getSubscriptionPricesDetails(subscriptionPriceId);
    //         loading.Stop();
    //         if (!res.data.statusCode) {
    //             helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.subscriptionPricesDetails;
    //         commit('setSubscriptionPricesDetails', list);
    //     }
    //     catch (err) {
    //         helper.catch(err)
    //     }
    // },
    async getStoreTypes({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getStoreTypes();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.storeTypes;
            commit('setStoreTypes', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getCustomers({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getCustomers();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.data;
            commit('setCustomers', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getStoreServices({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getStoreServices();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.data;
            commit('setStoreServices', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getHallServices({ commit },id) {
        try {
            commit('startLoad');
            const res = await api.lookup.getHallServices(id);
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.data;
            commit('setHallServices', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getStoreProducts({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getStoreProducts();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.data;
            commit('setStoreProducts', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    // async getStores({ commit }) {
    //     try {
    //         commit('startLoad');
    //         const res = await api.lookup.getStores();
    //         commit('stopLoad');
    //         if (!res.data.statusCode) {
    //             helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.stores;
    //         commit('setStores', list);
    //     }
    //     catch (err) {
    //         helper.catch(err)
    //     }
    // },
    async getAllStoreBranches({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getAllStoreBranches();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.allStoreBranches;
            commit('setAllStoreBranches', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getAllHallBranches({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getAllHallBranches();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.allHallBranches;
            commit('setAllHallBranches', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getCategories({ commit }) {
        try {
            commit('startLoad');
            const res = await api.lookup.getCategories();
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.categories;
            commit('setCategories', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getSubCategories({ commit }, id) {
        try {
            commit('startLoad');
            const res = await api.lookup.getSubCategories(id);
            commit('stopLoad');
            if (!res.data.statusCode) {
                helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
                return;
            }
            let list = res.data.subCategories;
            commit('setSubCategories', list);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    // async getSubscriptionPrices({ commit }) {
    //     try {
    //         commit('startLoad');
    //         const res = await api.lookup.getSubscriptionPrices();
    //         commit('stopLoad');
    //         if (!res.data.statusCode) {
    //             helper.message('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا','error');
    //             return;
    //         }
    //         let list = res.data.subscriptionPrices;
    //         commit('setSubscriptionPrices', list);
    //     }
    //     catch (err) {
    //         helper.catch(err)
    //     }
    // },
}
export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};