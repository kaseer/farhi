﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import storeBranch from '@/api/controllers/storeBranch'
const state = {
    storeBranches: [],
    storeBranch: {},
    storeBranchForEdit: {},
    total: 0,
    video: ""
}
const mutations = {
    setStoreBranches(state, payload) {
        state.storeBranches = payload
    },
    setStoreBranch(state, payload) {
        state.storeBranch = payload
    },
    setStoreBranchForEdit(state, payload) {
        state.storeBranchForEdit = payload
    },
    edit(state, { index, payload }) {
        state.storeBranches.splice(index, 1, payload);
    },
    delete(state, index) {
        state.storeBranches.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
    video(state, payload) {
        state.video = payload
    },
}
const getters = {
    index(id) {
        return state.storeBranches.findIndex(x => x.storeBranchId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.storeBranches.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            let total = res.data.total;
            commit('setStoreBranches', data);
            commit('total', total);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.storeBranches.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setStoreBranch', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            return Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async lock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.lock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('lock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            console.log("sdfdsfsdfsd")
            helper.catch(err)
        }
    },
    async unlock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.unlock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('unlock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async delete({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.delete(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('delete', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getImages(_, id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.getImages(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async addImages(_, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.addImages(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let data = res.data.data;
            helper.startNotify(message, "success");
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async deleteImages(_, checkedImages) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.deleteImages(checkedImages);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            // let data = res.data.data;
            helper.startNotify(message, "success");
            // return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async getVideo(_, id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.getVideo(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },

    
    async addVideo(_, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.addVideo(id, payload);
            console.log(res.data.statusCode)
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            helper.startNotify(message, "success");
            //return Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async deleteVideo(_,id) {
        try {
            helper.startLoading();
            const res = await api.storeBranches.deleteVideo(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            // let data = res.data.data;
            helper.startNotify(message, "success");
            // return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};