﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import storeReservation from '@/api/controllers/storeReservation'
const state = {
    storeReservations: [],
    storeReservation: {},
    storeReservationForEdit: {},
    total: 0,
    price: 0,
}
const mutations = {
    setStoreReservations(state, payload) {
        state.storeReservations = payload
    },
    setStoreReservation(state, payload) {
        state.storeReservation = payload
    },
    setStoreReservationForEdit(state, payload) {
        state.storeReservationForEdit = payload
    },
    add(state, payload) {
        state.storeReservations.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.storeReservations.splice(index, 1, payload);
    },
    accept(state, index) {
        state.storeReservations[index].status = 4;
    },
    reject(state, index) {
        state.storeReservations[index].status = 5;
    },
    total(state, payload) {
        state.total = payload
    },
    setPrice(state, payload) {
        state.price = payload
    },
}
const getters = {
    index(id) {
        return state.storeReservations.findIndex(x => x.serviceReservationId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.storeReservations.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            let total = res.data.total;
            commit('setStoreReservations', data);
            commit('total', total);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.storeReservations.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setStoreReservation', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.storeReservations.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            helper.startLoading();
            const res = await api.storeReservations.add(payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.storeReservations.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            //return Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async accept({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.storeReservations.accept(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            console.log(index)
            commit('accept', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            console.log("sdfdsfsdfsd")
            helper.catch(err)
        }
    },
    async reject({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.storeReservations.reject(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('reject', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },

    
    async getPrice({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.storeReservations.getPrice(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setPrice', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};