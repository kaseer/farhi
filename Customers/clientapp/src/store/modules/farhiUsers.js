﻿import api from '@/api/index';
import helper from '@/shared/helper';

// import {eventBus} from '@/main.js'
// import farhiUser from '@/api/controllers/farhiUser'
const state = {
    farhiUsers: [],
    farhiUser: {},
    total: 0,
    features: []
}
const mutations = {
    setFarhiUsers(state, payload) {
        state.farhiUsers = payload
    },
    setFarhiUser(state, payload) {
        state.farhiUser = payload
    },
    setFeatures(state, payload) {
        state.features = payload
    },
    add(state, payload) {
        state.farhiUsers.unshift(payload);
    },
    edit(state, { index, payload }) {
        state.farhiUsers.splice(index, 1, payload);
    },
    lock(state, index ) {
        state.farhiUsers[index].status = 2;
    },
    unlock(state, index) {
        state.farhiUsers[index].status = 1;
    },
    delete(state, index) {
        state.farhiUsers.splice(index, 1);
    },
    total(state, payload) {
        state.total = payload
    },
}
const getters = {
    index(id) {
        return state.farhiUsers.findIndex(x => x.farhiUserId == id);
    }
}
const actions = {
    async getAll({ commit }, params) {
        var query = JSON.stringify(params)
            .replace(/{/g, "").replace(/}/g, "")
            .replace(/"/g, "").replace(/:/g, "=")
            .replace(/,/g, "&");
        try {
            helper.startLoading();
            const res = await api.farhiUsers.getAll(query);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            let total = res.data.total;
            commit('setFarhiUsers', data);
            commit('total', total);
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getDetails({ commit }, id) {

        try {
            helper.startLoading();
            const res = await api.farhiUsers.getDetails(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setFarhiUser', data)
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async getForEdit(_, id) {
        try {
            helper.startLoading();
            const res = await api.farhiUsers.getForEdit(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            return Promise.resolve(data);
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async add({ commit }, payload) {

        try {
            helper.startLoading();
            const res = await api.farhiUsers.add(payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا', 'error');
            }
            let data = res.data.data;
            let message = res.data.message;
            commit('add', data)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }

    },
    async edit({ commit }, { id, payload }) {
        try {
            helper.startLoading();
            const res = await api.farhiUsers.edit(id, payload);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            payload = res.data.data;
            let message = res.data.message;
            let index = getters.index(id);
            commit('edit', { index, payload})
            helper.startNotify(message, "success");
            //return Promise.resolve();
        }
        catch (err) {
            helper.catch(err)
            return new Promise.reject(err);
        }
    },
    async lock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.farhiUsers.lock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('lock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async unlock({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.farhiUsers.unlock(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('unlock', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    async delete({ commit }, id) {
        try {
            helper.startLoading();
            const res = await api.farhiUsers.delete(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            let index = getters.index(id);
            commit('delete', index)
            helper.startNotify(message, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },
    
    async resetPassword(_, id) {
        try {
            helper.startLoading();
            const res = await api.farhiUsers.resetPassword(id);
            helper.stopLoading();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let message = res.data.message;
            helper.openAlert(message + ", كلمة المرور الجديدة :" + res.data.data, "success");
        }
        catch (err) {
            helper.catch(err)
        }
    },

    async getFeatures({commit}) {

        try {
            const res = await api.farhiUsers.getFeatures();
            if (!res.data.statusCode) {
                helper.openAlert('تعذر الاتصال بالخادم, يرجي المحاولة لاحقا');
            }
            let data = res.data.data;
            commit('setFeatures', data);
        }
        catch (err) {
            helper.catch(err)
        }
    },

}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};