import Vue from 'vue'
import Vuex from 'vuex'
import samples from './modules/sample'
import hallBranches from './modules/hallBranches'
import storeBranches from './modules/storeBranches'
import lookup from './modules/lookup'
import loading from './modules/ui/loading'
import products from './modules/products'
import storeReservations from './modules/storeReservations'
import productReservations from './modules/productReservations'
import hallReservations from './modules/hallReservations'
import customers from './modules/customers'
import farhiUsers from './modules/farhiUsers'
import notify from './modules/ui/notify'

Vue.use(Vuex)

export const store = new Vuex.Store({

    modules: {
        // ui:{
        loading,
        notify,
        // },
        samples,
        hallBranches,
        storeBranches,
        lookup,
        products,
        storeReservations,
        productReservations,
        hallReservations,
        customers,
        farhiUsers
    }
})
