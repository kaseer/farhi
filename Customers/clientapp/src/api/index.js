import sample from './controllers/sample.js'
import hallBranches from './controllers/hallBranches.js'
import storeBranches from './controllers/storeBranches.js'
import lookup from './controllers/lookup.js'
import products from './controllers/products.js'
import storeReservations from './controllers/storeReservations.js'
import productReservations from './controllers/productReservations.js'
import hallReservations from './controllers/hallReservations.js'
import customers from './controllers/customers.js'
import farhiUsers from './controllers/farhiUsers.js'

//axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

export default {
    sample,
    hallBranches,
    storeBranches,
    lookup,
    products,
    storeReservations,
    productReservations,
    hallReservations,
    customers,
    farhiUsers
}