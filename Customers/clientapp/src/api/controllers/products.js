import axios from 'axios';

const api = "api/products"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    async getForEdit(id) {
        return axios.get(`${api}/${id}/getForEdit`);
    },
    async edit(id,data) {
        return axios.put(`${api}/${id}`, data);
    },
    async lock(id) {
        return axios.put(`${api}/${id}/lock`);
    },
    async unlock(id) {
        return axios.put(`${api}/${id}/unlock`);
    },
    async delete(id) {
        return axios.delete(`${api}/${id}`);
    },
    async getImages(id) {
        return axios.get(`${api}/${id}/getImages`);
    },
    async addImages(id,file) {
        return axios.post(`${api}/${id}/addImages`, file);
    },
    async deleteImages(checkedImages) {
        return axios.put(`${api}/deleteImages`, checkedImages);
    },
}