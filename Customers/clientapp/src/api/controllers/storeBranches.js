import axios from 'axios';

const api = "api/storeBranches"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    async getForEdit(id) {
        return axios.get(`${api}/${id}/getForEdit`);
    },
    async edit(id,data) {
        return axios.put(`${api}/${id}`, data);
    },
    async delete(id) {
        return axios.delete(`${api}/${id}`);
    },
    async getImages(id) {
        return axios.get(`${api}/${id}/getImages`);
    },
    async addImages(id,file) {
        return axios.post(`${api}/${id}/addImages`, file);
    },
    async deleteImages(checkedImages) {
        return axios.put(`${api}/deleteImages`, checkedImages);
    },
    async getVideo(id) {
        return axios.get(`${api}/${id}/getVideo`);
    },
    async addVideo(id,file) {
        return axios.post(`${api}/${id}/addVideo`, file);
    },
    async deleteVideo(id) {
        return axios.delete(`${api}/${id}/deleteVideo`);
    },
}