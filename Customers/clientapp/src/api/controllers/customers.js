import axios from 'axios';

const api = "api/customers"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    
    lock(id) {
        return axios.put(`${api}/${id}/lock`);
    },
    unlock(id) {
        return axios.put(`${api}/${id}/unlock`);
    },
    
    resetPassword(id) {
        return axios.put(`${api}/${id}/resetPassword`);
    },
    
}