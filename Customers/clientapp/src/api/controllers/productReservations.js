import axios from 'axios';

const api = "api/productReservations"
export default {
    async getAll(params) {
        return axios.get(`${api}?${params}`);
    },
    async add(data) {
        return axios.post(`${api}`, data);
    },
    async getDetails(id) {
        return axios.get(`${api}/${id}`);
    },
    async getForEdit(id) {
        return axios.get(`${api}/${id}/getForEdit`);
    },
    async edit(id,data) {
        return axios.put(`${api}/${id}`, data);
    },
    async accept(id) {
        return axios.put(`${api}/${id}/accept`);
    },
    async reject(id) {
        return axios.put(`${api}/${id}/reject`);
    },
    async getPrice(id) {
        return axios.get(`${api}/${id}/getPrice`);
    },
}