﻿using AutoMapper;
using Common;
using Customers.ViewModels;
using Models;
using System;

namespace Customers.AutoMapper
{
    public class HallReservationsMapper : Profile
    {
        //private readonly IHttpContextAccess _context;
        public HallReservationsMapper()
        {
            //_context = context;

            CreateMap<HallReservationsVM, Hallreservation>()
                .ForMember(x => x.Hallreservationdetails, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => { 
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom(src => Status.Active);
                })

                .ForMember(x => x.CustomerId, opt => {
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom(src => src.CustomerId);
                })
                .ForMember(x => x.CreatedBy, opt => {
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.ModifiedBy, opt => {
                    opt.PreCondition(x => x.Status is not null);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.CreatedOn, opt => {
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom(src => DateTime.Now);
                })
                .ForMember(x => x.ModifiedOn, opt => {
                    opt.PreCondition(x => x.Status is not null);
                    opt.MapFrom(src => DateTime.Now);
                });

        }
    }
}
