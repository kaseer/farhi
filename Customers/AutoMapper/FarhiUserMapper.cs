﻿using AutoMapper;
using Common;
using Customers.ViewModels;
using Models;
using System;

namespace Customers.AutoMapper
{
    public class FarhiUserMapper : Profile
    {
        //private readonly IHttpContextAccess _context;
        public FarhiUserMapper()
        {
            //_context = context;

            CreateMap<FarhiUserData, Farhiuser>()
                .ForMember(x => x.Password, opt => opt.Ignore())
                .ForMember(x => x.Salt, opt => opt.Ignore())
                .ForMember(x => x.FarhiuserpermissionUsers, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => { 
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom(src => Status.Active);
                })
                .ForMember(x => x.CreatedBy, opt => {
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.CreatedOn, opt => {
                    opt.PreCondition(x => x.Status is null);
                    opt.MapFrom(src => DateTime.Now);
                })
                .ForMember(x => x.ModifiedBy, opt => {
                    opt.PreCondition(x => x.Status is not null);
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.ModifiedOn, opt => {
                    opt.PreCondition(x => x.Status is not null);
                    opt.MapFrom(src => DateTime.Now);
                });
        }
    }
}
