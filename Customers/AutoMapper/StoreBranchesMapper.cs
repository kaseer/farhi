﻿using AutoMapper;
using Common;
using Customers.ViewModels;
using Models;
using System;

namespace Customers.AutoMapper
{
    public class StoreBranchesMapper : Profile
    {
        //private readonly IHttpContextAccess _context;
        public StoreBranchesMapper()
        {
            //_context = context;

            CreateMap<StoreBranchesVM, Storebranch>()
                .ForMember(x => x.AdminCreatedBy, opt => opt.Ignore())
                .ForMember(x => x.AdminCreatedOn, opt => opt.Ignore())
                .ForMember(x => x.ModifiedBy, opt => opt.Ignore())
                .ForMember(x => x.ModifiedOn, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => opt.Ignore())
                .ForMember(x => x.Storeworkdays, opt => opt.Ignore())

                .ForMember(x => x.StartTime, opt => {
                    opt.MapFrom(src => TimeOnly.Parse(src.TimeRange[0]));
                })
                .ForMember(x => x.EndTime, opt => {
                    opt.MapFrom(src => TimeOnly.Parse(src.TimeRange[1]));
                })
                .ForMember(x => x.ModifiedBy, opt => {
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.ModifiedOn, opt => {
                    opt.MapFrom(src => DateTime.Now);
                });
            
        }
    }
}
