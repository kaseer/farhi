﻿using AutoMapper;
using Common;
using Customers.ViewModels;
using Models;
using System;

namespace Customers.AutoMapper
{
    public class HallBranchesMapper : Profile
    {
        //private readonly IHttpContextAccess _context;
        public HallBranchesMapper()
        {
            //_context = context;

            CreateMap<HallBranchesVM, Hallbranch>()
                .ForMember(x => x.CreatedBy, opt => opt.Ignore())
                .ForMember(x => x.CreatedOn, opt => opt.Ignore())
                .ForMember(x => x.ModifiedBy, opt => opt.Ignore())
                .ForMember(x => x.ModifiedOn, opt => opt.Ignore())
                .ForMember(x => x.Status, opt => opt.Ignore())
                .ForMember(x => x.Hallappointments, opt => opt.Ignore())
                .ForMember(x => x.Hallbranchservices, opt => opt.Ignore())
                .ForMember(x => x.Hallbranchconditions, opt => opt.Ignore())

                .ForMember(x => x.StartTime, opt => {
                    opt.MapFrom(src => TimeOnly.Parse(src.TimeRange[0]));
                })
                .ForMember(x => x.EndTime, opt => {
                    opt.MapFrom(src => TimeOnly.Parse(src.TimeRange[1]));
                })
                .ForMember(x => x.UserModifiedBy, opt => {
                    opt.MapFrom((src, dest, destMember, context) => context.Items["UserId"]);
                })
                .ForMember(x => x.UserModifiedOn, opt => {
                    opt.MapFrom(src => DateTime.Now);
                });
            
        }
    }
}
