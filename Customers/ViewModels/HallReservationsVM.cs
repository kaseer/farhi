﻿using Common;
using FluentValidation;
using System;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    public class HallReservationsVM
    {        
        public int CustomerId { get; set; }
        public DateTime ReservationDate { get; set; }
        public int HallBranchId { get; set; }
        public sbyte? Status { get; set; }
        public List<HallReservationDetails> HallReservationDetails { get; set; }        

    }


    public class HallReservationDetails
    {
        public int HallAppointmentId { get; set; }
        public List<int> Services { get; set; }

    }
    public class HallReservationsVMValidator : AbstractValidator<HallReservationsVM>
    {
        public HallReservationsVMValidator()
        {

            RuleFor(x => x.CustomerId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الزبون");
            RuleFor(x => x.HallBranchId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الخدمة");
            RuleFor(x => x.ReservationDate).NotEmpty().WithErrorCode("VE1101").WithMessage("يرجي إختيار الكمية");
            RuleFor(x => x.HallReservationDetails.Count).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الكمية");

        }
    }
}
