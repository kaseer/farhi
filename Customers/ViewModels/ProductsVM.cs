﻿using FluentValidation;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    public class ProductsVM
    {
        public int StoreBranchId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SubCategoryId { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public sbyte? Status { get; set; }
    }

    public class ProductsVMValidator : AbstractValidator<ProductsVM>
    {
        public ProductsVMValidator()
        {


            RuleFor(x => x.StoreBranchId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار المحل");

            RuleFor(x => x.Name).NotEmpty().WithErrorCode("VE1113").WithMessage("يرجي اختيار اسم للمنتج");
            RuleFor(x => x.Name).MaximumLength(40).WithErrorCode("VE1103").WithMessage("يرجي ان يكون الاسم لايتجاوز عن 30 خانة");

            RuleFor(x => x.Description).NotEmpty().WithErrorCode("VE1113").WithMessage("يرجي اختيار وصف للمنتج");
            RuleFor(x => x.Description).MaximumLength(40).WithErrorCode("VE1103").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            RuleFor(x => x.SubCategoryId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار نوع الصنف");

            RuleFor(x => x.Qty).GreaterThan(-1).WithErrorCode("VE1126").WithMessage("يرجي اختيار الكمية");

            RuleFor(x => x.Price).GreaterThan(-1).WithErrorCode("VE1126").WithMessage("يرجي تحديد السعر");

            
        }
    }
}
