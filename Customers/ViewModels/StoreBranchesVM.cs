﻿using Common;
using FluentValidation;
using System;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    
    public class StoreBranchesVM
    {

        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public int StoreId { get; set; }
        //public int StoreTypeId { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public string[] TimeRange { get; set; }
        public List<StoreWorkDays> StoreWorkDays { get; set; }
        public List<StoreServices> Services { get; set; }

        //public decimal? StandardPrice { get; set; }
        //public sbyte Status { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }

        //public Store Store { get; set; }
    }

    public class StoreServices
    {
        public int? ServiceId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int? ReservationStatus { get; set; }//Ignore
    }

    public class StoreWorkDays
    {
        public sbyte WorkDayId { get; set; }
        public string[] TimeRange { get; set; }
    }
    
    


    public class StoreBranchesVMValidator : AbstractValidator<StoreBranchesVM>
    {
        public StoreBranchesVMValidator()
        {
            //RuleFor(x => x.Store).NotEmpty().WithErrorCode("VE1111").WithMessage("يرجي تعبئة الوصف");
            //RuleFor(x => x.Store.StoreId).NotEmpty().WithErrorCode("VE1111").WithMessage("يرجي تعبئة الوصف");

            //RuleFor(x => x.Store.StoreName).NotEmpty().WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");
            //RuleFor(x => x.Store.StoreName).MaximumLength(40).WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            //RuleFor(x => x.Store.OwnerName).NotEmpty().WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");
            //RuleFor(x => x.Store.OwnerName).MaximumLength(40).WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            //RuleFor(x => x.Store.Email).NotEmpty().WithErrorCode("VE1119").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Store.Email).MaximumLength(50).WithErrorCode("VE1110").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Store.Email).Must(Validation.IsValidEmail).WithErrorCode("VE111144").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            //RuleFor(x => x.Store.PhoneNo1).NotEmpty().WithErrorCode("VE111155").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Store.PhoneNo1).MaximumLength(15).WithErrorCode("VE111166").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Store.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE111177").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                           
            //RuleFor(x => x.Store.PhoneNo2).MaximumLength(15).WithErrorCode("VE111188").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Store.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE111199").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");


            RuleFor(x => x.AddressId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.AddressDescription).MaximumLength(40).WithErrorCode("VE1103").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            RuleFor(x => x.Longitude).ExclusiveBetween(-180,180).WithErrorCode("VE1105").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.Latitude).ExclusiveBetween(-90, 90).WithErrorCode("VE1106").WithMessage("يرجي تعبئة الوصف");


            RuleFor(x => x.TimeRange.Length).Equal(2).WithErrorCode("VE1107").WithMessage("خطأ في تعبئة التوقيت");
            RuleFor(x => x.TimeRange[0]).NotEmpty().WithErrorCode("VE1108").WithMessage("يرجي تعبئة بداية ساعات العمل");
            RuleFor(x => x.TimeRange[1]).NotEmpty().WithErrorCode("VE1109").WithMessage("يرجي تعبئة نهاية ساعات العمل");

            RuleFor(x => x.TimeRange[0]).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1110").WithMessage("يرجي تعبئة بداية ساعات العمل بطريقة صحيحة");
            RuleFor(x => x.TimeRange[1]).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1111").WithMessage("يرجي تعبئة نهاية ساعات العمل بطريقة صحيحة");

            //RuleFor(x => Convert.ToDateTime(x.TimeRange[1])).GreaterThan(x => Convert.ToDateTime(x.TimeRange[0])).WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");

            RuleFor(x => x.PhoneNo1).NotEmpty().WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PhoneNo1).MaximumLength(15).WithErrorCode("VE1114").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE1115").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            RuleFor(x => x.PhoneNo2).MaximumLength(15).WithErrorCode("VE1116").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE1117").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            RuleForEach(x => x.StoreWorkDays).ChildRules(p =>
            {
                p.RuleFor(x => x.WorkDayId).GreaterThan((sbyte)0).WithErrorCode("VE1118").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                p.RuleFor(x => x.TimeRange.Length).Equal(2).WithErrorCode("VE1120").WithMessage("يرجي تعبئة الوصف");
                p.RuleFor(x => x.TimeRange[0]).NotEmpty().When(x => x.TimeRange[1] != "").WithErrorCode("VE1121").WithMessage("يرجي تعبئة الوصف");
                p.RuleFor(x => x.TimeRange[1]).NotEmpty().When(x => x.TimeRange[0] != "").WithErrorCode("VE1122").WithMessage("يرجي تعبئة الوصف");
                p.RuleFor(x => x.TimeRange[0]).Must(Validation.IsValidTimeFormat).When(x => x.TimeRange[0] != "").WithErrorCode("VE1123").WithMessage("يرجي تعبئة الوصف");
                p.RuleFor(x => x.TimeRange[1]).Must(Validation.IsValidTimeFormat).When(x => x.TimeRange[1] != "").WithErrorCode("VE1124").WithMessage("يرجي تعبئة الوصف");
                p.RuleFor(x => Convert.ToDateTime(x.TimeRange[1])).GreaterThan(x => Convert.ToDateTime(x.TimeRange[0])).When(x => x.TimeRange[0] != "").WithErrorCode("VE1125").WithMessage("يرجي تعبئة الوقت بشكل صحيح");
            });

            RuleForEach(x => x.Services).ChildRules(p =>
            {
                p.RuleFor(x => x.Description).NotEmpty().WithErrorCode("VE1129").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                p.RuleFor(x => x.Price).GreaterThan(-1).WithErrorCode("VE1126").WithMessage("يرجي تحديد السعر");
            });

        }
    }
}
