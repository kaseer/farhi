﻿using Common;
using FluentValidation;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    public class UserData
    {
        public int? Id { get; set; }
        public List<int> Branches { get; set; }
    }

    public class NewFarhiUserData : FarhiUserData
    {
        public string Password { get; set; }
    }
    //public class FarhiStoreUserData : FarhiUserData
    //{
    //    public List<UserData> Stores { get; set; }
    //}
    public class FarhiUserData
    {
        public string UserName { get; set; }
        public string LoginName { get; set; }
        public string Email { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public List<int> Permissions { get; set; }
        public List<UserData> Stores { get; set; }
        public List<UserData> Halls { get; set; }
        public sbyte? Status { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }
        public class FarhiUserDataValidator : AbstractValidator<FarhiUserData>
        {
            public FarhiUserDataValidator()
            {
                RuleFor(x => x.UserName).NotEmpty().WithErrorCode("VE1111").WithMessage("يرجي تعبئة الوصف");
                RuleFor(x => x.UserName).MaximumLength(40).WithErrorCode("VE1111").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.LoginName).NotEmpty().WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");
                RuleFor(x => x.LoginName).MaximumLength(40).WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.LoginName).Must(Validation.IsValidLoginName).WithErrorCode("VE1114").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");


                RuleFor(x => x.Email).NotEmpty().WithErrorCode("VE1119").WithMessage("يرجي ادخال البريد الالكتروني");
                RuleFor(x => x.Email).MaximumLength(50).WithErrorCode("VE1110").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Email).Must(Validation.IsValidEmail).WithErrorCode("VE111144").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.PhoneNo1).NotEmpty().WithErrorCode("VE111155").WithMessage("يرجي ادخال رقم الهاتف الاول");
                RuleFor(x => x.PhoneNo1).MaximumLength(15).WithErrorCode("VE111166").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE111177").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

                RuleFor(x => x.PhoneNo2).MaximumLength(15).WithErrorCode("VE111188").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE111199").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            }
        }

        public class NewFarhiUserDataValidator : AbstractValidator<NewFarhiUserData>
        {
            public NewFarhiUserDataValidator()
            {
                Include(new FarhiUserDataValidator());
                RuleFor(x => x.Password).NotEmpty().WithErrorCode("VE1115").WithMessage("يرجي ادخال كلمة المرور");
                RuleFor(x => x.Password.Length).GreaterThanOrEqualTo(8).WithErrorCode("VE1116").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Password).Must(Validation.IsValidPassword).WithErrorCode("VE1117").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                RuleFor(x => x.Halls.Count + x.Stores.Count).GreaterThan(0).WithErrorCode("VE1118").WithMessage("يرجي تعبئة صالة أو محل واحد علي الاقل");
                //RuleFor(x => x.HallBranches.Count).GreaterThan(0).Unless(x => x.HallId != null).WithErrorCode("VE1115").WithMessage("يرجي اختيار صاله او فروع");
            }
        }
    }
}
