﻿using Common;
using FluentValidation;
using System;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    public class ProductReservationsVM
    {
        public int ProductId { get; set; }
        public int Qty { get; set; }
        public int CustomerId { get; set; }
        public sbyte? Status { get; set; }
    }

    public class ProductReservationsVMValidator : AbstractValidator<ProductReservationsVM>
    {
        public ProductReservationsVMValidator()
        {


            RuleFor(x => x.ProductId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الخدمة");
            RuleFor(x => x.CustomerId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الزبون");
            RuleFor(x => x.Qty).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الكمية");

        }
    }
}
