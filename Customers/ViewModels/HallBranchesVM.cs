﻿using Common;
using FluentValidation;
using System;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    
    public class HallBranchesVM
    {

        public int AddressId { get; set; }
        public string AddressDescription { get; set; }
        public int HallId { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Latitude { get; set; }
        public int PeapleCapacity { get; set; }
        public int CarsCapacity { get; set; }
        public string[] TimeRange { get; set; }
        public List<HallAppontments> HallAppointments { get; set; }
        public List<HallServices> HallServices { get; set; }
        public List<Conditions> Conditions { get; set; }

        //public decimal? StandardPrice { get; set; }
        //public sbyte Status { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }

        //public Hall Hall { get; set; }
    }

    public class Conditions
    {
        public int ConditionId { get; set; }
        public string Description { get; set; }
    }

    public class HallAppontments
    {
        public sbyte WorkDayId { get; set; }
        public List<AppointmentData> AppointmentData { get; set; }
    }
    public class AppointmentData
    {
        public int? HallAppointmentId { get; set; }
        public int ReservationType { get; set; }
        public string[] TimeRange { get; set; }
        public decimal Price { get; set; }
        public int? ReservationStatus { get; set; }//Ignore

    }
    //public class Hall
    //{
    //    public int HallId { get; set; }
    //    public string HallName { get; set; }
    //    public string OwnerName { get; set; }
    //    public string PhoneNo1 { get; set; }
    //    public string PhoneNo2 { get; set; }
    //    public string Email { get; set; }
    //    public string Logo { get; set; }//Ignore
    //    //public int? MainBranchId { get; set; }
    //    //public sbyte Status { get; set; }
    //    //public int CreatedBy { get; set; }
    //    //public DateTime CreatedOn { get; set; }
    //    //public int? ModifiedBy { get; set; }
    //    //public DateTime? ModifiedOn { get; set; }
    //}
    
    public class HallServices
    {
        public int? HallBranchServiceId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int? ReservationStatus { get; set; }//Ignore
    }


    public class HallBranchesVMValidator : AbstractValidator<HallBranchesVM>
    {
        public HallBranchesVMValidator()
        {
            //RuleFor(x => x.Hall).NotEmpty().WithErrorCode("VE1111").WithMessage("يرجي تعبئة الوصف");
            //RuleFor(x => x.Hall.HallId).NotEmpty().WithErrorCode("VE1111").WithMessage("يرجي تعبئة الوصف");

            //RuleFor(x => x.Hall.HallName).NotEmpty().WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");
            //RuleFor(x => x.Hall.HallName).MaximumLength(40).WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            //RuleFor(x => x.Hall.OwnerName).NotEmpty().WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");
            //RuleFor(x => x.Hall.OwnerName).MaximumLength(40).WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            //RuleFor(x => x.Hall.Email).NotEmpty().WithErrorCode("VE1119").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Hall.Email).MaximumLength(50).WithErrorCode("VE1110").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Hall.Email).Must(Validation.IsValidEmail).WithErrorCode("VE111144").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            //RuleFor(x => x.Hall.PhoneNo1).NotEmpty().WithErrorCode("VE111155").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Hall.PhoneNo1).MaximumLength(15).WithErrorCode("VE111166").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Hall.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE111177").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                           
            //RuleFor(x => x.Hall.PhoneNo2).MaximumLength(15).WithErrorCode("VE111188").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            //RuleFor(x => x.Hall.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE111199").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");


            RuleFor(x => x.AddressId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.AddressDescription).MaximumLength(40).WithErrorCode("VE1103").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PeapleCapacity).GreaterThan(0).WithErrorCode("VE1103").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.CarsCapacity).GreaterThan(0).WithErrorCode("VE1104").WithMessage("يرجي تعبئة الوصف");

            RuleFor(x => x.Longitude).ExclusiveBetween(-180,180).WithErrorCode("VE1105").WithMessage("يرجي تعبئة الوصف");
            RuleFor(x => x.Latitude).ExclusiveBetween(-90, 90).WithErrorCode("VE1106").WithMessage("يرجي تعبئة الوصف");


            RuleFor(x => x.TimeRange.Length).Equal(2).WithErrorCode("VE1107").WithMessage("خطأ في تعبئة التوقيت");
            RuleFor(x => x.TimeRange[0]).NotEmpty().WithErrorCode("VE1108").WithMessage("يرجي تعبئة بداية ساعات العمل");
            RuleFor(x => x.TimeRange[1]).NotEmpty().WithErrorCode("VE1109").WithMessage("يرجي تعبئة نهاية ساعات العمل");

            RuleFor(x => x.TimeRange[0]).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1110").WithMessage("يرجي تعبئة بداية ساعات العمل بطريقة صحيحة");
            RuleFor(x => x.TimeRange[1]).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1111").WithMessage("يرجي تعبئة نهاية ساعات العمل بطريقة صحيحة");

            //RuleFor(x => Convert.ToDateTime(x.TimeRange[1])).GreaterThan(x => Convert.ToDateTime(x.TimeRange[0])).WithErrorCode("VE1112").WithMessage("يرجي تعبئة الوصف");

            RuleFor(x => x.PhoneNo1).NotEmpty().WithErrorCode("VE1113").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PhoneNo1).MaximumLength(15).WithErrorCode("VE1114").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PhoneNo1).Must(Validation.IsValidNumber).WithErrorCode("VE1115").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            RuleFor(x => x.PhoneNo2).MaximumLength(15).WithErrorCode("VE1116").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            RuleFor(x => x.PhoneNo2).Must(Validation.IsValidNumber).WithErrorCode("VE1117").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");

            RuleForEach(x => x.HallAppointments).ChildRules(p =>
            {
                p.RuleFor(x => x.WorkDayId).GreaterThan((sbyte)0).WithErrorCode("VE1118").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                p.RuleForEach(x => x.AppointmentData).ChildRules(z =>
                {


                    z.RuleFor(x => x.TimeRange.Length).Equal(2).WithErrorCode("VE1120").WithMessage("يرجي تعبئة الوصف");
                    z.RuleFor(x => x.TimeRange[0]).NotEmpty().WithErrorCode("VE1121").WithMessage("يرجي تعبئة الوصف");
                    z.RuleFor(x => x.TimeRange[1]).NotEmpty().WithErrorCode("VE1122").WithMessage("يرجي تعبئة الوصف");
                    z.RuleFor(x => x.TimeRange[0]).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1123").WithMessage("يرجي تعبئة الوصف");
                    z.RuleFor(x => x.TimeRange[1]).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1124").WithMessage("يرجي تعبئة الوصف");
                    z.RuleFor(x => Convert.ToDateTime(x.TimeRange[1])).GreaterThan(x => Convert.ToDateTime(x.TimeRange[0])).WithErrorCode("VE1125").WithMessage("يرجي تعبئة الوقت بشكل صحيح");


                    z.RuleFor(x => x.Price).GreaterThan(-1).WithErrorCode("VE1126").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                    //z.RuleFor(x => x.ReservationStatus).GreaterThan(0).WithErrorCode("VE1127").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                });
            });

            RuleForEach(x => x.HallServices).ChildRules(p =>
            {
                p.RuleFor(x => x.Description).NotEmpty().WithErrorCode("VE1129").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
                //p.RuleFor(x => x.ReservationStatus).GreaterThan(0).WithErrorCode("VE1130").WithMessage("يرجي ان يكون الوصف لايتجاوز عن 30 خانة");
            });
        }
    }
}
