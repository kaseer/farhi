﻿using Common;
using FluentValidation;
using System;
using System.Collections.Generic;

namespace Customers.ViewModels
{
    public class StoreReservationsVM
    {
        public int ServiceId { get; set; }
        public int CustomerId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public sbyte WorkDayId { get; set; }
        public sbyte? Status { get; set; }
    }

    public class StoreReservationsVMValidator : AbstractValidator<StoreReservationsVM>
    {
        public StoreReservationsVMValidator()
        {


            RuleFor(x => x.ServiceId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الخدمة");
            RuleFor(x => x.CustomerId).GreaterThan(0).WithErrorCode("VE1101").WithMessage("يرجي إختيار الزبون");
            RuleFor(x => x.StartTime).NotEmpty().WithErrorCode("VE1121").WithMessage("يرجي تعبئة وقت بداية الحجز");
            RuleFor(x => x.EndTime).NotEmpty().WithErrorCode("VE1122").WithMessage("يرجي تعبئة وقت نهاية الحجز");
            RuleFor(x => x.StartTime).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1123").WithMessage("يرجي تعبئة وقت بداية الحجز بطريقة صحيحة");
            RuleFor(x => x.EndTime).Must(Validation.IsValidTimeFormat).WithErrorCode("VE1124").WithMessage("يرجي تعبئة وقت نهاية الحجز بطريقة صحيحة");
            RuleFor(x => Convert.ToDateTime(x.EndTime)).GreaterThan(x => Convert.ToDateTime(x.StartTime)).WithErrorCode("VE1125").WithMessage("وقت البداية يجب ان يكون اقل من النهاية");
            RuleFor(x => x.WorkDayId).GreaterThan((sbyte) 0).WithErrorCode("VE1101").WithMessage("يرجي إختيار اليوم");
            
        }
    }
}
