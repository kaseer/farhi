import Vue from 'vue';
// import App from './App.vue';
import './plugins/element.js'
import Layout from './layout/Layout.vue'


// import ElementUI from 'element-ui';
// import locale from 'element-ui/lib/index.js';
import router from './router';
import dataService from './shared/DataService';
import BlockUIService from './shared/BlockUIService.js';
import './plugins/element.js'
import './styles/Vendor.scss';
import './styles/Site.scss'
import './styles/iconfont/material-icons.css'


// Vue.use(ElementUI, { locale });

Vue.prototype.$http = dataService;
Vue.prototype.$blockUI = BlockUIService;

Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(Layout)
}).$mount('#app')
