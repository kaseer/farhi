import MyHeader from './header/Header.vue'
import SideBar from './sideBar/SideBar.vue'
import Login from '../views/login/Login.vue'
export default {
  name: 'app',
  components: {
    MyHeader,
    SideBar,
    Login
  }
}