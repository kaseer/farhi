import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
// import Layout from '../layout/Layout.vue'
// import Login from '../views/login/Login.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  // {
  //   path: '/login',
  //   name: 'Login',
  //   component: Login
  // }
]

const router = new VueRouter({
  routes
})

export default router
