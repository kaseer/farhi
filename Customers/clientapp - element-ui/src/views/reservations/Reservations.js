import addReservation from './addReservation/AddReservation.vue'
import editReservation from './editReservation/EditReservation.vue'
import viewReservation from './viewReservation/ViewReservation.vue'
export default {
  components: {
    'add-reservation': addReservation,
    editReservation,
    viewReservation,
  },
  created() {
    this.getHallBranches();
  },
  data: () => {
    return {
      filter: {
        pageNo: 1,
        pageSize: 10,
        hallBranchId: null,
        searchByName: null
      },
      total: 100,
      status: 0,
      hallBranches: [],
      reservations: [],
      hallBranchId: null,
      customerHallBranchIds: {
        customerId: null,
        hallBranchId:null
      },
    }
  },

  methods: {
    getHallBranches() {
      this.hallBranches = [
        { hallBranchId: 1, hallBranchDescription: 'صالة الهناء - فرع بن عاشور' },
        { hallBranchId: 2, hallBranchDescription: 'صالة الهناء - فرع الظهرة' },
        { hallBranchId: 3, hallBranchDescription: 'صالة الهناء - فرع صلاح الدين' },
        { hallBranchId: 4, hallBranchDescription: 'صالة الهناء - فرع تاجورا' },
      ];
    },
    getReservations() {
      this.reservations = [
        {
          customerId: 1,
          customerName: 1,
          appointmentsCount: "sdfdsfsd",
          createdOn: "2020-06-24 21:25:02",
          fullName: "sfsdfs",
          total: 2
        },
        {
          customerId: 2,
          customerName: 1,
          appointmentsCount: "sdfdsfsd",
          createdOn: "2020-06-24 21:25:02",
          fullName: "sfsdfs",
          total: 2
        },
        {
          customerId: 3,
          customerName: 1,
          appointmentsCount: "sdfdsfsd",
          createdOn: "2020-06-24 21:25:02",
          fullName: "sfsdfs",
          total: 2
        },
      ]
    },
    PageChanged(pageNo) {
      this.filter.pageNo = pageNo;
    },
    addReservation() {
      if (!this.filter.hallBranchId) {
        this.$globalAlerts.notify("تحذير", "يرجي اختيار الصالة ", "warning");
        return;
      }
      this.hallBranchId = this.filter.hallBranchId;
      this.status = 1;
    },
    viewReservation(index) {
      this.customerHallBranchIds.customerId = this.reservations[index].customerId;
      this.customerHallBranchIds.hallBranchId = this.filter.hallBranchId;
      this.status = 3;
    },
    editReservation(index) {
      this.customerHallBranchIds.customerId = this.reservations[index].customerId;
      this.customerHallBranchIds.hallBranchId = this.filter.hallBranchId;
      this.status = 2;
    },
    deleteReservation(index) {
      this.$confirm('This will permanently delete the file. Continue?', 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning',
        center: true
      }).then(() => {
        this.$message({
          type: 'success',
          message: 'Delete completed'
        });
        this.reservations.splice(index,1);
      }).catch(() => {});
    },
  }
}
