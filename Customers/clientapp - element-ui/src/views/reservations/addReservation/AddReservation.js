export default {
  created() {
    this.setRules();
    this.getAppointments();
    this.getHallBranchServices();
  },
  props:{
    hallBranchId:Number,
  },
  data: () => {
    return {
      rules: {},
      appointments: [],
      hallBranchServices:[],
      checkCustomerData:null,
      reservationData: {
        customerData: null,
        reservationDate:null,
        appointmentId: null,
        services:[],
      },
      reservations:[],
      reservationsTable:[]
    }
  },

  methods: {
    checkCustomer(){
      this.reservationData.customerData = 1;
    },
    getAppointments() {
      this.appointments = [
        { appointmentId: 1,price:10, appointmentDescription: 'عرس - 12:00 - 14:00' },
        { appointmentId: 2,price:10, appointmentDescription:  'غذاء - 16:00 - 18:00' },
        { appointmentId: 3,price:10, appointmentDescription:  'غذاء - 16:00 - 18:00' },
        { appointmentId: 4,price:10, appointmentDescription:  'غذاء - 16:00 - 18:00' },
        { appointmentId: 5,price:10, appointmentDescription:  'غذاء - 16:00 - 18:00' },
        { appointmentId: 6,price:10, appointmentDescription:  'غذاء - 16:00 - 18:00' },
      ];
    },
    dd(){
      console.log("fsdfsdf")
    },
    getHallBranchServices(){
      this.hallBranchServices = [
        { hallBranchServiceId: 1,price:20, hallBranchServiceDescription: 'غذاء + مش عارف شو ـ 20 دينار'},
        { hallBranchServiceId: 2,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 3,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 4,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 5,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 6,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 7,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 8,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 9,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 10,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 11,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 12,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 13,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 14,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },
        { hallBranchServiceId: 15,price:20, hallBranchServiceDescription:  'غذاء + مش عارف شو ـ 20 دينار' },

      ];
    },
    addToReservations(){
      let totalPrice = 0;
      let services = [];
      this.reservationData.services.forEach((x)=>{
        let data = this.hallBranchServices.find(s=>s.hallBranchServiceId == x);
        totalPrice = data.price;
        services.push(data.hallBranchServiceDescription)
      });

      totalPrice = totalPrice + this.appointments.find(x=>x.appointmentId == this.reservationData.appointmentId).price;
      this.reservationsTable.push({
        appointmentDescription: this.appointments.find(x=>x.appointmentId == this.reservationData.appointmentId).appointmentDescription,
        services: services,
        reservationDate:this.reservationData.reservationDate,
        total :totalPrice
      });
      this.reservations.push(this.reservationData);
      this.reservationData.reservationDate = null;
      this.reservationData.appointmentId = null;
      this.reservationData.services = [];
    },
    addResarvation(){
      
    },
    setRules() {
      this.rules = {
        
        phoneNo1: [
          { required: true, message: 'الرجاء ادخال رقم الهاتف', trigger: 'change' },
          { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
        ],
        phoneNo2: [
          { validator: this.$validation.checkPhoneNumber(), trigger: 'change' }
        ],
        addressId: [
          { validator: this.$validation.checkSelect(), trigger: 'change' }
        ],
        addressDescriptione: [
          { required: false, trigger: 'change' },
          { max: 50, message: 'يرجي عدم الزايدة عن 50 حرف' }
        ],
        longitude: [
          { validator: this.$validation.checkLongitude(), trigger: 'change' }
        ],
        latitude: [
          { validator: this.$validation.checkLatitude(), trigger: 'change' }
        ],
        peapleCapacity: [
          { validator: this.$validation.checkInputNumber(), trigger: 'change' }
        ],
        carsCapacit: [
          { validator: this.$validation.checkInputNumber(), trigger: 'change' }
        ],
        fromDay: [
          { validator: this.$validation.checkSelect(), trigger: 'change' }
        ],
        toDay: [
          { validator: this.$validation.checkSelect(), trigger: 'change' }
        ],
        timeRange: [
          { validator: this.$validation.checkDate(), trigger: 'change' }
        ],
      }
    },
    handleDelete(index, row) {
      console.log(index, row);
    },
    back() {
      this.$parent.status = 0;
    }


  }
}
