export default{
    props:{
        customerId:Number
    },
    created(){
        this.getReservationData();
    },
    data(){
        return{
            reservationData:{
                customerName:null,
                customerNumber:null,
                oppointmentData:[
                    {
                        reservationDate:null,
                        oppointmentDay:null,
                        services:[]
                    }
                ],
            },
            activeNames: []
        };
    },
    methods:{
        getReservationData(){
            this.reservationData = {
                customerName:"salim",
                customerNumber:"063522244",
                oppointmentData:[
                    {
                        reservationDate: "2020-06-24",
                        oppointmentDay:"عرس - 12:00 - 14:00",
                        services:["غذاء + مش عارف شو ـ 20 دينار", "غذاء + مش عارف شو ـ 20 دينار"]
                    },
                    {
                        reservationDate: "2020-06-24",
                        oppointmentDay:"عرس - 12:00 - 14:00",
                        services:["غذاء + مش عارف شو ـ 20 دينار", "غذاء + مش عارف شو ـ 20 دينار"]
                    },
                    {
                        reservationDate: "2020-06-24",
                        oppointmentDay:"عرس - 12:00 - 14:00",
                        services:["غذاء + مش عارف شو ـ 20 دينار", "غذاء + مش عارف شو ـ 20 دينار"]
                    }
                ],
            }
        },
        back(){
            this.$parent.status=0;
        }
    }
}