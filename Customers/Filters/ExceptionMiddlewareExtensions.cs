﻿using Common;
using Common.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace Customers.Filters
{
    public static class ExceptionMiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILoggerManager logger)
        {
            app.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                    //contextFeature.RouteValues.Values[0] -> view[1] controllername;
                    string StatusCode = "";
                    foreach (var value in contextFeature.RouteValues.Values)
                    {
                        StatusCode += value.ToString() + " - ";
                    }
                    if (contextFeature != null)
                    {
                        logger.LogError($"Something went wrong: {contextFeature.Error}");
                        await context.Response.WriteAsync(new ErrorModel()
                        {
                            StatusCode = StatusCode + context.Response.StatusCode.ToString(),
                            Message    = AppMessages.ErrorMessages.ServerError,
                            Log = contextFeature.Error.Message
                        }.ToString());
                    }
                });
            });
        }
    }
}
