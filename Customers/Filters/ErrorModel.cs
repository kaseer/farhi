﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Customers.Filters
{
    public class ErrorModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string Log { get; set; }
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
