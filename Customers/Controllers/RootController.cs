﻿using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class RootController : Controller
    {
        public farhiContext db;

        public RootController(farhiContext context)
        {
            db = context;
        }

        public int UserId()
        {
            var claims = HttpContext.User.Claims.ToList();
            int userId = Convert.ToInt32(claims.Where(p => p.Type == "UserId").Select(p => p.Value).SingleOrDefault());
            return 1;
        }

        public async Task<List<int>> Accessable()
        {
            try
            {
                var halls = new List<int>(); var stores = new List<int>();

                var accessable = await (from c in db.Farhiusershallsstores
                                        where c.FarhiUserId == UserId()
                                        select new
                                        {
                                            hallBranches = c.Farhiusershallbranches.Select(x => x.BranchId).ToList(),
                                            storeBranches = c.Farhiusersstorebranches.Select(x => x.BranchId).ToList()
                                        }).ToListAsync();
                accessable.ForEach(x => {
                    halls.AddRange(x.hallBranches.Where(p => p > 0));
                    stores.AddRange(x.storeBranches.Where(p => p > 0));
                });
                var hallsquery = from c in db.Farhiusershallbranches
                                 where c.IdNavigation.FarhiUser.Status != Status.Deleted
                                 && halls.Contains(c.BranchId)
                                 select c.IdNavigation.FarhiUserId;
                var storesquery = from c in db.Farhiusersstorebranches
                                  where c.IdNavigation.FarhiUser.Status != Status.Deleted
                                  && stores.Contains(c.BranchId)
                                  select c.IdNavigation.FarhiUserId;
                var users = hallsquery.Union(storesquery).Distinct().ToList();

                return users;
            }
            catch
            {
                return null;
            }
        }
    }
}
