﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Customers.ViewModels;
using Customers.Global;
using System.IO;
using System.Collections.Generic;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class ProductReservationsController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public ProductReservationsController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, int storeBranchId, string search)
        {
            
                //var dd = x.;
                var query = from c in db.Productreservations
                            where c.Status != Status.Deleted
                            && (
                                c.Product.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select c;
                if (storeBranchId > 0)
                    query = query.Where(x => x.Product.StoreBranchId == storeBranchId);

                if (search is not null)
                    query = query.Where(x => x.Product.Name.Contains(search));

                var data = await query.Select(c => new
                {
                    c.ProductReservationId,
                    c.ProductId,
                    c.Customer.CustomerName,
                    Description = c.Product.Name,
                    StoreName = c.Product.StoreBranch.Store.StoreName + " - " + c.Product.StoreBranch.Address.AddressName + "_" + c.Product.StoreBranch.AddressDescription,
                    c.Price,
                    CreatedBy = c.CreatedByNavigation.UserName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1, data, total });
            
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
                var data = await (from c in db.Productreservations
                                  where c.Status != Status.Deleted
                                  && c.ProductReservationId == id
                                  && (
                                      c.Product.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      StoreName = c.Product.StoreBranch.Store.StoreName + " - " + c.Product.StoreBranch.Address.AddressName + "_" + c.Product.StoreBranch.AddressDescription,
                                      c.Customer.CustomerName,
                                      Description = c.Product.Name,
                                      c.Qty,
                                      c.Price,
                                      CreatedBy = c.CreatedByNavigation.UserName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                      ModifiedBy = c.ModifiedByNavigation.UserName,
                                      ModifiedOn = c.ModifiedOn != null ? c.ModifiedOn.Value.ToString("yyyy/MM/dd") : "-",
                                      c.Status,

                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE20001", message = "الرجاء التاكد اختيار فرع الصالة " });
                }



                var data = await (from c in db.Productreservations
                                  where c.Status != Status.Deleted
                                  && c.ProductReservationId == id
                                  && (
                                      c.Product.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      ProductReservation = new ProductReservationsVM
                                      {
                                          ProductId = c.ProductId,
                                          CustomerId = c.CustomerId,
                                          Status = c.Status,
                                          Qty = c.Qty
                                      },
                                      c.Customer.CustomerName
                                  }).SingleOrDefaultAsync();

                if (data is null)
                {
                    return NotFound(new { StatusCode = "RE20002", message = AppMessages.ErrorMessages.NotFoundError });
                }

                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] ProductReservationsVM dataVM)
        {
            

                var add = mapper.Map<Productreservation>(dataVM, opt => opt.Items["UserId"] = UserId());
                var price = await GetProductPrice(dataVM.ProductId);
                add.Price = price;
                db.Add(add);
                await db.SaveChangesAsync();

                var data = await db.Productreservations
                    .Where(x => x.ProductReservationId == add.ProductReservationId)
                    .Select(c => new
                    {
                        c.ProductReservationId,
                        c.ProductId,
                        c.Customer.CustomerName,
                        Description = c.Product.Name,
                        StoreName = c.Product.StoreBranch.Store.StoreName + " - " + c.Product.StoreBranch.Address.AddressName + "_" + c.Product.StoreBranch.AddressDescription,
                        c.Price,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم إضافة البيانات", data });
            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] ProductReservationsVM dataVM)
        {
            
                var reservarion = await (from c in db.Productreservations
                                         where c.ProductReservationId == id
                                         && (
                                             c.Product.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                         )
                                         && c.Status != Status.Deleted
                                         select c).SingleOrDefaultAsync();
                if (reservarion is null) return NotFound(new { StatusCode = "RE20003", message = AppMessages.ErrorMessages.NotFoundError });

                var edit = mapper.Map(dataVM, reservarion, opt => opt.Items["UserId"] = UserId());
                var price = await GetProductPrice(dataVM.ProductId);
                edit.Price = price;

                await db.SaveChangesAsync();

                var data = await db.Productreservations
                    .Where(x => x.ProductReservationId == id)
                    .Select(c => new
                    {
                        c.ProductReservationId,
                        c.ProductId,
                        c.Customer.CustomerName,
                        Description = c.Product.Name,
                        StoreName = c.Product.StoreBranch.Store.StoreName + " - " + c.Product.StoreBranch.Address.AddressName + "_" + c.Product.StoreBranch.AddressDescription,
                        c.Price,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data });
            
        }



        [HttpPut("{id}/accept")]
        public async Task<IActionResult> accept(int id)
        {
            
                var item = await (from c in db.Productreservations
                                  where c.ProductReservationId == id
                                  && (
                                      c.Product.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && c.Status == Status.Active
                                  select c).SingleOrDefaultAsync();
                if (item is null) return NotFound(new { StatusCode = "RE20004", message = AppMessages.ErrorMessages.NotFoundError });
                if (item.Status == Status.Accept) return BadRequest(new { StatusCode = "RE20005", message = "حالة هذا الحجز مقبول مسبقا" });

                item.Status = Status.Accept;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قبول الحجز" });
            
        }

        [HttpPut("{id}/reject")]
        public async Task<IActionResult> reject(int id)
        {
            
                var item = await (from c in db.Productreservations
                                  where c.ProductReservationId == id
                                  && (
                                      c.Product.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && (c.Status == Status.Active || c.Status == Status.Accept)
                                  select c).SingleOrDefaultAsync();
                if (item is null) return NotFound(new { StatusCode = "RE20006", message = AppMessages.ErrorMessages.NotFoundError });
                if (item.Status == Status.Reject) return BadRequest(new { StatusCode = "RE20007", message = "حالة هذا الحجز مرفوض مسبقا" });

                item.Status = Status.Reject;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم رفض الحجز" });
            
        }


        [HttpGet("{id}/GetPrice")]
        public async Task<IActionResult> GetPrice(int id)
        {
                var data = await GetProductPrice(id);
                return Ok(new { StatusCode = 1, data });
            
        }

        public async Task<decimal> GetProductPrice(int id)
        {
            return await (from c in db.Products
                          where c.Status != Status.Deleted
                          && c.ProductId == id
                          && (
                              c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                          )
                          select c.Price).SingleOrDefaultAsync();
        }
    }
}
