﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Customers.ViewModels;
using Customers.Global;
using System.IO;
using System.Collections.Generic;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class ProductsController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public ProductsController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, int storeBranchId, string search)
        {
            
                var query = from c in db.Products
                            where c.Status != Status.Deleted
                            && (
                                c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select c;
                if (storeBranchId > 0)
                    query = query.Where(x => x.StoreBranchId == storeBranchId);

                if (search is not null)
                    query = query.Where(x => x.Description.Contains(search));

                var data = await query.Select(c => new
                {
                    c.ProductId,
                    c.Name,
                    StoreName = c.StoreBranch.Store.StoreName + " - " + c.StoreBranch.Address.AddressName + "_" + c.StoreBranch.AddressDescription,
                    c.Price,
                    CreatedBy = c.CreatedByNavigation.UserName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1, data, total });
            
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
                var data = await (from c in db.Products
                                  where c.Status != Status.Deleted
                                  && c.ProductId == id
                                  && (
                                      c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      StoreName = c.StoreBranch.Store.StoreName + " - " + c.StoreBranch.Address.AddressName + "_" + c.StoreBranch.AddressDescription,
                                      c.Name,
                                      c.Description,
                                      c.Price,
                                      c.Qty,
                                      SubCategory = c.SubCategory.Description,
                                      Category = c.SubCategory.Category.Description,
                                      CreatedBy = c.CreatedByNavigation.UserName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                      ModifiedBy = c.ModifiedByNavigation.UserName,
                                      ModifiedOn = c.ModifiedOn != null ? c.ModifiedOn.Value.ToString("yyyy/MM/dd") : "-",
                                      c.Status,

                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE18001", message = "الرجاء التاكد اختيار فرع الصالة " });
                }



                var data = await (from c in db.Products
                                  where c.Status != Status.Deleted
                                  && c.ProductId == id
                                  && (
                                      c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new 
                                  {
                                      product = new ProductsVM
                                      {
                                          Status = c.Status,
                                          Name = c.Name,
                                          Description = c.Description,
                                          Price = c.Price,
                                          Qty = c.Qty,
                                          StoreBranchId = c.StoreBranchId,
                                          SubCategoryId = c.SubCategoryId,
                                      },
                                      c.SubCategory.CategoryId
                                      
                                  }).SingleOrDefaultAsync();

                if (data is null)
                {
                    return NotFound(new { StatusCode = "RE18002", message = AppMessages.ErrorMessages.NotFoundError });
                }

                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] ProductsVM dataVM)
        {
            

                var add = mapper.Map<Product>(dataVM, opt => opt.Items["UserId"] = UserId());
                db.Add(add);
                await db.SaveChangesAsync();

                var data = await db.Products
                    .Where(x => x.ProductId == add.ProductId)
                    .Select(c => new
                    {
                        c.ProductId,
                        Name = c.Name,
                        c.Description,
                        StoreName = c.StoreBranch.Store.StoreName + " - " + c.StoreBranch.Address.AddressName + "_" + c.StoreBranch.AddressDescription,
                        c.Price,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم إضافة البيانات", data });
            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] ProductsVM dataVM)
        {
            
                var product = await (from c in db.Products
                                     where c.ProductId == id
                                     && (
                                         c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                     )
                                     && c.Status != Status.Deleted
                                     select c).SingleOrDefaultAsync();
                if (product is null) return NotFound(new { StatusCode = "RE18003", message = AppMessages.ErrorMessages.NotFoundError });

                var edit = mapper.Map(dataVM, product, opt => opt.Items["UserId"] = UserId());

                
                await db.SaveChangesAsync();

                var data = await db.Products
                    .Where(x => x.ProductId == id)
                    .Select(c => new
                    {
                        c.ProductId,
                        c.Name,
                        StoreName = c.StoreBranch.Store.StoreName + " - " + c.StoreBranch.Address.AddressName + "_" + c.StoreBranch.AddressDescription,
                        c.Price,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data });
            
        }



        [HttpPut("{id}/lock")]
        public async Task<IActionResult> Lock(int id)
        {
            
                var item = await (from c in db.Products
                                  where c.ProductId == id
                                  && (
                                      c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (item is null) return BadRequest(new { StatusCode = "RE18004", message = " لم يتم العثور علي بيانات لهذا المنتج" });
                if (item.Status == Status.Locked) return BadRequest(new { StatusCode = "RE18005", message = "حالة هذا المنتج مقفلة مسبقا" });

                item.Status = Status.Locked;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قفل حالة المنتج بنجاح" });
            
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> Unlock(int id)
        {
            
                var item = await (from c in db.Products
                                  where c.ProductId == id
                                  && (
                                      c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (item is null) return BadRequest(new { StatusCode = "RE18006", message = " لم يتم العثور علي بيانات لهذا المنتج" });
                if (item.Status == Status.Active) return BadRequest(new { StatusCode = "RE18007", resumessagelt = "حالة هذا المنتج فعالة مسبقا" });

                item.Status = Status.Active;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم تفعيل حالة المنتج بنجاح" });
            
        }
        // DELETE api/<FarhiAdminsController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            
                //TODO -- check ability to delete user 

                var item = await (from c in db.Products.Include(x => x.Productreservations.Where(x => x.Status == Status.Active))
                                  where c.ProductId == id
                                  && (
                                      c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (item is null) return BadRequest(new { StatusCode = "RE18008", message = " لم يتم العثور علي بيانات لهذا المنتج" });
                if(item.Productreservations.Count() > 0) return BadRequest(new { StatusCode = "RE18009", message = " لا يمكن حذف منتج لديه حجوزات فعالة" });
                item.Status = Status.Deleted;
                foreach(var del in item.Productreservations)
                {
                    item.Status = Status.Deleted;
                }
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم حذف المنتج بنجاح" });
            
        }


        [HttpGet("{id}/getImages")]
        public async Task<IActionResult> getImages(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE18010", message = "يرجي التاكد من بيانات الفرع" });
                }
                var imagesQuery = await (from m in db.Productimages
                                         where m.ProductId == id
                                         select m).ToListAsync();


                if (imagesQuery.Count <= 0)
                {
                    return Ok(new { StatusCode = 1, data = imagesQuery });
                }
                //for(int i = 0; i < imagesQuery.Count; i++)
                //{
                //    imagesQuery[0].Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + images[0].Path));
                //}
                var path = ConfigurationItems.GetProductImages(configuration);
                var data = imagesQuery.Select(x => new Classes.Images()
                {
                    ImageId = x.ProductImageId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(path + x.Path))
                });
                return Ok(new { StatusCode = 1, data });
            
        }


        [HttpPost("{id}/addImages")]
        public async Task<IActionResult> addImages(int id, IFormFile file)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE18011", message = "يرجي التاكد من بيانات المنتج" });
                }
                var productInfo = await (from h in db.Products
                                       where h.ProductId == id
                                       && h.Status != Status.Deleted
                                       select h).SingleOrDefaultAsync();
                if (productInfo == null)
                {
                    return NotFound(new { StatusCode = "RE18012", message = "لا توجد بيانات لهذا المنتج" });
                }
                string FileName = productInfo.Name + "_" + id;
                var path = ConfigurationItems.GetProductImages(configuration);

                FileName = FileName + "_" + ConfigurationItems.unique() + ".jpg";
                var filePath = Path.Combine(path, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    //FileStream.Synchronized(stream);
                    await file.CopyToAsync(stream);
                    //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                }

                Productimage addImage = new Productimage()
                {
                    ProductId = id,
                    Path = FileName,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                };
                await db.Productimages.AddAsync(addImage);
                await db.SaveChangesAsync();
                var image = new Classes.Images()
                {
                    ImageId = addImage.ProductImageId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(filePath))
                };
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data = image });

            
        }

        [HttpPut("deleteImages")]
        public async Task<IActionResult> deleteImages([FromBody] List<int> checkedImages)
        {
           
                if (checkedImages.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE18013", message = "يرجي التاكد من اختيار الصور المراد حذفها" });
                }
                foreach (var image in checkedImages)
                {
                    var deleteImage = await (from m in db.Productimages
                                             where m.ProductImageId == image
                                             select m).SingleOrDefaultAsync();
                    if (deleteImage != null)
                    {
                        db.Productimages.Remove(deleteImage);
                        await db.SaveChangesAsync();
                        var filePath = Path.Combine(ConfigurationItems.GetProductImages(configuration), deleteImage.Path);
                        using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose))
                        {
                            //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                        }
                    }
                }

                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" });
            
        }
    }
}
