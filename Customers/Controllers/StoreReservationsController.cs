﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Customers.ViewModels;
using Customers.Global;
using System.IO;
using System.Collections.Generic;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class StoreReservationsController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public StoreReservationsController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, int storeBranchId, string search)
        {
            
                var query = from c in db.Storeservicereservations
                            where c.Status != Status.Deleted
                            && (
                                c.Service.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select c;
                if (storeBranchId > 0)
                    query = query.Where(x => x.Service.StoreBranchId == storeBranchId);

                if (search is not null)
                    query = query.Where(x => x.Service.Description.Contains(search));

                var data = await query.Select(c => new
                {
                    c.ServiceReservationId,
                    c.ServiceId,
                    c.Customer.CustomerName,
                    c.Service.Description,
                    StoreName = c.Service.StoreBranch.Store.StoreName + " - " + c.Service.StoreBranch.Address.AddressName + "_" + c.Service.StoreBranch.AddressDescription,
                    c.Price,
                    CreatedBy = c.CreatedByNavigation.UserName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1, data, total });
            
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
                var data = await (from c in db.Storeservicereservations
                                  where c.Status != Status.Deleted
                                  && c.ServiceReservationId == id
                                  && (
                                      c.Service.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      StoreName = c.Service.StoreBranch.Store.StoreName + " - " + c.Service.StoreBranch.Address.AddressName + "_" + c.Service.StoreBranch.AddressDescription,
                                      c.Customer.CustomerName,
                                      c.Service.Description,
                                      Day = Constant.SelectDay(c.WorkDayId),
                                      StartTime = c.StartTime.ToString(@"HH\:mm"),
                                      EndTime = c.EndTime.ToString(@"HH\:mm"),
                                      c.Price,
                                      CreatedBy = c.CreatedByNavigation.UserName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                      ModifiedBy = c.ModifiedByNavigation.UserName,
                                      ModifiedOn = c.ModifiedOn != null ? c.ModifiedOn.Value.ToString("yyyy/MM/dd") : "-",
                                      c.Status,

                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE19001", message = "الرجاء التاكد اختيار فرع الصالة " });
                }



                var data = await (from c in db.Storeservicereservations
                                  where c.Status != Status.Deleted
                                  && c.ServiceReservationId == id
                                  && (
                                      c.Service.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      StoreReservation = new StoreReservationsVM
                                      {
                                          ServiceId = c.ServiceId,
                                          CustomerId = c.CustomerId,
                                          WorkDayId = c.WorkDayId,
                                          StartTime = c.StartTime.ToString(@"HH\:mm"),
                                          EndTime = c.EndTime.ToString(@"HH\:mm"),
                                          Status = c.Status,
                                      },
                                      c.Customer.CustomerName
                                  }).SingleOrDefaultAsync();

                if (data is null)
                {
                    return NotFound(new { StatusCode = "RE19002", message = AppMessages.ErrorMessages.NotFoundError });
                }

                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] StoreReservationsVM dataVM)
        {
            

                var add = mapper.Map<Storeservicereservation>(dataVM, opt => opt.Items["UserId"] = UserId());
                var price = await GetServicePrice(dataVM.ServiceId);
                add.Price = price;
                db.Add(add);
                await db.SaveChangesAsync();

                var data = await db.Storeservicereservations
                    .Where(x => x.ServiceReservationId == add.ServiceReservationId)
                    .Select(c => new
                    {
                        c.ServiceReservationId,
                        c.ServiceId,
                        c.Customer.CustomerName,
                        c.Service.Description,
                        StoreName = c.Service.StoreBranch.Store.StoreName + " - " + c.Service.StoreBranch.Address.AddressName + "_" + c.Service.StoreBranch.AddressDescription,
                        c.Price,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم إضافة البيانات", data });
            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] StoreReservationsVM dataVM)
        {
            
                var reservarion = await (from c in db.Storeservicereservations
                                     where c.ServiceReservationId == id
                                     && (
                                         c.Service.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                     )
                                     && c.Status != Status.Deleted
                                     select c).SingleOrDefaultAsync();
                if (reservarion is null) return NotFound(new { StatusCode ="RE19003", message = AppMessages.ErrorMessages.NotFoundError });

                var edit = mapper.Map(dataVM, reservarion, opt => opt.Items["UserId"] = UserId());
                var price = await GetServicePrice(dataVM.ServiceId);
                edit.Price = price;

                await db.SaveChangesAsync();

                var data = await db.Storeservicereservations
                    .Where(x => x.ServiceReservationId == id)
                    .Select(c => new
                    {
                        c.ServiceReservationId,
                        c.ServiceId,
                        c.Customer.CustomerName,
                        c.Service.Description,
                        StoreName = c.Service.StoreBranch.Store.StoreName + " - " + c.Service.StoreBranch.Address.AddressName + "_" + c.Service.StoreBranch.AddressDescription,
                        c.Price,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data });
            
        }



        [HttpPut("{id}/accept")]
        public async Task<IActionResult> accept(int id)
        {
            
                var item = await (from c in db.Storeservicereservations
                                  where c.ServiceReservationId == id
                                  && (
                                      c.Service.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && c.Status == Status.Active
                                  select c).SingleOrDefaultAsync();
                if (item is null) return NotFound(new { StatusCode = "RE19004", message = AppMessages.ErrorMessages.NotFoundError });
                if (item.Status == Status.Accept) return BadRequest(new { StatusCode = "RE19005", message = "حالة هذا الحجز مقبول مسبقا" });

                item.Status = Status.Accept;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قبول الحجز" });
            
        }

        [HttpPut("{id}/reject")]
        public async Task<IActionResult> reject(int id)
        {
            
                var item = await (from c in db.Storeservicereservations
                                  where c.ServiceReservationId == id
                                  && (
                                      c.Service.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && (c.Status == Status.Active || c.Status == Status.Accept)
                                  select c).SingleOrDefaultAsync();
                if (item is null) return NotFound(new { StatusCode = "RE19006", message = AppMessages.ErrorMessages.NotFoundError });
                if (item.Status == Status.Reject) return BadRequest(new { StatusCode = "RE19007", message = "حالة هذا الحجز مرفوض مسبقا" });

                item.Status = Status.Reject;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم رفض الحجز" });
            
        }


        [HttpGet("{id}/GetPrice")]
        public async Task<IActionResult> GetPrice(int id)
        {
            
                var data = await GetServicePrice(id);
                return Ok(new { StatusCode = 1, data });
            
        }

        public async Task<decimal>  GetServicePrice(int id)
        {
            return await(from c in db.Storeservices
                         where c.Status != Status.Deleted
                         && c.ServiceId == id
                         && (
                             c.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                         )
                         select c.Price).SingleOrDefaultAsync();
        }
    }
}
