﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Customers.ViewModels;
using Customers.Global;
using System.IO;
using System.Collections.Generic;
using Common.EmailService;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class HallBranchesController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public HallBranchesController(farhiContext context, IConfiguration configuration, IMapper mapper, IEmailSender emailSender) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
        {

            var query = from c in db.Hallbranches
                            where c.Status == Status.Active
                            && (
                                c.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select c;
                if (search is not null)
                    query = query.Where(x => x.Address.AddressName.Contains(search) || x.AddressDescription.Contains(search));

                var data = await query.Select(c => new {
                    c.Hall.HallName,
                    c.HallBranchId,
                    AddressName = c.Address.AddressName + " - " + c.AddressDescription,
                    c.PhoneNo1,
                    c.Hall.MainBranchId,
                    workTimes = c.StartTime.ToString(@"hh\:mm\:ss") + "  -  " + c.EndTime.ToString(@"hh\:mm\:ss"),
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1, data, total });
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
                var data = await(from c in db.Hallbranches
                            where c.Status == Status.Active
                            && c.HallBranchId == id
                            && (
                                c.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select new {
                                c.Address.AddressName,
                                c.AddressDescription,
                                c.PhoneNo1,
                                c.PhoneNo2,
                                c.Longitude,
                                c.Latitude,
                                c.PeapleCapacity,
                                c.CarsCapacity,
                                StartTime = c.StartTime.ToString(@"hh\:mm\:ss"),
                                EndTime = c.EndTime.ToString(@"hh\:mm\:ss"),
                                HallAppointments = c.Hallappointments
                                            .Where(x => x.Status != Status.Deleted)
                                            .Select(x => new {
                                                WorkDay = Constant.SelectDay(x.WorkDayId),
                                                x.ReservationType.Description,
                                                StartTime = x.StartTime.ToString(@"hh\:mm\:ss"),
                                                EndTime = x.EndTime.ToString(@"hh\:mm\:ss"),
                                                x.Price,
                                            }).ToList(),
                                HallServices = c.Hallbranchservices
                                            .Where(x => x.Status != Status.Deleted)
                                            .Select(x => new {
                                                x.Description,
                                                x.Price,
                                            }).ToList(),
                                Subscriptions =
                                          c.Subscriptions.Select(x => new
                                          {
                                              subscriptions = x.Subscriptiondetails.Select(c => new {
                                                  c.SubscriptionPriceDetails.SubscriptionPrice.Description,
                                                  StartDate = c.StartDate.ToString("yyyy/MM/dd"),
                                                  EndDate = c.EndDate.ToString("yyyy/MM/dd"),
                                                  c.Price
                                              }).ToList(),
                                              date = x.CreatedOn.ToString("yyyy/MM/dd")
                                          }).ToList(),
                                Conditions = c.Hallbranchconditions
                                    .Where(x => x.Status != Status.Deleted)
                                    .Select(x => x.Description).ToList(),
                            }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02001", message = "الرجاء التاكد اختيار فرع الصالة " });
                }


                var HallAppontmentsQuery = await (from p in db.Hallappointments
                                                  where p.HallBranchId == id
                                                  && (
                                                        p.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                                    )
                                                  select new
                                                  {
                                                      p.HallAppointmentId,
                                                      p.WorkDayId,
                                                      p.ReservationTypeId,
                                                      TimeRange = new[] { p.StartTime.ToString(@"HH\:mm\:ss"), p.EndTime.ToString(@"HH\:mm\:ss") },
                                                      p.Price,
                                                      reservationStatus = p.Hallreservationdetails
                                                      .Select(f => f).Count(),
                                                  }).ToListAsync();

                var hallAppointments = HallAppontmentsQuery
                    .GroupBy(u => u.WorkDayId)
                    .OrderBy(u => u.Key)
                    .Select(x => new HallAppontments
                    {
                        WorkDayId = x.Key,
                        AppointmentData = x.Select(c => new AppointmentData
                        {
                            HallAppointmentId = c.HallAppointmentId,
                            ReservationType = c.ReservationTypeId,
                            TimeRange = c.TimeRange,
                            Price = c.Price,
                            ReservationStatus = c.reservationStatus
                        }).ToList()
                    }).ToList();

                var data = await (from c in db.Hallbranches
                                            where c.Status != Status.Deleted
                                            && c.HallBranchId == id
                                            && (
                                                c.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                            )
                                            select new HallBranchesVM 
                                            {
                                                HallId = c.HallId,
                                                AddressId =  c.AddressId,
                                                AddressDescription = c.AddressDescription,
                                                PhoneNo1 = c.PhoneNo1,
                                                PhoneNo2 = c.PhoneNo2,
                                                Longitude = c.Longitude,
                                                Latitude = c.Latitude,
                                                PeapleCapacity = c.PeapleCapacity,
                                                CarsCapacity = c.CarsCapacity,
                                                TimeRange = new[] { c.StartTime.ToString(@"HH\:mm\:ss"), c.EndTime.ToString(@"HH\:mm\:ss") },
                                                HallAppointments = hallAppointments,
                                                HallServices = c.Hallbranchservices
                                                  .Where(x => x.Status != Status.Deleted)
                                                  .Select(x => new HallServices
                                                  {
                                                      HallBranchServiceId = x.HallBranchServiceId,
                                                      Description = x.Description,
                                                      Price = x.Price,
                                                      ReservationStatus = x.Hallservicereservations
                                                        .Where(f => f.Status == Status.Active)
                                                        .Select(f => f).Count(),
                                                  }).ToList(),
                                                Conditions = c.Hallbranchconditions
                                                        .Where(x => x.Status != Status.Deleted)
                                                        .Select(x => new Conditions {
                                                            Description = x.Description,
                                                            ConditionId = x.ConditionId
                                                        }).ToList(),
                                            }).SingleOrDefaultAsync();

                if (data is null)
                {
                    return NotFound(new { StatusCode = "RE02002", message = AppMessages.ErrorMessages.NotFoundError });
                }

                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] HallBranchesVM dataVM)
        {
            
                var branch = await (from c in db.Hallbranches
                           .Include(x => x.Hall)
                           .Include(x => x.Hallappointments)
                           .Include(x => x.Hallbranchservices)
                           .Include(x => x.Hallbranchconditions)
                           where c.HallBranchId == id
                           && c.Status != Status.Deleted
                           select c).SingleOrDefaultAsync();
                if (branch is null) return NotFound(new {StatusCode = "RE02003", message = "Not Fount"});

                var edit = mapper.Map(dataVM, branch, opt => opt.Items["UserId"] = UserId());

                foreach(var apt in edit.Hallappointments)
                {
                    var check = dataVM.HallAppointments.Where(x => x.WorkDayId == apt.WorkDayId).Single().AppointmentData.ToList();
                    var dataApt = check.Where(x => x.HallAppointmentId == apt.HallAppointmentId).SingleOrDefault();
                    if (dataApt is null)
                    {
                        var checkreserv = await (from c in db.Hallreservationdetails
                                          where c.HallAppointmentId == apt.HallAppointmentId
                                          && c.HallReservation.Status != Status.Deleted
                                          select c).AnyAsync();
                        if (checkreserv) return BadRequest(new { StatusCode = "RE02004", Message = "لا يمكن حذف موعد يحتوي علي حجوزات" });
                        edit.Hallappointments.Remove(apt);
                    }
                    else
                    {
                        apt.StartTime = TimeOnly.Parse(dataApt.TimeRange[0]);
                        apt.EndTime = TimeOnly.Parse(dataApt.TimeRange[1]);
                        apt.Price = dataApt.Price;
                        apt.ReservationTypeId = dataApt.ReservationType;
                        apt.ModifiedBy = UserId();
                        apt.ModifiedOn = DateTime.Now;
                    }
                }

                foreach(var apt in dataVM.HallAppointments)
                {
                    var newApt = new Hallappointment();
                    
                    foreach(var d in apt.AppointmentData)
                    {
                        if (d.HallAppointmentId is null)
                        {
                            newApt.WorkDayId = apt.WorkDayId;
                            newApt.Status = Status.Active;
                            newApt.Price = d.Price;
                            newApt.StartTime = TimeOnly.Parse(d.TimeRange[0]);
                            newApt.EndTime = TimeOnly.Parse(d.TimeRange[1]);
                            newApt.HallBranchId = id;
                            newApt.ReservationTypeId = d.ReservationType;
                            newApt.CreatedBy = UserId();
                            newApt.CreatedOn = DateTime.Now;
                            edit.Hallappointments.Add(newApt); //Adding
                        }
                    }
                }

                foreach(var ser in edit.Hallbranchservices)
                {
                    var check = dataVM.HallServices.Where(x => x.HallBranchServiceId == ser.HallBranchServiceId).SingleOrDefault();
                    if (check is not null)
                    {
                        ser.Description = check.Description;
                        ser.ModifiedBy = UserId();
                        ser.ModifiedOn = DateTime.Now;
                        ser.Price = check.Price;
                    }
                    else
                    {
                        edit.Hallbranchservices.Remove(ser);
                    }
                }
                foreach(var ser in dataVM.HallServices)
                {
                    if (ser.HallBranchServiceId == 0)
                    {
                        edit.Hallbranchservices.Add(new Hallbranchservice() { 
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Price = ser.Price,
                            Status = Status.Active,
                            Description = ser.Description,
                        });
                    }
                }

                foreach (var con in edit.Hallbranchconditions)
                {
                    var check = dataVM.Conditions.Where(x => x.ConditionId == con.ConditionId).SingleOrDefault();
                    if (check is not null)
                    {
                        con.Description = check.Description;
                        con.ModifiedBy = UserId();
                        con.ModifiedOn = DateTime.Now;
                    }
                    else
                    {
                        edit.Hallbranchconditions.Remove(con);
                    }
                }
                foreach (var con in dataVM.Conditions)
                {
                    if (con.ConditionId == 0)
                    {
                        edit.Hallbranchconditions.Add(new Hallbranchcondition()
                        {
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Status = Status.Active,
                            Description = con.Description,
                        });
                    }
                }
                await db.SaveChangesAsync();

                var data = await db.Hallbranches
                    .Where( x => x.HallBranchId == id)
                    .Select(c => new {
                    c.Hall.HallName,
                    c.HallBranchId,
                    AddressName = c.Address.AddressName + " - " + c.AddressDescription,
                    c.PhoneNo1,
                    c.Hall.MainBranchId,
                    workTimes = c.StartTime.ToString(@"hh\:mm\:ss") + "  -  " + c.EndTime.ToString(@"hh\:mm\:ss"),
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).SingleOrDefaultAsync();
                return Ok(new {StatusCode = 1, message = "تم تعديل البيانات", data});
            
        }

        [HttpGet("{id}/getImages")]
        public async Task<IActionResult> getImages(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02005", message = "يرجي التاكد من بيانات الفرع" });
                }
                var imagesQuery = await (from m in db.Media
                                         where m.HallBranchId == id
                                         && m.MediaType == 1
                                         && m.Status != Status.Deleted
                                         select m).ToListAsync();


                if (imagesQuery.Count <= 0)
                {
                    return Ok(new { StatusCode = 1, data = imagesQuery });
                }
                //for(int i = 0; i < imagesQuery.Count; i++)
                //{
                //    imagesQuery[0].Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + images[0].Path));
                //}
                var path = ConfigurationItems.GetHallImages(configuration);
                var data = imagesQuery.Select(x => new Classes.Images()
                {
                    ImageId = x.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(path + x.Path))
                });
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getVideo")]
        public async Task<IActionResult> getVideo(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02006", message = "يرجي التاكد من بيانات الفرع" });
                }
                var video = await (from m in db.Media
                                   where m.HallBranchId == id
                                   && m.MediaType == 2
                                   && m.Status != Status.Deleted
                                   select m.Path
                                   ).SingleOrDefaultAsync();


                if (video == null)
                {
                    return Ok(new { StatusCode = 1, data = video });
                }
                var path = ConfigurationItems.GetHallVideos(configuration);
                video = "data:video/mp4;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(path + video));
                return Ok(new { StatusCode = 1, data = video });
            
        }

        [HttpPost("{id}/addVideo")]
        public async Task<IActionResult> addVideo(int id, IFormFile file)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02007", message = "يرجي التاكد من بيانات الفرع" });
                }
                var HallInfo = await (from h in db.Hallbranches
                                       .Include(m => m.Media.Where(x => x.MediaType == 2))
                                       .Include(m => m.Hall)
                                       where h.HallBranchId == id
                                       && h.Status != Status.Deleted
                                       select h).SingleOrDefaultAsync();
                if (HallInfo == null)
                {
                    return NotFound(new { StatusCode = "RE02008", message = "لا توجد بيانات لهذا الصالة" });
                }
                if (HallInfo.Media.Count > 0)
                {
                    foreach (var item in HallInfo.Media)
                    {
                        db.Media.Remove(item);
                    }
                }
                string FileName = HallInfo.Hall.HallName + "_" + id + ".mp4";
                var videopath = ConfigurationItems.GetHallVideos(configuration);
                var filePath = Path.Combine(videopath, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                    Medium addVideo = new Medium()
                    {
                        MediaType = 2,
                        Path = FileName,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                        HallBranchId = id,
                        Status = Status.Active,
                    };
                    await db.Media.AddAsync(addVideo);
                    await db.SaveChangesAsync();
                }
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", });
            
        }

        [HttpPost("{id}/addImages")]
        public async Task<IActionResult> addImages(int id, IFormFile file)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02009", message = "يرجي التاكد من بيانات الفرع" });
                }
                var HallInfo = await (from h in db.Hallbranches
                                       .Include(m => m.Hall)
                                       where h.HallBranchId == id
                                       && h.Status != Status.Deleted
                                       select h).SingleOrDefaultAsync();
                if (HallInfo == null)
                {
                    return NotFound(new { StatusCode = "RE02010", message = "لا توجد بيانات لهذا الصالة" });
                }
                string FileName = HallInfo.Hall.HallName + "_" + id;
                var path = ConfigurationItems.GetHallImages(configuration);

                FileName = FileName + "_" + ConfigurationItems.unique() + ".jpg";
                var filePath = Path.Combine(path, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }

                Medium addImage = new Medium()
                {
                    MediaType = 1,
                    Path = FileName,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    HallBranchId = id,
                    Status = Status.Active,
                };
                await db.Media.AddAsync(addImage);
                await db.SaveChangesAsync();
                var image = new Classes.Images()
                {
                    ImageId = addImage.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(filePath))
                };
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data = image });
            
        }

        [HttpPut("deleteImages")]
        public async Task<IActionResult> deleteImages([FromBody] List<int> checkedImages)
        {
            
                if (checkedImages.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE02011", message = "يرجي التاكد من اختيار الصور المراد حذفها" });
                }
                foreach (var image in checkedImages)
                {
                    var deleteImage = await (from m in db.Media
                                             where m.MediaId == image
                                             select m).SingleOrDefaultAsync();
                    if (deleteImage != null)
                    {
                        db.Media.Remove(deleteImage);
                        await db.SaveChangesAsync();
                        var filePath = Path.Combine(ConfigurationItems.GetHallImages(configuration), deleteImage.Path);
                        using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose)){}
                    }
                }

                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" });
            
        }
        [HttpDelete("{id}/deleteVideo")]
        public async Task<IActionResult> deleteVideo(int id)
        {
            
                var delete = await (from m in db.Media
                                    where m.MediaType == 2
                                    && m.HallBranchId == id
                                    select m).SingleOrDefaultAsync();
                db.Media.Remove(delete);
                await db.SaveChangesAsync();
                var filePath = Path.Combine(ConfigurationItems.GetHallVideos(configuration), delete.Path);
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose)) { }


                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" });
            
        }
    }
}
