﻿using Common;
using Customers.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class FarhiUsersController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public FarhiUsersController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiUsersController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
        {
            

                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11001", message = "خطأ في جلب البيانات" });

                var query = from c in db.Farhiusers
                            where c.Status != Status.Deleted
                            && accessable.Contains(c.FarhiUserId)
                            select c;
                if (search is not null)
                    query = query.Where(x => x.UserName.Contains(search) || x.LoginName.Contains(search));


                var data = await query
                    .Select(c => new {
                        c.FarhiUserId,
                        c.UserName,
                        c.LoginName,
                        c.Email,
                        c.PhoneNo1,
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.Value.ToString("yyyy/MM/dd"),
                        c.Status
                    }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();

                return Ok(new { StatusCode = 1, data, total });
            
        }

        // GET api/<FarhiUsersController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetails(int id)
        {
            
                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11002", message = "خطأ في جلب البيانات" });
                var features = (from p in db.Features
                                where p.Status != Status.Deleted
                                //&& db.Permissions.Where(x => x.Adminpermissions.Where(x => x.AdminId == id).Count() > 0).Count() > 0
                                select new
                                {
                                    p.FeatureName,
                                    Permissions = p.Permissions
                                    .Where(p => p.Farhiuserpermissions.Select(x => x.UserId).Contains(id))
                                    .Select(x => x.PermissionName)
                                }).Where(x => x.Permissions.Count() > 0).ToList();

                var data = await (from c in db.Farhiusers
                                  where c.Status != Status.Deleted
                                  && c.FarhiUserId == id
                                  && accessable.Contains(id)
                                  select new {
                                      c.UserName,
                                      c.LoginName,
                                      c.Email,
                                      c.PhoneNo1,
                                      c.PhoneNo2,
                                      halls = c.Farhiusershallsstores.Where(x => x.HallId != null).Select(x => new
                                      {
                                          x.Hall.HallName,
                                          branches = x.Farhiusershallbranches.Select(z => "فرع " + z.Branch.Address.AddressName + " " + z.Branch.AddressDescription).ToList()
                                      }).ToList(),
                                      stores = c.Farhiusershallsstores.Where(x => x.StoreId != null).Select(x => new
                                      {
                                          x.Store.StoreName,
                                          branches = x.Farhiusersstorebranches.Select(z => "فرع " + z.Branch.Address.AddressName + " " + z.Branch.AddressDescription).ToList()
                                      }).ToList(),
                                      features
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE11003", message = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> GetForEdit(int id)
        {
            
                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11004", message = "خطأ في جلب البيانات" });

                var data = await (from c in db.Farhiusers
                                  where c.Status != Status.Deleted
                                  && c.FarhiUserId == id
                                  && accessable.Contains(id)
                                  select new
                                  {
                                      c.FarhiUserId,
                                      c.UserName,
                                      c.LoginName,
                                      c.Email,
                                      c.PhoneNo1,
                                      c.PhoneNo2,
                                      Permissions = c.FarhiuserpermissionUsers.Select(x => x.PermissionId).ToList(),
                                      halls = (from p in db.Farhiusershallbranches
                                               where p.IdNavigation.FarhiUserId == c.FarhiUserId
                                               && p.IdNavigation.HallId != null
                                               select p.BranchId).ToList(),

                                      stores = (from p in db.Farhiusersstorebranches
                                                where p.IdNavigation.FarhiUserId == c.FarhiUserId
                                                && p.IdNavigation.StoreId != null
                                                select p.BranchId).ToList(),
                                      //HallBranches = c.Farhiuserhallbranches.Select(x => x.HallBranchId).ToList(),
                                      //HallId = c.HallId == null ? c.Farhiuserhallbranches.Select(x => x.HallBranch.HallId).SingleOrDefault() : c.HallId
                                  }).SingleOrDefaultAsync();
                if (data is null) return BadRequest(new { StatusCode = "RE11005", message = "لا توجد يبانات لهذا المستخدم" });
                return Ok(new { StatusCode = 1, data });
            
        }

        // POST api/<FarhiUsersController>
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] NewFarhiUserData dataVM)
        {
            
                var check = await (from c in db.Farhiusers where c.LoginName == dataVM.LoginName && c.Status != Status.Deleted select c).AnyAsync();
                if (check) return BadRequest(new { StatusCode = "RE11006", message = "اسم الدخول المدخل موجود مسبقا" });

                var user = mapper.Map<Farhiuser>(dataVM, opt => opt.Items["UserId"] = UserId());
                var hashing = Security.HashPassword(dataVM.Password, configuration["Secret"]);
                user.Password = hashing.Password;
                user.Salt = hashing.Salt;

                //if(user.HallId != null) user.HallId = user.HallId.Value;
                //else
                //{
                foreach (var hall in dataVM.Halls)
                {
                    Farhiusershallsstore userhalls = new Farhiusershallsstore()
                    {
                        FarhiUserId = user.FarhiUserId,
                        HallId = hall.Id,
                        CreatedOn = DateTime.Now,
                    };
                    user.Farhiusershallsstores.Add(userhalls);

                    if (hall.Branches.Count == 0)
                    {
                        Farhiusershallbranch userbranches = new Farhiusershallbranch()
                        {
                            BranchId = (from c in db.Halls where c.HallId == hall.Id select c.MainBranchId.Value).SingleOrDefault(),
                            Id = userhalls.Id,
                            CreatedOn = DateTime.Now
                        };
                        userhalls.Farhiusershallbranches.Add(userbranches);
                    }

                    foreach (var branch in hall.Branches)
                    {
                        Farhiusershallbranch userbranches = new Farhiusershallbranch()
                        {
                            BranchId = branch,
                            Id = userhalls.Id,
                            CreatedOn = DateTime.Now
                        };
                        userhalls.Farhiusershallbranches.Add(userbranches);
                    }
                }

                foreach (var store in dataVM.Stores)
                {
                    Farhiusershallsstore userstores = new Farhiusershallsstore()
                    {
                        FarhiUserId = user.FarhiUserId,
                        StoreId = store.Id,
                        CreatedOn = DateTime.Now,
                    };
                    user.Farhiusershallsstores.Add(userstores);

                    if (store.Branches.Count == 0)
                    {
                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
                        {
                            BranchId = (from c in db.Stores where c.StoreId == store.Id select c.MainBranchId.Value).SingleOrDefault(),
                            Id = userstores.Id,
                            CreatedOn = DateTime.Now
                        };
                        userstores.Farhiusersstorebranches.Add(userbranches);
                    }

                    foreach (var branch in store.Branches)
                    {
                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
                        {
                            BranchId = branch,
                            Id = userstores.Id,
                            CreatedOn = DateTime.Now
                        };
                        userstores.Farhiusersstorebranches.Add(userbranches);
                    }
                }
                //}
                foreach (var per in dataVM.Permissions)
                {
                    Farhiuserpermission adminpermission = new Farhiuserpermission()
                    {
                        PermissionId = per,
                        UserId = user.FarhiUserId,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                    };
                    user.FarhiuserpermissionUsers.Add(adminpermission);
                }
                db.Add(user);
                await db.SaveChangesAsync();

                var data = await (from c in db.Farhiusers
                                  where c.FarhiUserId == user.FarhiUserId
                                  select new
                                  {
                                      c.FarhiUserId,
                                      c.UserName,
                                      c.LoginName,
                                      c.Email,
                                      c.PhoneNo1,
                                      CreatedBy = c.CreatedByNavigation.UserName,
                                      CreatedOn = c.CreatedOn.Value.ToString("yyyy/MM/dd"),
                                      c.Status
                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data, message = "تمت إضافة المستخدم بنجاح" });
            
        }

        // PUT api/<FarhiUsersController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, [FromBody] FarhiUserData dataVM)
        {
            
                var check = await (from c in db.Farhiusers
                                   where c.LoginName == dataVM.LoginName
                                   && c.Status != Status.Deleted
                                   && c.FarhiUserId != id
                                   select c).AnyAsync();
                if (check) return BadRequest(new { StatusCode = "RE11007", message = "اسم الدخول المدخل موجود مسبقا" });

                var user = await (from c in db.Farhiusers
                                  .Include(x => x.FarhiuserpermissionUsers)
                                  .Include(x => x.Farhiusershallsstores)
                                  where c.Status != Status.Deleted
                                  && c.FarhiUserId == id
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE11008", message = "لا توجد يبانات لهذا المستخدم" });
                var edit = mapper.Map(dataVM, user, opt => opt.Items["UserId"] = UserId());
                //var user = mapper.Map<Adminuser>(dataVM, opt => opt.Items["UserId"] = UserId());
                //TODO don't remove halls, stores or permissions, just check and remove to keep the one's that not listed under the admin
                foreach (var hall in user.Farhiusershallsstores)
                {
                    var remove = await (from c in db.Farhiusershallbranches where c.Id == hall.Id select c).ToListAsync();
                    foreach (var item in remove)
                    {
                        db.Farhiusershallbranches.Remove(item);
                    }
                    db.Farhiusershallsstores.Remove(hall);
                }

                foreach (var hall in dataVM.Halls)
                {
                    Farhiusershallsstore userhalls = new Farhiusershallsstore()
                    {
                        FarhiUserId = id,
                        HallId = hall.Id,
                        CreatedOn = DateTime.Now,
                    };
                    user.Farhiusershallsstores.Add(userhalls);

                    if (hall.Branches.Count == 0)
                    {
                        Farhiusershallbranch userbranches = new Farhiusershallbranch()
                        {
                            BranchId = (from c in db.Halls where c.HallId == hall.Id select c.MainBranchId.Value).SingleOrDefault(),
                            Id = userhalls.Id,
                            CreatedOn = DateTime.Now
                        };
                        userhalls.Farhiusershallbranches.Add(userbranches);
                    }

                    foreach (var branch in hall.Branches)
                    {
                        Farhiusershallbranch userbranches = new Farhiusershallbranch()
                        {
                            BranchId = branch,
                            Id = userhalls.Id,
                            CreatedOn = DateTime.Now
                        };
                        userhalls.Farhiusershallbranches.Add(userbranches);
                    }
                }

                foreach (var store in dataVM.Stores)
                {
                    Farhiusershallsstore userstores = new Farhiusershallsstore()
                    {
                        FarhiUserId = user.FarhiUserId,
                        StoreId = store.Id,
                        CreatedOn = DateTime.Now,
                    };
                    user.Farhiusershallsstores.Add(userstores);

                    if (store.Branches.Count == 0)
                    {
                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
                        {
                            BranchId = (from c in db.Stores where c.StoreId == store.Id select c.MainBranchId.Value).SingleOrDefault(),
                            Id = userstores.Id,
                            CreatedOn = DateTime.Now
                        };
                        userstores.Farhiusersstorebranches.Add(userbranches);
                    }

                    foreach (var branch in store.Branches)
                    {
                        Farhiusersstorebranch userbranches = new Farhiusersstorebranch()
                        {
                            BranchId = branch,
                            Id = userstores.Id,
                            CreatedOn = DateTime.Now
                        };
                        userstores.Farhiusersstorebranches.Add(userbranches);
                    }
                }


                foreach (var per in user.FarhiuserpermissionUsers)
                    db.Farhiuserpermissions.Remove(per);

                foreach (var per in dataVM.Permissions)
                {
                    Farhiuserpermission userpermission = new Farhiuserpermission()
                    {
                        PermissionId = per,
                        UserId = user.FarhiUserId,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                    };
                    user.FarhiuserpermissionUsers.Add(userpermission);
                }
                await db.SaveChangesAsync();

                var data = await (from c in db.Farhiusers
                                  where c.FarhiUserId == user.FarhiUserId
                                  select new
                                  {
                                      c.FarhiUserId,
                                      c.UserName,
                                      c.LoginName,
                                      c.Email,
                                      c.PhoneNo1,
                                      CreatedBy = c.CreatedByNavigation.UserName,
                                      CreatedOn = c.CreatedOn.Value.ToString("yyyy/MM/dd"),
                                      c.Status
                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data, message = "تم تعديل المستخدم بنجاح" });
            
        }

        [HttpPut("{id}/lock")]
        public async Task<IActionResult> Lock(int id)
        {
            
                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11009", message = "خطأ في جلب البيانات" });

                var user = await (from c in db.Farhiusers
                                   where c.FarhiUserId == id
                                   && c.Status != Status.Deleted
                                   && accessable.Contains(id)
                                   select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE11010", message = " لم يتم العثور علي بيانات لهذا المستخدم" });
                if (user.Status == Status.Locked) return BadRequest(new { StatusCode = "RE11011", message = "حالة هذا المستخدم مقفلة مسبقا" });

                user.Status = Status.Locked;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قفل حالة المستخدم بنجاح"});
            
        }

        [HttpPut("{id}/unlock")]
        public async Task<IActionResult> Unlock(int id)
        {
            
                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11001", message = "خطأ في جلب البيانات" });

                var user = await (from c in db.Farhiusers
                                  where c.FarhiUserId == id
                                  && c.Status != Status.Deleted
                                  && accessable.Contains(id)
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE11012", message = " لم يتم العثور علي بيانات لهذا المستخدم" });
                if (user.Status == Status.Active) return BadRequest(new { StatusCode = "RE11013", message = "حالة هذا المستخدم فعالة مسبقا" });

                user.Status = Status.Active;
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم تفعيل حالة المستخدم بنجاح" });
            
        }
        // DELETE api/<FarhiUsersController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
           
                //TODO -- check ability to delete user 
                //if(UserId() == id) return BadRequest(new { StatusCode = "RE0000", message = "لا يمكن حذف المستخدم مستخدم من قبل النظام حاليا" });
                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11014", message = "خطأ في جلب البيانات" });

                var user = await (from c in db.Farhiusers
                                  .Include(x => x.FarhiuserpermissionUsers)
                                  .Include(x => x.Farhiusershallsstores)
                                  where c.FarhiUserId == id
                                  && accessable.Contains(id)
                                  && c.Status != Status.Deleted
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE11015", message = " لم يتم العثور علي بيانات لهذا المستخدم" });

                user.Status = Status.Deleted;
                foreach (var c in user.FarhiuserpermissionUsers)
                {
                    db.Farhiuserpermissions.Remove(c);
                }

                foreach (var item in user.Farhiusershallsstores)
                {
                    var remove = await (from c in db.Farhiusershallbranches where c.Id == item.Id select c).ToListAsync();
                    foreach (var i in remove)
                    {
                        db.Farhiusershallbranches.Remove(i);
                    }
                    db.Farhiusershallsstores.Remove(item);

                    var removestore = await (from c in db.Farhiusersstorebranches where c.Id == item.Id select c).ToListAsync();
                    foreach (var i in removestore)
                    {
                        db.Farhiusersstorebranches.Remove(i);
                    }
                    db.Farhiusershallsstores.Remove(item);
                }
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم حذف المستخدم بنجاح" });
            
        }

        [HttpPut("{id}/resetPassword")]
        public async Task<IActionResult> ResetPassword(int id)
        {
            
                var accessable = await Accessable();
                if (accessable == null) return BadRequest(new { StatusCode = "RE11016", message = "خطأ في جلب البيانات" });

                var user = await (from c in db.Farhiusers
                                  where c.FarhiUserId == id
                                  && c.Status != Status.Deleted
                                  && accessable.Contains(id)
                                  select c).SingleOrDefaultAsync();
                if (user is null) return BadRequest(new { StatusCode = "RE11017", message = " لم يتم العثور علي بيانات لهذا المستخدم" });
                var password = Security.GeneratePassword();

                var hashing = Security.HashPassword(password, configuration["Secret"]);
                user.Password = hashing.Password;
                user.Salt = hashing.Salt;

                await db.SaveChangesAsync();
                return Ok(new { StatusCode = 1, message = "تم إعادة تعيين كلمة مرور المستخدم بنجاح", data =  password});
            
        }
        [HttpGet("getFeatures")]
        public async Task<IActionResult> GetFeatures()
        {
           
                var data = await (from c in db.Features
                                  where c.Status != Status.Deleted
                                  && c.FeatureType == 2
                                    select new {
                                        c.FeatureId,
                                        c.FeatureName,
                                        checkAll = false,
                                        Permissions = c.Permissions
                                            .Select(p => new
                                            {
                                                p.PermissionId,
                                                p.PermissionName
                                            }).ToList()
                                    }).ToListAsync();
                return Ok(new { StatusCode = 1, data});
            
        }
    }
}
