﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Customers.ViewModels;
using Customers.Global;
using System.IO;
using System.Collections.Generic;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class HallReservationsController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        public HallReservationsController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, int HallBranchId, DateTime? date)
        {
            
                var query = from c in db.Hallreservations
                            where c.Status != Status.Deleted
                            && (
                                c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select c;
                if (HallBranchId > 0)
                    query = query.Where(x => x.HallBranchId == HallBranchId);
                if (date is not null)
                    query = query.Where(x => x.ReservationDate == date.Value);
                var data = await query.Select(c => new
                {
                    c.HallReservationId,
                    ReservationDate = c.ReservationDate.ToString("yyyy/MM/dd"),
                    c.Customer.CustomerName,
                    HallName = c.HallBranch.Hall.HallName + " - " + c.HallBranch.Address.AddressName + "_" + c.HallBranch.AddressDescription,
                    Apponiments =  c.Hallreservationdetails.Where(x => x.Status != Status.Deleted).Count(),
                    CreatedBy = c.CreatedByNavigation.UserName,
                    CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1, data, total });
            
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
                var data = await (from c in db.Hallreservations
                                  where c.Status != Status.Deleted
                                  && c.HallReservationId == id
                                  && (
                                      c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      HallName = c.HallBranch.Hall.HallName + " - " + c.HallBranch.Address.AddressName + "_" + c.HallBranch.AddressDescription,
                                      c.Customer.CustomerName,
                                      c.Status,
                                      ReservationDate = c.ReservationDate.ToString("yyyy/MM/dd"),
                                      Details = c.Hallreservationdetails.Where(x => x.Status != Status.Deleted).Select(x => new
                                      {
                                          StartTime = x.HallAppointment.StartTime.ToString(@"HH\:mm"),
                                          EndTime = x.HallAppointment.EndTime.ToString(@"HH\:mm"),
                                          x.Price,
                                          ReservationType = x.HallAppointment.ReservationType.Description, 
                                          services = x.Hallservicereservations.Where(p => p.Status != Status.Deleted).Select(p => new
                                          {
                                              p.HallBranchService.Description,
                                              p.Price
                                          })
                                      }).ToList(),


                                      CreatedBy = c.CreatedByNavigation.UserName,
                                      CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                                      ModifiedBy = c.ModifiedByNavigation.UserName,
                                      ModifiedOn = c.ModifiedOn != null ? c.ModifiedOn.Value.ToString("yyyy/MM/dd") : "-",

                                  }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE21001", message = "الرجاء التاكد اختيار فرع الصالة " });
                }


                var data = await (from c in db.Hallreservations
                                  where c.Status != Status.Deleted
                                  && c.HallReservationId == id
                                  && (
                                      c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  select new
                                  {
                                      HallReservation = new 
                                      {
                                          CustomerId = c.CustomerId,
                                          HallBranchId = c.HallBranchId,
                                          ReservationDate = c.ReservationDate.ToString("yyyy-MM-dd"),  
                                          Status = c.Status,
                                          HallReservationDetails = c.Hallreservationdetails.Where(x => x.Status != Status.Deleted).Select(x => new HallReservationDetails
                                          {
                                              HallAppointmentId = x.HallAppointmentId,
                                              Services = x.Hallservicereservations.Where(p => p.Status != Status.Deleted).Select(p => p.HallBranchServiceId).ToList()
                                          }).ToList(),
                                      },
                                      
                                      selectedAppointments = c.Hallreservationdetails.Select(x => new
                                      {
                                            x.HallAppointmentId,
                                            reservationType = x.HallAppointment.ReservationType.Description,
                                            StartTime = x.HallAppointment.StartTime.ToString(@"HH\:mm"),
                                            EndTime = x.HallAppointment.EndTime.ToString(@"HH\:mm"),
                                            x.Price
                                      }).ToList()
                                      //Details = c.Hallreservationdetails.Where(x => x.Status != Status.Deleted).Select(x => new
                                      //{
                                      //    x.HallReservationDetailsId,
                                      //    StartTime = x.HallAppointment.StartTime.ToString(@"HH\:mm"),
                                      //    EndTime = x.HallAppointment.EndTime.ToString(@"HH\:mm"),
                                      //    x.Price,
                                      //    ReservationType = x.HallAppointment.ReservationType.Description,
                                      //    services = x.Hallservicereservations.Where(p => p.Status != Status.Deleted).Select(p => new
                                      //    {
                                      //        p.HallBranchService.Description,
                                      //        p.Price
                                      //    })
                                      //}).ToList(),
                                  }).SingleOrDefaultAsync();
                if (data.HallReservation.Status != Status.Active) return BadRequest(new { StatusCode = "RE21002", message = "لا يمكن التعديل علي هذا الحجز" });

                var availibleAppointments = await getAvailibleAppointments(data.HallReservation.HallBranchId, Convert.ToDateTime(data.HallReservation.ReservationDate));

                if (data is null)
                {
                    return NotFound(new { StatusCode = "RE21003", message = AppMessages.ErrorMessages.NotFoundError });
                }

                return Ok(new { StatusCode = 1, data, availibleAppointments });
            
        }

        [HttpPost]
        public async Task<IActionResult> add([FromBody] HallReservationsVM dataVM)
        {
            

                var add = mapper.Map<Hallreservation>(dataVM, opt => opt.Items["UserId"] = UserId());
                db.Add(add);
                foreach(var item in dataVM.HallReservationDetails)
                {
                    var price = await GetAppointmentPrice(item.HallAppointmentId);

                    var Hallreservationdetail = new Hallreservationdetail
                    {
                        CreatedOn = DateTime.Now,
                        Status = Status.Active,
                        Price = price,
                        HallAppointmentId = item.HallAppointmentId,
                        HallReservationId = add.HallReservationId,
                    };
                    foreach (var item2 in item.Services)
                    {
                        price = await GetServicePrice(item2);
                        Hallreservationdetail.Hallservicereservations.Add(new Hallservicereservation
                        {
                            CreatedOn= DateTime.Now,
                            HallBranchServiceId = item2,
                            HallReservationDetailsId = Hallreservationdetail.HallReservationDetailsId,
                            Price = price,
                            Status = Status.Active, 
                        });
                    }
                    add.Hallreservationdetails.Add(Hallreservationdetail);
                }
                await db.SaveChangesAsync();

                var data = await db.Hallreservations
                    .Where(x => x.HallReservationId == add.HallReservationId)
                    .Select(c => new
                    {
                        c.HallReservationId,
                        ReservationDate = c.ReservationDate.ToString("yyyy/MM/dd"),
                        c.Customer.CustomerName,
                        HallName = c.HallBranch.Hall.HallName + " - " + c.HallBranch.Address.AddressName + "_" + c.HallBranch.AddressDescription,
                        Apponiments = c.Hallreservationdetails.Where(x => x.Status != Status.Deleted).Count(),
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, message = "تم إضافة البيانات", data });
            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] HallReservationsVM dataVM)
        {
            
                if(dataVM.Status != Status.Active) return BadRequest(new {StatusCode = "RE21004", message = "لا يمكن التعديل علي هذا الحجز"});
                var reservarion = await (from c in db.Hallreservations
                                         .Include(x => x.Hallreservationdetails)
                                         where c.HallReservationId == id
                                         && (
                                             c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                         )
                                         && c.Status != Status.Deleted
                                         select c).SingleOrDefaultAsync();
                if (reservarion is null) return NotFound(new { StatusCode = "RE21005", message = AppMessages.ErrorMessages.NotFoundError });

                var edit = mapper.Map(dataVM, reservarion, opt => opt.Items["UserId"] = UserId());

                foreach (var item in reservarion.Hallreservationdetails)
                {
                    var services = await (from c in db.Hallservicereservations 
                                          where c.HallReservationDetailsId == item.HallReservationDetailsId
                                          select c).ToListAsync();
                    foreach(var item2 in services)
                    {
                        db.Remove(item2);
                    }
                    db.Remove(item);
                }

                foreach (var item in dataVM.HallReservationDetails)
                {
                    var price = await GetAppointmentPrice(item.HallAppointmentId);
                    var Hallreservationdetail = new Hallreservationdetail
                    {
                        CreatedOn = DateTime.Now,
                        Status = Status.Active,
                        Price = price,
                        HallAppointmentId = item.HallAppointmentId,
                        HallReservationId = edit.HallReservationId,
                    };

                    foreach (var item2 in item.Services)
                    {
                        price = await GetServicePrice(item2);
                        Hallreservationdetail.Hallservicereservations.Add(new Hallservicereservation
                        {
                            CreatedOn = DateTime.Now,
                            HallBranchServiceId = item2,
                            HallReservationDetailsId = Hallreservationdetail.HallReservationDetailsId,
                            Price = price,
                            Status = Status.Active,
                        });
                    }
                    edit.Hallreservationdetails.Add(Hallreservationdetail);

                }
                await db.SaveChangesAsync();
                var data = await db.Hallreservations
                    .Where(x => x.HallReservationId == id)
                    .Select(c => new
                    {
                        c.HallReservationId,
                        ReservationDate = c.ReservationDate.ToString("yyyy/MM/dd"),
                        c.Customer.CustomerName,
                        HallName = c.HallBranch.Hall.HallName + " - " + c.HallBranch.Address.AddressName + "_" + c.HallBranch.AddressDescription,
                        Apponiments = c.Hallreservationdetails.Where(x => x.Status != Status.Deleted).Count(),
                        CreatedBy = c.CreatedByNavigation.UserName,
                        CreatedOn = c.CreatedOn.ToString("yyyy/MM/dd"),
                        c.Status
                    }).SingleOrDefaultAsync();

                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data });
            
        }



        [HttpPut("{id}/accept")]
        public async Task<IActionResult> accept(int id)
        {
            
                var item = await (from c in db.Hallreservations
                                  .Include(x => x.Hallreservationdetails)
                                  where c.HallReservationId == id
                                  && (
                                      c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && c.Status == Status.Active
                                  select c).SingleOrDefaultAsync();
                if (item is null) return NotFound(new { StatusCode = "RE21006", message = AppMessages.ErrorMessages.NotFoundError });
                if (item.Status == Status.Accept) return BadRequest(new { StatusCode = "RE21007", message = "حالة هذا الحجز مقبول مسبقا" });

                item.Status = Status.Accept;
                foreach(var item2 in item.Hallreservationdetails)
                {
                    item2.Status = Status.Accept;
                    var services = await (from c in db.Hallservicereservations
                                          where c.Status == Status.Active
                                          && c.HallReservationDetailsId == item2.HallReservationDetailsId
                                          select c).ToListAsync();
                    foreach(var service in services)
                    {
                        service.Status = Status.Accept;
                    }
                }
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم قبول الحجز" });
            
        }

        [HttpPut("{id}/reject")]
        public async Task<IActionResult> reject(int id)
        {
            
                var item = await (from c in db.Hallreservations
                                  where c.HallReservationId == id
                                  && (
                                      c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                  )
                                  && (c.Status == Status.Active || c.Status == Status.Accept)
                                  select c).SingleOrDefaultAsync();
                if (item is null) return NotFound(new { StatusCode = "RE21008", message = AppMessages.ErrorMessages.NotFoundError });
                if (item.Status == Status.Reject) return BadRequest(new { StatusCode = "RE21009", message = "حالة هذا الحجز مرفوض مسبقا" });

                item.Status = Status.Reject;
                foreach (var item2 in item.Hallreservationdetails)
                {
                    item2.Status = Status.Reject;
                    var services = await (from c in db.Hallservicereservations
                                          where (c.Status == Status.Active || c.Status == Status.Accept)
                                          && c.HallReservationDetailsId == item2.HallReservationDetailsId
                                          select c).ToListAsync();
                    foreach (var service in services)
                    {
                        service.Status = Status.Reject;
                    }
                }
                await db.SaveChangesAsync();

                return Ok(new { StatusCode = 1, message = "تم رفض الحجز" });
            
        }


        [HttpGet("{id}/servicePrice")]
        public async Task<IActionResult> ServicePrice(int id)
        {
            
                var data = await GetServicePrice(id);
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/appointmentPrice")]
        public async Task<IActionResult> AppointmentPrice(int id)
        {
            
                var data = await GetAppointmentPrice(id);
                return Ok(new { StatusCode = 1, data });
            
        }

        public async Task<decimal> GetAppointmentPrice(int id)
        {
            return await (from c in db.Hallappointments
                          where c.Status != Status.Deleted
                          && c.HallAppointmentId == id
                          && (
                              c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                          )
                          select c.Price).SingleOrDefaultAsync();
        }
        public async Task<decimal> GetServicePrice(int id)
        {
            return await (from c in db.Hallbranchservices
                          where c.Status != Status.Deleted
                          && c.HallBranchServiceId == id
                          && (
                              c.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                          )
                          select c.Price).SingleOrDefaultAsync();
        }



        public async Task<object> getAvailibleAppointments(int branchId, DateTime reservationDate)
        {
            var AppointmentIds = await (from c in db.Hallreservationdetails
                                        where DateOnly.FromDateTime(c.HallReservation.ReservationDate) == DateOnly.FromDateTime(reservationDate.Date) 
                                        && c.HallReservation.HallBranchId == branchId
                                        && (c.Status == Status.Active || c.Status == Status.Expired)
                                        select c.HallAppointmentId).ToListAsync();
            var tt = DateOnly.FromDateTime(reservationDate.Date);
            var availibleAppointments = await (from c in db.Hallappointments
                                               where c.Status == Status.Active
                                               && c.HallBranchId == branchId
                                               && c.WorkDayId == (sbyte)reservationDate.DayOfWeek
                                               && !AppointmentIds.Contains(c.HallAppointmentId)
                                               select new
                                               {
                                                   c.HallAppointmentId,
                                                   ReservationType = c.ReservationType.Description,
                                                   StartTime = c.StartTime.ToString(@"HH\:mm"),
                                                   EndTime = c.EndTime.ToString(@"HH\:mm"),
                                                   c.Price
                                               }).ToListAsync();
            return availibleAppointments;
        }

        [HttpGet("{branchId}/getAvailibleAppointments")]
        public async Task<IActionResult> GetAvailibleAppointments(int branchId, DateTime reservationDate)
        {
                return Ok(new { StatusCode = 1, data = await getAvailibleAppointments(branchId, reservationDate) });
        }

        [HttpGet("{branchId}/checkAvailibility")]
        public async Task<bool> checkAvailibility(int branchId, int AppointmentId, DateTime reservationDate)
        {
            try
            {
                var check = await (from c in db.Hallreservationdetails
                                            where c.HallReservation.ReservationDate == reservationDate
                                            && c.HallReservation.HallBranchId == branchId
                                            && c.Status == Status.Active
                                            && c.HallAppointmentId == AppointmentId
                                            select c).AnyAsync();
                return check;
            }
            catch
            {
                return false;
            }
        }
    }
}
