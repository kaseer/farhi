﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class LookupController : RootController
    {

        public LookupController(farhiContext context) : base(context)
        {
        }
        [HttpGet("getAddresses")]
        public async Task<IActionResult> getAddresses()
        {
            
                var addresses = await (from a in db.Addresses
                                       where a.Status != Status.Deleted
                                       select new { 
                                            value = a.AddressId,
                                            text = a.AddressName
                                       }).ToListAsync();
                return Ok(new { StatusCode = 1,  addresses });
            
        }

        [HttpGet("getHalls")]
        public async Task<IActionResult> getHalls()
        {
            
                var halls = await (from a in db.Halls
                                       where a.Status != Status.Deleted
                                       select new
                                       {
                                           value = a.HallId,
                                           text = a.HallName
                                       }).ToListAsync();
                return Ok(new { StatusCode = 1,  halls }) ;
            
        }
        [HttpGet("getReservationTypes")]
        public async Task<IActionResult> getReservationTypes()
        {
           
                var ReservationTypes = await (from a in db.Reservationtypes
                                   where a.Status != Status.Deleted
                                   select new
                                   {
                                       value =a.ReservationTypeId,
                                       text = a.Description
                                   }).ToListAsync();
                return Ok(new { StatusCode = 1,  ReservationTypes });
            
        }
        [HttpGet("getSubscriptionPrices")]
        public async Task<IActionResult> getSubscriptionPrices(int type)
        {
            
                if (type <= 0)
                {
                    return BadRequest(new { StatusCode = "RE99001", message = AppMessages.ErrorMessages.ServerError });
                }
                var SubscriptionPrices = await (from s in db.Subscriptionprices
                                                where s.Status == Status.Active
                                                && (s.Type == type || s.Type == 3)
                                                select new { 
                                                    value =s.SubscriptionPriceId,
                                                    text = s.Description
                                                }).ToListAsync();
                return Ok(new { StatusCode = 1,  SubscriptionPrices });
            
        }

        [HttpGet("getSubscriptionPricesDetails")]
        public async Task<IActionResult> getSubscriptionPricesDetails(int subscriptionPriceId)
        {
            
                if (subscriptionPriceId <= 0)
                {
                    return BadRequest(new { StatusCode = "RE99002", message = AppMessages.ErrorMessages.ServerError });
                }
                var SubscriptionPricesDetails = await (from s in db.Subscriptionpricedetails
                                                where s.SubscriptionPrice.Status == Status.Active
                                                && s.SubscriptionPriceId == subscriptionPriceId
                                                       select new
                                                {
                                                    s.SubscriptionPriceDetailsId,
                                                    s.DurationTime,
                                                    s.DurationTypeNavigation.DurationTypeName,
                                                    s.Price
                                                }).ToListAsync();
                return Ok(new { StatusCode = 1,  SubscriptionPricesDetails });
            
        }

        [HttpGet("getStoreTypes")]
        public async Task<IActionResult> getStoreTypes()
        {
            
                var StoreTypes = await (from a in db.Storetypes
                                       where a.Status != Status.Deleted
                                       select new
                                       {
                                           value =a.StoreTypeId,
                                           text = a.Description
                                       }).ToListAsync();
                return Ok(new { StatusCode = 1,  StoreTypes });
            
        }

        [HttpGet("getStores")]
        public async Task<IActionResult> getStores()
        {
            
                var stores = await (from a in db.Stores
                                   where a.Status != Status.Deleted
                                   select new
                                   {
                                       a.StoreId,
                                       a.StoreName
                                   }).ToListAsync();
                return Ok(new { StatusCode = 1,  stores });
            
        }

        [HttpGet("getCategories")]
        public async Task<IActionResult> getCategories()
        {
            

                var categories = await (from a in db.Categories
                                        where a.Status != Status.Deleted
                                        select new
                                        {
                                            value = a.CategoryId,
                                            text = a.Description
                                        }).ToListAsync();
                return Ok(new { StatusCode = 1, categories });
            
        }
        [HttpGet("{id}/getSubCategories")]
        public async Task<IActionResult> getSubCategories(int id)
        {
            

                var SubCategories = await (from a in db.Subcategories
                                           where a.Status != Status.Deleted
                                           && a.CategoryId == id
                                        select new
                                        {
                                            value = a.SubCategoryId,
                                            text = a.Description
                                        }).ToListAsync();
                return Ok(new { StatusCode = 1, SubCategories });
            
        }

        [HttpGet("{id}/getHallBranches")]
        public async Task<IActionResult> getHallBranches(int id)
        {
            
                var HallBranches = await (from a in db.Hallbranches
                                          where a.HallId == id
                                          && a.Status != Status.Deleted
                                          select new
                                          {
                                              Value = a.HallBranchId,
                                              text = "فرع " + a.Address.AddressName + " - " + a.AddressDescription
                                          }).ToListAsync();
                return Ok(new { StatusCode = 1, HallBranches });
            
        }
        [HttpGet("{id}/getStoreBranches")]
        public async Task<IActionResult> getStoreBranches(int id)
        {
            
                var storeBranches = await (from a in db.Storebranches
                                           where a.StoreId == id
                                           && a.Status != Status.Deleted
                                           select new
                                           {
                                               Value = a.StoreBranchId,
                                               text = "فرع " + a.Address.AddressName + " - " + a.AddressDescription
                                           }).ToListAsync();
                return Ok(new { StatusCode = 1, storeBranches });
            
        }

        [HttpGet("getSubscriptionPrices")]
        public async Task<IActionResult> getSubscriptionPrices()
        {
            
                var subscriptionPrices = await (from a in db.Subscriptionprices
                                           where a.Status != Status.Deleted
                                           select new
                                           {
                                               Value = a.SubscriptionPriceId,
                                               text = a.Description
                                           }).ToListAsync();
                return Ok(new { StatusCode = 1, subscriptionPrices });
            
        }


        [HttpGet("getAllHallBranches")]
        public async Task<IActionResult> getAllHallBranches()
        {
            
                var allHallBranches = await (from a in db.Hallbranches
                                              where a.Status != Status.Deleted
                                              && (
                                                   a.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                               )
                                              select new
                                              {
                                                  Value = a.HallBranchId,
                                                  text = a.Hall.HallName + " / " + "فرع " + a.Address.AddressName + " - " + a.AddressDescription,
                                                  a.HallId
                                              }).ToListAsync();
                allHallBranches.Insert(0, new
                {
                    Value = 0,
                    text = "الكل",
                    HallId = 0
                });
                return Ok(new { StatusCode = 1, allHallBranches });
            
        }


        [HttpGet("getAllStoreBranches")]
        public async Task<IActionResult> getAllStoreBranches()
        {
            
                var allStoreBranches = await (from a in db.Storebranches
                                              where a.Status != Status.Deleted
                                              && (
                                                   a.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                               )
                                              select new
                                              {
                                                  Value = a.StoreBranchId,
                                                  text = a.Store.StoreName + " / " + "فرع " + a.Address.AddressName + " - " + a.AddressDescription,
                                                  a.StoreId
                                              }).ToListAsync();
                allStoreBranches.Insert(0, new
                {
                    Value = 0,
                    text = "الكل",
                    StoreId = 0
                });
                return Ok(new { StatusCode = 1, allStoreBranches });
            
        }

        [HttpGet("getStoreServices")]
        public async Task<IActionResult> getStoreServices()
        {
            
                var data = await (from a in db.Storeservices
                                  where a.Status != Status.Deleted
                                  && (
                                       a.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                   )
                                  select new
                                  {
                                      Value = a.ServiceId,
                                      text = a.Description
                                  }).ToListAsync();
                return Ok(new { StatusCode = 1, data });
            
        }
        [HttpGet("{id}/getHallServices")]
        public async Task<IActionResult> getHallServices(int id)
        {
           
                var data = await (from a in db.Hallbranchservices
                                  where a.Status != Status.Deleted
                                  && a.HallBranchId == id
                                  && (
                                       a.HallBranch.Farhiusershallbranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                   )
                                  select new
                                  {
                                      Value = a.HallBranchServiceId,
                                      text = a.Description,
                                      a.Price
                                  }).ToListAsync();
                return Ok(new { StatusCode = 1, data });
            
        }
        [HttpGet("getStoreProducts")]
        public async Task<IActionResult> getStoreProducts()
        {
            
                var data = await (from a in db.Products
                                  where a.Status == Status.Active 
                                  && a.Qty > 0
                                  && (
                                       a.StoreBranch.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                   )
                                  select new
                                  {
                                      Value = a.ProductId,
                                      text = a.Name
                                  }).ToListAsync();
                return Ok(new { StatusCode = 1, data });
            
        }



        [HttpGet("getCustomers")]
        public async Task<IActionResult> getCustomers()
        {
            
                var data = await (from a in db.Customers
                                                where a.Status == Status.Active
                                                select new
                                                {
                                                    Value = a.CustomerId,
                                                    text = a.CustomerName
                                                }).ToListAsync();
                return Ok(new { StatusCode = 1, data });
            
        }
    }
}
