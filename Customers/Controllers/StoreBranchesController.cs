﻿using AutoMapper;
using Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Customers.ViewModels;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Customers.Global;

namespace Customers.Controllers
{
    [Route("api/[controller]")]
    public class StoreBranchesController : RootController
    {
        public IConfiguration configuration;
        private readonly IMapper mapper;
        //private IWebHostEnvironment _env;
        

        //private string imagePath = "/img/";
        public StoreBranchesController(farhiContext context, IConfiguration configuration, IMapper mapper) : base(context)
        {
            this.configuration = configuration;
            this.mapper = mapper;
            //_env = env;
        }
        // GET: api/<FarhiAdminsController>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] Pagination pagination, string search)
        {
            
                var query = from c in db.Storebranches
                            where c.Status == Status.Active
                            && (
                                c.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select c;
                if (search is not null)
                    query = query.Where(x => x.Address.AddressName.Contains(search) || x.AddressDescription.Contains(search));

                var data = await query.Select(c => new {
                    c.Store.StoreName,
                    c.StoreBranchId,
                    StoreType = c.Store.StoreType.Description,
                    AddressName = c.Address.AddressName + " - " + c.AddressDescription,
                    c.PhoneNo1,
                    c.Store.MainBranchId,
                    workTimes = c.StartTime.ToString(@"hh\:mm\:ss") + "  -  " + c.EndTime.ToString(@"hh\:mm\:ss"),
                    CreatedOn = c.AdminCreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).Skip((pagination.PageNo - 1) * pagination.PageSize).Take(pagination.PageSize).ToListAsync();
                var total = await query.CountAsync();
                return Ok(new { StatusCode = 1, data, total });
            
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            
                var data = await(from c in db.Storebranches
                            where c.Status == Status.Active
                            && c.StoreBranchId == id
                            && (
                                c.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                            )
                            select new {
                                c.Address.AddressName,
                                c.AddressDescription,
                                StoreType = c.Store.StoreType.Description,
                                c.PhoneNo1,
                                c.PhoneNo2,
                                c.Longitude,
                                c.Latitude,
                                StartTime = c.StartTime.ToString(@"hh\:mm\:ss"),
                                EndTime = c.EndTime.ToString(@"hh\:mm\:ss"),
                                StoreWorkDays = c.Storeworkdays
                                            .Select(x => new {
                                                WorkDay = Constant.SelectDay(x.WorkDayId),
                                                StartTime = x.StartTime.ToString(@"hh\:mm\:ss"),
                                                EndTime = x.EndTime.ToString(@"hh\:mm\:ss"),
                                            }).ToList(),
                                Services = c.Storeservices
                                            .Where(x => x.Status != Status.Deleted)
                                            .Select(x => new {
                                                x.Description,
                                                x.Price,
                                            }).ToList(),
                                Subscriptions =
                                          c.Subscriptions.Select(x => new
                                          {
                                              subscriptions = x.Subscriptiondetails.Select(c => new {
                                                  c.SubscriptionPriceDetails.SubscriptionPrice.Description,
                                                  StartDate = c.StartDate.ToString("yyyy/MM/dd"),
                                                  EndDate = c.EndDate.ToString("yyyy/MM/dd"),
                                                  c.Price
                                              }).ToList(),
                                              date = x.CreatedOn.ToString("yyyy/MM/dd")
                                          }).ToList(),
                            }).SingleOrDefaultAsync();
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getForEdit")]
        public async Task<IActionResult> getForEdit(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04001", message = "الرجاء التاكد اختيار فرع الصالة " });
                }


                var data = await (from c in db.Storebranches
                                            where c.Status != Status.Deleted
                                            && c.StoreBranchId == id
                                            && (
                                                c.Farhiusersstorebranches.Select(x => x.IdNavigation.FarhiUserId).Contains(UserId())
                                            )
                                            select new StoreBranchesVM 
                                            {
                                                StoreId = c.StoreId,
                                                AddressId =  c.AddressId,
                                                AddressDescription = c.AddressDescription,
                                                PhoneNo1 = c.PhoneNo1,
                                                PhoneNo2 = c.PhoneNo2,
                                                Longitude = c.Longitude,
                                                Latitude = c.Latitude,
                                                TimeRange = new[] { c.StartTime.ToString(@"HH\:mm\:ss"), c.EndTime.ToString(@"HH\:mm\:ss") },
                                                StoreWorkDays = c.Storeworkdays.Select(x => new StoreWorkDays
                                                {
                                                    WorkDayId = x.WorkDayId,
                                                    TimeRange = new[] { x.StartTime.ToString(@"HH\:mm\:ss"), x.EndTime.ToString(@"HH\:mm\:ss") },
                                                }).ToList(),
                                                Services = c.Storeservices
                                                  .Where(x => x.Status != Status.Deleted)
                                                  .Select(x => new StoreServices
                                                  {
                                                      ServiceId = x.ServiceId,
                                                      Description = x.Description,
                                                      Price = x.Price,
                                                      ReservationStatus = x.Storeservicereservations
                                                        .Where(f => f.Status == Status.Active)
                                                        .Select(f => f).Count(),
                                                  }).ToList(),
                                            }).SingleOrDefaultAsync();
                var StoreWorkDays = new List<StoreWorkDays>();
                for (sbyte i = 1; i <= 7; i++)
                {
                    var check = data.StoreWorkDays.Find(x => x.WorkDayId == i);
                    if (check is not null) StoreWorkDays.Add(check);
                    else StoreWorkDays.Add(new StoreWorkDays() { WorkDayId = i, TimeRange = new string[] { "", "" } } );
                }
                data.StoreWorkDays = StoreWorkDays;
                if (data is null)
                {
                    return NotFound(new { StatusCode = "RE04002", message = AppMessages.ErrorMessages.NotFoundError });
                }

                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> edit(int id, [FromBody] StoreBranchesVM dataVM)
        {
            
                var branch = await (from c in db.Storebranches
                           .Include(x => x.Storeworkdays)
                            .Include(x => x.Storeservices)
                                    where c.StoreBranchId == id
                           && c.Status != Status.Deleted
                           select c).SingleOrDefaultAsync();
                if (branch is null) return NotFound(new {StatusCode = "RE04003", message = "Not Fount"});

                var edit = mapper.Map(dataVM, branch, opt => opt.Items["UserId"] = UserId());

                foreach(var apt in edit.Storeworkdays)
                {
                    edit.Storeworkdays.Remove(apt);
                }

                foreach(var item in dataVM.StoreWorkDays)
                {
                    if (item.TimeRange[0] != "")
                    {
                        var newday = new Storeworkday()
                        {
                            WorkDayId = item.WorkDayId,
                            StartTime = TimeOnly.Parse(item.TimeRange[0]),
                            EndTime = TimeOnly.Parse(item.TimeRange[1]),
                        };
                        edit.Storeworkdays.Add(newday);
                    }
                }

                foreach (var ser in edit.Storeservices)
                {
                    var check = dataVM.Services.Where(x => x.ServiceId == ser.ServiceId).SingleOrDefault();
                    if (check is not null)
                    {
                        ser.Description = check.Description;
                        ser.ModifiedBy = UserId();
                        ser.ModifiedOn = DateTime.Now;
                        ser.Price = check.Price;
                    }
                    else
                    {
                        edit.Storeservices.Remove(ser);
                    }
                }
                foreach (var ser in dataVM.Services)
                {
                    if (ser.ServiceId == 0)
                    {
                        edit.Storeservices.Add(new Storeservice()
                        {
                            CreatedBy = UserId(),
                            CreatedOn = DateTime.Now,
                            Price = ser.Price,
                            Status = Status.Active,
                            Description = ser.Description,
                        });
                    }
                }

                await db.SaveChangesAsync();

                var data = await db.Storebranches
                    .Where( x => x.StoreBranchId == id)
                    .Select(c => new {
                    c.Store.StoreName,
                    c.StoreBranchId,
                    StoreType = c.Store.StoreType.Description,
                    AddressName = c.Address.AddressName + " - " + c.AddressDescription,
                    c.PhoneNo1,
                    c.Store.MainBranchId,
                    workTimes = c.StartTime.ToString(@"hh\:mm\:ss") + "  -  " + c.EndTime.ToString(@"hh\:mm\:ss"),
                    CreatedOn = c.AdminCreatedOn.ToString("yyyy/MM/dd"),
                    c.Status
                }).SingleOrDefaultAsync();
                return Ok(new {StatusCode = 1, message = "تم تعديل البيانات", data});
            
        }

        [HttpGet("{id}/getImages")]
        public async Task<IActionResult> getImages(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04004", message = "يرجي التاكد من بيانات الفرع" });
                }
                var imagesQuery = await (from m in db.Media
                                         where m.StoreBranchId == id
                                         && m.MediaType == 1
                                         && m.Status != Status.Deleted
                                         select m).ToListAsync();


                if (imagesQuery.Count <= 0)
                {
                    return Ok(new { StatusCode = 1, data = imagesQuery });
                }
                //for(int i = 0; i < imagesQuery.Count; i++)
                //{
                //    imagesQuery[0].Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(_env.WebRootPath + imagePath + images[0].Path));
                //}
                var path = ConfigurationItems.GetStoreImages(configuration);
                var data = imagesQuery.Select(x => new Classes.Images()
                {
                    ImageId = x.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(path + x.Path))
                });
                return Ok(new { StatusCode = 1, data });
            
        }

        [HttpGet("{id}/getVideo")]
        public async Task<IActionResult> getVideo(int id)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04005", message = "يرجي التاكد من بيانات الفرع" });
                }
                var video = await (from m in db.Media
                                   where m.StoreBranchId == id
                                   && m.MediaType == 2
                                   && m.Status != Status.Deleted
                                   select m.Path
                                   ).SingleOrDefaultAsync();


                if (video == null)
                {
                    return Ok(new { StatusCode = 1, data = video });
                }
                var path = ConfigurationItems.GetStoreVideos(configuration);
                video = "data:video/mp4;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(path + video));
                return Ok(new { StatusCode = 1, data = video });
            
        }

        [HttpPost("{id}/addVideo")]
        public async Task<IActionResult> addVideo(int id, IFormFile file)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04006", message = "يرجي التاكد من بيانات الفرع" });
                }
                var StoreInfo = await (from h in db.Storebranches
                                       .Include(m => m.Media.Where(x => x.MediaType == 2))
                                       .Include(m => m.Store)
                                       where h.StoreBranchId == id
                                       && h.Status != Status.Deleted
                                       select h).SingleOrDefaultAsync();
                if (StoreInfo == null)
                {
                    return NotFound(new { StatusCode = "RE04007", message = "لا توجد بيانات لهذا المحل" });
                }
                if (StoreInfo.Media.Count > 0)
                {
                    foreach (var item in StoreInfo.Media)
                    {
                        db.Media.Remove(item);
                    }
                }
                string FileName = StoreInfo.Store.StoreName + "_" + id + ".mp4";
                var videopath = ConfigurationItems.GetStoreVideos(configuration);
                var filePath = Path.Combine(videopath, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                    //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                    Medium addVideo = new Medium()
                    {
                        MediaType = 2,
                        Path = FileName,
                        CreatedBy = UserId(),
                        CreatedOn = DateTime.Now,
                        StoreBranchId = id,
                        Status = Status.Active,
                    };
                    await db.Media.AddAsync(addVideo);
                    await db.SaveChangesAsync();
                }
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", });
            
        }

        [HttpPost("{id}/addImages")]
        public async Task<IActionResult> addImages(int id, IFormFile file)
        {
            
                if (id <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04008", message = "يرجي التاكد من بيانات الفرع" });
                }
                var StoreInfo = await (from h in db.Storebranches
                                       .Include(m => m.Store)
                                       where h.StoreBranchId == id
                                       && h.Status != Status.Deleted
                                       select h).SingleOrDefaultAsync();
                if (StoreInfo == null)
                {
                    return NotFound(new { StatusCode = "RE04009", message = "لا توجد بيانات لهذا المحل" });
                }
                string FileName = StoreInfo.Store.StoreName + "_" + id;
                var path = ConfigurationItems.GetStoreImages(configuration);

                FileName = FileName + "_" + ConfigurationItems.unique() + ".jpg";
                var filePath = Path.Combine(path, FileName);
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    //FileStream.Synchronized(stream);
                    await file.CopyToAsync(stream);
                    //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                }

                Medium addImage = new Medium()
                {
                    MediaType = 1,
                    Path = FileName,
                    CreatedBy = UserId(),
                    CreatedOn = DateTime.Now,
                    StoreBranchId = id,
                    Status = Status.Active,
                };
                await db.Media.AddAsync(addImage);
                await db.SaveChangesAsync();
                var image = new Classes.Images()
                {
                    ImageId = addImage.MediaId,
                    Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(filePath))
                };
                return Ok(new { StatusCode = 1, message = "تم تعديل البيانات", data = image });

                //var filePath = Path.Combine(path, FileName);
                //List<Classes.Images> images = new List<Classes.Images>();
                //foreach (var file in files)
                //{
                //    FileName = FileName + "_" + ConfigurationItems.unique() + ".jpg";
                //    var filePath = Path.Combine(path, FileName);
                //    using (var stream = new FileStream(filePath, FileMode.Create))
                //    {
                //        await file.CopyToAsync(stream);
                //        Medium addImage = new Medium()
                //        {
                //            MediaType = 1,
                //            Path = FileName,
                //            CreatedBy = UserId(),
                //            CreatedOn = DateTime.Now,
                //            StoreBranchId = id,
                //            Status = Status.Active,
                //        };
                //        await db.Media.AddAsync(addImage);
                //        await db.SaveChangesAsync();
                //        images.Add(new Classes.Images() { 
                //            ImageId = addImage.MediaId,
                //            Path = "data:image/jpeg;base64," + Convert.ToBase64String(System.IO.File.ReadAllBytes(filePath))
                //        });
                //        //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                //    }
                //}

                //return Ok(new { StatusCode = 1, message = "خطأ"});
            
        }

        [HttpPut("deleteImages")]
        public async Task<IActionResult> deleteImages([FromBody] List<int> checkedImages)
        {
            
                if (checkedImages.Count <= 0)
                {
                    return BadRequest(new { StatusCode = "RE04010", message = "يرجي التاكد من اختيار الصور المراد حذفها" });
                }
                foreach (var image in checkedImages)
                {
                    var deleteImage = await (from m in db.Media
                                             where m.MediaId == image
                                             select m).SingleOrDefaultAsync();
                    if (deleteImage != null)
                    {
                        db.Media.Remove(deleteImage);
                        await db.SaveChangesAsync();
                        var filePath = Path.Combine(ConfigurationItems.GetStoreImages(configuration), deleteImage.Path);
                        using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose))
                        {
                            //_videoLogic.SaveVideo(new Video(userId, video.VideoId, video.Description, file.FileName, DateTime.Now, url, video.CategoryId));
                        }
                    }
                }

                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" });
            
        }
        [HttpDelete("{id}/deleteVideo")]
        public async Task<IActionResult> deleteVideo(int id)
        {
            
                var delete = await (from m in db.Media
                                         where m.MediaType == 2
                                         && m.StoreBranchId == id
                                         select m).SingleOrDefaultAsync();
                db.Media.Remove(delete);
                await db.SaveChangesAsync();
                var filePath = Path.Combine(ConfigurationItems.GetStoreVideos(configuration), delete.Path);
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose)) {}


                return Ok(new { StatusCode = 1, message = "تم الحذف بنجاح" });
            
        }

    }
}
